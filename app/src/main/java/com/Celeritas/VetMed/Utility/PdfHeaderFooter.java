package com.Celeritas.VetMed.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;

import com.Celeritas.VetMed.R;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Created by Kanaya Lal on 11/5/2018.
 */
public class PdfHeaderFooter extends PdfPageEventHelper {

    private Context context;

    private Image imgFooter;
    private Image imgHeader;
    private Bitmap headerImage;
    private Bitmap footerImage;
    private static String HEADER = "/sdcard/."+ "header.jpg";
    private static String FOOTER = "/sdcard/."+ "footer.jpg";


    public PdfHeaderFooter(Context context,Bitmap headerImage,Bitmap footerImage) {
        this.context = context;
        this.headerImage = headerImage;
        this.headerImage = headerImage;

    }

    @Override
    public void onEndPage(PdfWriter writer, Document pdf) {
        try {

            BaseColor colorGreen = new BaseColor(47, 118, 102);

            Font fntText = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, colorGreen);

            ColumnText.showTextAligned(writer.getDirectContent(),
                    Element.ALIGN_RIGHT,
                    new Phrase(String.format("%d", writer.getPageNumber()),
                            fntText), pdf.getPageSize().getWidth() - 20, 20, 0);
            Drawable dFooter = context.getResources().getDrawable(R.drawable.pet_animal);
//            Bitmap bmpFooter = ((BitmapDrawable) dFooter).getBitmap();
            File fileHeader = new File(FOOTER);

            Bitmap bmpFooter = BitmapFactory.decodeFile(fileHeader.getAbsolutePath());//footerImage;
            ByteArrayOutputStream streamFooter = new ByteArrayOutputStream();
            bmpFooter.compress(Bitmap.CompressFormat.PNG, 100, streamFooter);
            byte[] byteFooter = streamFooter.toByteArray();
           // writer.getDirectContent().addImage(imgFooter);
            try {
                imgFooter = Image.getInstance(byteFooter);
            } catch (BadElementException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Drawable dHeader;


//            if (ConfigUS.VERSION_US) {
                dHeader = context.getResources().getDrawable(R.drawable.vet_med_logo);
        /*    } else {
                dHeader = context.getResources().getDrawable(R.drawable.pet_animal);
            }*/
            //Bitmap bmpHeader = ((BitmapDrawable) dHeader).getBitmap();
            File fileFooter = new File(HEADER);
            Bitmap bmpHeader = BitmapFactory.decodeFile(fileFooter.getAbsolutePath());
            ByteArrayOutputStream streamHeader = new ByteArrayOutputStream();
            bmpHeader.compress(Bitmap.CompressFormat.PNG, 100, streamHeader);
            byte[] byteHeader = streamHeader.toByteArray();
            try {
                imgHeader = Image.getInstance(byteHeader);
            } catch (BadElementException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            imgFooter.setAbsolutePosition(0, 0);
            imgFooter.scaleAbsolute(pdf.getPageSize().getWidth(), 80);
            pdf.add(imgFooter);

            imgHeader.setAbsolutePosition(1, pdf.getPageSize().getHeight() - 10);
            imgHeader.scaleAbsolute(pdf.getPageSize().getWidth(), 100);
            imgHeader.setWidthPercentage(100f);
            imgHeader.setScaleToFitHeight(true);
            pdf.add(imgHeader);
            writer.getDirectContent().addImage(imgHeader);



        } catch (DocumentException e) {
            Log.e("Error", e.getMessage());
        }
    }
}