package com.Celeritas.VetMed.Utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;

import com.Celeritas.VetMed.R;

public class ProgressDialogueUtility {
    private static ProgressDialog progressDialog;

    public static void showProgressDialogue(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_dailogue);
       /* progressBar.getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(this,
                        android.R.color.darker_gray), PorterDuff.Mode.SRC_IN );*/
    }

    public static void hideProgressDialogue() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
