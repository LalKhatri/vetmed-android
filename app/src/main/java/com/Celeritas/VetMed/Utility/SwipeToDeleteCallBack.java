package com.Celeritas.VetMed.Utility;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.Celeritas.VetMed.R;


public abstract class SwipeToDeleteCallBack extends ItemTouchHelper.SimpleCallback {
    private Drawable deleteIcon;
    private int intrinsicWidth;
    private int intrinsicHeight;
    private Context context;
    protected SwipeToDeleteCallBack(Context context) {
        super(0, ItemTouchHelper.LEFT);
      this.deleteIcon=  ContextCompat.getDrawable(context, R.drawable.delete);
       this.intrinsicWidth = deleteIcon.getIntrinsicWidth();
       this.intrinsicHeight= deleteIcon.getIntrinsicHeight();
       this.context= context;
    }
    @Override
    public boolean onMove(RecyclerView recyclerView,
                          RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {

        return false;
    }
    @Override
    public void onChildDraw(Canvas c,
                            RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder,
                            float dX,
                            float dY,
                            int actionState,
                            boolean isCurrentlyActive) {

        View itemView = viewHolder.itemView;
        int itemHeight = itemView.getBottom() - itemView.getTop();

        boolean isCancelled = dX == 0f && !isCurrentlyActive;

        if (isCancelled) {
            clearCanvas(c, itemView.getTop(),
                    itemView.getLeft(),
                    itemView.getRight(),
                    itemView.getBottom());

            super.onChildDraw(c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive);
            return;
        }

        ColorDrawable background = new ColorDrawable();
        background.setColor(ContextCompat.getColor(context, R.color.orange));
        background.setBounds(itemView.getRight() + ((int) dX),
                itemView.getTop(),
                itemView.getRight(),
                itemView.getBottom());
        background.draw(c);

        // Calculate position of delete icon
        int deleteIconTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
        int deleteIconMargin = (itemHeight - intrinsicHeight) / 2;
        int deleteIconLeft = itemView.getRight() - deleteIconMargin - intrinsicWidth;
        int deleteIconRight = itemView.getRight() - deleteIconMargin;
        int deleteIconBottom = deleteIconTop + intrinsicHeight;
        // Draw the delete icon
        deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
        deleteIcon.draw(c);
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }
    private void clearCanvas(Canvas canvas,
                             float top,
                             float left,
                             float right,
                             float bottom) {
        Paint paint = new Paint();
        paint.setColor(Color.TRANSPARENT);
        canvas.drawRect(left, top, right, bottom, paint);
    }
}