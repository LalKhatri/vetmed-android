package com.Celeritas.VetMed.Utility;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SharedPrefManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class ResourcesService extends Service {
    private Integer userID;
    Context context;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        //Toast.makeText(this, "Invoke background service onCreate method.", Toast.LENGTH_LONG).show();
        context = this;
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        userID = intent.getIntExtra(NetworkUtils.USER_ID,0);

        //Toast.makeText(this, "Invoke background service onStartCommand method.", Toast.LENGTH_LONG).show();
        getDisease(NetworkUtils.GET_RESOURCES,userID,NetworkUtils.DISEASE_TABLE);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "Invoke background service onDestroy method.", Toast.LENGTH_LONG).show();
    }

    private void getDisease(String ServiceName, Integer userID, String tableName) {
        WebserviceClient client = new WebserviceClient(this, ServiceName, true, getDiseases_clincal_diagnostic_usefull(userID, tableName), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("Disease: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    int responseStatus = jsonObject.getInt("status");
                    if (responseStatus == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        new SharedPrefManager(context).setStringForKey(jsonArray.toString(),Constants.DISEASES_OBJECT);
                        getDiagnostic(NetworkUtils.GET_RESOURCES,userID,NetworkUtils.DIAGNOSTIC_TABLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {

            }
        }, true);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getDiagnostic(String ServiceName, Integer userID, String tableName) {
        WebserviceClient client = new WebserviceClient(this, ServiceName, true, getDiseases_clincal_diagnostic_usefull(userID, tableName), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("DIAGNOSTIC: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    int responseStatus = jsonObject.getInt("status");
                    if (responseStatus == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        new SharedPrefManager(context).setStringForKey(jsonArray.toString(),Constants.DIAGNOSTIC_TEST_OBJECT);
                        getClinical(NetworkUtils.GET_RESOURCES,userID,NetworkUtils.CLINICAL_TABLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {

            }
        }, true);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getClinical(String ServiceName, Integer userID, String tableName) {
        WebserviceClient client = new WebserviceClient(this, ServiceName, true, getDiseases_clincal_diagnostic_usefull(userID, tableName), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("CLINICAL: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    int responseStatus = jsonObject.getInt("status");
                    if (responseStatus == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        new SharedPrefManager(context).setStringForKey(jsonArray.toString(),Constants.CLINICAL_SIGNS_OBJECT);
                        getUseFull(NetworkUtils.GET_RESOURCES,userID,NetworkUtils.USEFUL_TABLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {

            }
        }, true);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getUseFull(String ServiceName, Integer userID, String tableName) {
        WebserviceClient client = new WebserviceClient(this, ServiceName, true, getDiseases_clincal_diagnostic_usefull(userID, tableName), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("USEFULL: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    int responseStatus = jsonObject.getInt("status");
                    if (responseStatus == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        new SharedPrefManager(context).setStringForKey(jsonArray.toString(),Constants.USEFUL_LINKS_OBJECT);
                        stopSelf();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {

            }
        }, true);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private List<NameValuePair> getDiseases_clincal_diagnostic_usefull(Integer userID, String tableName) {
        try {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(NetworkUtils.USER_ID, String.valueOf(userID)));
            pairs.add(new BasicNameValuePair(NetworkUtils.TABLE_NAME, tableName));
            pairs.add(new BasicNameValuePair(NetworkUtils.GET_COMPLETED_DATA, NetworkUtils.FRESH_DATA));
            return pairs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
