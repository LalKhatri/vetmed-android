package com.Celeritas.VetMed.Utility;

/**
 * Created by Kanaya Lal on 11/1/2018.
 */

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;

public class Style {
    public static void headerCellStyle(PdfPCell cell) {

// alignment
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);

// padding
        cell.setPaddingTop(7f);
        cell.setPaddingBottom(7f);
        cell.setBorderColor(BaseColor.LIGHT_GRAY);

// background color
        cell.setBackgroundColor(new BaseColor(250, 160, 60));

// border
       // cell.setBorder(0);
       // cell.setBorderWidthBottom(2f);

    }
    public static void space(PdfPCell cell) {

// alignment
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);

// padding
        cell.setPaddingTop(7f);
        cell.setPaddingBottom(7f);

// background color
        cell.setBackgroundColor(new BaseColor(255,255,255));

// border
        // cell.setBorder(0);
        // cell.setBorderWidthBottom(2f);

    }

    public static void labelCellStyle(PdfPCell cell) {
// alignment
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

// padding
        cell.setPaddingLeft(12f);
        cell.setPaddingTop(12f);
        cell.setPaddingBottom(12f);
        cell.setPaddingRight(12f);
        cell.setBorderColor(BaseColor.LIGHT_GRAY);


// background color


// border
       // cell.setBorder(0);



// height
        cell.setMinimumHeight(24f);
    }

    public static void valueCellStyle(PdfPCell cell) {
// alignment
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

// padding
        cell.setPaddingTop(0f);
        cell.setPaddingBottom(5f);

// border
        cell.setBorder(0);
        cell.setBorderWidthBottom(0.5f);

// height
        cell.setMinimumHeight(18f);
    }
}