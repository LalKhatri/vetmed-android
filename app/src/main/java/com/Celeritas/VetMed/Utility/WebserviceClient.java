package com.Celeritas.VetMed.Utility;


import android.content.Context;
import android.os.AsyncTask;
import android.provider.SyncStateContract;
import android.util.Log;

import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by anas on 7/14/2018.
 */

public class WebserviceClient extends AsyncTask<String, String, String> {
    private final int TYPE_NORMAL = 1;

    JSONObject jsonObject;
    String serviceName;
    Boolean isPost;
    Context context;
    WebserviceCallback serviceCallback;
    boolean isBackGroundService;
    private String webServiceUrl = null;
    private int mType;
    List<NameValuePair> pairs;


    public WebserviceClient(Context context,
                            String serviceName,
                            Boolean isPost,
                            List<NameValuePair> pairs,
                            WebserviceCallback serviceCallback,
                            boolean isBackGroundService) {
        this.mType = TYPE_NORMAL;
        this.context = context;
        this.serviceName = serviceName;
        this.isPost = isPost;
        this.pairs = pairs;
        this.serviceCallback = serviceCallback;
        this.isBackGroundService = isBackGroundService;
    }

    public WebserviceClient(Context context,
                            Boolean isPost,
                            String queryString,
                            WebserviceCallback serviceCallback,
                            boolean isBackGroundService) {

        this.context = context;
        this.isPost = isPost;
        this.webServiceUrl = queryString;
        this.serviceCallback = serviceCallback;
        this.isBackGroundService = isBackGroundService;
        this.mType = TYPE_NORMAL;
    }

    public void setWebServiceUrl(String webServiceUrl) {
        this.webServiceUrl = webServiceUrl;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (UtilityClass.isConnected(context)) {
            if (!isBackGroundService) {
                ProgressDialogueUtility.showProgressDialogue(context);
            }
        } else {
            UtilityClass.showPopup(context,context.getString(R.string.error),context.getString(R.string.internet));
        }
    }

    @Override
    protected String doInBackground(String... args) {
        String jsonResponseString = null;
        if (UtilityClass.isConnected(context))
            if(mType==TYPE_NORMAL)
                jsonResponseString = invokeWebservice();
        return jsonResponseString;
    }

    @Override
    protected void onPostExecute(String jsonResponseString) {
        ProgressDialogueUtility.hideProgressDialogue();
        if (jsonResponseString == null)
            serviceCallback.onResponseNotReceived();
        else
            serviceCallback.onResponseReceived(jsonResponseString);
    }

    private String invokeWebservice() {
        String data = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            String link = NetworkUtils.BASE_URL + serviceName;
            if (isPost) {
                final HttpPost httppost = new HttpPost(link);
                List<NameValuePair> pairs1 = pairs;
                httppost.setEntity(new UrlEncodedFormEntity(pairs1,"UTF-8"));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                final String response = httpclient.execute(httppost,
                        responseHandler);
                data = response;
            } else {
                if (pairs != null) {
                    String paramString = URLEncodedUtils.format(pairs, "utf-8");
                    link += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(link);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                final String response = httpclient.execute(httpGet, responseHandler);
                data = response;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}

