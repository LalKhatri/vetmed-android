/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.Celeritas.VetMed.Utility;

import android.net.Uri;


import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * These utilities will be used to communicate with the network.
 */
public class NetworkUtils {

    public final static String BASE_URL = "http://ec2-52-86-8-213.compute-1.amazonaws.com/vetmed/apis/";
    /*Create Account*/
    public static String ADD_USER = "addUser.php";
    public static String FORGOT_PASSWORD = "sendResetPasswordEmail.php";
    public static String ACTIVATE_USER = "activateUser.php";
    public static String LOGIN = "login.php";

    public static String ADD_PET_DATA = "addPetData.php";
    public static String GET_PETS = "getPets.php";
    public static String UPDATE_PETS = "editPetData.php";
    public static String DELETE_PETS = "deletePetData.php";
    public static String ADD_PET_IMAGES= "addPetImages.php";

    public static String GET_RESOURCES = "getResources.php";

    public static String GET_PETS_MEDICAL_HISTORY = "getPetsMedicalHistory.php";
    public static String ADD_PET_MEDICAL_HISTORY = "addPetMedicalHistory.php";
    public static String UPDATE_PET_MEDICAL_HISTORY = "editPetMedicalHistory.php";
    public static String DELETE_PET_MEDICAL_HISTORY = "deletePetMedicalHistory.php";

    public static String ADD_PET_MEDICATION = "addMedication.php";
    public static String GET_PET_MEDICATION = "getPetMedications.php";
    public static String UPDATE_PET_MEDICATION = "editPetMedication.php";
    public static String DELETE_PET_MEDICATION = "medicationDelete.php";

    public static String GET_PET_DIET = "getPetDiet.php";
    public static String ADD_PET_DIET = "addDiet.php";
    public static String UPDATE_PET_DIET = "editPetDiet.php";
    public static String DELETE_PET_DIET = "dietDelete.php";

    public static String GET_PREVENTATIVE_VACCINE = "getPreventativeVaccines.php";
    public static String UPDATE_PREVENTATIVE_VACCINE = "editPreventativeVaccines.php";
    public static String ADD_PREVENTATIVE_VACCINE = "addPreventativeVaccines.php";
    public static String DELETE_PREVENTATIVE_VACCINE = "deletePreventativeVaccines.php";

    public static String GET_PREVENTATIVE_DEWORMING = "getPreventativeDeworming.php";
    public static String UPDATE_PREVENTATIVE_DEWORMING = "editPreventativeDeworming.php";
    public static String ADD_PREVENTATIVE_DEWORMING = "addPreventativeDeworming.php";
    public static String DELETE_PREVENTATIVE_DEWORMING = "deletePreventativeDeworming.php";

    public static String GET_VETERINARIAN = "getVeterinarianInformation.php";
    public static String UPDATE_VETERINANRIAN = "editVeterinarianInformation.php";
    public static String ADD_VETERINARIAN = "addVeterinarianInformation.php";
    public static String DELETE_VETERINARIAN = "deleteVeterinarianInformation.php";

    public static String GET_INSURANCE_INFORMATION = "getPetInsuranceInformation.php";
    public static String ADD_INSURANCE_INFORMATION = "addInsuranceInformation.php";
    public static String UPDATE_INSURANCE_INFORMATION = "editInsuranceInformation.php";
    public static String DELETE_INSURANCE_INFORMATION = "deleteInsuranceInformation.php";

    public static String GET_APPOINTMENT = "getAppointments.php";
    public static String ADD_APPOINTMENT = "addAppointments.php";
    public static String UPDATE_APPOINTMENT = "editAppointments.php";
    public static String DELETE_APPOINTMENT = "deleteAppointments.php";

    public static String GET_NOTES = "getNotes.php";
    public static String ADD_NOTES = "addNote.php";
    public static String UPDATE_NOTES = "editPetNote.php";
    public static String DELETE_NOTES = "deleteNote.php";

    public static String USER_NAME = "UserName";
    public static String USER_EMAIL = "UserEmail";
    public static String USER_PASSWORD = "UserPassword";
    public static String USER_ID = "UserID";
    public static String ACTIVATION_CODE = "ActivationCode";

    public static String ID = "ID";
    public static String PET_NAME = "PetName";
    public static String PET_SEX = "PetSex";
    public static String PET_BREED = "PetBreed";
    public static String PET_DOB = "PetDOB";
    public static String PET_WEIGHT = "PetWeight";
    public static String PET_WEIGHT_UNIT = "PetWeightUnit";

    public static String PROFILE_IMAGE = "ProfileImage";
    public static String PET_IMAGES = "PetImages";

    public static String PET_ID = "PetID";
    public static String DISEASE_NAME = "DiseaseName";
    public static String DISEASE_DATE = "DiseaseDate";
    public static String DISEASE_NOTE = "DiseaseNotes";

    public static String TABLE_NAME = "TableName";
    public static String GET_COMPLETED_DATA = "getCompleteData";
    public static String FRESH_DATA = "FreshData";

    public static String DISEASE_TABLE = "Diseases";
    public static String DIAGNOSTIC_TABLE = "DiagnosticTests";
    public static String CLINICAL_TABLE = "ClinicalSigns";
    public static String USEFUL_TABLE = "UsefulLinks";

    public static String MEDICATION_NAME = "MedicationName";
    public static String MEDICATION_FOR_HOW_LONG = "MedicationForHowLong";
    public static String MEDICATION_DOSAGE = "MedicationDosage";
    public static String MEDICATION_FREQUENCY = "MedicationFrequency";
    public static String MEDICATION_ROUTE = "MedicationRoute";
    public static String MEDICATION_DATE_STARTED = "MedicationDateStarted";
    public static String MEDICATION_ACTIVE = "MedicationActive";

    public static String DIET_NAME = "DietName";
    public static String DIET_HOW_MUCH = "DietHowMuch";
    public static String DIET_HOW_OFTEN = "DietHowOften";
    public static String DIET_END_DATE = "DietEndDate";
    public static String DIET_DATE_STARTED = "DietDateStarted";
    public static String DIET_TYPE = "DietType";
    public static String DIET_NOTES = "DietNotes";
    public static String DIET_ACTIVE = "DietActive";

    public static String VACCINE_NAME = "VaccineName";
    public static String VACCINE_DATE_GIVEN = "VaccineDateGiven";

    public static String VACCINE_SIDE_EFFECTS = "VaccineSideEffects";
    public static String VACCINE_NOTES = "VaccineNotes";

    public static String DEWORMING_PRODUCT_NAME = "DFTProductName";
    public static String DEWORMING_DATE_STARTED = "DFTDateStarted";
    public static String DEWORMING_END_DATE = "DFTEndDate";
    public static String DEWORMING_HOW_OFTEN = "DFTHowOften";
    public static String DEWORMING_ACTIVE = "DFTActive";
    public static String DEWORMING_NOTES = "DFTNotes";

    public static String VETERINARIAN_HOSPITAL_NAME = "VIHospital";
    public static String VETERINARIAN_DOCTOR_NAME = "VIDoctorName";
    public static String VETERINARIAN_ADDRESS_NAME = "VIAddress";
    public static String VETERINARIAN_EMAIL_NAME = "VIEmail";
    public static String VETERINARIAN_PHONE_NUMBER = "VIPhoneNumber";
    public static String VETERINARIAN_WEBSITE = "VIWebsite";
    public static String VETERINARIAN_NOTES = "VINotes";

    public static String INSURANCE_COMPANY_NAME = "InsuranceInfoName";
    public static String INSURANCE_POLICY_NUMBER = "InsuranceInfoPolicyNumber";
    public static String INSURANCE_EMAIL = "InsuranceInfoEmail";
    public static String INSURANCE_WEBSITE = "InsuranceInfoWebsite";
    public static String INSURANCE_PHONE_NUMBER = "InsuranceInfoPhoneNumber";

    public static String APPOINTMENT_DATE = "AppointmentDate";
    public static String APPOINTMENT_TIME = "AppointmentTime";
    public static String APPOINTMENT_LOCATION = "AppointmentLocation";
    public static String APPOINTMENT_DOCTOR_NAME = "AppointmentDoctorName";
    public static String APPOINTMENT_NOTES = "AppointmentNotes";

    public static String NOTE_TITLE = "NoteTitle";
    public static String NOTE_TEXT = "NoteText";

}