package com.Celeritas.VetMed.Utility;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Callback.IAlertLisetner;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.R;

import org.w3c.dom.Text;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.Celeritas.VetMed.Utility.FilePath.getDataColumn;
import static com.Celeritas.VetMed.Utility.FilePath.isDownloadsDocument;


/**
 * Created by anas on 7/21/2018.
 */

public class UtilityClass{

    public static final SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
    private static final String TAG = "UtilityClass";
    private final Context context;
    private final SharedPreferences preferences;

    SharedPreferences.Editor editor;

    public static final String[] daysValue = { " Today", " One", " Two", " Three", " Four", " Five", " Six"};
    public static final String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Jul","Aug","Sep","Oct","Nov","Dec"};


    public UtilityClass(Context context) {

        this.context = context;
        preferences = context.getSharedPreferences("VetMed", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void savedata(String key, String val) {
        editor.putString(key, val).commit();
    }

    public String getdata(String key) {
        String value = preferences.getString(key, "");
        if (value.isEmpty()) {
            Log.i(TAG, key + " not found.");
            return "";
        }
        return value;
    }

    //Show popup
    public static void showPopup(Context context, String title, String message,IAlertLisetner lisetner){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.custom_dailog, null);
        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        ((TextView)promptsView.findViewById(R.id.alert_title)).setText(title);
        ((TextView)promptsView.findViewById(R.id.msg)).setText(message);
        TextView ok = (TextView) promptsView.findViewById(R.id.alert_ok);
        ok.setOnClickListener(v -> {
            lisetner.onOkClick();
            alertDialog.dismiss();
        });

        alertDialog.show();
    }

    public static void showDeletPopup(Context context, String title, String message,IAlertLisetner lisetner){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.custom_dailog, null);
        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        ((TextView)promptsView.findViewById(R.id.alert_title)).setText(title);
        ((TextView)promptsView.findViewById(R.id.msg)).setText(message);
        ((LinearLayout) promptsView.findViewById(R.id.delet_alert)).setVisibility(View.VISIBLE);
        ((TextView) promptsView.findViewById(R.id.alert_ok)).setVisibility(View.GONE);
        TextView yes = (TextView) promptsView.findViewById(R.id.alert_yes);
        yes.setOnClickListener(v -> {
            lisetner.onOkClick();
            alertDialog.dismiss();
        });
        TextView no = (TextView) promptsView.findViewById(R.id.alert_No);
        no.setOnClickListener(view -> {
            alertDialog.dismiss();
        });
        alertDialog.show();
    }

    public static void showDeletPopup(Context context, String title, String message,IAlertListner lisetner){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.custom_dailog, null);
        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        ((TextView)promptsView.findViewById(R.id.alert_title)).setText(title);
        ((TextView)promptsView.findViewById(R.id.msg)).setText(message);
        ((LinearLayout) promptsView.findViewById(R.id.delet_alert)).setVisibility(View.VISIBLE);
        ((TextView) promptsView.findViewById(R.id.alert_ok)).setVisibility(View.GONE);
        TextView yes = (TextView) promptsView.findViewById(R.id.alert_yes);
        yes.setOnClickListener(v -> {
            lisetner.onYesClick();
            alertDialog.dismiss();
        });
        TextView no = (TextView) promptsView.findViewById(R.id.alert_No);
        no.setOnClickListener(view -> {
            lisetner.onNoClick();
            alertDialog.dismiss();
        });
        alertDialog.show();
    }

    public static void showPopup(Context context, String title, String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.custom_dailog, null);
        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        ((TextView)promptsView.findViewById(R.id.alert_title)).setText(title);
        ((TextView)promptsView.findViewById(R.id.msg)).setText(message);
        TextView ok = (TextView) promptsView.findViewById(R.id.alert_ok);
        ok.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        alertDialog.show();
    }

    public static String converListToComaSeperated(List<String> list){
        String allKeys = TextUtils.join(",", list);
        return allKeys;
    }


    public static void showPopupwithCancel(Context context, String title, String message){
        new AlertDialog.Builder(context).setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }



    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable()){
            return true;
        }
        return false;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    public static boolean isValidPhone(String phone)
    {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone))
        {
            if(phone.length() < 6 || phone.length() > 15)
            {
                check = false;

            }
            else
            {
                check = true;

            }
        }
        else
        {
            check=false;
        }
        return check;
    }

    public final static boolean isValidWebSite(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.WEB_URL.matcher(target).matches();
        }
    }
    public static String getCurrentDate(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
        String formattedDate = df.format(c);
        return formattedDate;
    }
    public static String getCurrentDateRequiredFormate(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = df.format(c);
        return formattedDate;
    }
    public static int  calculateDays(String date){

        SimpleDateFormat myFormat = new SimpleDateFormat("dd MMM yyyy");
//        String year = "/20"+date.split("/")[2];
       // String givenDate = date.split("/")[0]+"/"+date.split("/")[1]+year;
        float daysBetween = 0;
        try {
            Date dateBefore = myFormat.parse(date);
            Date dateAfter = myFormat.parse(getCurrentDateRequiredFormate());

            long difference = dateAfter.getTime() - dateBefore.getTime();
             daysBetween = (difference / (1000*60*60*24));
            /* You can also convert the milliseconds to days using this method
             * float daysBetween =
             *         TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS)
             */
            System.out.println("Number of Days between dates: "+daysBetween);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (int)daysBetween;
    }
    public static String convertNumberIntoWords(int days){
        String numberWord = "";
        String AGO = " Ago";
        String DAYS = " Days";
        String DAY = " Day";
        String WEEK = " Week";
        String WEEKS = " Weeks";
        String MONTH = " Month";
        String MONTHS = " Months";
        String YEAR = " Year";
        String YEARS = " Years";

        switch (days){
            case 0:
                numberWord = daysValue[days];
                break;
            case 1:
                numberWord = daysValue[days]+DAY+AGO;
                break;
            case 2:
                numberWord = daysValue[days]+DAYS+AGO;
                break;
            case 3:
                numberWord = daysValue[days]+DAYS+AGO;
                break;
            case 4:
                numberWord = daysValue[days]+DAYS+AGO;
                break;
            case 5:
                numberWord = daysValue[days]+DAYS+AGO;
                break;
            case 6:
                numberWord = daysValue[days]+DAYS+AGO;
                break;
            case 7:
                numberWord = daysValue[days]+WEEK+AGO;
                break;
            default:
                if (days>7 && days <14){
                    numberWord ="More Than"+ daysValue[days]+WEEK+AGO;
                }
                if (days>14 && days <30){
                    numberWord ="More Than"+ daysValue[2]+WEEKS+AGO;
                }
                if (days>30 && days <60){
                    numberWord ="More Than"+ daysValue[1]+MONTH+AGO;
                }
                if (days>60 && days <90){
                    numberWord ="More Than"+ daysValue[2]+MONTHS+AGO;
                }
                if (days>=366 || days>=365){
                    numberWord =""+ daysValue[1]+YEAR+AGO;
                }
                break;
        }
        return numberWord;
    }
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    @SuppressLint("NewApi")
    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                String id;
                if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.O){

                    id = DocumentsContract.getDocumentId(uri);
                    if (!TextUtils.isEmpty(id)) {
                        if (id.startsWith("raw:")) {
                            return id.replaceFirst("raw:", "");
                        }
                        try {
                            uri = ContentUris.withAppendedId(
                                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                            return getDataColumn(context, uri, null, null);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                    //id = DocumentsContract.getDocumentId(uri);
                    //uri = ContentUris.withAppendedId(Uri.parse("content://Download/public_downloads"), Long.valueOf(id));
                }else {
                    id = DocumentsContract.getDocumentId(uri);
                    uri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                }

            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }
}
