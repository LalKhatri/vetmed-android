package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class MedicalHistoryAdapter extends RecyclerView.Adapter<MedicalHistoryAdapter.MyViewHolder> {
    private List<MedicalHistoryModelUpdated> medicalHistoryList;
    private Context context;
    private int index;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView diseaseName, date, note;


        public MyViewHolder(View view) {
            super(view);
            diseaseName = view.findViewById(R.id.disease_name_tv);
            date = view.findViewById(R.id.medical_date);
            note = view.findViewById(R.id.disease_notes);
        }
    }

    public MedicalHistoryAdapter(List<MedicalHistoryModelUpdated> medicalHistoryList, Context context) {
        this.medicalHistoryList = medicalHistoryList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medical_history_item, parent, false);
        return new MyViewHolder(itemView); //
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        setDataInComponents(holder, position);

    }

    private void setDataInComponents(final MyViewHolder holder, final int position) {
        holder.date.setText(context.getResources().getString(R.string.medical_date_of_diagnosis) +" "+ medicalHistoryList.get(position).getDiseaseDate());
        holder.diseaseName.setText(context.getResources().getString(R.string.medi_disease_name) +" "+ medicalHistoryList.get(position).getDiseaseName());
        holder.note.setText(context.getResources().getString(R.string.disease_note) +" "+ medicalHistoryList.get(position).getDiseaseNotes());

    }

    @Override
    public int getItemCount() {
        return medicalHistoryList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}