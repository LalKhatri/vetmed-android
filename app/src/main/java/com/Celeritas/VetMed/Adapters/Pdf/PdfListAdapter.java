package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.Celeritas.VetMed.Models.PdfDetails;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class PdfListAdapter extends RecyclerView.Adapter<PdfListAdapter.MyViewHolder> {
    private List<PdfDetails> list;
    private Context context;
    private int index;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CheckBox checkBox;



        public MyViewHolder(View view) {
            super(view);
            checkBox = view.findViewById(R.id.checkbox);

        }
    }
    public PdfListAdapter(List<PdfDetails> list, Context context) {
        this.list = list;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_share_pdf, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder, int position) {
        holder.checkBox.setText(list.get(position).getName());
        if(list.get(position).isSelected()){
            holder.checkBox.setChecked(true);
        }else {
            holder.checkBox.setChecked(false);
        }
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                list.get(position).setSelected(isChecked);
            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    public List<PdfDetails> selectedQuesList() {
        return list;
    }
}