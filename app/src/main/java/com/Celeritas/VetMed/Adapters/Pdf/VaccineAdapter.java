package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.VaccineModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class VaccineAdapter extends RecyclerView.Adapter<VaccineAdapter.MyViewHolder> {
    private List<VaccineModelUpdated> vaccineList;
    private Context context;
    private int index;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView VaccineName, date, note,sidEffect;


        public MyViewHolder(View view) {
            super(view);
            VaccineName = view.findViewById(R.id.vaccine_name_tv);
            date = view.findViewById(R.id.date_given_tv);
            note = view.findViewById(R.id.notes);
            sidEffect = view.findViewById(R.id.side_effect_tv);
        }
    }

    public VaccineAdapter(List<VaccineModelUpdated> vaccineList, Context context) {
        this.vaccineList = vaccineList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.preventive_item, parent, false);
        return new MyViewHolder(itemView); //
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        setDataInComponents(holder, position);

    }

    private void setDataInComponents(final MyViewHolder holder, final int position) {
        holder.date.setText(context.getResources().getString(R.string.date_given) +" "+ vaccineList.get(position).getVaccineDateGiven());
        holder.VaccineName.setText(context.getResources().getString(R.string.vaccine_name) +" "+ vaccineList.get(position).getVaccineName());
        holder.note.setText(context.getResources().getString(R.string.disease_note) +" "+ vaccineList.get(position).getVaccineNotes());
        holder.sidEffect.setText(context.getResources().getString(R.string.vi_side_effects) +" "+ vaccineList.get(position).getVaccineSideEffects());

    }

    @Override
    public int getItemCount() {
        return vaccineList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}