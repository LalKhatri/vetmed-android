package com.Celeritas.VetMed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    List<Item> mData;
    IitemClickListener listener;
    List<Item> ListFiltered;

    public BaseAdapter(List<Item> mData) {
        this.mData = mData;
        ListFiltered = new ArrayList<Item>();
        ListFiltered.addAll(mData);
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).getViewType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case Constants.GENERAL_INFORMATION_DETAIL: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_general_information_detail, parent, false);
                return new GeneratInformationDetailViewHolder(itemView,listener);
            }
            case Constants.PET_MAIN_DETAIL: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pet_detail_row, parent, false);
                return new PetDetailViewHolder(itemView,listener);
            }
            case Constants.RESOURSE_MAIN: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.resourses_row, parent, false);
                return new ResoursesViewHolder(itemView,listener);
            }
            case Constants.VETERINARIAN: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_veterinarian_information_detail, parent, false);
                return new VeterinarianViewHolder(itemView,listener);
            }
            case Constants.INSURANCE: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_insurance, parent, false);
                return new InsuranceViewHolder(itemView,listener);
            }case Constants.APPOINTMENT: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_appointment, parent, false);
                return new InsuranceViewHolder(itemView,listener);
            }/*case Constants.GALLERY: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gallary, parent, false);
                return new GalleryViewHolder(itemView,listener);
            }*/
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Item item = mData.get(position);
        switch (item.getViewType()) {
            case Constants.GENERAL_INFORMATION_DETAIL: {
                GeneratInformationDetailViewHolder generalHolder = (GeneratInformationDetailViewHolder) holder;
                generalHolder.bind(item);
                break;
            }
            case Constants.PET_MAIN_DETAIL:{
                PetDetailViewHolder detailHolder = (PetDetailViewHolder) holder;
                detailHolder.bind(item);
                break;
            }
            case Constants.RESOURSE_MAIN:{
                ResoursesViewHolder resoursesHolder = (ResoursesViewHolder) holder;
                resoursesHolder.bind(item);
                break;
            }
            case Constants.VETERINARIAN:{
                VeterinarianViewHolder veterinarianViewHolder = (VeterinarianViewHolder) holder;
                veterinarianViewHolder.bind(item);
                break;
            }
            case Constants.INSURANCE:{
                InsuranceViewHolder insuranceViewHolder = (InsuranceViewHolder) holder;
                insuranceViewHolder.bind(item);
                break;
            }
            case Constants.APPOINTMENT:{
                InsuranceViewHolder insuranceViewHolder = (InsuranceViewHolder) holder;
                insuranceViewHolder.bind(item);
                break;
            }/*case Constants.GALLERY:{
                GalleryViewHolder galleryViewHolder = (GalleryViewHolder) holder;
                galleryViewHolder.bind(item);
                break;
            }*/
        }
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public void setOnItemClickListner(IitemClickListener itemClickListner){
        this.listener = itemClickListner;
    }

    public interface IitemClickListener{
        void onItemClick(int postion, View view);
    }

    public List<Item> filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mData.clear();
        if (charText.length() == 0) {
            mData.addAll(ListFiltered);

        } else {
            for (Item item : ListFiltered) {
                if (charText.length() != 0 && item.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mData.add(item);
                }

            }
        }
        notifyDataSetChanged();
        return mData;
    }

}
