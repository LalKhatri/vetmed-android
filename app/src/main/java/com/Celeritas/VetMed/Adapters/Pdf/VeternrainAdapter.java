package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.pets.DietAddActivity;
import com.Celeritas.VetMed.Models.pets.VeterinarianModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class VeternrainAdapter extends RecyclerView.Adapter<VeternrainAdapter.MyViewHolder> {
    private List<VeterinarianModelUpdated> veternarianList;
    private Context context;
    private int index;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView doctorName,hospitalName, address, phone, website,note,email;



        public MyViewHolder(View view) {
            super(view);
            doctorName = view.findViewById(R.id.dr_name_tv);
            hospitalName = view.findViewById(R.id.name_tv);
            address = view.findViewById(R.id.address_tv);
            phone = view.findViewById(R.id.phone_tv);
            website = view.findViewById(R.id.website_tv);
            email= view.findViewById(R.id.email_tv);
            note= view.findViewById(R.id.notes);
        }
    }
    public VeternrainAdapter(List<VeterinarianModelUpdated> veternarianList, Context context) {
        this.veternarianList = veternarianList;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.veternirian_item, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder, int position) {
        holder.hospitalName.setText(context.getResources().getString(R.string.hospital_name) +" "+ veternarianList.get(position).getVIHospital());
        holder.doctorName.setText(context.getResources().getString(R.string.dr_name)+" "+ veternarianList.get(position).getVIDoctorName());
        holder.address.setText(context.getResources().getString(R.string.vet_address)+" "+ veternarianList.get(position).getVIAddress());
        holder.phone.setText(context.getResources().getString(R.string.vet_phone)+" "+ veternarianList.get(position).getVIPhoneNumber());
        holder.email.setText(context.getResources().getString(R.string.vet_email)+" "+ veternarianList.get(position).getVIEmail());
        holder.website.setText(context.getResources().getString(R.string.vet_website)+" "+ veternarianList.get(position).getVIWebsite());
        holder.note.setText(context.getResources().getString(R.string.medi_how_often)+" "+ veternarianList.get(position).getVINotes());
    }

    @Override
    public int getItemCount() {
        return veternarianList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}