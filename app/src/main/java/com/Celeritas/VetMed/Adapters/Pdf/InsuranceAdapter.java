package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.InsuranceModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class InsuranceAdapter extends RecyclerView.Adapter<InsuranceAdapter.MyViewHolder> {
    private List<InsuranceModelUpdated> insuranceList;
    private Context context;
    private int index;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView companyName, policyNumber, email, website, phone;



        public MyViewHolder(View view) {
            super(view);
            companyName = view.findViewById(R.id.name_tv);
            policyNumber = view.findViewById(R.id.policy_tv);
            email = view.findViewById(R.id.email_tv);
            website = view.findViewById(R.id.website_tv);
            phone = view.findViewById(R.id.phone_tv);
        }
    }
    public InsuranceAdapter(List<InsuranceModelUpdated> insuranceList, Context context) {
        this.insuranceList = insuranceList;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.inssurance_item, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder, int position) {
        holder.companyName.setText(context.getResources().getString(R.string.company_name)+" "+ insuranceList.get(position).getInsuranceInfoName());
        holder.policyNumber.setText(context.getResources().getString(R.string.insurance_policy) +" "+ insuranceList.get(position).getInsuranceInfoPolicyNumber());
        holder.email.setText(context.getResources().getString(R.string.vet_email)+" "+ insuranceList.get(position).getInsuranceInfoEmail());
        holder.website.setText(context.getResources().getString(R.string.vet_website)+" "+ insuranceList.get(position).getInsuranceInfoWebsite());
        holder.phone.setText(context.getResources().getString(R.string.vet_phone)+" "+ insuranceList.get(position).getInsuranceInfoPhoneNumber());



    }

    @Override
    public int getItemCount() {
        return insuranceList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}