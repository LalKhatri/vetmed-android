package com.Celeritas.VetMed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;

public class VeterinarianViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    TextView title;
    TextView discription;
    ImageView icon;
    BaseAdapter.IitemClickListener listener;

    public VeterinarianViewHolder(View itemView, BaseAdapter.IitemClickListener listener) {
        super(itemView);
        this.listener = listener;
        title = itemView.findViewById(R.id.title);
        discription = itemView.findViewById(R.id.discription);
        icon = itemView.findViewById(R.id.icon);
        itemView.setOnClickListener(this);
    }

    public void bind(Item item) {
        title.setText(item.getTitle());
        discription.setText(item.getDescription());
        icon.setImageResource(item.getImage());

    }
    @Override
    public void onClick(View view) {
        if (listener != null){
            listener.onItemClick(getAdapterPosition(),view);
        }
    }
}
