package com.Celeritas.VetMed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;

import butterknife.BindView;

public class PetDetailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    //@BindView(R.id.title)
    TextView title;
    BaseAdapter.IitemClickListener listener;

    public PetDetailViewHolder(View itemView, BaseAdapter.IitemClickListener listener) {
        super(itemView);
        this.listener = listener;
        title = (TextView) itemView.findViewById(R.id.title);
        itemView.setOnClickListener(this);
    }

    public void bind(final Item item) {

        title.setText(item.getTitle());
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClick(getAdapterPosition(), view);
        }
    }
}
