package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.DewormingModelUpdated;
import com.Celeritas.VetMed.R;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class DewormingAdapter extends RecyclerView.Adapter<DewormingAdapter.MyViewHolder> {
    private List<DewormingModelUpdated> dewormingModelUpdatedList;
    private Context context;
    private int index;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView medicationName, date,dateEnd, active,howOften,notes;



        public MyViewHolder(View view) {
            super(view);
            medicationName = view.findViewById(R.id.medication_name_tv);
            date = view.findViewById(R.id.started_tv);
            dateEnd = view.findViewById(R.id.end_date_tv);
            howOften = view.findViewById(R.id.how_often_tv);
            active = view.findViewById(R.id.active_tv);
            notes = view.findViewById(R.id.notes);
        }
    }
    public DewormingAdapter(List<DewormingModelUpdated> dewormingModelUpdatedList, Context context) {
        this.dewormingModelUpdatedList = dewormingModelUpdatedList;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deworming_item, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder, int position) {

        holder.medicationName.setText(context.getResources().getString(R.string.product_name)+" "+ dewormingModelUpdatedList.get(position).getDFTProductName());
        holder.date.setText(context.getResources().getString(R.string.diet_date_started) +" "+ dewormingModelUpdatedList.get(position).getDFTDateStarted());
        holder.dateEnd.setText(context.getResources().getString(R.string.date_end) +" "+ dewormingModelUpdatedList.get(position).getDFTEndDate());
        holder.howOften.setText(context.getResources().getString(R.string.medi_how_often)+" "+ dewormingModelUpdatedList.get(position).getDFTHowOften());
        holder.active.setText(context.getResources().getString(R.string.med_active)+" "+ dewormingModelUpdatedList.get(position).getDFTActive());
        holder.notes.setText(context.getResources().getString(R.string.disease_note)+" "+ dewormingModelUpdatedList.get(position).getDFTNotes());

    }

    @Override
    public int getItemCount() {
        return dewormingModelUpdatedList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}