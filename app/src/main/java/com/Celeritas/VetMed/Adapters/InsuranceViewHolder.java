package com.Celeritas.VetMed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;

public class InsuranceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    TextView title;
    TextView policyNo;
    BaseAdapter.IitemClickListener listener;

    public InsuranceViewHolder(View itemView, BaseAdapter.IitemClickListener listener) {
        super(itemView);
        itemView.setOnClickListener(this);
        this.listener = listener;
        title = itemView.findViewById(R.id.title);
        policyNo = itemView.findViewById(R.id.policyNum);
    }

    public void bind(Item item) {
        title.setText(item.getTitle());
        policyNo.setText(item.getDescription());

    }

    @Override
    public void onClick(View view) {
        if (listener != null){
            listener.onItemClick(getAdapterPosition(),view);
        }
    }
}
