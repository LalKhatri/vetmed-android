package com.Celeritas.VetMed.Adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;
import java.util.Random;

import static com.Celeritas.VetMed.Constants.IMAGES_PATH;

public class MyPetAdapter extends RecyclerView.Adapter<MyPetAdapter.MyViewHolder>{

    IitemClickListener listener;
    List<PetModelUpdated> arrayList;
    Context context;
    private Random rand = new Random();
    private int x = rand.nextInt(10);

    public MyPetAdapter(List<PetModelUpdated> list){
        this.arrayList = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pet_row,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.petName.setText(arrayList.get(position).getPetName());
        setProfile(arrayList.get(position),holder.petImage);
        /*if (!arrayList.get(position).getPetImages().equalsIgnoreCase("NA")){
            holder.petImage.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH+arrayList.get(position).getPetImages().split(",")[0]+".jpg")));
        }*/

    }

    private void setProfile(PetModelUpdated model,ImageView imageView) {
        if (!model.getProfileImage().equalsIgnoreCase("NA") && !model.getProfileImage().equalsIgnoreCase("Randomly")){
            //holder.petImage.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH+arrayList.get(position).getProfileImage()+".jpg")));
            if (model.getProfileImage().contains("Image_")){
                imageView.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH+model.getId()+"/"+model.getProfileImage()+".jpg")));
            }else {
                imageView.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH+model.getId()+"/"+model.getPetImages()+".jpg")));
            }
        }else {
            if (model.getPetImages()!=null && model.getPetImages().split(",").length!=0){
                if (model.getPetImages().split(",").length>=2){
                    if (model.getProfileImage().equalsIgnoreCase("Randomly")){
                        x = rand.nextInt(model.getPetImages().split(",").length);
                        imageView.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH+model.getId()+"/"+model.getPetImages().split(",")[x]+".jpg")));
                    }

                }else {
                    if (!model.getPetImages().split(",")[0].equalsIgnoreCase("NA")){
                        imageView.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH+model.getId()+"/"+model.getPetImages().split(",")[0]+".jpg")));
                    }
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void removeItem(int position) {
        arrayList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(PetModelUpdated item, int position) {
        arrayList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        TextView petName;
        ImageView petImage;
        public RelativeLayout viewBackground, viewForeground;
        public MyViewHolder(View v) {
            super(v);
            petName = (TextView) v.findViewById(R.id.petName);
            petImage = (ImageView) v.findViewById(R.id.img);
            viewBackground = v.findViewById(R.id.view_background);
            viewForeground = v.findViewById(R.id.view_foreground);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null){
                listener.onItemClick(getAdapterPosition(),view);
            }
        }
    }

    public void setOnItemClickListner(IitemClickListener itemClickListner){
        this.listener = itemClickListner;
    }

    public interface IitemClickListener{
        void onItemClick(int postion, View view);
    }
}