package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.NotesModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {
    private List<NotesModelUpdated> notesList;
    private Context context;
    private int layout;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView noteTitle,notes;




        public MyViewHolder(View view) {
            super(view);
            noteTitle = view.findViewById(R.id.note_title);
            notes = view.findViewById(R.id.notes);

        }
    }
    public NotesAdapter(List<NotesModelUpdated> notesList, Context context, int layout) {
        this.notesList = notesList;
        this.context = context;
        this.layout = layout;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder,int position) {
                holder.noteTitle.setText(context.getResources().getString(R.string.note_title)+" "+ notesList.get(position).getNoteTitle());
                holder.notes.setText(context.getResources().getString(R.string.disease_note)+" "+ notesList.get(position).getNoteText());




    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}