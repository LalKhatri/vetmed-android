package com.Celeritas.VetMed.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.Celeritas.VetMed.Checkbox.CheckableRelativeLayout;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by fazal on 9/11/2017.
 */

public class CheckboxAdapter extends ArrayAdapter<String> {
    List<String> list;
    private ViewHolder holder;

    public CheckboxAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.list = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            Context context = parent.getContext();
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.checked_item,null,false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        String item = getItem(position);
        ListView listView = (ListView)parent;
        holder.title_tv.setText(item);
        holder.layout.setChecked(listView.isItemChecked(position));



        return convertView;

    }

    class ViewHolder {
        TextView title_tv;
        CheckBox checkBox;
        CheckableRelativeLayout layout;

        public ViewHolder(View itemView) {
            title_tv = itemView.findViewById(R.id.title_tv);
            checkBox = itemView.findViewById(R.id.checkbox);
            layout = (CheckableRelativeLayout)itemView.findViewById(R.id.layout);
        }
    }
}
