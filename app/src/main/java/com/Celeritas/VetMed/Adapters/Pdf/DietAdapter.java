package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class DietAdapter extends RecyclerView.Adapter<DietAdapter.MyViewHolder> {
    private List<DietModelUpdated> medicalHistoryList;
    private Context context;
    private int index;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView dietName, startDate,endDate,  type,active, howMuch,howOften,note;



        public MyViewHolder(View view) {
            super(view);
            dietName = view.findViewById(R.id.name_tv);
            startDate = view.findViewById(R.id.start_date_tv);
            endDate = view.findViewById(R.id.end_date_tv);
            howOften = view.findViewById(R.id.how_often_tv);
            howMuch = view.findViewById(R.id.how_much_tv);
            active = view.findViewById(R.id.active_tv);
            note = view.findViewById(R.id.notes);
            type = view.findViewById(R.id.type_tv);
        }
    }
    public DietAdapter(List<DietModelUpdated> medicalHistoryList, Context context) {
        this.medicalHistoryList = medicalHistoryList;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.diet_item, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder, int position) {

        holder.dietName.setText(context.getResources().getString(R.string.medi_name)+" "+medicalHistoryList.get(position).getDietName());
        holder.type.setText(context.getResources().getString(R.string.type)+" "+medicalHistoryList.get(position).getDietType());
        holder.howMuch.setText(context.getResources().getString(R.string.how_much)+" "+medicalHistoryList.get(position).getDietHowMuch());
        if (medicalHistoryList.get(position).getDietHowOften().contains(context.getResources().getString(R.string.any_other))){
            holder.howOften.setText(context.getResources().getString(R.string.medi_how_often)+" "+medicalHistoryList.get(position).getDietHowOften().split(",")[1]);
        }else {
            holder.howOften.setText(context.getResources().getString(R.string.medi_how_often)+" "+medicalHistoryList.get(position).getDietHowOften());
        }

        holder.startDate.setText(context.getResources().getString(R.string.diet_date_started) +" "+ medicalHistoryList.get(position).getDietDateStarted());
        holder.endDate.setText(context.getResources().getString(R.string.date_end) +" "+ medicalHistoryList.get(position).getDietEndDate());
        holder.active.setText(context.getResources().getString(R.string.med_active)+" "+medicalHistoryList.get(position).getDietActive());
        holder.note.setText(context.getResources().getString(R.string.disease_note)+" "+medicalHistoryList.get(position).getDietNotes());


    }

    @Override
    public int getItemCount() {
        return medicalHistoryList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}