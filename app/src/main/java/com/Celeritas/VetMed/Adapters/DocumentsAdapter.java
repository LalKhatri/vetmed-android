package com.Celeritas.VetMed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Celeritas.VetMed.Activities.pets.InsuranceInformationActivity;
import com.Celeritas.VetMed.Activities.pets.documents.DocumentsGalleryActivity;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.Documents;
import com.Celeritas.VetMed.Models.pets.InsuranceModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.UtilityClass;

import java.util.List;

import static com.Celeritas.VetMed.R.drawable.ic_file_two;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.MyViewHolder> {
    private List<Documents> documentsList;
    private Context context;
    private boolean isFromDelete;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView fileName;
        private ImageView fileImage;
        private ImageView ivRemoveDoc;





        public MyViewHolder(View view) {
            super(view);
            fileName = view.findViewById(R.id.file_name);
            ivRemoveDoc = view.findViewById(R.id.iv_remove_doc);
            fileImage = view.findViewById(R.id.picture);

        }
    }
    public DocumentsAdapter(List<Documents> documentsList, Context context,boolean isFromDelete) {
        this.documentsList = documentsList;
        this.context = context;
        this.isFromDelete = isFromDelete;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_documents_gallary, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder,int position) {
        if (documentsList.get(position).getDocName().contains("/")){
            String []doc = documentsList.get(position).getDocName().split("/");
            holder.fileName.setText(documentsList.get(position).getDocName().split("/")[doc.length-1]);
        }else {
            holder.fileName.setText(documentsList.get(position).getDocName());
        }


//                holder.fileName.setText(documentsList.get(position).getDocName());
                if (isFromDelete){
                    holder.ivRemoveDoc.setVisibility(View.VISIBLE);
                }else {
                    holder.ivRemoveDoc.setVisibility(View.GONE);
                }
                if (documentsList.get(position).getDocName().contains(".jpg") ||documentsList.get(position).getDocName().contains(".png") ||documentsList.get(position).getDocName().contains(".jpeg")){
                    holder.fileImage.setImageResource(R.drawable.ic_file_one);
                }else {
                    holder.fileImage.setImageResource(R.drawable.ic_file_two);

                }
                holder.ivRemoveDoc.setOnClickListener(view ->
                {
                    UtilityClass.showDeletPopup(context, context.getResources().getString(R.string.alert), context.getResources().getString(R.string.document_delete_confirmation), new IAlertListner() {
                        @Override
                        public void onYesClick() {
                            Documents selectedDocument =  documentsList.get(position);
                            Documents note = Documents.findById(Documents.class, selectedDocument.getId());
                            note.delete();
                            documentsList.remove(selectedDocument);
                            notifyDataSetChanged();
                            ((DocumentsGalleryActivity)context).showOrHideAddDocView(documentsList);
                            //populateList(petID);
                        }

                        @Override
                        public void onNoClick() {
                            notifyDataSetChanged();
                        }
                    });
                    //Toast.makeText(context, "Remove", Toast.LENGTH_SHORT).show();
                });
        holder.fileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (documentsList.get(position).getDocPath() !=null && documentsList.get(position).getDocPath()!="" && !documentsList.get(position).getDocPath().isEmpty()){
                    ((DocumentsGalleryActivity)context).openFile(documentsList.get(position).getDocPath(),context);
                }else {
                    ((DocumentsGalleryActivity)context).openFile(((DocumentsGalleryActivity)context).docPath,context);
                }

            }
        });
                //holder.fileName.setText(documentsList.get(position).getDocName());

    }

    @Override
    public int getItemCount() {
        return documentsList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}