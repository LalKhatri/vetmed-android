package com.Celeritas.VetMed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;

import butterknife.BindView;

public class GeneratInformationDetailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    TextView title;
    TextView discription;
    BaseAdapter.IitemClickListener listener;

    public GeneratInformationDetailViewHolder(View itemView , BaseAdapter.IitemClickListener listener) {
        super(itemView);
        this.listener = listener;
        title = itemView.findViewById(R.id.title);
        discription = itemView.findViewById(R.id.discription);
        itemView.setOnClickListener(this);
    }

    public void bind(final Item item) {
        title.setText(item.getTitle());
        discription.setText(item.getDescription());

    }

    @Override
    public void onClick(View view) {
        if (listener != null){
            listener.onItemClick(getAdapterPosition(),view);
        }
    }
}
