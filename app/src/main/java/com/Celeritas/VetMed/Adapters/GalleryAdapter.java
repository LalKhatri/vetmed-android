package com.Celeritas.VetMed.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.Celeritas.VetMed.Models.GalleryModel;
import com.Celeritas.VetMed.R;
import com.squareup.picasso.Picasso;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.List;

import static com.Celeritas.VetMed.Constants.IMAGES_PATH;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    IitemClickListener listener;
    IitemLongClickListener listener1;
    Context context;
    String imagePath;
    List<GalleryModel> arrayList;
    private int row_index = -1;
    private Bitmap bmp;

    public GalleryAdapter(List<GalleryModel> list, String imagePath){
        this.arrayList = list;
        this.imagePath = imagePath;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gallary, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {



        try {
           /* ExifInterface ei = new ExifInterface(arrayList.get(position).getLocalImagePath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bmp, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bmp, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bmp, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bmp;
            }*/


            String url = arrayList.get(position).getServerImagePath();
            String localPath = arrayList.get(position).getLocalImagePath();
            if (imagePath!=null && !imagePath.equalsIgnoreCase("NA") && localPath.equalsIgnoreCase("") && !imagePath.equalsIgnoreCase("null")){
/*                Picasso.with(context)
                        .load(url)
                        .placeholder(R.drawable.logo)
                        .into(holder.picture);*/
                holder.picture.setImageBitmap(BitmapFactory.decodeFile(url));
            }else {
                bmp = getBitmapFromUri(Uri.parse(arrayList.get(position).getLocalImagePath()));
                holder.picture.setImageBitmap(bmp);
            }

            holder.imageLayout.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemClick(position, view);
                    row_index = position;
                    notifyDataSetChanged();
                }
            });

            if (row_index == position) {
                holder.imageLayout.setBackgroundResource(R.drawable.selected_profile_image);
            } else {
                holder.imageLayout.setBackgroundResource(R.color.white);
            }



        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
       return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{
        // each data item is just a string in this case
        RelativeLayout imageLayout;
        ImageView picture;
        public MyViewHolder(View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.picture);
            imageLayout = itemView.findViewById(R.id.linear_image);
            //itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public boolean onLongClick(View view) {
            if (listener1 != null){
                listener1.onItemLongClick(getAdapterPosition(),view);
            }
            return true;
        }
    }
    public void removeItem(int position) {
        arrayList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }
    public void setOnItemClickListner(IitemClickListener itemClickListner){
        this.listener = itemClickListner;
    }

    public void setOnItemLongClickListner(IitemLongClickListener itemLongClickListner){
        this.listener1 = itemLongClickListner;
    }

    public interface IitemClickListener{
        void onItemClick(int postion, View view);
    }

    public interface IitemLongClickListener{
        void onItemLongClick(int position,View view);
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
}
