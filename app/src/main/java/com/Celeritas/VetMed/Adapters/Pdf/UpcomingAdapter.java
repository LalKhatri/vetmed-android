package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.AppointmentModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class UpcomingAdapter extends RecyclerView.Adapter<UpcomingAdapter.MyViewHolder> {
    private List<AppointmentModelUpdated> appointmentList;
    private Context context;
    private int index;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView doctorName, date, time, location, addCalender,note;



        public MyViewHolder(View view) {
            super(view);
            doctorName = view.findViewById(R.id.dr_name_tv);
            date = view.findViewById(R.id.date_tv);
            time = view.findViewById(R.id.time_tv);
            addCalender = view.findViewById(R.id.add_calendar_tv);
            location = view.findViewById(R.id.location_tv);
            note = view.findViewById(R.id.notes);
        }
    }
    public UpcomingAdapter(List<AppointmentModelUpdated> appointmentList, Context context) {
        this.appointmentList = appointmentList;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.upcoming_events_item, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder, int position) {
        holder.date.setText(context.getResources().getString(R.string.started) +" "+ appointmentList.get(position).getAppointmentDate());
        holder.doctorName.setText(context.getResources().getString(R.string.dr_name)+" "+ appointmentList.get(position).getAppointmentDoctorName());
        holder.time.setText(context.getResources().getString(R.string.time_12h_or_24h)+" "+ appointmentList.get(position).getAppointmentTime());
        holder.addCalender.setText(context.getResources().getString(R.string.add_to_calendar)+" "+ appointmentList.get(position).getAddToCalendar());
        holder.location.setText(context.getResources().getString(R.string.location)+" "+ appointmentList.get(position).getAppointmentLocation());
        holder.note.setText(context.getResources().getString(R.string.disease_note)+" "+ appointmentList.get(position).getAppointmentNotes());

    }

    @Override
    public int getItemCount() {
        return appointmentList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}