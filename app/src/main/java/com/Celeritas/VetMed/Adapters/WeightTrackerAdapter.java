package com.Celeritas.VetMed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.pets.WeightTrackerAddActivity;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.pets.WeightTrackerModel;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.List;

import static com.Celeritas.VetMed.Utility.UtilityClass.calculateDays;
import static com.Celeritas.VetMed.Utility.UtilityClass.convertNumberIntoWords;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class WeightTrackerAdapter extends RecyclerView.Adapter<WeightTrackerAdapter.MyViewHolder> {
    private List<WeightTrackerModel> weightTrackerModelList;
    private Context context;
    private int layout;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView date,weight;
        private RelativeLayout item;



        public MyViewHolder(View view) {
            super(view);
            date = view.findViewById(R.id.date);
            weight = view.findViewById(R.id.weight);
            item = view.findViewById(R.id.view_background);
        }
    }
    public WeightTrackerAdapter(List<WeightTrackerModel> weightTrackerModelList, Context context,int layout) {
        this.weightTrackerModelList = weightTrackerModelList;
        this.context = context;
        this.layout = layout;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder,int position) {
        switch (layout){
            case R.layout.weight_tracker_item:
                holder.date.setText(context.getResources().getString(R.string.medical_date)+" "+weightTrackerModelList.get(position).getDate());
                holder.weight.setText(context.getResources().getString(R.string.weights)+" "+weightTrackerModelList.get(position).getPetWeight());
                break;
                default:
                    holder.date.setText(weightTrackerModelList.get(position).getDate());
                    holder.weight.setText( weightTrackerModelList.get(position).getPetWeight());
                    holder.item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context,WeightTrackerAddActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(Constants.PET_WEIGHT_TRACKER_OBJECT,new Gson().toJson(weightTrackerModelList.get(position)));
                            context.startActivity(intent);
                        }
                    });
                    break;
        }




    }

    @Override
    public int getItemCount() {
        return weightTrackerModelList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}