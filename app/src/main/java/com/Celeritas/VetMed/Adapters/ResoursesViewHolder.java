package com.Celeritas.VetMed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;

public class ResoursesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final ImageView img;
    TextView title;
    BaseAdapter.IitemClickListener listener;

    public ResoursesViewHolder(View itemView, BaseAdapter.IitemClickListener listener) {
        super(itemView);
        this.listener = listener;
        title = (TextView) itemView.findViewById(R.id.title);
        img = (ImageView) itemView.findViewById(R.id.img);
        itemView.setOnClickListener(this);
    }

    public void bind(final Item item) {
        img.setImageResource(item.getImage());
        title.setText(item.getTitle());
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onItemClick(getAdapterPosition(), view);
        }
    }
}
