package com.Celeritas.VetMed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.pets.JournalAddActivity;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.pets.JournalModel;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.List;

import static com.Celeritas.VetMed.Utility.UtilityClass.calculateDays;
import static com.Celeritas.VetMed.Utility.UtilityClass.convertNumberIntoWords;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class JournalAdapter extends RecyclerView.Adapter<JournalAdapter.MyViewHolder> {
    private List<JournalModel> journalModelList;
    private Context context;
    private int layout;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView date, notes, petsMode;
        private RelativeLayout item;
        private ImageView petMode;


        public MyViewHolder(View view) {
            super(view);
            date = view.findViewById(R.id.date);
            notes = view.findViewById(R.id.notes);
            item = view.findViewById(R.id.view_background);
            petMode = view.findViewById(R.id.mode);
            petsMode = view.findViewById(R.id.pet_mode_tv);
        }
    }

    public JournalAdapter(List<JournalModel> journalModelList, Context context, int layout) {
        this.journalModelList = journalModelList;
        this.context = context;
        this.layout = layout;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new MyViewHolder(itemView); //
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder, position);

    }

    private void setDataInComponents(MyViewHolder holder, int position) {
        // happy =
        // sick :-|
        // confuse :-(

        switch (layout) {
            case R.layout.journal_item:
                holder.date.setText(context.getResources().getString(R.string.medical_date)+" "+journalModelList.get(position).getDate());
                holder.notes.setText(context.getResources().getString(R.string.disease_note)+" "+journalModelList.get(position).getNotes());
                if (journalModelList.get(position).getPetMode().equalsIgnoreCase(context.getResources().getString(R.string.happy))) {
                    holder.petsMode.setText(context.getResources().getString(R.string.pets_mode)+"\t :-)");
                }
                if (journalModelList.get(position).getPetMode().equalsIgnoreCase(context.getResources().getString(R.string.sick))) {
                    holder.petsMode.setText(context.getResources().getString(R.string.pets_mode)+"\t :-|");
                }
                if (journalModelList.get(position).getPetMode().equalsIgnoreCase(context.getResources().getString(R.string.confuse))) {
                    holder.petsMode.setText(context.getResources().getString(R.string.pets_mode)+"\t :-(");
                }
                break;
            default:
                holder.date.setText(journalModelList.get(position).getDate());
                holder.notes.setText(journalModelList.get(position).getNotes());
                switch (journalModelList.get(position).getPetMode()) {
                    case "Happy":
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            holder.petMode.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_happy_active, context.getApplicationContext().getTheme()));

                        } else {
                            holder.petMode.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_happy_active));
                        }
                        break;
                    case "Sick":
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            holder.petMode.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_sick_active, context.getApplicationContext().getTheme()));

                        } else {
                            holder.petMode.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_sick_active));
                        }
                        break;
                    case "Confuse":
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            holder.petMode.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_confuse_active, context.getApplicationContext().getTheme()));

                        } else {
                            holder.petMode.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_confuse_active));
                        }
                        break;
                }
                holder.item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, JournalAddActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.PET_JOURNAL_OBJECT, new Gson().toJson(journalModelList.get(position)));
                        context.startActivity(intent);
                    }
                });
                break;
        }


    }

    @Override
    public int getItemCount() {
        return journalModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}