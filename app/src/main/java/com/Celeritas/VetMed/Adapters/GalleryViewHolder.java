package com.Celeritas.VetMed.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;
import com.squareup.picasso.Picasso;

import java.io.FileDescriptor;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class GalleryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    ImageView picture;
    BaseAdapter.IitemClickListener listener;
    Context mContext;
    LinearLayout imageLayout;
    private int row_index = -1;
    private static int mPosition;
    private static SparseBooleanArray sSelectedItems = new SparseBooleanArray();
    public static final int MULTI_SELECTION = 2;
    public static final int SINGLE_SELECTION = 1;

    public GalleryViewHolder(View itemView, BaseAdapter.IitemClickListener listener) {
        super(itemView);
        mContext = itemView.getContext();
        itemView.setOnClickListener(this);
        this.listener = listener;
        picture = itemView.findViewById(R.id.picture);
        imageLayout = itemView.findViewById(R.id.linear_image);
    }

    public void bind(Item item) {
        try {
            String url = item.getTitle();
            if (url.contains("https://")){
                Picasso.with(mContext)
                        .load(url)
                        .placeholder(R.drawable.logo)
                        .into(picture);
            }else {
                Bitmap bmp = getBitmapFromUri(Uri.parse(item.getDescription()));
                picture.setImageBitmap(bmp);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        /*if (sSelectedItems.get(getAdapterPosition(), false)) {
            sSelectedItems.delete(getAdapterPosition());
            view.setSelected(false);
        } else {
            for (int i = 0;i<sSelectedItems.size();i++){
                sSelectedItems.delete(i);
                view.setSelected(false);
            }
            sSelectedItems.put(getAdapterPosition(), true);
            view.setSelected(true);
        }*/

        if (listener != null){
            listener.onItemClick(getAdapterPosition(),view);
            /*row_index = getAdapterPosition();

            if(row_index==getAdapterPosition()){
                    imageLayout.setBackgroundResource(R.drawable.selected_profile_image);

                }
                else
                {
                    imageLayout.setBackgroundResource(R.color.white);
                }*/
            }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                mContext.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
}
