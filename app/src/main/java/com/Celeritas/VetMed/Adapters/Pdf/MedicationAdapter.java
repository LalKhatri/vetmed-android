package com.Celeritas.VetMed.Adapters.Pdf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Models.pets.MedicationModelUpdated;
import com.Celeritas.VetMed.R;

import java.util.List;

/**
 * Created by Kanaya Lal on 6/6/2018.
 */
public final class MedicationAdapter extends RecyclerView.Adapter<MedicationAdapter.MyViewHolder> {
    private List<MedicationModelUpdated> medicalHistoryList;
    private Context context;
    private int index;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView medicationName, date, dosage,route,active,howLong,howOften;



        public MyViewHolder(View view) {
            super(view);
            medicationName = view.findViewById(R.id.medication_name_tv);
            date = view.findViewById(R.id.started_tv);
            dosage = view.findViewById(R.id.medication_dosage_tv);
            howOften = view.findViewById(R.id.how_often_tv);
            howLong = view.findViewById(R.id.how_long_tv);
            active = view.findViewById(R.id.active_tv);
            route = view.findViewById(R.id.route_tv);
        }
    }
    public MedicationAdapter(List<MedicationModelUpdated> medicalHistoryList, Context context) {
        this.medicalHistoryList = medicalHistoryList;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medication_item, parent, false);
        return new MyViewHolder(itemView); //
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        setDataInComponents(holder,position);

    }

    private void setDataInComponents(MyViewHolder holder, int position) {
        holder.date.setText(context.getResources().getString(R.string.started) +" "+ medicalHistoryList.get(position).getMedicationDateStarted());
        holder.medicationName.setText(context.getResources().getString(R.string.medi_name)+" "+medicalHistoryList.get(position).getMedicationName());
        holder.dosage.setText(context.getResources().getString(R.string.dosage)+" "+medicalHistoryList.get(position).getMedicationDosage());
        holder.active.setText(context.getResources().getString(R.string.med_active)+" "+medicalHistoryList.get(position).getMedicationActive());
        holder.route.setText(context.getResources().getString(R.string.route)+" "+medicalHistoryList.get(position).getMedicationRoute());
        holder.howLong.setText(context.getResources().getString(R.string.how_long)+" "+medicalHistoryList.get(position).getMedicationForHowLong());
        holder.howOften.setText(context.getResources().getString(R.string.medi_how_often)+" "+medicalHistoryList.get(position).getMedicationFrequency());
    }

    @Override
    public int getItemCount() {
        return medicalHistoryList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}