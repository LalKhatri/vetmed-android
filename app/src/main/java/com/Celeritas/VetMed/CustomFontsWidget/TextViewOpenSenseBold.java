package com.Celeritas.VetMed.CustomFontsWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by anas on 10/6/2017.
 */

public class TextViewOpenSenseBold extends TextView {
    public TextViewOpenSenseBold(Context context) {
        super(context);
        init();
    }

    public TextViewOpenSenseBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewOpenSenseBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "opensansbold.ttf");
            setTypeface(tf);
        }
    }
}
