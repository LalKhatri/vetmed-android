package com.Celeritas.VetMed.Interface;

/**
 * Created by anas on 10/9/2017.
 */

public interface DrawableClickListener {
    public static enum DrawablePosition { TOP, BOTTOM, LEFT, RIGHT };
    public void onClick(DrawablePosition target);
}
