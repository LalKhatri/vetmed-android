package com.Celeritas.VetMed.Models;

public class GalleryModel {
    private String serverImagePath;
    private String localImagePath;

    public GalleryModel(){

    }

    public GalleryModel(String serverImagePath, String localImagePath) {
        this.serverImagePath = serverImagePath;
        this.localImagePath = localImagePath;
    }

    public String getServerImagePath() {
        return serverImagePath;
    }

    public void setServerImagePath(String serverImagePath) {
        this.serverImagePath = serverImagePath;
    }

    public String getLocalImagePath() {
        return localImagePath;
    }

    public void setLocalImagePath(String localImagePath) {
        this.localImagePath = localImagePath;
    }
}
