package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VaccineModel implements Serializable{
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("PetID")
    @Expose
    private Integer petID;
    @SerializedName("VaccineType")
    @Expose
    private String vaccineType;
    @SerializedName("VaccineName")
    @Expose
    private String vaccineName;
    @SerializedName("VaccineDateGiven")
    @Expose
    private String vaccineDateGiven;
    @SerializedName("VaccineSideEffects")
    @Expose
    private String vaccineSideEffects;
    @SerializedName("VaccineNotes")
    @Expose
    private String vaccineNotes;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getPetID() {
        return petID;
    }

    public void setPetID(Integer petID) {
        this.petID = petID;
    }

    public String getVaccineType() {
        return vaccineType;
    }

    public void setVaccineType(String vaccineType) {
        this.vaccineType = vaccineType;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getVaccineDateGiven() {
        return vaccineDateGiven;
    }

    public void setVaccineDateGiven(String vaccineDateGiven) {
        this.vaccineDateGiven = vaccineDateGiven;
    }

    public String getVaccineSideEffects() {
        return vaccineSideEffects;
    }

    public void setVaccineSideEffects(String vaccineSideEffects) {
        this.vaccineSideEffects = vaccineSideEffects;
    }

    public String getVaccineNotes() {
        return vaccineNotes;
    }

    public void setVaccineNotes(String vaccineNotes) {
        this.vaccineNotes = vaccineNotes;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
