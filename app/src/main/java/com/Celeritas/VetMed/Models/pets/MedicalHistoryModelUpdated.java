package com.Celeritas.VetMed.Models.pets;

import com.Celeritas.VetMed.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class MedicalHistoryModelUpdated extends SugarRecord implements Comparator<MedicalHistoryModelUpdated> {
    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("DiseaseName")
    @Expose
    private String diseaseName;
    @SerializedName("DiseaseDate")
    @Expose
    private String diseaseDate;
    @SerializedName("DiseaseNotes")
    @Expose
    private String diseaseNotes;
    @SerializedName("AddedDateTime")
    @Expose
    private String addedDateTime;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public MedicalHistoryModelUpdated() {
    }

    public MedicalHistoryModelUpdated(String pet_id, String diseaseName, String diseaseDate, String diseaseNotes, String addedDateTime, String updateDateTime) {
        this.pet_id = pet_id;
        this.diseaseName = diseaseName;
        this.diseaseDate = diseaseDate;
        this.diseaseNotes = diseaseNotes;
        this.addedDateTime = addedDateTime;
        this.updateDateTime = updateDateTime;
    }

    public String getID() {
        return pet_id;
    }

    public void setID(String iD) {
        this.pet_id = iD;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getDiseaseDate() {
        return diseaseDate;
    }

    public void setDiseaseDate(String diseaseDate) {
        this.diseaseDate = diseaseDate;
    }

    public String getDiseaseNotes() {
        return diseaseNotes;
    }

    public void setDiseaseNotes(String diseaseNotes) {
        this.diseaseNotes = diseaseNotes;
    }

    public String getAddedDateTime() {
        return addedDateTime;
    }

    public void setAddedDateTime(String addedDateTime) {
        this.addedDateTime = addedDateTime;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    /*    @Override sort by newest to oldest
        public int compare(MedicalHistoryModelUpdated date1, MedicalHistoryModelUpdated date2) {
            DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            Date dateOne = null;
            Date dateTwo = null;
            try {
                dateOne = format.parse(date1.getDiseaseDate());
                dateTwo = format.parse(date2.getDiseaseDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return dateTwo.compareTo(dateOne);
        }*/
    @Override //sort by oldest to newest.
    public int compare(MedicalHistoryModelUpdated date1, MedicalHistoryModelUpdated date2) {
        DateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
        Date dateOne = null;
        Date dateTwo = null;
        int result = 0;
        try {
            dateOne = format.parse(date1.getDiseaseDate());
            dateTwo = format.parse(date2.getDiseaseDate());
            if (dateOne.before(dateTwo)) {
                result = -1;
            } else if (dateOne.after(dateTwo)) {
                result = 1;
            } else {
                result = 0;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }
}
