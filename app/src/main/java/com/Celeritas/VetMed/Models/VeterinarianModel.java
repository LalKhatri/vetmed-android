package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VeterinarianModel implements Serializable {
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("PetID")
    @Expose
    private Integer petID;
    @SerializedName("VIHospital")
    @Expose
    private String vIHospital;
    @SerializedName("VIDoctorName")
    @Expose
    private String vIDoctorName;
    @SerializedName("VIAddress")
    @Expose
    private String vIAddress;
    @SerializedName("VIEmail")
    @Expose
    private String vIEmail;
    @SerializedName("VIPhoneNumber")
    @Expose
    private String vIPhoneNumber;
    @SerializedName("VIWebsite")
    @Expose
    private String vIWebsite;
    @SerializedName("VINotes")
    @Expose
    private String vINotes;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getPetID() {
        return petID;
    }

    public void setPetID(Integer petID) {
        this.petID = petID;
    }

    public String getVIHospital() {
        return vIHospital;
    }

    public void setVIHospital(String vIHospital) {
        this.vIHospital = vIHospital;
    }

    public String getVIDoctorName() {
        return vIDoctorName;
    }

    public void setVIDoctorName(String vIDoctorName) {
        this.vIDoctorName = vIDoctorName;
    }

    public String getVIAddress() {
        return vIAddress;
    }

    public void setVIAddress(String vIAddress) {
        this.vIAddress = vIAddress;
    }

    public String getVIEmail() {
        return vIEmail;
    }

    public void setVIEmail(String vIEmail) {
        this.vIEmail = vIEmail;
    }

    public String getVIPhoneNumber() {
        return vIPhoneNumber;
    }

    public void setVIPhoneNumber(String vIPhoneNumber) {
        this.vIPhoneNumber = vIPhoneNumber;
    }

    public String getVIWebsite() {
        return vIWebsite;
    }

    public void setVIWebsite(String vIWebsite) {
        this.vIWebsite = vIWebsite;
    }

    public String getVINotes() {
        return vINotes;
    }

    public void setVINotes(String vINotes) {
        this.vINotes = vINotes;
    }
}
