package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class PetModelUpdated extends SugarRecord {


    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("PetName")
    @Expose
    private String petName;

    @SerializedName("PetSex")
    @Expose
    private String petSex;
    @SerializedName("PetBreed")
    @Expose
    private String petBreed;
    @SerializedName("PetDOB")
    @Expose
    private String petDOB;
    @SerializedName("PetWeight")
    @Expose
    private String petWeight;
    @SerializedName("PetWeightUnit")
    @Expose
    private String petWeightUnit;
    @SerializedName("AddedDateTime")
    @Expose
    private String addedDateTime;
    @SerializedName("ProfileImage")
    @Expose
    private String profileImage;
    @SerializedName("PetImages")
    @Expose
    private String petImages;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    @SerializedName("petType")
    @Expose
    private String petType;
    public PetModelUpdated() {
    }

    public PetModelUpdated(String userId,String petName, String petSex, String petBreed, String petDOB, String petWeight, String petWeightUnit,String profileImage,String petImages
            ,String addedDateTime,String updateDateTime,String petType)
    {

        this.userId = userId;
        this.petName = petName;
        this.petSex = petSex;
        this.petBreed = petBreed;
        this.petDOB = petDOB;
        this.petWeight = petWeight;
        this.petWeightUnit = petWeightUnit;
        this.addedDateTime = addedDateTime;
        this.profileImage = profileImage;
        this.petImages = petImages;
        this.updateDateTime = updateDateTime;
        this.petType = petType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPetSex() {
        return petSex;
    }

    public void setPetSex(String petSex) {
        this.petSex = petSex;
    }

    public String getPetBreed() {
        return petBreed;
    }

    public void setPetBreed(String petBreed) {
        this.petBreed = petBreed;
    }

    public String getPetDOB() {
        return petDOB;
    }

    public void setPetDOB(String petDOB) {
        this.petDOB = petDOB;
    }

    public String getPetWeight() {
        return petWeight;
    }

    public void setPetWeight(String petWeight) {
        this.petWeight = petWeight;
    }

    public String getPetWeightUnit() {
        return petWeightUnit;
    }

    public void setPetWeightUnit(String petWeightUnit) {
        this.petWeightUnit = petWeightUnit;
    }

    public String getAddedDateTime() {
        return addedDateTime;
    }

    public void setAddedDateTime(String addedDateTime) {
        this.addedDateTime = addedDateTime;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getPetImages() {
        return petImages;
    }

    public void setPetImages(String petImages) {
        this.petImages = petImages;
    }
}
