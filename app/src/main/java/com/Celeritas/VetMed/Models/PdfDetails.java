package com.Celeritas.VetMed.Models;

/**
 * Created by Kanaya Lal on 10/25/2018.
 */

public class PdfDetails {
    private String name;
    private boolean isSelected;

    public
    PdfDetails(String name, boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
