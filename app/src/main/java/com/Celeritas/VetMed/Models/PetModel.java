package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PetModel implements Serializable{
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("PetName")
    @Expose
    private String petName;
    @SerializedName("PetSex")
    @Expose
    private String petSex;
    @SerializedName("PetBreed")
    @Expose
    private String petBreed;
    @SerializedName("PetDOB")
    @Expose
    private String petDOB;
    @SerializedName("PetWeight")
    @Expose
    private Integer petWeight;
    @SerializedName("PetWeightUnit")
    @Expose
    private String petWeightUnit;
    @SerializedName("AddedDateTime")
    @Expose
    private String addedDateTime;
    @SerializedName("ProfileImage")
    @Expose
    private String profileImage;
    @SerializedName("PetImages")
    @Expose
    private String petImages;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPetSex() {
        return petSex;
    }

    public void setPetSex(String petSex) {
        this.petSex = petSex;
    }

    public String getPetBreed() {
        return petBreed;
    }

    public void setPetBreed(String petBreed) {
        this.petBreed = petBreed;
    }

    public String getPetDOB() {
        return petDOB;
    }

    public void setPetDOB(String petDOB) {
        this.petDOB = petDOB;
    }

    public Integer getPetWeight() {
        return petWeight;
    }

    public void setPetWeight(Integer petWeight) {
        this.petWeight = petWeight;
    }

    public String getPetWeightUnit() {
        return petWeightUnit;
    }

    public void setPetWeightUnit(String petWeightUnit) {
        this.petWeightUnit = petWeightUnit;
    }

    public String getAddedDateTime() {
        return addedDateTime;
    }

    public void setAddedDateTime(String addedDateTime) {
        this.addedDateTime = addedDateTime;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getPetImages() {
        return petImages;
    }

    public void setPetImages(String petImages) {
        this.petImages = petImages;
    }
}
