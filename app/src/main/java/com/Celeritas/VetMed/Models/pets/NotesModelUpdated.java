package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class NotesModelUpdated extends SugarRecord{

    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("NoteTitle")
    @Expose
    private String noteTitle;
    @SerializedName("NoteText")
    @Expose
    private String noteText;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public NotesModelUpdated() {
    }

    public NotesModelUpdated(String pet_id, String noteTitle, String noteText, String updateDateTime) {
        this.pet_id = pet_id;
        this.noteTitle = noteTitle;
        this.noteText = noteText;
        this.updateDateTime = updateDateTime;
    }

    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
