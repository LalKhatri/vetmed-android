package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UseFullLinks {
    @SerializedName("LinkID")
    @Expose
    private Integer linkID;
    @SerializedName("LinkType")
    @Expose
    private String linkType;
    @SerializedName("LinkTitle")
    @Expose
    private String linkTitle;
    @SerializedName("LinkURL")
    @Expose
    private String linkURL;
    @SerializedName("UpdateDateTime")
    @Expose
    private Integer updateDateTime;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Action")
    @Expose
    private String action;

    public Integer getLinkID() {
        return linkID;
    }

    public void setLinkID(Integer linkID) {
        this.linkID = linkID;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getLinkTitle() {
        return linkTitle;
    }

    public void setLinkTitle(String linkTitle) {
        this.linkTitle = linkTitle;
    }

    public String getLinkURL() {
        return linkURL;
    }

    public void setLinkURL(String linkURL) {
        this.linkURL = linkURL;
    }

    public Integer getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Integer updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
