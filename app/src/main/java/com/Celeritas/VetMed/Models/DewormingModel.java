package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DewormingModel implements Serializable {
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("PetID")
    @Expose
    private Integer petID;
    @SerializedName("DFTProductName")
    @Expose
    private String dFTProductName;
    @SerializedName("DFTDateStarted")
    @Expose
    private String dFTDateStarted;
    @SerializedName("DFTEndDate")
    @Expose
    private String dFTEndDate;
    @SerializedName("DFTHowOften")
    @Expose
    private String dFTHowOften;
    @SerializedName("DFTActive")
    @Expose
    private String dFTActive;
    @SerializedName("DFTNotes")
    @Expose
    private String dFTNotes;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getPetID() {
        return petID;
    }

    public void setPetID(Integer petID) {
        this.petID = petID;
    }

    public String getDFTProductName() {
        return dFTProductName;
    }

    public void setDFTProductName(String dFTProductName) {
        this.dFTProductName = dFTProductName;
    }

    public String getDFTDateStarted() {
        return dFTDateStarted;
    }

    public void setDFTDateStarted(String dFTDateStarted) {
        this.dFTDateStarted = dFTDateStarted;
    }

    public String getDFTEndDate() {
        return dFTEndDate;
    }

    public void setDFTEndDate(String dFTEndDate) {
        this.dFTEndDate = dFTEndDate;
    }

    public String getDFTHowOften() {
        return dFTHowOften;
    }

    public void setDFTHowOften(String dFTHowOften) {
        this.dFTHowOften = dFTHowOften;
    }

    public String getDFTActive() {
        return dFTActive;
    }

    public void setDFTActive(String dFTActive) {
        this.dFTActive = dFTActive;
    }

    public String getDFTNotes() {
        return dFTNotes;
    }

    public void setDFTNotes(String dFTNotes) {
        this.dFTNotes = dFTNotes;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
