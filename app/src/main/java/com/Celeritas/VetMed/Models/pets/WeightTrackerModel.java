package com.Celeritas.VetMed.Models.pets;

import com.Celeritas.VetMed.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class WeightTrackerModel extends SugarRecord implements Comparator<WeightTrackerModel> {
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;
    @SerializedName("PetWeight")
    @Expose
    private String petWeight;
    @SerializedName("Date")
    @Expose
    private String date;

    public WeightTrackerModel() {
    }

    public String getPetWeight() {
        return petWeight;
    }

    public WeightTrackerModel(String petID,String updateDateTime, String petWeight, String date) {
        this.pet_id = petID;
        this.updateDateTime = updateDateTime;
        this.petWeight = petWeight;
        this.date = date;
    }

    public void setPetWeight(String petWeight) {
        this.petWeight = petWeight;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
    @Override //sort by newest to oldest
    public int compare(WeightTrackerModel date1, WeightTrackerModel date2) {
        DateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
        Date dateOne = null;
        Date dateTwo = null;
        try {
            dateOne = format.parse(date1.getDate());
            dateTwo = format.parse(date2.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTwo.compareTo(dateOne);
    }
}
