package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiagnosticTestModel implements Serializable{
    @SerializedName("DiagnosticTestID")
    @Expose
    private Integer diagnosticTestID;
    @SerializedName("DiagnosticTestName")
    @Expose
    private String diagnosticTestName;
    @SerializedName("DiagnosticTestImage")
    @Expose
    private String diagnosticTestImage;
    @SerializedName("DiagnosticTestDescription")
    @Expose
    private String diagnosticTestDescription;
    @SerializedName("UpdateDateTime")
    @Expose
    private Integer updateDateTime;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Action")
    @Expose
    private String action;

    public Integer getDiagnosticTestID() {
        return diagnosticTestID;
    }

    public void setDiagnosticTestID(Integer diagnosticTestID) {
        this.diagnosticTestID = diagnosticTestID;
    }

    public String getDiagnosticTestName() {
        return diagnosticTestName;
    }

    public void setDiagnosticTestName(String diagnosticTestName) {
        this.diagnosticTestName = diagnosticTestName;
    }

    public String getDiagnosticTestImage() {
        return diagnosticTestImage;
    }

    public void setDiagnosticTestImage(String diagnosticTestImage) {
        this.diagnosticTestImage = diagnosticTestImage;
    }

    public String getDiagnosticTestDescription() {
        return diagnosticTestDescription;
    }

    public void setDiagnosticTestDescription(String diagnosticTestDescription) {
        this.diagnosticTestDescription = diagnosticTestDescription;
    }

    public Integer getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Integer updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
