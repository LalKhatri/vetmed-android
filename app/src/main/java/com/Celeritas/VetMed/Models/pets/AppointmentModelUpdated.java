package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class AppointmentModelUpdated extends SugarRecord{

    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("AppointmentDate")
    @Expose
    private String appointmentDate;
    @SerializedName("AppointmentTime")
    @Expose
    private String appointmentTime;
    @SerializedName("AppointmentLocation")
    @Expose
    private String appointmentLocation;
    @SerializedName("AppointmentDoctorName")
    @Expose
    private String appointmentDoctorName;
    @SerializedName("AppointmentNotes")
    @Expose
    private String appointmentNotes;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;
    @SerializedName("addToCalendar")
    @Expose
    private String addToCalendar;

    public AppointmentModelUpdated() {
    }

    public AppointmentModelUpdated(String pet_id, String appointmentDate, String appointmentTime, String appointmentLocation, String appointmentDoctorName, String appointmentNotes, String updateDateTime,String addToCalendar) {
        this.pet_id = pet_id;
        this.appointmentDate = appointmentDate;
        this.appointmentTime = appointmentTime;
        this.appointmentLocation = appointmentLocation;
        this.appointmentDoctorName = appointmentDoctorName;
        this.appointmentNotes = appointmentNotes;
        this.updateDateTime = updateDateTime;
        this.addToCalendar = addToCalendar;
    }
    public String getAddToCalendar() {
        return addToCalendar;
    }

    public void setAddToCalendar(String addToCalendar) {
        this.addToCalendar = addToCalendar;
    }

    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getAppointmentLocation() {
        return appointmentLocation;
    }

    public void setAppointmentLocation(String appointmentLocation) {
        this.appointmentLocation = appointmentLocation;
    }

    public String getAppointmentDoctorName() {
        return appointmentDoctorName;
    }

    public void setAppointmentDoctorName(String appointmentDoctorName) {
        this.appointmentDoctorName = appointmentDoctorName;
    }

    public String getAppointmentNotes() {
        return appointmentNotes;
    }

    public void setAppointmentNotes(String appointmentNotes) {
        this.appointmentNotes = appointmentNotes;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
