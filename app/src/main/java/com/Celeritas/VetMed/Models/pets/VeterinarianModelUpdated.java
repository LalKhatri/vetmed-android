package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class VeterinarianModelUpdated extends SugarRecord {
    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("VIHospital")
    @Expose
    private String vIHospital;
    @SerializedName("VIDoctorName")
    @Expose
    private String vIDoctorName;
    @SerializedName("VIAddress")
    @Expose
    private String vIAddress;
    @SerializedName("VIEmail")
    @Expose
    private String vIEmail;
    @SerializedName("VIPhoneNumber")
    @Expose
    private String vIPhoneNumber;
    @SerializedName("VIWebsite")
    @Expose
    private String vIWebsite;
    @SerializedName("VINotes")
    @Expose
    private String vINotes;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;



    public VeterinarianModelUpdated() {

    }

    public VeterinarianModelUpdated(String pet_id, String vIHospital, String vIDoctorName, String vIAddress, String vIEmail, String vIPhoneNumber, String vIWebsite, String vINotes, String updateDateTime) {
        this.pet_id = pet_id;
        this.vIHospital = vIHospital;
        this.vIDoctorName = vIDoctorName;
        this.vIAddress = vIAddress;
        this.vIEmail = vIEmail;
        this.vIPhoneNumber = vIPhoneNumber;
        this.vIWebsite = vIWebsite;
        this.vINotes = vINotes;
        this.updateDateTime = updateDateTime;
    }
    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getVIHospital() {
        return vIHospital;
    }

    public void setVIHospital(String vIHospital) {
        this.vIHospital = vIHospital;
    }

    public String getVIDoctorName() {
        return vIDoctorName;
    }

    public void setVIDoctorName(String vIDoctorName) {
        this.vIDoctorName = vIDoctorName;
    }

    public String getVIAddress() {
        return vIAddress;
    }

    public void setVIAddress(String vIAddress) {
        this.vIAddress = vIAddress;
    }

    public String getVIEmail() {
        return vIEmail;
    }

    public void setVIEmail(String vIEmail) {
        this.vIEmail = vIEmail;
    }

    public String getVIPhoneNumber() {
        return vIPhoneNumber;
    }

    public void setVIPhoneNumber(String vIPhoneNumber) {
        this.vIPhoneNumber = vIPhoneNumber;
    }

    public String getVIWebsite() {
        return vIWebsite;
    }

    public void setVIWebsite(String vIWebsite) {
        this.vIWebsite = vIWebsite;
    }

    public String getVINotes() {
        return vINotes;
    }

    public void setVINotes(String vINotes) {
        this.vINotes = vINotes;
    }
}
