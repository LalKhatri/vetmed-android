package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ClinicalSigns implements Serializable{

    @SerializedName("ClinicalSignID")
    @Expose
    private Integer clinicalSignID;
    @SerializedName("ClinicalSignName")
    @Expose
    private String clinicalSignName;
    @SerializedName("ClinicalSignImage")
    @Expose
    private String clinicalSignImage;
    @SerializedName("ClinicalSignDescription")
    @Expose
    private String clinicalSignDescription;
    @SerializedName("UpdateDateTime")
    @Expose
    private Integer updateDateTime;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Action")
    @Expose
    private String action;

    public Integer getClinicalSignID() {
        return clinicalSignID;
    }

    public void setClinicalSignID(Integer clinicalSignID) {
        this.clinicalSignID = clinicalSignID;
    }

    public String getClinicalSignName() {
        return clinicalSignName;
    }

    public void setClinicalSignName(String clinicalSignName) {
        this.clinicalSignName = clinicalSignName;
    }

    public String getClinicalSignImage() {
        return clinicalSignImage;
    }

    public void setClinicalSignImage(String clinicalSignImage) {
        this.clinicalSignImage = clinicalSignImage;
    }

    public String getClinicalSignDescription() {
        return clinicalSignDescription;
    }

    public void setClinicalSignDescription(String clinicalSignDescription) {
        this.clinicalSignDescription = clinicalSignDescription;
    }

    public Integer getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Integer updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
