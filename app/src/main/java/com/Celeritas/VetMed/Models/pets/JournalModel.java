package com.Celeritas.VetMed.Models.pets;

import com.Celeritas.VetMed.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class JournalModel extends SugarRecord implements Comparator<JournalModel> {
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;
    @SerializedName("PetMode")
    @Expose
    private String petMode;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("Notes")
    @Expose
    private String notes;



    public JournalModel() {
    }

    public String getPetMode() {
        return petMode;
    }

    public JournalModel(String petID, String updateDateTime, String petMode, String date, String notes) {
        this.pet_id = petID;
        this.updateDateTime = updateDateTime;
        this.petMode = petMode;
        this.date = date;
        this.notes = notes;
    }
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    public void setPetMode(String petMode) {
        this.petMode = petMode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
           @Override //sort by newest to oldest
        public int compare(JournalModel date1, JournalModel date2) {
               DateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
            Date dateOne = null;
            Date dateTwo = null;
            try {
                dateOne = format.parse(date1.getDate());
                dateTwo = format.parse(date2.getDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return dateTwo.compareTo(dateOne);
        }
}
