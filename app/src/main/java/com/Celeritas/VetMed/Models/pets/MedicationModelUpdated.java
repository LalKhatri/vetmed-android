package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class MedicationModelUpdated extends SugarRecord{
    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("MedicationName")
    @Expose
    private String medicationName;
    @SerializedName("MedicationDosage")
    @Expose
    private String medicationDosage;
    @SerializedName("MedicationFrequency")
    @Expose
    private String medicationFrequency;
    @SerializedName("MedicationRoute")
    @Expose
    private String medicationRoute;
    @SerializedName("MedicationForHowLong")
    @Expose
    private String medicationForHowLong;
    @SerializedName("MedicationDateStarted")
    @Expose
    private String medicationDateStarted;
    @SerializedName("MedicationActive")
    @Expose
    private String medicationActive;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public MedicationModelUpdated(String pet_id, String medicationName, String medicationDosage, String medicationFrequency, String medicationRoute, String medicationForHowLong, String medicationDateStarted, String medicationActive, String updateDateTime) {
        this.pet_id = pet_id;
        this.medicationName = medicationName;
        this.medicationDosage = medicationDosage;
        this.medicationFrequency = medicationFrequency;
        this.medicationRoute = medicationRoute;
        this.medicationForHowLong = medicationForHowLong;
        this.medicationDateStarted = medicationDateStarted;
        this.medicationActive = medicationActive;
        this.updateDateTime = updateDateTime;
    }

    public MedicationModelUpdated() {

    }


    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getMedicationDosage() {
        return medicationDosage;
    }

    public void setMedicationDosage(String medicationDosage) {
        this.medicationDosage = medicationDosage;
    }

    public String getMedicationFrequency() {
        return medicationFrequency;
    }

    public void setMedicationFrequency(String medicationFrequency) {
        this.medicationFrequency = medicationFrequency;
    }

    public String getMedicationRoute() {
        return medicationRoute;
    }

    public void setMedicationRoute(String medicationRoute) {
        this.medicationRoute = medicationRoute;
    }

    public String getMedicationForHowLong() {
        return medicationForHowLong;
    }

    public void setMedicationForHowLong(String medicationForHowLong) {
        this.medicationForHowLong = medicationForHowLong;
    }

    public String getMedicationDateStarted() {
        return medicationDateStarted;
    }

    public void setMedicationDateStarted(String medicationDateStarted) {
        this.medicationDateStarted = medicationDateStarted;
    }

    public String getMedicationActive() {
        return medicationActive;
    }

    public void setMedicationActive(String medicationActive) {
        this.medicationActive = medicationActive;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
