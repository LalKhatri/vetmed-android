package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class DietModelUpdated extends SugarRecord{

    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("DietName")
    @Expose
    private String dietName;
    @SerializedName("DietType")
    @Expose
    private String dietType;
    @SerializedName("DietHowMuch")
    @Expose
    private String dietHowMuch;
    @SerializedName("DietHowOften")
    @Expose
    private String dietHowOften;
    @SerializedName("DietDateStarted")
    @Expose
    private String dietDateStarted;
    @SerializedName("DietEndDate")
    @Expose
    private String dietEndDate;
    @SerializedName("DietActive")
    @Expose
    private String dietActive;
    @SerializedName("DietNotes")
    @Expose
    private String dietNotes;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;
    public DietModelUpdated(String pet_id, String dietName, String dietType, String dietHowMuch, String dietHowOften,
                            String dietDateStarted, String dietEndDate, String dietActive, String dietNotes, String updateDateTime) {
        this.pet_id = pet_id;
        this.dietName = dietName;
        this.dietType = dietType;
        this.dietHowMuch = dietHowMuch;
        this.dietHowOften = dietHowOften;
        this.dietDateStarted = dietDateStarted;
        this.dietEndDate = dietEndDate;
        this.dietActive = dietActive;
        this.dietNotes = dietNotes;
        this.updateDateTime = updateDateTime;
    }

    public DietModelUpdated() {

    }
    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getDietName() {
        return dietName;
    }

    public void setDietName(String dietName) {
        this.dietName = dietName;
    }

    public String getDietType() {
        return dietType;
    }

    public void setDietType(String dietType) {
        this.dietType = dietType;
    }

    public String getDietHowMuch() {
        return dietHowMuch;
    }

    public void setDietHowMuch(String dietHowMuch) {
        this.dietHowMuch = dietHowMuch;
    }

    public String getDietHowOften() {
        return dietHowOften;
    }

    public void setDietHowOften(String dietHowOften) {
        this.dietHowOften = dietHowOften;
    }

    public String getDietDateStarted() {
        return dietDateStarted;
    }

    public void setDietDateStarted(String dietDateStarted) {
        this.dietDateStarted = dietDateStarted;
    }

    public String getDietEndDate() {
        return dietEndDate;
    }

    public void setDietEndDate(String dietEndDate) {
        this.dietEndDate = dietEndDate;
    }

    public String getDietActive() {
        return dietActive;
    }

    public void setDietActive(String dietActive) {
        this.dietActive = dietActive;
    }

    public String getDietNotes() {
        return dietNotes;
    }

    public void setDietNotes(String dietNotes) {
        this.dietNotes = dietNotes;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
