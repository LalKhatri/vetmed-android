package com.Celeritas.VetMed.Models.pets;

import com.Celeritas.VetMed.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class Documents extends SugarRecord /*implements Comparator<Documents>*/ {

    private String pet_id;
    private String docType;
    private String date;
    private String docName;
    private String docPath;



    public Documents() {
    }

    public Documents(String pet_id, String docType, String date, String docName,String docPath) {
        this.pet_id = pet_id;
        this.docType = docType;
        this.date = date;
        this.docName = docName;
        this.docPath = docPath;
    }

    public String getDocType() {

        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }
    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }
/*    @Override //sort by newest to oldest
    public int compare(Documents date1, Documents date2) {
        DateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
        Date dateOne = null;
        Date dateTwo = null;
        try {
            dateOne = format.parse(date1.getDate());
            dateTwo = format.parse(date2.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTwo.compareTo(dateOne);
    }*/
}
