package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiseaseModel implements Serializable{
  @SerializedName("DiseaseID")
  @Expose
  private Integer diseaseID;
  @SerializedName("DiseaseCategory")
  @Expose
  private String diseaseCategory;
  @SerializedName("DiseaseName")
  @Expose
  private String diseaseName;
  @SerializedName("DiseaseImage")
  @Expose
  private String diseaseImage;
  @SerializedName("DiseaseDescription")
  @Expose
  private String diseaseDescription;
  @SerializedName("DiseaseDiagnosis")
  @Expose
  private String diseaseDiagnosis;
  @SerializedName("DiseaseTreatment")
  @Expose
  private String diseaseTreatment;
  @SerializedName("DiseaseReference")
  @Expose
  private String diseaseReference;
  @SerializedName("UpdateDateTime")
  @Expose
  private Integer updateDateTime;
  @SerializedName("Action")
  @Expose
  private String action;
  @SerializedName("Status")
  @Expose
  private Integer status;

  public Integer getDiseaseID() {
    return diseaseID;
  }

  public void setDiseaseID(Integer diseaseID) {
    this.diseaseID = diseaseID;
  }

  public String getDiseaseCategory() {
    return diseaseCategory;
  }

  public void setDiseaseCategory(String diseaseCategory) {
    this.diseaseCategory = diseaseCategory;
  }

  public String getDiseaseName() {
    return diseaseName;
  }

  public void setDiseaseName(String diseaseName) {
    this.diseaseName = diseaseName;
  }

  public String getDiseaseImage() {
    return diseaseImage;
  }

  public void setDiseaseImage(String diseaseImage) {
    this.diseaseImage = diseaseImage;
  }

  public String getDiseaseDescription() {
    return diseaseDescription;
  }

  public void setDiseaseDescription(String diseaseDescription) {
    this.diseaseDescription = diseaseDescription;
  }

  public String getDiseaseDiagnosis() {
    return diseaseDiagnosis;
  }

  public void setDiseaseDiagnosis(String diseaseDiagnosis) {
    this.diseaseDiagnosis = diseaseDiagnosis;
  }

  public String getDiseaseTreatment() {
    return diseaseTreatment;
  }

  public void setDiseaseTreatment(String diseaseTreatment) {
    this.diseaseTreatment = diseaseTreatment;
  }

  public String getDiseaseReference() {
    return diseaseReference;
  }

  public void setDiseaseReference(String diseaseReference) {
    this.diseaseReference = diseaseReference;
  }

  public Integer getUpdateDateTime() {
    return updateDateTime;
  }

  public void setUpdateDateTime(Integer updateDateTime) {
    this.updateDateTime = updateDateTime;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }
}
