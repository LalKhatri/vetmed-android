package com.Celeritas.VetMed.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Item implements Parcelable{
    private int viewType;
    private String title;
    private String description;
    private int image;

    public Item() {
    }

    public Item(int viewType, String title, String description) {
        this.viewType = viewType;
        this.title = title;
        this.description = description;
    }

    protected Item(Parcel in) {
        viewType = in.readInt();
        title = in.readString();
        description = in.readString();
        image = in.readInt();
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.viewType);
        parcel.writeString(this.description);
        parcel.writeString(this.title);
        parcel.writeInt(this.image);
    }
}
