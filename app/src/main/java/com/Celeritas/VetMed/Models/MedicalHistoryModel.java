package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MedicalHistoryModel implements Serializable{
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("DiseaseName")
    @Expose
    private String diseaseName;
    @SerializedName("DiseaseDate")
    @Expose
    private String diseaseDate;
    @SerializedName("DiseaseNotes")
    @Expose
    private String diseaseNotes;
    @SerializedName("AddedDateTime")
    @Expose
    private String addedDateTime;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getDiseaseDate() {
        return diseaseDate;
    }

    public void setDiseaseDate(String diseaseDate) {
        this.diseaseDate = diseaseDate;
    }

    public String getDiseaseNotes() {
        return diseaseNotes;
    }

    public void setDiseaseNotes(String diseaseNotes) {
        this.diseaseNotes = diseaseNotes;
    }

    public String getAddedDateTime() {
        return addedDateTime;
    }

    public void setAddedDateTime(String addedDateTime) {
        this.addedDateTime = addedDateTime;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
