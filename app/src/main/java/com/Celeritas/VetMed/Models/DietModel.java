package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DietModel implements Serializable{
  @SerializedName("ID")
  @Expose
  private Integer iD;
  @SerializedName("PetID")
  @Expose
  private Integer petID;
  @SerializedName("DietName")
  @Expose
  private String dietName;
  @SerializedName("DietType")
  @Expose
  private String dietType;
  @SerializedName("DietHowMuch")
  @Expose
  private String dietHowMuch;
  @SerializedName("DietHowOften")
  @Expose
  private String dietHowOften;
  @SerializedName("DietDateStarted")
  @Expose
  private String dietDateStarted;
  @SerializedName("DietEndDate")
  @Expose
  private String dietEndDate;
  @SerializedName("DietActive")
  @Expose
  private String dietActive;
  @SerializedName("DietNotes")
  @Expose
  private String dietNotes;
  @SerializedName("UpdateDateTime")
  @Expose
  private String updateDateTime;

  public Integer getID() {
    return iD;
  }

  public void setID(Integer iD) {
    this.iD = iD;
  }

  public Integer getPetID() {
    return petID;
  }

  public void setPetID(Integer petID) {
    this.petID = petID;
  }

  public String getDietName() {
    return dietName;
  }

  public void setDietName(String dietName) {
    this.dietName = dietName;
  }

  public String getDietType() {
    return dietType;
  }

  public void setDietType(String dietType) {
    this.dietType = dietType;
  }

  public String getDietHowMuch() {
    return dietHowMuch;
  }

  public void setDietHowMuch(String dietHowMuch) {
    this.dietHowMuch = dietHowMuch;
  }

  public String getDietHowOften() {
    return dietHowOften;
  }

  public void setDietHowOften(String dietHowOften) {
    this.dietHowOften = dietHowOften;
  }

  public String getDietDateStarted() {
    return dietDateStarted;
  }

  public void setDietDateStarted(String dietDateStarted) {
    this.dietDateStarted = dietDateStarted;
  }

  public String getDietEndDate() {
    return dietEndDate;
  }

  public void setDietEndDate(String dietEndDate) {
    this.dietEndDate = dietEndDate;
  }

  public String getDietActive() {
    return dietActive;
  }

  public void setDietActive(String dietActive) {
    this.dietActive = dietActive;
  }

  public String getDietNotes() {
    return dietNotes;
  }

  public void setDietNotes(String dietNotes) {
    this.dietNotes = dietNotes;
  }

  public String getUpdateDateTime() {
    return updateDateTime;
  }

  public void setUpdateDateTime(String updateDateTime) {
    this.updateDateTime = updateDateTime;
  }
}
