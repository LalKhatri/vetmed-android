package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class DewormingModelUpdated extends SugarRecord{
    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("DFTProductName")
    @Expose
    private String dFTProductName;
    @SerializedName("DFTDateStarted")
    @Expose
    private String dFTDateStarted;
    @SerializedName("DFTEndDate")
    @Expose
    private String dFTEndDate;
    @SerializedName("DFTHowOften")
    @Expose
    private String dFTHowOften;
    @SerializedName("DFTActive")
    @Expose
    private String dFTActive;
    @SerializedName("DFTNotes")
    @Expose
    private String dFTNotes;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public DewormingModelUpdated() {
    }

    public DewormingModelUpdated(String pet_id, String dFTProductName, String dFTDateStarted, String dFTEndDate, String dFTHowOften, String dFTActive, String dFTNotes, String updateDateTime) {
        this.pet_id = pet_id;
        this.dFTProductName = dFTProductName;
        this.dFTDateStarted = dFTDateStarted;
        this.dFTEndDate = dFTEndDate;
        this.dFTHowOften = dFTHowOften;
        this.dFTActive = dFTActive;
        this.dFTNotes = dFTNotes;
        this.updateDateTime = updateDateTime;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }
    public String getPetID() {
        return pet_id;
    }


    public String getDFTProductName() {
        return dFTProductName;
    }

    public void setDFTProductName(String dFTProductName) {
        this.dFTProductName = dFTProductName;
    }

    public String getDFTDateStarted() {
        return dFTDateStarted;
    }

    public void setDFTDateStarted(String dFTDateStarted) {
        this.dFTDateStarted = dFTDateStarted;
    }

    public String getDFTEndDate() {
        return dFTEndDate;
    }

    public void setDFTEndDate(String dFTEndDate) {
        this.dFTEndDate = dFTEndDate;
    }

    public String getDFTHowOften() {
        return dFTHowOften;
    }

    public void setDFTHowOften(String dFTHowOften) {
        this.dFTHowOften = dFTHowOften;
    }

    public String getDFTActive() {
        return dFTActive;
    }

    public void setDFTActive(String dFTActive) {
        this.dFTActive = dFTActive;
    }

    public String getDFTNotes() {
        return dFTNotes;
    }

    public void setDFTNotes(String dFTNotes) {
        this.dFTNotes = dFTNotes;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
