package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MedicationModel implements Serializable{
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("PetID")
    @Expose
    private Integer petID;
    @SerializedName("MedicationName")
    @Expose
    private String medicationName;
    @SerializedName("MedicationDosage")
    @Expose
    private String medicationDosage;
    @SerializedName("MedicationFrequency")
    @Expose
    private String medicationFrequency;
    @SerializedName("MedicationRoute")
    @Expose
    private String medicationRoute;
    @SerializedName("MedicationForHowLong")
    @Expose
    private String medicationForHowLong;
    @SerializedName("MedicationDateStarted")
    @Expose
    private String medicationDateStarted;
    @SerializedName("MedicationActive")
    @Expose
    private String medicationActive;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getPetID() {
        return petID;
    }

    public void setPetID(Integer petID) {
        this.petID = petID;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getMedicationDosage() {
        return medicationDosage;
    }

    public void setMedicationDosage(String medicationDosage) {
        this.medicationDosage = medicationDosage;
    }

    public String getMedicationFrequency() {
        return medicationFrequency;
    }

    public void setMedicationFrequency(String medicationFrequency) {
        this.medicationFrequency = medicationFrequency;
    }

    public String getMedicationRoute() {
        return medicationRoute;
    }

    public void setMedicationRoute(String medicationRoute) {
        this.medicationRoute = medicationRoute;
    }

    public String getMedicationForHowLong() {
        return medicationForHowLong;
    }

    public void setMedicationForHowLong(String medicationForHowLong) {
        this.medicationForHowLong = medicationForHowLong;
    }

    public String getMedicationDateStarted() {
        return medicationDateStarted;
    }

    public void setMedicationDateStarted(String medicationDateStarted) {
        this.medicationDateStarted = medicationDateStarted;
    }

    public String getMedicationActive() {
        return medicationActive;
    }

    public void setMedicationActive(String medicationActive) {
        this.medicationActive = medicationActive;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
