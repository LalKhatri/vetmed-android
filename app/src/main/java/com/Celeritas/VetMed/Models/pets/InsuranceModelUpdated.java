package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class InsuranceModelUpdated extends SugarRecord {
    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("InsuranceInfoName")
    @Expose
    private String insuranceInfoName;
    @SerializedName("InsuranceInfoPolicyNumber")
    @Expose
    private String insuranceInfoPolicyNumber;
    @SerializedName("InsuranceInfoEmail")
    @Expose
    private String insuranceInfoEmail;
    @SerializedName("InsuranceInfoWebsite")
    @Expose
    private String insuranceInfoWebsite;
    @SerializedName("InsuranceInfoPhoneNumber")
    @Expose
    private String insuranceInfoPhoneNumber;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public InsuranceModelUpdated() {
    }

    public InsuranceModelUpdated(String pet_id, String insuranceInfoName, String insuranceInfoPolicyNumber, String insuranceInfoEmail, String insuranceInfoWebsite, String insuranceInfoPhoneNumber, String updateDateTime) {

        this.pet_id = pet_id;
        this.insuranceInfoName = insuranceInfoName;
        this.insuranceInfoPolicyNumber = insuranceInfoPolicyNumber;
        this.insuranceInfoEmail = insuranceInfoEmail;
        this.insuranceInfoWebsite = insuranceInfoWebsite;
        this.insuranceInfoPhoneNumber = insuranceInfoPhoneNumber;
        this.updateDateTime = updateDateTime;
    }

    public String getPetID() {
        return pet_id;
    }

    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getInsuranceInfoName() {
        return insuranceInfoName;
    }

    public void setInsuranceInfoName(String insuranceInfoName) {
        this.insuranceInfoName = insuranceInfoName;
    }

    public String getInsuranceInfoPolicyNumber() {
        return insuranceInfoPolicyNumber;
    }

    public void setInsuranceInfoPolicyNumber(String insuranceInfoPolicyNumber) {
        this.insuranceInfoPolicyNumber = insuranceInfoPolicyNumber;
    }

    public String getInsuranceInfoEmail() {
        return insuranceInfoEmail;
    }

    public void setInsuranceInfoEmail(String insuranceInfoEmail) {
        this.insuranceInfoEmail = insuranceInfoEmail;
    }

    public String getInsuranceInfoWebsite() {
        return insuranceInfoWebsite;
    }

    public void setInsuranceInfoWebsite(String insuranceInfoWebsite) {
        this.insuranceInfoWebsite = insuranceInfoWebsite;
    }

    public String getInsuranceInfoPhoneNumber() {
        return insuranceInfoPhoneNumber;
    }

    public void setInsuranceInfoPhoneNumber(String insuranceInfoPhoneNumber) {
        this.insuranceInfoPhoneNumber = insuranceInfoPhoneNumber;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
