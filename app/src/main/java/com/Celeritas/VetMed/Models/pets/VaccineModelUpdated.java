package com.Celeritas.VetMed.Models.pets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class VaccineModelUpdated extends SugarRecord {//

    @SerializedName("pet_id")
    @Expose
    private String pet_id;
    @SerializedName("VaccineName")
    @Expose
    private String vaccineName;
    @SerializedName("VaccineDateGiven")
    @Expose
    private String vaccineDateGiven;
    @SerializedName("nextDueDate")
    @Expose
    private String nextDueDate;
    @SerializedName("VaccineSideEffects")
    @Expose
    private String vaccineSideEffects;
    @SerializedName("VaccineNotes")
    @Expose
    private String vaccineNotes;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;



    public VaccineModelUpdated(String pet_id, String vaccineName, String vaccineDateGiven,String nextDueDate, String vaccineSideEffects, String vaccineNotes, String updateDateTime) {
        this.pet_id = pet_id;
        this.vaccineName = vaccineName;
        this.vaccineDateGiven = vaccineDateGiven;
        this.nextDueDate = nextDueDate;
        this.vaccineSideEffects = vaccineSideEffects;
        this.vaccineNotes = vaccineNotes;
        this.updateDateTime = updateDateTime;
    }
    public String getNextDueDate() {
        return nextDueDate;
    }

    public void setNextDueDate(String nextDueDate) {
        this.nextDueDate = nextDueDate;
    }
    public void setPetID(String petID) {
        this.pet_id = petID;
    }

    public String getPetID() {
        return pet_id;
    }

    public VaccineModelUpdated() {
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getVaccineDateGiven() {
        return vaccineDateGiven;
    }

    public void setVaccineDateGiven(String vaccineDateGiven) {
        this.vaccineDateGiven = vaccineDateGiven;
    }

    public String getVaccineSideEffects() {
        return vaccineSideEffects;
    }

    public void setVaccineSideEffects(String vaccineSideEffects) {
        this.vaccineSideEffects = vaccineSideEffects;
    }

    public String getVaccineNotes() {
        return vaccineNotes;
    }

    public void setVaccineNotes(String vaccineNotes) {
        this.vaccineNotes = vaccineNotes;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
