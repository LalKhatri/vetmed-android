package com.Celeritas.VetMed.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InsuranceModel implements Serializable {
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("PetID")
    @Expose
    private Integer petID;
    @SerializedName("InsuranceInfoName")
    @Expose
    private String insuranceInfoName;
    @SerializedName("InsuranceInfoPolicyNumber")
    @Expose
    private String insuranceInfoPolicyNumber;
    @SerializedName("InsuranceInfoEmail")
    @Expose
    private String insuranceInfoEmail;
    @SerializedName("InsuranceInfoWebsite")
    @Expose
    private String insuranceInfoWebsite;
    @SerializedName("InsuranceInfoPhoneNumber")
    @Expose
    private String insuranceInfoPhoneNumber;
    @SerializedName("UpdateDateTime")
    @Expose
    private String updateDateTime;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getPetID() {
        return petID;
    }

    public void setPetID(Integer petID) {
        this.petID = petID;
    }

    public String getInsuranceInfoName() {
        return insuranceInfoName;
    }

    public void setInsuranceInfoName(String insuranceInfoName) {
        this.insuranceInfoName = insuranceInfoName;
    }

    public String getInsuranceInfoPolicyNumber() {
        return insuranceInfoPolicyNumber;
    }

    public void setInsuranceInfoPolicyNumber(String insuranceInfoPolicyNumber) {
        this.insuranceInfoPolicyNumber = insuranceInfoPolicyNumber;
    }

    public String getInsuranceInfoEmail() {
        return insuranceInfoEmail;
    }

    public void setInsuranceInfoEmail(String insuranceInfoEmail) {
        this.insuranceInfoEmail = insuranceInfoEmail;
    }

    public String getInsuranceInfoWebsite() {
        return insuranceInfoWebsite;
    }

    public void setInsuranceInfoWebsite(String insuranceInfoWebsite) {
        this.insuranceInfoWebsite = insuranceInfoWebsite;
    }

    public String getInsuranceInfoPhoneNumber() {
        return insuranceInfoPhoneNumber;
    }

    public void setInsuranceInfoPhoneNumber(String insuranceInfoPhoneNumber) {
        this.insuranceInfoPhoneNumber = insuranceInfoPhoneNumber;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
