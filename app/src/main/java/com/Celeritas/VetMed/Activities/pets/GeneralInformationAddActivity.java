package com.Celeritas.VetMed.Activities.pets;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Constants.sdf;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class GeneralInformationAddActivity extends BaseActivity implements IActionBar{

    private TextView petName;
    private Spinner sexSpinner,petType;
    private TextView breedName;
    private TextView datepicker;
    private TextView weight;
    private TextView kg;
    private TextView lbs;
    private Calendar myCalendar;
    private Calendar myCalendarCurrent;
    private TextView other;
    String petWeightUnit = "Kg";
    private PetModelUpdated model;
    private ArrayList<String> gender;
    private ArrayList<String> petTypes;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_general_information);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_general_information, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.general_information));
        initializeView();
    }

    private void initializeView() {
        petName =  findViewById(R.id.petName);
        sexSpinner = findViewById(R.id.spinner);
        petType = findViewById(R.id.pet_type_spinner);
        other = findViewById(R.id.other);
        breedName =  findViewById(R.id.breedName);

        datepicker = findViewById(R.id.datepicker);
        weight =  findViewById(R.id.weight);
        kg =  findViewById(R.id.kg);
        lbs =  findViewById(R.id.lbs);

        myCalendar = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();

        toogle();
        datepickerClick();
        setSexSpinner();
        setPetTypeSpinner();
        extras = getIntent().getExtras();
        if (extras != null){
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_MODEL_OBJECT), PetModelUpdated.class);
            UpdatePetData(model);
        }
        petName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
        breedName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});

    }

    //set previous data for update
    private void UpdatePetData(PetModelUpdated model) {

            petName.setText(model.getPetName());
            breedName.setText(model.getPetBreed());
            datepicker.setText(model.getPetDOB());
            weight.setText(model.getPetWeight()+"");
            petWeightUnit = model.getPetWeightUnit();
            if (petWeightUnit.equalsIgnoreCase(getResources().getString(R.string.kg))){
                kg.setBackgroundResource(R.drawable.toogle_on);
                kg.setTextColor(getResources().getColor(R.color.white));
                lbs.setBackgroundResource(R.drawable.toogle_off);
                lbs.setTextColor(getResources().getColor(R.color.orange));
            }else {
                lbs.setBackgroundResource(R.drawable.toogle_on);
                lbs.setTextColor(getResources().getColor(R.color.white));
                kg.setBackgroundResource(R.drawable.toogle_off);
                kg.setTextColor(getResources().getColor(R.color.orange));
            }
            String sex = model.getPetSex();
            for (int i =0 ; i < gender.size(); i++){
                if (!gender.get(i).contains(model.getPetSex())){
                    other.setVisibility(View.VISIBLE);
                    other.setText(sex);
                    sexSpinner.setSelection(5);
                }else {
                    other.setVisibility(View.GONE);
                    other.setText("");
                    sexSpinner.setSelection(i);
                    break;
                }
            }
        for (int i = 0; i < petTypes.size(); i++) {
            if (petTypes.get(i).contains(model.getPetType())) {
                petType.setSelection(i);
            }
        }
            ((TextView)findViewById(R.id.save)).setText(getResources().getString(R.string.update));

    }

    private void setSexSpinner() {
        gender = new ArrayList<>();
        gender.add(getResources().getString(R.string.select));
        gender.add(getResources().getString(R.string.male_intact));
        gender.add(getResources().getString(R.string.male_neutered));
        gender.add(getResources().getString(R.string.female_intact));
        gender.add(getResources().getString(R.string.female_Spayed));
        gender.add(getResources().getString(R.string.other));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, gender);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sexSpinner.setAdapter(dataAdapter);
        sexSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (gender.get(i).equalsIgnoreCase(getResources().getString(R.string.other))){
                    other.setVisibility(View.VISIBLE);
                }else {
                    other.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    private void setPetTypeSpinner() {
        petTypes = new ArrayList<>();
        petTypes.add(getResources().getString(R.string.select));
        petTypes.add(getString(R.string.cat));
        petTypes.add(getString(R.string.dog));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, petTypes);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        petType.setAdapter(dataAdapter);
        petType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setSelectedDate() {
        datepicker.setText(sdf.format(myCalendar.getTime()));
    }

    private void datepickerClick() {
        datepicker.setOnClickListener(view -> {
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (datepicker.getText().toString() != null && !datepicker.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = datepicker.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                }).minDate(2001, 0, 1)
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(2001, 0, 1)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
    }

    private void toogle() {
        kg.setOnClickListener(view -> {
            kg.setBackgroundResource(R.drawable.toogle_on);
            kg.setTextColor(getResources().getColor(R.color.white));
            lbs.setBackgroundResource(R.drawable.toogle_off);
            lbs.setTextColor(getResources().getColor(R.color.orange));
            petWeightUnit = getResources().getString(R.string.kg);
        });

        lbs.setOnClickListener(view -> {
            lbs.setBackgroundResource(R.drawable.toogle_on);
            lbs.setTextColor(getResources().getColor(R.color.white));
            kg.setBackgroundResource(R.drawable.toogle_off);
            kg.setTextColor(getResources().getColor(R.color.orange));
            petWeightUnit = getResources().getString(R.string.lbs);
        });
    }

    private boolean isEmpty() {
        if (petName.getText().length() > 0 && breedName.getText().length() > 0 &&
                datepicker.getText().length() > 0 && weight.getText().length() > 0
                && !sexSpinner.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                && !petType.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                ) {
            if (sexSpinner.getSelectedItem().equals(getResources().getString(R.string.other))){
                if (other.getText().length() == 0){
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public void Save(View view){
        if (isEmpty()){
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }

        String petName1 = petName.getText().toString();
        String breedName1 = breedName.getText().toString();
        String gender;
        String petDOB = datepicker.getText().toString();
        String petWeight = weight.getText().toString();
        String PetWeightUnit = petWeightUnit;
        String type = petType.getSelectedItem().toString();
        if (!sexSpinner.getSelectedItem().equals(getResources().getString(R.string.other))){
            gender = sexSpinner.getSelectedItem().toString();
        }else {
            gender = other.getText().toString();
        }
        if (model !=null){
            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    PetModelUpdated selectedPet = PetModelUpdated.findById(PetModelUpdated.class,model.getId());
                    selectedPet.setUserId(String.valueOf(SessionClass.getInstance().getUser(GeneralInformationAddActivity.this).getID()));
                    selectedPet.setPetName(petName1);
                    selectedPet.setPetSex(gender);
                    selectedPet.setPetBreed(breedName1);
                    selectedPet.setPetDOB(petDOB);
                    selectedPet.setPetWeight(petWeight);
                    selectedPet.setPetWeightUnit(petWeightUnit);
                    selectedPet.setAddedDateTime(selectedPet.getAddedDateTime());
                    selectedPet.setUpdateDateTime(getCurrentDate());
                    selectedPet.setProfileImage(selectedPet.getProfileImage());
                    selectedPet.setPetImages(selectedPet.getPetImages());
                    selectedPet.setPetType(type);
                    selectedPet.save();
                    SessionClass.getInstance().saveUpdatePetModelUpdated(GeneralInformationAddActivity.this,selectedPet);
                    onBackPressed();

                }

                @Override
                public void onNoClick() {

                }
            });



        }else {
            PetModelUpdated petGeneralInformation = new PetModelUpdated(String.valueOf(SessionClass.getInstance().getUser(this).getID()),petName1, gender, breedName1, petDOB, petWeight, PetWeightUnit,"NA","NA",getCurrentDate(),"Na",type);
            petGeneralInformation.save();
            SessionClass.getInstance().saveUpdatePetModelUpdated(this,petGeneralInformation);
            onBackPressed();
        }


    }


    public void Cancel(View view){
        onBackPressed();
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {

    }

    @Override
    protected void onRestart() {
        if (model!=null){
            UpdatePetData(model);
        }

        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
