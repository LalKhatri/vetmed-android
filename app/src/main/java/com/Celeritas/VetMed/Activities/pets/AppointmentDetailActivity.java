package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.AppointmentModel;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.AppointmentModelUpdated;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class AppointmentDetailActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;
    private AppointmentModelUpdated model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_appointment_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_appointment_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));
        initialize();
    }

    private void initialize() {
        mRecyclerView = (RecyclerView)findViewById(R.id.mReyclerView);
        Bundle extras = getIntent().getExtras();
        model = new Gson().fromJson(extras.getString(Constants.PET_APPOINTMENT_OBJECT),AppointmentModelUpdated.class);
        poplulateList(model);
    }

    private void poplulateList(AppointmentModelUpdated model) {

        if (model != null){
            toolbarTitle.setText(model.getAppointmentDate());
            toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);

            List<Item> mItemList = new ArrayList<>();
            Item TIME = new Item();
            String time = getResources().getString(R.string.appointment_time1);
            TIME.setTitle(time);
            TIME.setDescription(model.getAppointmentTime());
            TIME.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(TIME);

            Item Address = new Item();
            String address = getResources().getString(R.string.appointment_location);
            Address.setTitle(address);
            Address.setDescription(model.getAppointmentLocation());
            Address.setImage(R.drawable.location);
            Address.setViewType(Constants.VETERINARIAN);
            mItemList.add(Address);

            Item Doctor = new Item();
            String doctor = getResources().getString(R.string.doctor_name);
            Doctor.setTitle(doctor);
            Doctor.setDescription(model.getAppointmentDoctorName());
            Doctor.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(Doctor);

            /*Item EMAIL = new Item();
            String email = getResources().getString(R.string.veterinarian_email);
            EMAIL.setTitle(email);
            EMAIL.setDescription(model.getVIEmail());
            EMAIL.setImage(R.drawable.email_orange);
            EMAIL.setViewType(Constants.VETERINARIAN);
            mItemList.add(EMAIL);*/

            Item Notes = new Item();
            String noteTitle = getResources().getString(R.string.diet_note);
            Notes.setTitle(noteTitle);
            Notes.setDescription(model.getAppointmentNotes());
            Notes.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(Notes);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            BaseAdapter mAdapter = new BaseAdapter(mItemList);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.setOnItemClickListner((postion, view) -> {
                switch (postion){
                    case 1:{
                        //UtilityClass.showPopup(this,model.getAppointmentLocation(),"Need clarification");
                        String url = "http://maps.google.co.in/maps?q=" +" "+model.getAppointmentLocation();
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,  Uri.parse(url));
                        startActivity(intent);
                        break;
                    }
                    default:{
                        break;
                    }
                }
            });
        }
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        Intent intent = new Intent(AppointmentDetailActivity.this,AppointmentAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PET_APPOINTMENT_OBJECT,new Gson().toJson(model));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        model = AppointmentModelUpdated.findById(AppointmentModelUpdated.class, model.getId());
        poplulateList(model);
        super.onRestart();
    }
}
