package com.Celeritas.VetMed.Activities.pets;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Constants.sdf;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class MedicalHistoryAddActivity extends BaseActivity implements IActionBar {

    private Calendar myCalendar;
    private Calendar myCalendarCurrent;
    private TextView datepicker;
    private TextView diseaseName;
    private TextView note;
    private Bundle extras;
    private String petID;
    private MedicalHistoryModelUpdated model;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_medical_history);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_medical_history_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.medical_history));
        initializeView();
    }

    private void initializeView() {
        datepicker = findViewById(R.id.datepicker);
        diseaseName = findViewById(R.id.diseaseName);
        note = findViewById(R.id.note);
       // RightClick.setVisibility(View.GONE);
        myCalendar = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();


        extras = getIntent().getExtras();
        datepickerClick();
        if (extras != null){
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_MEDICAL_HISTORY_OBJECT), MedicalHistoryModelUpdated.class);
            if (getIntent().getStringExtra(Constants.PET_MEDICAL_HISTORY_OBJECT) != null){
                MedicalHistoryModelUpdated medicalHistoryModel = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_MEDICAL_HISTORY_OBJECT), MedicalHistoryModelUpdated.class);
                diseaseName.setText(medicalHistoryModel.getDiseaseName());
                datepicker.setText(medicalHistoryModel.getDiseaseDate());
                note.setText(medicalHistoryModel.getDiseaseNotes());
                ((TextView)findViewById(R.id.save)).setText(getResources().getString(R.string.update));
                petID = String.valueOf(medicalHistoryModel.getID());
                toolbarTitle.setText(getResources().getString(R.string.edit));
            }
        }

        diseaseName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
        note.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        note.setImeOptions(EditorInfo.IME_ACTION_DONE);
        note.setRawInputType(InputType.TYPE_CLASS_TEXT);
        note.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
    }

    private void setSelectedDate() {

        datepicker.setText(sdf.format(myCalendar.getTime()));
    }

    private void datepickerClick() {
        datepicker.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            if (date ==myCalendarCurrent.get(Calendar.DAY_OF_MONTH) && month == myCalendarCurrent.get(Calendar.MONTH) && year1 ==myCalendarCurrent.get(Calendar.YEAR)){
                myCalendarCurrent.add(Calendar.DATE,1);
            }
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (datepicker.getText().toString() != null && !datepicker.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = datepicker.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
    }

    public void Save(View view){
        if (isEmpty()){
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }


        String diseaseName1 = diseaseName.getText().toString();
        String diseaseDate = datepicker.getText().toString();
        String diseaseNote1 = note.getText().toString();
        if (isDateValid(diseaseDate)) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    "You Can't add future Date In History");
            return;
        }
        if (model !=null){
            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    MedicalHistoryModelUpdated selectedPetMedicalHistory = MedicalHistoryModelUpdated.findById(MedicalHistoryModelUpdated.class,model.getId());
                    selectedPetMedicalHistory.setID(model.getID());
                    selectedPetMedicalHistory.setDiseaseName(diseaseName1);
                    selectedPetMedicalHistory.setDiseaseDate(diseaseDate);
                    selectedPetMedicalHistory.setDiseaseNotes(diseaseNote1);
                    selectedPetMedicalHistory.setAddedDateTime(model.getAddedDateTime());
                    selectedPetMedicalHistory.setUpdateDateTime(getCurrentDate());
                    selectedPetMedicalHistory.save();
                    onBackPressed();

                }

                @Override
                public void onNoClick() {

                }
            });

        }else {
            MedicalHistoryModelUpdated petGeneralInformation = new MedicalHistoryModelUpdated(petID, diseaseName1, diseaseDate, diseaseNote1, getCurrentDate(), getCurrentDate());
            petGeneralInformation.save();
            onBackPressed();
        }

    }

    public void Cancel(View view){
        onBackPressed();
    }

    private boolean isEmpty() {
        if (diseaseName.getText().length() > 0 && datepicker.getText().length() > 0) {
            return false;
        }
        return true;
    }



    @Override
    public void MenuClick() {}

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    public static boolean isDateValid(String selectedDate) {
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
       // String currentDate = ""+myCalendarCurrent.get(Calendar.MONTH)+"/" +myCalendarCurrent.get(Calendar.DAY_OF_MONTH)+"/"+(String.valueOf(myCalendarCurrent.get(Calendar.YEAR))).substring(2);
       // String currentDate = myCalendarCurrent.get(Calendar.DAY_OF_MONTH)+" "+myCalendarCurrent.get(Calendar.MONTH)+" "+(String.valueOf(myCalendarCurrent.get(Calendar.YEAR)));
        Calendar currentDates =  Calendar.getInstance();
        String currentDate = sdf.format(currentDates.getTime()); //myCalendarCurrent.get(Calendar.DAY_OF_MONTH)+" "+myCalendarCurrent.get(Calendar.MONTH)+" "+(String.valueOf(myCalendarCurrent.get(Calendar.YEAR)));
        String date = selectedDate;//String.valueOf((Integer.valueOf(selectedDate.split("/")[0]))-1)+"/"+selectedDate.split("/")[1]+"/"+selectedDate.split("/")[2];
        try {
            Date arg0Date = format.parse(date);
            Date arg1Date = format.parse(currentDate);
            if (arg1Date.after(arg0Date) || arg0Date.equals(arg1Date)) {
                return false;
            } else {
                return true;

            }
        }catch (Exception e){
            e.printStackTrace();

        }
        return false;
    }
}
