package com.Celeritas.VetMed.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

public class ResourcesActivity extends BaseActivity implements IActionBar{

    private RecyclerView recyclerView;
    private BaseAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_resources);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_resources, content, true);

        LeftBack.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.resources));
        initializeViews();
    }

    private void initializeViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        populateList();
    }

    private void populateList() {
        List<Item> mItemList = new ArrayList<>();
        Item diseases = new Item();
        String name = getResources().getString(R.string.diseases);
        diseases.setTitle(name);
        diseases.setImage(R.drawable.dieases);
        diseases.setViewType(Constants.RESOURSE_MAIN);
        mItemList.add(diseases);

        Item diagnosticTest = new Item();
        String name1 = getResources().getString(R.string.diagnostic);
        diagnosticTest.setTitle(name1);
        diagnosticTest.setImage(R.drawable.diagnostic);
        diagnosticTest.setViewType(Constants.RESOURSE_MAIN);
        mItemList.add(diagnosticTest);

        Item clinical = new Item();
        String name2 = getResources().getString(R.string.clinical);
        clinical.setTitle(name2);
        clinical.setImage(R.drawable.clinical);
        clinical.setViewType(Constants.RESOURSE_MAIN);
        mItemList.add(clinical);

        Item usefullLinks = new Item();
        String name3 = getResources().getString(R.string.useful_link);
        usefullLinks.setTitle(name3);
        usefullLinks.setImage(R.drawable.usefull);
        usefullLinks.setViewType(Constants.RESOURSE_MAIN);
        mItemList.add(usefullLinks);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ResourcesActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BaseAdapter(mItemList);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListner((postion, view) -> {
            Intent intent = new Intent(this,ResourcesSubActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.RESOURSE_NAME,mItemList.get(postion).getTitle());
            startActivity(intent);
        });
    }

    @Override
    public void MenuClick() {
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void LeftBackClick() {

    }

    @Override
    public void RightClick() {

    }
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }
}
