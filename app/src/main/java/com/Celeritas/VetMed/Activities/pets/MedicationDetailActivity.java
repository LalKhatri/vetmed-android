package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.MedicationModelUpdated;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MedicationDetailActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;
    private MedicationModelUpdated model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_medication_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_medication_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));


        initialize();
    }

    private void initialize() {
        mRecyclerView = (RecyclerView)findViewById(R.id.medication_detail);
        poplulateList();
    }

    private void poplulateList() {
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_MEDICATION_OBJECT), MedicationModelUpdated.class);
            model = MedicationModelUpdated.findById(MedicationModelUpdated.class,model.getId());
            toolbarTitle.setText(model.getMedicationName());
            toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            List<Item> mItemList = new ArrayList<>();
            Item dosageDetail = new Item();
            String nameDosage = getResources().getString(R.string.medication_dosage);
            dosageDetail.setTitle(nameDosage);
            dosageDetail.setDescription(model.getMedicationDosage().replace(","," "));
            dosageDetail.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(dosageDetail);

            Item Frequency = new Item();
            String frequency = getResources().getString(R.string.medication_frequency);
            Frequency.setTitle(frequency);
            if (model.getMedicationFrequency().contains(getResources().getString(R.string.any_other))){
                Frequency.setDescription(model.getMedicationFrequency().split(",")[1]);
            }else {
                Frequency.setDescription(model.getMedicationFrequency());
            }

            Frequency.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(Frequency);

            Item Route = new Item();
            String route = getResources().getString(R.string.medication_route);
            Route.setTitle(route);
            Route.setDescription(model.getMedicationRoute());
            Route.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(Route);

            Item ForHowLong = new Item();
            String howLong = getResources().getString(R.string.medication_for_how_long);
            ForHowLong.setTitle(howLong);
            if (model.getMedicationForHowLong().contains(getResources().getString(R.string.any_other))){
                ForHowLong.setDescription(model.getMedicationForHowLong().split(",")[1]);
            }else {
                ForHowLong.setDescription(model.getMedicationForHowLong());
            }
            ForHowLong.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(ForHowLong);

            Item StartedDate = new Item();
            String dateTitle = getResources().getString(R.string.medication_date_started);
            StartedDate.setTitle(dateTitle);
            StartedDate.setDescription(model.getMedicationDateStarted());
            StartedDate.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(StartedDate);

            Item Active = new Item();
            String activeTitle = getResources().getString(R.string.medication_active);
            Active.setTitle(activeTitle);
            Active.setDescription(model.getMedicationActive());
            Active.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(Active);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            BaseAdapter mAdapter = new BaseAdapter(mItemList);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        Intent intent = new Intent(MedicationDetailActivity.this,MedicationAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PET_MEDICATION_OBJECT,new Gson().toJson(model));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        poplulateList();
        super.onRestart();
    }
}
