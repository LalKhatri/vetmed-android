package com.Celeritas.VetMed.Activities.pets;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.Models.pets.VaccineModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static com.Celeritas.VetMed.Activities.pets.DietAddActivity.hideOrShowKeyboard;
import static com.Celeritas.VetMed.Activities.pets.MedicalHistoryAddActivity.isDateValid;
import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Constants.sdf;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class PreventativeVaccineAddActivity extends BaseActivity implements IActionBar {

    private TextView vaccineName;
    private TextView dateGiven;
    private Spinner spinnerSideEffects;
    private Spinner spinnerNextDue;
    private TextView vaccineNote;
    private Calendar myCalendar,myCalendarCurrent;
    private ArrayList<String> SideEffects;
    private ArrayList<String> nextDue;
    private Bundle extras;
    private VaccineModelUpdated model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_preventative_vaccine_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_preventative_vaccine_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.vaccines));
        initialize();
    }

    private void initialize() {
        vaccineName = findViewById(R.id.vaccineName);
        dateGiven = findViewById(R.id.datepickerGiven);
        spinnerSideEffects = findViewById(R.id.spinnerEffects);
        spinnerNextDue = findViewById(R.id.spinnerNextDue);
        vaccineNote = findViewById(R.id.note);
        //RightClick.setVisibility(View.GONE);

        myCalendar = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();

        datepickerClick();
        setSideEffectSpinner();
        setNextDueSpinner();
        extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(extras.getString(Constants.PET_VACCINE_OBJECT), VaccineModelUpdated.class);
            loadData(model);
        }
        vaccineName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
        vaccineNote.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        vaccineNote.setImeOptions(EditorInfo.IME_ACTION_DONE);
        vaccineNote.setRawInputType(InputType.TYPE_CLASS_TEXT);
        vaccineNote.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

    }

    //set previous data for update
    private void loadData(VaccineModelUpdated model) {

        if (model != null) {
            vaccineName.setText(model.getVaccineName());
            dateGiven.setText(model.getVaccineDateGiven());
            vaccineNote.setText(model.getVaccineNotes());
            for (int i = 0; i < SideEffects.size(); i++) {
                if (SideEffects.get(i).contains(model.getVaccineSideEffects())) {
                    spinnerSideEffects.setSelection(i);
                }
            }
            for (int i = 0; i < nextDue.size(); i++) {
                if (nextDue.get(i).contains(model.getNextDueDate())) {
                    spinnerNextDue.setSelection(i);
                }
            }

            toolbarTitle.setText(getResources().getString(R.string.edit));
            ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
           // RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.delete));
        }

    }

    private void setSelectedDate() {

        dateGiven.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    protected void onRestart() {
        if (model != null) {
            model = VaccineModelUpdated.findById(VaccineModelUpdated.class, model.getId());
            loadData(model);
        }
        super.onRestart();
    }

    private void datepickerClick() {
        dateGiven.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            if (date ==myCalendarCurrent.get(Calendar.DAY_OF_MONTH) && month == myCalendarCurrent.get(Calendar.MONTH) && year1 ==myCalendarCurrent.get(Calendar.YEAR)){
                myCalendarCurrent.add(Calendar.DATE,1);
            }
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (dateGiven.getText().toString() != null && !dateGiven.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = dateGiven.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
    }

    private boolean isEmpty() {
        if (vaccineName.getText().length() > 0 && dateGiven.getText().length() > 0
                && !spinnerSideEffects.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                && !spinnerNextDue.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                ) {
            return false;
        }
        return true;
    }

    public void Save(View view) {
        if (isEmpty()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }

        String vaccineName1 = vaccineName.getText().toString();
        String dateGiven1 = dateGiven.getText().toString();
        String sideEffects1 = spinnerSideEffects.getSelectedItem().toString();
        String nextDue = spinnerNextDue.getSelectedItem().toString();
        String vaccineNotes1 = vaccineNote.getText().toString();
        if (isDateValid(dateGiven1)) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    "You Can't add future Date");
            return;
        }


        if (model == null) {
            String petID = extras.getString(NetworkUtils.PET_ID);

            VaccineModelUpdated vaccineModel = new VaccineModelUpdated(petID, vaccineName1, dateGiven1, nextDue, sideEffects1, vaccineNotes1, getCurrentDate());
            vaccineModel.save();
            onBackPressed();

        } else {
            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    VaccineModelUpdated vaccineSelected = VaccineModelUpdated.findById(VaccineModelUpdated.class, model.getId());
                    vaccineSelected.setPetID(model.getPetID());
                    vaccineSelected.setVaccineName(vaccineName1);
                    vaccineSelected.setVaccineDateGiven(dateGiven1);
                    vaccineSelected.setNextDueDate(nextDue);
                    vaccineSelected.setVaccineSideEffects(sideEffects1);
                    vaccineSelected.setVaccineNotes(vaccineNotes1);
                    vaccineSelected.setUpdateDateTime(getCurrentDate());
                    vaccineSelected.save();
                    onBackPressed();

                }

                @Override
                public void onNoClick() {

                }
            });


        }

    }

    private void setSideEffectSpinner() {
        SideEffects = new ArrayList<>();
        SideEffects.add(getResources().getString(R.string.select));
        SideEffects.add(getResources().getString(R.string.yes));
        SideEffects.add(getResources().getString(R.string.no));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, SideEffects);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSideEffects.setAdapter(dataAdapter);
        spinnerSideEffects.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(PreventativeVaccineAddActivity.this);
                return false;
            }
        });

    }

    private void setNextDueSpinner() {
        nextDue = new ArrayList<>();
        nextDue.add(getResources().getString(R.string.select));
        nextDue.add(getResources().getString(R.string.six_months));
        nextDue.add(getResources().getString(R.string.one_year));
        nextDue.add(getResources().getString(R.string.three_year));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, nextDue);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNextDue.setAdapter(dataAdapter);
        spinnerNextDue.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(PreventativeVaccineAddActivity.this);
                return false;
            }
        });
    }

    public void Cancel(View view) {
        onBackPressed();
    }

    @Override
    public void MenuClick() {
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        // nothing to do override method is called.
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
