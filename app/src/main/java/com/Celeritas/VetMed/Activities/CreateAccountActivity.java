package com.Celeritas.VetMed.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.Celeritas.VetMed.Callback.IAlertLisetner;
import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.PasswordValidator;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.Celeritas.VetMed.Utility.WebserviceClient;
import com.klinker.android.link_builder.Link;
import com.klinker.android.link_builder.LinkBuilder;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.ContentValues.TAG;
import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;

public class CreateAccountActivity extends Activity {

    @BindView(R.id.terms_text)
    TextView terms_tv;

    @BindView(R.id.footer_tv)
    TextView footer_tv;

    @BindView(R.id.fname_et)
    EditText fname_et;

    @BindView(R.id.email_et)
    EditText email_et;

    @BindView(R.id.pass_et)
    EditText pass_et;

    @BindView(R.id.conf_pass_et)
    EditText conf_pass_et;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        ButterKnife.bind(this);



        pass_et.setTag(R.drawable.hide_pass_icon);
        conf_pass_et.setTag(R.drawable.hide_pass_icon);
        fname_et.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
        setSpecificWordTextColor();
        terms_tv.setOnClickListener(view -> {
            UtilityClass.showPopup(this, getString(R.string.terms_and_conditions),
                    getString(R.string.dummy_text));
        });
    }


    //this method is used to change color of textview word
    //like Click here
    private void setSpecificWordTextColor() {
        Link link = new Link("terms & conditions")
                .setTextColor(getResources().getColor(R.color.orange))
                .setUnderlined(true)
                .setOnLongClickListener((clickedText)->{})
                .setOnClickListener((clickedText) ->{
                    // single clicked
                    //startActivity(new Intent(CreateAccountActivity.this, HomeActivity.class));
                });

        LinkBuilder.on(terms_tv)
                .addLink(link)
                .build();

        //Footer tv change Click here color & make it clickable
        link = new Link("Click Here")
                .setTextColor(getResources().getColor(R.color.orange))
                .setUnderlined(false)
                .setOnLongClickListener(new Link.OnLongClickListener() {
                    @Override
                    public void onLongClick(String clickedText) {
                        // long clicked
                    }
                })
                .setOnClickListener(new Link.OnClickListener() {
                    @Override
                    public void onClick(String clickedText) {
                        // single clicked
                        startActivity(new Intent(CreateAccountActivity.this, SignInActivity.class));
                        finish();
                    }
                });

        LinkBuilder.on(footer_tv)
                .addLink(link)
                .build();

    }


    //this method will check all the required field provided or not
    private boolean isEmpty() {
        if (fname_et.getText().length() > 0 && email_et.getText().length() > 0 &&
                pass_et.getText().length() > 0 && conf_pass_et.getText().length() > 0) {
            return false;
        }

        return true;
    }


    //this method will check if password match or not
    private boolean isPasswordMatch(){
        if(pass_et.getText().toString().equalsIgnoreCase(conf_pass_et.getText().toString())){
            return true;
        }

        return false;
    }

    //navigate to activation activity after registration
    public void signUpClick(View view) {
        if(isEmpty()){
            UtilityClass.showPopup(CreateAccountActivity.this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }

        if(!UtilityClass.isValidEmail(email_et.getText().toString())){
            UtilityClass.showPopup(CreateAccountActivity.this,getResources().getString(R.string.error), getResources().getString(R.string.invalid_email));
            return;
        }

        if(!isPasswordMatch()){
            UtilityClass.showPopup(CreateAccountActivity.this,getResources().getString(R.string.error),
                    getResources().getString(R.string.password_mismatched));
            return;
        }

        if(!(new PasswordValidator().validate(pass_et.getText().toString()))){
            UtilityClass.showPopup(CreateAccountActivity.this,getResources().getString(R.string.alert),
                    getResources().getString(R.string.password_validation));
            return;
        }

        /*if(!UtilityClass.isConnected(CreateAccountActivity.this)){
            UtilityClass.showPopup(CreateAccountActivity.this,
                    getResources().getString(R.string.error), getResources().getString(R.string.internet));
            return;
        }*/

        registerUser();


    }

    private void registerUser() {
        String pass = null;
        try {
            pass = URLEncoder.encode(pass_et.getText().toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String OS = Build.VERSION.RELEASE;
        String model = Build.MODEL;
        Random rnd = new Random();
        final int activation_code = 10000000 + rnd.nextInt(90000000);
        String userName = fname_et.getText().toString();
        String userEmail = email_et.getText().toString();
        String userPassword = pass_et.getText().toString();
        sendUserDataOnServer(NetworkUtils.ADD_USER,userName,userEmail,userPassword);

    }
    //Add new user
    private void sendUserDataOnServer(String ServiceName, String userName, String userEmail, String userPassword) {
        WebserviceClient client = new WebserviceClient(this, ServiceName, true, addNewUser(userName, userEmail, userPassword), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("Response: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    Integer responseStatus = jsonObject.getInt("status");
                    if (responseStatus == 1) {
                        UtilityClass.showPopup(CreateAccountActivity.this, getResources().getString(R.string.success),
                                getResources().getString(R.string.account_created), () -> {
                                    Intent intent = new Intent(CreateAccountActivity.this, ActivationActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra(NetworkUtils.USER_EMAIL, email_et.getText().toString());
                                    startActivity(intent);
                                    finish();
                                });
                    } else {
                        UtilityClass.showPopup(CreateAccountActivity.this, getResources().getString(R.string.alert),
                                getResources().getString(R.string.acount_already));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {

            }
        }, false);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private List<NameValuePair> addNewUser(String userName, String userEmail, String userPassword) {
        try {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(NetworkUtils.USER_NAME, userName));
            pairs.add(new BasicNameValuePair(NetworkUtils.USER_EMAIL, userEmail));
            pairs.add(new BasicNameValuePair(NetworkUtils.USER_PASSWORD, userPassword));
            return pairs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void showhidepassClick(View view) {
        Integer integer = (Integer) pass_et.getTag();
        switch (integer) {
            case R.drawable.hide_pass_icon: {
                pass_et.setInputType(InputType.TYPE_CLASS_TEXT);
                ((ImageView)findViewById(R.id.showhidepass)).
                        setImageResource(R.drawable.show_pass_icon);
                pass_et.setSelection(pass_et.getText().length());
                Log.d("drawableclick", "true");

                pass_et.setTag(R.drawable.show_pass_icon);
                break;
            }

            case R.drawable.show_pass_icon: {
                pass_et.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                Log.d("drawableclick", "true");
                pass_et.setSelection(pass_et.getText().length());
                ((ImageView)findViewById(R.id.showhidepass)).
                        setImageResource(R.drawable.hide_pass_icon);
                pass_et.setTag(R.drawable.hide_pass_icon);
                break;
            }

        }
    }

    public void showhidepassconfirmClick(View view) {
        Integer integer = (Integer) conf_pass_et.getTag();
        switch (integer) {
            case R.drawable.hide_pass_icon: {
                conf_pass_et.setInputType(InputType.TYPE_CLASS_TEXT);
                ((ImageView)findViewById(R.id.showhide_confirmpass)).
                        setImageResource(R.drawable.show_pass_icon);
                conf_pass_et.setSelection(conf_pass_et.getText().length());
                Log.d("drawableclick", "true");

                conf_pass_et.setTag(R.drawable.show_pass_icon);
                break;
            }

            case R.drawable.show_pass_icon: {
                conf_pass_et.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                Log.d("drawableclick", "true");
                conf_pass_et.setSelection(conf_pass_et.getText().length());
                ((ImageView)findViewById(R.id.showhide_confirmpass)).
                        setImageResource(R.drawable.hide_pass_icon);
                conf_pass_et.setTag(R.drawable.hide_pass_icon);
                break;
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
