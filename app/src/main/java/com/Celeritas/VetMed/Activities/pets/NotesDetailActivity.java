package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.NotesModel;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.NotesModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.google.gson.Gson;

public class NotesDetailActivity extends BaseActivity implements IActionBar {

    private TextView noteText;
    private NotesModelUpdated model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_notes_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_notes_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));
        toolbarTitle.setText(getResources().getString(R.string.notes));

        initialize();
    }

    private void initialize() {
        noteText = findViewById(R.id.notes);
        updateDetails(model);
        Bundle extras = getIntent().getExtras();
        model = new Gson().fromJson(extras.getString(Constants.PET_NOTES_OBJECT),NotesModelUpdated.class);
        updateDetails(model);
    }

    private void updateDetails(NotesModelUpdated model) {
        if (model != null){
            toolbarTitle.setText(model.getNoteTitle());
            toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            noteText.setText(model.getNoteText());
        }
    }

    @Override
    public void MenuClick() {

    }
    @Override
    public void RightClick() {
        Intent intent = new Intent(NotesDetailActivity.this,NotesAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PET_NOTES_OBJECT,new Gson().toJson(model));
        startActivity(intent);
    }
    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    protected void onRestart() {
        model = NotesModelUpdated.findById(NotesModelUpdated.class,model.getId());
        updateDetails(model);
        super.onRestart();
    }
}
