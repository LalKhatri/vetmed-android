package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.pets.MedicationModelUpdated;
import com.Celeritas.VetMed.Models.pets.NotesModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;

import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;

public class NotesAddActivity extends BaseActivity implements IActionBar {

    private TextView noteTitle;
    private TextView noteText;
    private NotesModelUpdated model;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_notes_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_notes_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.add_notes));

        initializeView();
    }

    private void initializeView() {
        noteTitle = findViewById(R.id.title);
        noteText = findViewById(R.id.note);

        extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(extras.getString(Constants.PET_NOTES_OBJECT), NotesModelUpdated.class);
            UpdatePetNoteData(model);
        }
        noteTitle.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(), new InputFilter.LengthFilter(32)});
        noteText.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        noteText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        noteText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        noteText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

    }

    //set previous data for update
    private void UpdatePetNoteData(NotesModelUpdated model) {

        if (model != null) {
            noteTitle.setText(model.getNoteTitle());
            noteText.setText(model.getNoteText());
            toolbarTitle.setText(getResources().getString(R.string.edit));
            ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
            //RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.delete));
        }

    }

    private boolean isEmpty() {
        if (noteTitle.getText().length() > 0 && noteText.getText().length() > 0) {
            return false;
        }
        return true;
    }

    public void Save(View view) {
        if (isEmpty()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }

        String noteTitle1 = noteTitle.getText().toString();
        String noteText1 = noteText.getText().toString();

        if (model == null) {
            String petID = extras.getString(NetworkUtils.PET_ID);
            NotesModelUpdated notesModel = new NotesModelUpdated(petID, noteTitle1, noteText1, getCurrentDate());
            notesModel.save();
            onBackPressed();
        } else {
            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    NotesModelUpdated selectedNotes = NotesModelUpdated.findById(NotesModelUpdated.class, model.getId());
                    selectedNotes.setPetID(selectedNotes.getPetID());
                    selectedNotes.setNoteTitle(noteTitle1);
                    selectedNotes.setNoteText(noteText1);
                    selectedNotes.setUpdateDateTime(getCurrentDate());
                    selectedNotes.save();
                    onBackPressed();

                }

                @Override
                public void onNoClick() {

                }
            });

        }
    }

    public void Cancel(View view) {
        onBackPressed();
    }

    @Override
    public void MenuClick() {
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        if (model != null) {
            model = NotesModelUpdated.findById(NotesModelUpdated.class, model.getId());
            UpdatePetNoteData(model);
        }
        super.onRestart();
    }
}
