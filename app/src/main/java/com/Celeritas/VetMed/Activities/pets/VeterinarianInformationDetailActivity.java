package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.VeterinarianModel;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.VeterinarianModelUpdated;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class VeterinarianInformationDetailActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;
    private VeterinarianModelUpdated model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_veterinarian_detail);

        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_veterinarian_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));
        toolbarTitle.setText(getResources().getString(R.string.veterinarian_information));
        toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        initialize();
    }

    private void initialize() {
        mRecyclerView = (RecyclerView) findViewById(R.id.veterinarian_detail);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_VETERINARIAN_OBJECT), VeterinarianModelUpdated.class);
            poplulateList(model);
        }
    }

    private void poplulateList(VeterinarianModelUpdated model) {


        List<Item> mItemList = new ArrayList<>();
        Item Hospital = new Item();
        String hosiptal = getResources().getString(R.string.hospital);
        Hospital.setTitle(hosiptal);
        Hospital.setDescription(model.getVIHospital());
        Hospital.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(Hospital);

        Item Doctor = new Item();
        String doctor = getResources().getString(R.string.doctor_name);
        Doctor.setTitle(doctor);
        Doctor.setDescription(model.getVIDoctorName());
        Doctor.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(Doctor);

        Item Address = new Item();
        String address = getResources().getString(R.string.address);
        Address.setTitle(address);
        Address.setDescription(model.getVIAddress());
        Address.setImage(R.drawable.location);
        Address.setViewType(Constants.VETERINARIAN);
        mItemList.add(Address);

        Item EMAIL = new Item();
        String email = getResources().getString(R.string.veterinarian_email);
        EMAIL.setTitle(email);
        EMAIL.setDescription(model.getVIEmail());
        EMAIL.setImage(R.drawable.email_orange);
        EMAIL.setViewType(Constants.VETERINARIAN);
        mItemList.add(EMAIL);

        Item phoneNumber = new Item();
        String phone = getResources().getString(R.string.phone_number);
        phoneNumber.setTitle(phone);
        phoneNumber.setDescription(model.getVIPhoneNumber());
        phoneNumber.setImage(R.drawable.phone);
        phoneNumber.setViewType(Constants.VETERINARIAN);
        mItemList.add(phoneNumber);

        Item webSite = new Item();
        String site = getResources().getString(R.string.website);
        webSite.setTitle(site);
        webSite.setDescription(model.getVIWebsite());
        webSite.setImage(R.drawable.web);
        webSite.setViewType(Constants.VETERINARIAN);
        mItemList.add(webSite);

        Item Notes = new Item();
        String noteTitle = getResources().getString(R.string.diet_note);
        Notes.setTitle(noteTitle);
        Notes.setDescription(model.getVINotes());
        Notes.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(Notes);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        BaseAdapter mAdapter = new BaseAdapter(mItemList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListner((postion, view) -> {
            switch (postion) {
                case 2: {
                    //UtilityClass.showPopup(this,model.getVIAddress(),"Need clarification");
                    String url = "http://maps.google.co.in/maps?q=" +" "+model.getVIAddress();
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                    break;
                }
                case 3: {
                    //UtilityClass.showPopup(this,model.getVIEmail(),"Need clarification");
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{model.getVIEmail()});
                    i.putExtra(Intent.EXTRA_SUBJECT, model.getVIDoctorName());
                    //i.putExtra(Intent.EXTRA_TEXT   , "Need clarification");
                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case 4: {
                    String number = model.getVIPhoneNumber();
                    String uri = "tel:" + number.trim();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                    break;
                }
                case 5: {
                    String url = model.getVIWebsite();
                    if (!url.startsWith("http://") && !url.startsWith("https://"))
                        url = "http://" + url;
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                    break;
                }
                default: {
                    break;
                }
            }
        });
    }



    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        Intent intent = new Intent(VeterinarianInformationDetailActivity.this, VeterinarianInformationAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PET_VETERINARIAN_OBJECT, new Gson().toJson(model));
        startActivity(intent);
    }

    @Override
    protected void onRestart() {
        model = VeterinarianModelUpdated.findById(VeterinarianModelUpdated.class,model.getId());
        poplulateList(model);
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
