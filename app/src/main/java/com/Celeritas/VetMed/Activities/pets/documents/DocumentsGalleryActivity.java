package com.Celeritas.VetMed.Activities.pets.documents;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.DocumentsAdapter;
import com.Celeritas.VetMed.Adapters.GalleryAdapter;
import com.Celeritas.VetMed.BuildConfig;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Models.GalleryModel;
import com.Celeritas.VetMed.Models.pets.Documents;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.RealPathUtil;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.getCacheFile;
import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.handleSamplingAndRotationBitmap;
import static com.Celeritas.VetMed.Constants.DOCUMENTS;
import static com.Celeritas.VetMed.Constants.mimeTypes;
import static com.Celeritas.VetMed.Utility.FilePath.getDataColumn;
import static com.Celeritas.VetMed.Utility.FilePath.isDownloadsDocument;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.getFilePath;
import static com.Celeritas.VetMed.Utility.UtilityClass.isExternalStorageDocument;
import static com.Celeritas.VetMed.Utility.UtilityClass.isMediaDocument;

public class DocumentsGalleryActivity extends BaseActivity implements IActionBar {

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE = 112;
    private LinearLayout uploadPicture;
    private RecyclerView mRecycler;
    private static final int NO_OF_COLUMNS = 3;
    private static final int MAX_IMAGES_ALLOWED = 10;
    public static final int CAMERA_REQUEST_CODE = 110;
    private static final int GALLERY_REQUEST_CODE = 111;
    public static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    List<String> imageKeyList;
    List<String> selectedImagePathList;
    List<String> capturedImageList;
    List<String> imageKeyListUpdate;
    List<String> selectedImagePathListUpdate;
    private Uri fileUri;
    private String petID;
    private String docType;
    private long petIDSelected;
    private Integer userID;
    private CheckBox randomCheckbox;
    private AmazonS3Client s3Client;
    private String selectedPetProfileImage = "NA";
    private GalleryAdapter adapter;
    private ArrayList<GalleryModel> galleryModelList;
    private boolean isFromOptions = false;
    private String tempImageName;
    private String petImages;
    private ImageView iv_selected_image;
    private Bitmap bmp;
    private PetModelUpdated model;
    List<Documents> documentsList = new ArrayList<>();
    public String fileName, docPath;
    private boolean deleteSelected;

    // image rotation issue resolved by below link
    //https://stackoverflow.com/questions/14066038/why-does-an-image-captured-using-camera-intent-gets-rotated-on-some-devices-on-a/31720143

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_gallary);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_documents_gallary, content, true);
        initialize();
    }

    private void initialize() {
        randomCheckbox = findViewById(R.id.checkbox);
        uploadPicture = findViewById(R.id.add);
        mRecycler = findViewById(R.id.recyclerView);
        iv_selected_image = findViewById(R.id.iv_selected_image);
        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.add));

        findViewById(R.id.no_data_found_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenuItem();
            }
        });


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            docType = extras.getString(getString(R.string.doc_type));
            petIDSelected = Long.valueOf(petID);
            toolbarTitle.setText(extras.getString(getString(R.string.doc_type)));
            try {
                documentsList = Documents.findWithQuery(Documents.class, "Select * from " + DOCUMENTS + " where petID = ? and doc_type = ?", petID, docType);
                setDocsList(documentsList, mRecycler, this, false);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ((TextView) findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to) + " " + toolbarTitle.getText().toString().toLowerCase());

        showOrHideAddDocView(documentsList);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE);
        }
        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fileName != null && fileName != "") {
                    Documents documents = new Documents(petID, extras.getString(getString(R.string.doc_type)), getCurrentDate(), fileName, docPath);
                    documents.save();
                    showPopup(DocumentsGalleryActivity.this, "Success", "file successfully added");
                    fileName = null;
                    docPath = null;
                } else {
                    UtilityClass.showPopup(DocumentsGalleryActivity.this, getResources().getString(R.string.alert), getString(R.string.select_file));
                }

            }
        });

    }

    public void showOrHideAddDocView(List<Documents> documentsList) {
        if (documentsList != null && documentsList.size() > 0) {
            findViewById(R.id.bottom_sheet).setVisibility(View.VISIBLE);
            findViewById(R.id.no_data_found_layout).setVisibility(View.GONE);
        } else {
            findViewById(R.id.no_data_found_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.bottom_sheet).setVisibility(View.GONE);
        }
    }

    public void Cancel(View view) {
        if (deleteSelected) {
            setDocsList(documentsList, mRecycler, this, false);
        } else {
            onBackPressed();
        }

    }

    private void setDocsList(List<Documents> docList, RecyclerView dietRv, Context context, boolean isFromDelete) {
        deleteSelected = isFromDelete;
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL);
        dietRv.setLayoutManager(staggeredGridLayoutManager);
        DocumentsAdapter documentsAdapter = new DocumentsAdapter(docList, context, isFromDelete);
        dietRv.setAdapter(documentsAdapter);

    }


    // camera permission help
    //https://stackoverflow.com/questions/43042725/revoked-permission-android-permission-camera/43070198
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 7:
                if (resultCode == RESULT_OK) {
                    String PathHolder = data.getData().getPath();

                    // Toast.makeText(this, PathHolder, Toast.LENGTH_LONG).show();

                    File myFile = new File(PathHolder);
                    Documents documents = new Documents();
                    documents.setDocName(getRealPathFromURI(data.getData()));
                    Log.e("File Name", "" + getRealPathFromURI(data.getData()));
                    try {
                        Log.e("File ", ": -->" + getFilePath(this, data.getData()));
                        //openFile(),this);
                        docPath = getFilePath(this, data.getData());
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    documentsList.add(documents);
                    setDocsList(documentsList, mRecycler, this, false);
                    fileName = getRealPathFromURI(data.getData());
                    findViewById(R.id.bottom_sheet).setVisibility(View.VISIBLE);
                    findViewById(R.id.no_data_found_layout).setVisibility(View.GONE);
                    Log.e("File Path -->", "" + data.getData().getPath());
                    Log.e("File Path -->", "" + data.getDataString());

                    //RealPathUtil.getRealPathFromURI_API19(this,data.getData());

           /*         try {
                        DocumentsActivity.FileOpen.openFile(this, myFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(activity, "File does not exist", Toast.LENGTH_SHORT).show();
                    }
*/
                }
                break;
            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap bmp;
                    try {
                        bmp = handleSamplingAndRotationBitmap(this, fileUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                       /* File myFile = new File(fileUri.toString());
                        DocumentsActivity.FileOpen.openFile(this, myFile);*/
                    //String PathHolder = data.getData().getPath();
                    fileName = getRealPathFromURI(fileUri);
                    Documents documents = new Documents();
                    //documents.setDocName(fileName+".jpg");
                    documents.setDocName(getRealPathFromURI(fileUri));
                    documentsList.add(documents);
                    Log.e("File Name", "" + getRealPathFromURI(fileUri));
                    Log.e("Image Path -->", "" + fileUri.getPath());
                    setDocsList(documentsList, mRecycler, this, false);
                    findViewById(R.id.bottom_sheet).setVisibility(View.VISIBLE);
                    findViewById(R.id.no_data_found_layout).setVisibility(View.GONE);
                    try {
                        Log.e("File ", ": -->" + getFilePath(this, fileUri));
                        Log.e("File url ", ": -->" + getCacheFile(this, tempImageName));
                        //openFile(,this);
                        docPath = getCacheFile(this, tempImageName);
                        //Log.e("File ", ": -->" + getFilePath(this,data.getData()));
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    //RealPathUtil.getRealPathFromURI_API19(this,fileUri);

                    //getImageData(fileUri);
                }
                break;
        }
    }

    public void openFile(String fileName, Context context) {
        try{

        File file = new File(fileName);

        Intent i;
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.O) {
            /*String format = "https://drive.google.com/viewerng/viewer?embedded=true&url=%s";
            String fullPath = String.format(Locale.ENGLISH, format, file.getAbsolutePath());
            i = new Intent(Intent.ACTION_VIEW, Uri.parse(fullPath));
            //i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            startActivity(i);*/
            String path = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file).toString();
            String fileNameWithType = path.split("/")[path.split("/").length-1];
            String fileType = fileNameWithType.replace(".","-").split("-")[1];
            Log.e("File Uri Path -> ",path);
            Log.e("fileNameWithType-> ",fileNameWithType);
            Log.e("fileType-> ",fileType);
            if (fileType.equalsIgnoreCase("jpg") || fileType.equalsIgnoreCase("jpeg")|| fileType.equalsIgnoreCase("png")){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file));
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            }else {
                    String format = "https://drive.google.com/viewerng/viewer?embedded=true&url=%s";
            String fullPath =format+path;
                Intent intent = new Intent(Intent.ACTION_VIEW);
// set flag to give temporary permission to external app to use your FileProvider
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
// generate URI, I defined authority as the application ID in the Manifest, the last param is file I want to open
                Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
// I am opening a PDF file so I give it a valid MIME type
                intent.setData(uri);//
// validate that the device can open your File!
                PackageManager pm = this.getPackageManager();
                if (intent.resolveActivity(pm) != null) {
                    Intent chooser = Intent.createChooser(intent,"File : "+fileNameWithType);
                    chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

                    startActivity(chooser);
                }else {
                    Intent chooser = Intent.createChooser(intent,"File : "+fileNameWithType);
                    chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

                    startActivity(chooser);
                }
            }





        } else {

            i = new Intent(Intent.ACTION_VIEW, FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file));
            i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(i);
        }
        }catch (Exception e){
        e.printStackTrace();
            Toast.makeText(context, "An error occurred !", Toast.LENGTH_SHORT).show();
        }



    }

    public void cameraIntent() {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempImageName = String.valueOf(System.currentTimeMillis()) + ".jpg";
        File file = new File(getExternalCacheDir(),
                tempImageName);
        // 7.0 FileProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fileUri = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);
            i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            fileUri = Uri.fromFile(file);
        }

        i.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PackageManager.PERMISSION_GRANTED);
            } else {
                startActivityForResult(i, CAMERA_REQUEST_CODE);
            }

        }

    }


    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        showMenuItem();
    }

    private void showMenuItem() {
        PopupMenu popup = new PopupMenu(this, findViewById(R.id.RightImage));
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.dashboard_row, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.ic_add:
                        if (ContextCompat.checkSelfPermission(DocumentsGalleryActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(DocumentsGalleryActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
                        } else {
                            browseDocuments();
                        }
                        return true;
                    case R.id.ic_take_pick:
                        cameraIntent();
                        return true;
                    case R.id.ic_delete:
                        documentsList = Documents.findWithQuery(Documents.class, "Select * from " + DOCUMENTS + " where petID = ? and doc_type = ?", petID, docType);
                        setDocsList(documentsList, mRecycler, DocumentsGalleryActivity.this, true);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    private void browseDocuments() {


        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        startActivityForResult(Intent.createChooser(intent, "Choose File"), 7);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void showPopup(Context context, String title, String message) {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.custom_dailog, null);
        alertDialogBuilder.setView(promptsView);
        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        ((TextView) promptsView.findViewById(R.id.alert_title)).setText(title);
        ((TextView) promptsView.findViewById(R.id.msg)).setText(message);
        TextView ok = (TextView) promptsView.findViewById(R.id.alert_ok);
        ok.setOnClickListener(v -> {
            alertDialog.dismiss();
            onBackPressed();
        });
        alertDialog.show();
    }

    public String getRealPathFromURI(Uri contentURI) {

        Uri uri = contentURI;
        String uriString = uri.toString();
        File myFile = new File(uriString);
        String path = myFile.getAbsolutePath();
        String displayName = null;

        if (uriString.startsWith("content://")) {
            Cursor cursor = null;
            try {
                cursor = this.getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        } else if (uriString.startsWith("file://")) {
            displayName = myFile.getName();
        }
        return displayName;
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {

            cursor = context.getContentResolver().query(contentUri, null, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e("getRealPathFromURI 2", "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }





}
