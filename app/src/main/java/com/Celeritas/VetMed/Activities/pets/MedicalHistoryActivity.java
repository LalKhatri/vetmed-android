package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.Celeritas.VetMed.Constants.MED_HISTORY;

public class MedicalHistoryActivity extends BaseActivity implements IActionBar {


    private RecyclerView mRecycler;
    private RelativeLayout card_view;
    private Bundle extras;
    BaseAdapter adapter;
    private List<MedicalHistoryModelUpdated> petMedicalModelList;
    private String petID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_medical_history);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_medical_history, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.add));
        toolbarTitle.setText(getResources().getString(R.string.medical_history));
        ((TextView) findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to)+" "+toolbarTitle.getText().toString().toLowerCase());
        initialize();
    }

    private void initialize() {
        card_view = (RelativeLayout) findViewById(R.id.no_data_found_layout);

        mRecycler = (RecyclerView) findViewById(R.id.medicalHistory);
        extras = getIntent().getExtras();

        card_view.setOnClickListener(v -> {
            addDetails();
        });
        if (extras != null) {
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            //checkUpdatedPetsMedicalHistory(petID);
        }
        petMedicalModelList = new ArrayList<>();
        populateList();
    }


    private void populateList() {
        petMedicalModelList = MedicalHistoryModelUpdated.findWithQuery(MedicalHistoryModelUpdated.class, "Select * from " + MED_HISTORY + " where petID = ? ", petID);
        Collections.sort(petMedicalModelList, new MedicalHistoryModelUpdated());
        if ((petMedicalModelList.size() == 0)) {
            card_view.setVisibility(View.VISIBLE);


        } else {
            card_view.setVisibility(View.GONE);

        }
        List<Item> mItemList = new ArrayList<>();
        for (MedicalHistoryModelUpdated medicalHistoryModel : petMedicalModelList) {
            Item nameDetail = new Item();
            nameDetail.setTitle(medicalHistoryModel.getDiseaseName() + "\n ( " + medicalHistoryModel.getDiseaseDate() + " )");
            nameDetail.setViewType(Constants.PET_MAIN_DETAIL);
            mItemList.add(nameDetail);
        }
        adapter = new BaseAdapter(mItemList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setAdapter(adapter);
        setSwipeCallback();
        adapter.setOnItemClickListner((postion, view) -> {
            //       Toast.makeText(this, petModelList.get(postion).getPetName(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MedicalHistoryDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.PET_MEDICAL_HISTORY_OBJECT, new Gson().toJson(petMedicalModelList.get(postion)));
            intent.putExtra(Constants.PET_MODEL_OBJECT, new Gson().toJson(petMedicalModelList.get(postion)));
            startActivity(intent);
        });
    }

    @Override
    public void MenuClick() {

    }

    @Override
    protected void onRestart() {
        populateList();
        super.onRestart();
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        addDetails();
    }

    private void addDetails() {
        Intent intent = new Intent(this, MedicalHistoryAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(NetworkUtils.PET_ID, petID);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //SwipeToDelete Callback
    public void setSwipeCallback() {


        final SwipeToDeleteCallBack callBack = new SwipeToDeleteCallBack(this) {

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction) {
                if (direction == ItemTouchHelper.LEFT) {

                    UtilityClass.showDeletPopup(MedicalHistoryActivity.this, getResources().getString(R.string.alert), getResources().getString(R.string.medical_confirmation), new IAlertListner() {
                        @Override
                        public void onYesClick() {
                            MedicalHistoryModelUpdated selectedPet = petMedicalModelList.get(viewHolder.getAdapterPosition());
                            MedicalHistoryModelUpdated note = MedicalHistoryModelUpdated.findById(MedicalHistoryModelUpdated.class, selectedPet.getId());
                            note.delete();
                            petMedicalModelList.remove(viewHolder.getAdapterPosition());
                            adapter.notifyDataSetChanged();
                            populateList();
                            if ((petMedicalModelList.size() == 0)) {
                                card_view.setVisibility(View.VISIBLE);
                            } else {
                                card_view.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onNoClick() {
                            mRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    });

                }
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                if (viewHolder != null) {
                    if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);
                    } else {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);

                    }
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                viewHolder.itemView.setBackgroundColor(Color.WHITE);


            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(callBack);
        helper.attachToRecyclerView(mRecycler);


    }
}
