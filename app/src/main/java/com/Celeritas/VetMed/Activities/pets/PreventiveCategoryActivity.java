package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class PreventiveCategoryActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_preventive_category);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_preventive_category, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.preventive_medicine));

        populateList();
    }

    private void populateList() {
        mRecyclerView = findViewById(R.id.recyclerView);
        List<String> preventativeMedicines = new ArrayList<>();
        List<Item> mItemList = new ArrayList<>();
        preventativeMedicines.add(getResources().getString(R.string.vaccines));
        preventativeMedicines.add(getResources().getString(R.string.deworming));
        for (int i=0; i < preventativeMedicines.size(); i++){
            Item nameDetail = new Item();
            String name = preventativeMedicines.get(i);
            nameDetail.setTitle(name);
            nameDetail.setViewType(Constants.PET_MAIN_DETAIL);
            mItemList.add(nameDetail);
        }
        String petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PreventiveCategoryActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);

        BaseAdapter mAdapter = new BaseAdapter(mItemList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListner((postion, view) -> {
        switch (postion){
            case 0:{
                Intent intent = new Intent(PreventiveCategoryActivity.this,PreventativeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(NetworkUtils.PET_ID,petID);
                intent.putExtra(Constants.MEDICINE_TYPE,preventativeMedicines.get(postion));
                startActivity(intent);
                break;
            }case 1:{
                Intent intent = new Intent(PreventiveCategoryActivity.this,PreventativeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(NetworkUtils.PET_ID,petID);
                intent.putExtra(Constants.MEDICINE_TYPE,preventativeMedicines.get(postion));
                startActivity(intent);
                break;
            }
            default:break;
            }
        });
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
