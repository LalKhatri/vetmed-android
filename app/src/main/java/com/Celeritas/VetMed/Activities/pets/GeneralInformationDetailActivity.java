package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.AgeCalculation;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class GeneralInformationDetailActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;
    private BaseAdapter mAdapter;
    private PetModelUpdated model;
    private String finalAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_general_information_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_general_information_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));
        toolbarTitle.setText(getResources().getString(R.string.general_information));

        initializeViews();
    }

    private void initializeViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.pet_detail);
        populateList();
    }

    private void populateList() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_MODEL_OBJECT), PetModelUpdated.class);
            // model = (PetModel) getIntent().getSerializableExtra(Constants.PET_MODEL_OBJECT);

            loadInformation(model);


        }
    }

    private String getAge(int year, int month, int day) {
        AgeCalculation age = new AgeCalculation();
        age.getCurrentDate();
        age.setDateOfBirth(year, month, day);
        int calculatedYear = age.calcualteYear();
        int calculatedMonth = age.calcualteMonth();
        int calculatedDate = age.calcualteDay();
        Integer year1 = new Integer(calculatedYear);
        Integer month1 = new Integer(calculatedMonth);
        Integer day1 = new Integer(calculatedDate);
        String yearS = year1.toString();
        String monthS = month1.toString();
        String dayS = day1.toString();
        return dayS + ":" + monthS + ":" + yearS;

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void MenuClick() {

    }

    @Override
    protected void onRestart() {
        model = PetModelUpdated.findById(PetModelUpdated.class, model.getId());
        loadInformation(model);
        super.onRestart();

    }

    private void loadInformation(PetModelUpdated model) {
        List<Item> mItemList = new ArrayList<>();
        Item nameDetail = new Item();
        String name = getResources().getString(R.string.name);
        nameDetail.setTitle(name);
        nameDetail.setDescription(model.getPetName());
        nameDetail.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(nameDetail);

        Item gender = new Item();
        String sex = getResources().getString(R.string.sex);
        gender.setTitle(sex);
        gender.setDescription(model.getPetSex());
        gender.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(gender);

        Item breed = new Item();
        String breedTitle = getResources().getString(R.string.breed);
        breed.setTitle(breedTitle);
        breed.setDescription(model.getPetBreed());
        breed.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(breed);

        Item DOB = new Item();
        String date = getResources().getString(R.string.date_of_birth);
        DOB.setTitle(date);
        DOB.setDescription(model.getPetDOB());
        DOB.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(DOB);

        String dob = model.getPetDOB();
        String array1[] = dob.split(" ");
        int date1 = Integer.valueOf(array1[0]);
        int month = Arrays.asList(months).indexOf(array1[1]);;
        int year = Integer.valueOf(array1[2]);
        String age = getAge(year, month, date1);
        String array2[] = age.split(":");
        String days = array2[0];
        String months = array2[1];
        String years = array2[2];
        if (years.equalsIgnoreCase("0") && months.equalsIgnoreCase("0")) {
            finalAge = days + " days";
        } else if (years.equalsIgnoreCase("0") && !months.equalsIgnoreCase("0")) {
            finalAge = months + "mo, " + days + " days";
        } else {
            finalAge = years + "yr, " + months + " mo";
        }

        Item Age = new Item();
        String ageTitle = getResources().getString(R.string.age);
        Age.setTitle(ageTitle);
        Age.setDescription(finalAge);
        Age.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(Age);

        Item petType = new Item();
        String petTypeTitle = getResources().getString(R.string.pet_type);
        petType.setTitle(petTypeTitle);
        petType.setDescription(model.getPetType());
        petType.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(petType);

        Item Weight = new Item();
        String weightTitle = getResources().getString(R.string.weight);
        Weight.setTitle(weightTitle);
        Weight.setDescription(String.valueOf(model.getPetWeight()) + " " +" "+model.getPetWeightUnit());
        Weight.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(Weight);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(GeneralInformationDetailActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BaseAdapter(mItemList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        Intent intent = new Intent(GeneralInformationDetailActivity.this, GeneralInformationAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PET_MODEL_OBJECT, new Gson().toJson(model)); // put pet model
        startActivity(intent);
    }
}
