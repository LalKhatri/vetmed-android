package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.DewormingModel;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.VaccineModel;
import com.Celeritas.VetMed.Models.pets.DewormingModelUpdated;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.VaccineModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.Celeritas.VetMed.Constants.DEWORMING;
import static com.Celeritas.VetMed.Constants.VACCINE;

public class PreventativeActivity extends BaseActivity implements IActionBar {

    private RelativeLayout card_view;

    private RecyclerView mRecycler;
    private BaseAdapter adapter;
    private Bundle extras;
    private String petID;
    private List<VaccineModelUpdated> petVaccineModelList;
    private List<DewormingModelUpdated> petDewormingModelList;
    private String medicineType;
    private ArrayList<Item> mItemList = new ArrayList<>();
    public static String alertInfoText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_preventative);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_preventative, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.add));
        initialize();
        ((TextView) findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to)+" "+toolbarTitle.getText().toString().toLowerCase());
    }

    private void initialize() {
        card_view = (RelativeLayout) findViewById(R.id.no_data_found_layout);

        mRecycler = (RecyclerView) findViewById(R.id.recyclerView);
        extras = getIntent().getExtras();

        card_view.setOnClickListener(v -> {
            addDetails();
        });

        if (extras != null) {
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            medicineType = extras.getString(Constants.MEDICINE_TYPE);
            if (medicineType.equalsIgnoreCase(getResources().getString(R.string.vaccines)))
                loadVaccineData(petID);
            else
                loadDewormingData(petID);
            toolbarTitle.setText(medicineType.toString());
            toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        }
    }

    private void addDetails() {
        if (medicineType.equalsIgnoreCase(getResources().getString(R.string.vaccines))) {
            Intent intent = new Intent(this, PreventativeVaccineAddActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(NetworkUtils.PET_ID, petID);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, PreventativeDewormingAddActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(NetworkUtils.PET_ID, petID);
            startActivity(intent);
        }
    }

    private void loadDewormingData(String petID) {
        mItemList = new ArrayList<>();
        petDewormingModelList = DewormingModelUpdated.findWithQuery(DewormingModelUpdated.class, "Select * from " + DEWORMING + " where petID = ? ", petID);
        for (DewormingModelUpdated dewormingModel : petDewormingModelList) {
            Item deworming = new Item();
            String name = dewormingModel.getDFTProductName();
            deworming.setTitle(name);
            deworming.setViewType(Constants.PET_MAIN_DETAIL);
            mItemList.add(deworming);
        }
        if ((mItemList.size() == 0)) {
            card_view.setVisibility(View.VISIBLE);


        } else {
            card_view.setVisibility(View.GONE);

        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(mLayoutManager);
        adapter = new BaseAdapter(mItemList);
        mRecycler.setAdapter(adapter);
        setSwipeCallback();
        adapter.setOnItemClickListner((postion, view) -> {
            if (medicineType.equalsIgnoreCase(getResources().getString(R.string.vaccines))) {
                Intent intent = new Intent(this, PreventativeDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.PET_VACCINE_OBJECT, new Gson().toJson(petVaccineModelList.get(postion)));
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, PreventativeDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.PET_DEWORMING_OBJECT, new Gson().toJson(petDewormingModelList.get(postion)));
                startActivity(intent);
            }
        });
    }

    private void loadVaccineData(String petID) {
        mItemList = new ArrayList<>();
        petVaccineModelList = VaccineModelUpdated.findWithQuery(VaccineModelUpdated.class, "Select * from " + VACCINE + " where petID = ? ", petID);
        for (VaccineModelUpdated dewormingModel : petVaccineModelList) {
            Item deworming = new Item();
            String name = dewormingModel.getVaccineName();
            deworming.setTitle(name);
            deworming.setViewType(Constants.PET_MAIN_DETAIL);
            mItemList.add(deworming);
        }
        if ((mItemList.size() == 0)) {
            card_view.setVisibility(View.VISIBLE);
        } else {
            card_view.setVisibility(View.GONE);
        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(mLayoutManager);
        adapter = new BaseAdapter(mItemList);
        mRecycler.setAdapter(adapter);
        setSwipeCallback();
        adapter.setOnItemClickListner((postion, view) -> {
            if (medicineType.equalsIgnoreCase(getResources().getString(R.string.vaccines))) {
                Intent intent = new Intent(this, PreventativeDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.PET_VACCINE_OBJECT, new Gson().toJson(petVaccineModelList.get(postion)));
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, PreventativeDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.PET_DEWORMING_OBJECT, new Gson().toJson(petDewormingModelList.get(postion)));
                startActivity(intent);
            }
        });
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
addDetails();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {

        updateData(petID);
        super.onRestart();
    }

    public void updateData(String petID) {
        if (medicineType.equalsIgnoreCase(getResources().getString(R.string.vaccines)))
            loadVaccineData(petID);
        else
            loadDewormingData(petID);
    }

    //SwipeToDelete Callback
    public void setSwipeCallback() {

        if (medicineType.equalsIgnoreCase(getResources().getString(R.string.vaccines)))
            alertInfoText = getResources().getString(R.string.vaccine_confirmation);
        else
            alertInfoText = getResources().getString(R.string.deworming_confirmation);

        final SwipeToDeleteCallBack callBack = new SwipeToDeleteCallBack(this) {

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction) {
                if (direction == ItemTouchHelper.LEFT) {

                    UtilityClass.showDeletPopup(PreventativeActivity.this, getResources().getString(R.string.alert), alertInfoText, new IAlertListner() {
                        @Override
                        public void onYesClick() {
                            if (medicineType.equalsIgnoreCase(getResources().getString(R.string.vaccines))) {
                                VaccineModelUpdated selectedPet = petVaccineModelList.get(viewHolder.getAdapterPosition());
                                VaccineModelUpdated note = VaccineModelUpdated.findById(VaccineModelUpdated.class, selectedPet.getId());
                                note.delete();
                                petVaccineModelList.remove(viewHolder.getAdapterPosition());
                                loadVaccineData(petID);
                            } else {
                                DewormingModelUpdated selectedPet = petDewormingModelList.get(viewHolder.getAdapterPosition());
                                DewormingModelUpdated note = DewormingModelUpdated.findById(DewormingModelUpdated.class, selectedPet.getId());
                                note.delete();
                                petDewormingModelList.remove(viewHolder.getAdapterPosition());
                                loadDewormingData(petID);
                            }

                        }

                        @Override
                        public void onNoClick() {
                            mRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    });

                }
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                if (viewHolder != null) {
                    if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);
                    } else {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);

                    }
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                viewHolder.itemView.setBackgroundColor(Color.WHITE);


            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(callBack);
        helper.attachToRecyclerView(mRecycler);


    }
}
