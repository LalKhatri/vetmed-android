package com.Celeritas.VetMed.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Helper.SharedPrefManager;
import com.Celeritas.VetMed.Models.ClinicalSigns;
import com.Celeritas.VetMed.Models.DiagnosticTestModel;
import com.Celeritas.VetMed.Models.DiseaseModel;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.PetModel;
import com.Celeritas.VetMed.Models.UseFullLinks;
import com.Celeritas.VetMed.Models.UserModel;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.Celeritas.VetMed.Utility.WebserviceClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

public class ResourcesSubActivity extends BaseActivity implements IActionBar {

    private EditText searchEdit;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipRefresh;
    private List<DiseaseModel> diseaseModelList;
    private List<DiagnosticTestModel> diagnosticModelList;
    private List<ClinicalSigns> clinicalModelList;
    private List<UseFullLinks> usefulLinksModelList;
    private List<DiseaseModel> diseaseModelListFiltered;
    private List<DiagnosticTestModel> diagnosticModelListFiltered;
    private List<ClinicalSigns> clinicalModelListFiltered;
    private List<UseFullLinks> usefulLinksModelListFiltered;
    Integer userId;
    private String resourceType;
    List<Item> mItemList;
    List<Object> filterData = new ArrayList<Object>();
    private BaseAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_resources_sub);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_resources_sub, content, true);

        MenuClick.setVisibility(View.GONE);
        initialise();
    }

    private void initialise() {
        searchEdit = (EditText) findViewById(R.id.search_et);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
       // swipRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        if (!new SharedPrefManager(this).getBooleanByKey(Constants.DONT_SHOW)){
            showPopup(this);
        }
/*        swipRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Refresh();
                swipRefresh.setRefreshing(false);
            }
        });*/


        UserModel userModel = SessionClass.getInstance().getUser(this);
        if (userModel != null){
            userId = SessionClass.getInstance().getUser(this).getID();
        }else {
            userId = new SharedPrefManager(this).getIntegerByKey(NetworkUtils.USER_ID);
        }
        if (getIntent().getStringExtra(Constants.RESOURSE_NAME) != null){
            toolbarTitle.setText(getIntent().getStringExtra(Constants.RESOURSE_NAME));
            resourceType = getIntent().getStringExtra(Constants.RESOURSE_NAME);
            if (resourceType.equalsIgnoreCase(getResources().getString(R.string.diseases))){
               parseDiseaseJSON(new SharedPrefManager(this).getStringByKey(Constants.DISEASES_OBJECT));
            }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.diagnostic))){
                parseDiagnosticJSON(new SharedPrefManager(this).getStringByKey(Constants.DIAGNOSTIC_TEST_OBJECT));
            }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.clinical))){
                parseClinicalJSON(new SharedPrefManager(this).getStringByKey(Constants.CLINICAL_SIGNS_OBJECT));
            }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.useful_link))){
                parseUseFulJSON(new SharedPrefManager(this).getStringByKey(Constants.USEFUL_LINKS_OBJECT));
            }
            filterData();
        }

    }

    private void filterData() {
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s1) {
                try {


                mAdapter.filter(s1.toString());
                String s = s1.toString();
                s = s.toLowerCase(Locale.getDefault());
                if (diseaseModelList != null) {
                    if (diseaseModelList.size() > 0) {
                        diseaseModelListFiltered.clear();
                        if (s.length() == 0) {
                            diseaseModelListFiltered.addAll(diseaseModelList);
                        } else {
                            for (DiseaseModel item : diseaseModelList) {
                                if (s.length() != 0 && item.getDiseaseName().toLowerCase(Locale.getDefault()).contains(s)) {
                                    diseaseModelListFiltered.add(item);
                                }
                            }
                        }
                    }
                }
                if (diagnosticModelList != null){
                    if (diagnosticModelList.size() > 0) {
                        diagnosticModelListFiltered.clear();
                        if (s.length() == 0) {
                            diagnosticModelListFiltered.addAll(diagnosticModelList);
                        } else {
                            for (DiagnosticTestModel item : diagnosticModelList) {
                                if (s.length() != 0 && item.getDiagnosticTestName().toLowerCase(Locale.getDefault()).contains(s)) {
                                    diagnosticModelListFiltered.add(item);
                                }
                            }
                        }
                    }
                }if (clinicalModelList != null){
                    if (clinicalModelList.size() > 0) {
                        clinicalModelListFiltered.clear();
                        if (s.length() == 0) {
                            clinicalModelListFiltered.addAll(clinicalModelList);
                        } else {
                            for (ClinicalSigns item : clinicalModelList) {
                                if (s.length() != 0 && item.getClinicalSignName().toLowerCase(Locale.getDefault()).contains(s)) {
                                    clinicalModelListFiltered.add(item);
                                }
                            }
                        }
                    }
                }
                if (usefulLinksModelList != null){

                }
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(activity, "Data does not Exist", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void Refresh(){
        String resourceType = getIntent().getStringExtra(Constants.RESOURSE_NAME);
        if (resourceType.equalsIgnoreCase(getResources().getString(R.string.diseases))){
            getDisease_Clinical_Diagnostic_useful(NetworkUtils.GET_RESOURCES,userId,NetworkUtils.DISEASE_TABLE);
        }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.diagnostic))){
            getDisease_Clinical_Diagnostic_useful(NetworkUtils.GET_RESOURCES,userId,NetworkUtils.DIAGNOSTIC_TABLE);
        }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.clinical))){
            getDisease_Clinical_Diagnostic_useful(NetworkUtils.GET_RESOURCES,userId,NetworkUtils.CLINICAL_TABLE);
        }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.useful_link))){
            getDisease_Clinical_Diagnostic_useful(NetworkUtils.GET_RESOURCES,userId,NetworkUtils.USEFUL_TABLE);
        }
    }

    private void populateList() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BaseAdapter(mItemList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListner((postion, view) -> {
            if (resourceType.equalsIgnoreCase(getResources().getString(R.string.diseases))){
                Intent intent = new Intent(this,DiseaseDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.DISEASES_OBJECT,diseaseModelListFiltered.get(postion));
                startActivity(intent);
            }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.diagnostic))){
                Intent intent = new Intent(this,DianosticDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.DIAGNOSTIC_TEST_OBJECT,diagnosticModelListFiltered.get(postion));
                startActivity(intent);
            }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.clinical))){
                //UtilityClass.showPopup(this,getResources().getString(R.string.alert),getResources().getString(R.string.under_construction));
                Intent intent = new Intent(this,DianosticDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.CLINICAL_SIGNS_OBJECT,clinicalModelListFiltered.get(postion));
                startActivity(intent);
            }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.useful_link))){
                String url = usefulLinksModelListFiltered.get(postion).getLinkURL(); //"http://www.example.com";
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                //UtilityClass.showPopup(this,getResources().getString(R.string.alert),getResources().getString(R.string.under_construction));
            }
        });
    }

    private void parseDiseaseJSON(String jsonString) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<DiseaseModel>>(){}.getType();
        diseaseModelList = gson.fromJson(jsonString, type);
        mItemList = new ArrayList<>();
        if (diseaseModelList != null) {
            diseaseModelListFiltered = new ArrayList<>(diseaseModelList);
            for (DiseaseModel diseaseModel : diseaseModelList) {
                Item diseases = new Item();
                String name = diseaseModel.getDiseaseName();
                diseases.setTitle(name);
                diseases.setViewType(Constants.PET_MAIN_DETAIL);
                mItemList.add(diseases);
            }
            populateList();
        }
    }
    private void parseDiagnosticJSON(String jsonString) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<DiagnosticTestModel>>(){}.getType();
        mItemList = new ArrayList<>();
        diagnosticModelList = gson.fromJson(jsonString, type);
        if (diagnosticModelList != null) {
            diagnosticModelListFiltered = new ArrayList<>(diagnosticModelList);
            for (DiagnosticTestModel diagnosticTestModel : diagnosticModelList) {
                Item diagnostic = new Item();
                String name = diagnosticTestModel.getDiagnosticTestName();
                diagnostic.setTitle(name);
                diagnostic.setViewType(Constants.PET_MAIN_DETAIL);
                mItemList.add(diagnostic);
            }
            populateList();
        }
    }
    private void parseClinicalJSON(String jsonString) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<ClinicalSigns>>(){}.getType();
        clinicalModelList = gson.fromJson(jsonString, type);
        if (clinicalModelList != null) {
            clinicalModelListFiltered = new ArrayList<>(clinicalModelList);
            mItemList = new ArrayList<>();
            for (ClinicalSigns clinicalSigns : clinicalModelList) {
                Item clinical = new Item();
                String name = clinicalSigns.getClinicalSignName();
                clinical.setTitle(name);
                clinical.setViewType(Constants.PET_MAIN_DETAIL);
                mItemList.add(clinical);
            }
            populateList();
        }
    }
    private void parseUseFulJSON(String jsonString) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<UseFullLinks>>(){}.getType();
        usefulLinksModelList = gson.fromJson(jsonString, type);
        if (usefulLinksModelList != null) {
            usefulLinksModelListFiltered = new ArrayList<>(usefulLinksModelList);
            mItemList = new ArrayList<>();
            for (UseFullLinks useFullLinks : usefulLinksModelList) {
                Item usefulLinks = new Item();
                String name = useFullLinks.getLinkTitle();
                usefulLinks.setImage(R.drawable.link);
                usefulLinks.setTitle(name);
                usefulLinks.setViewType(Constants.RESOURSE_MAIN);
                mItemList.add(usefulLinks);
            }
            populateList();
        }
    }

    private void getDisease_Clinical_Diagnostic_useful(String ServiceName, Integer userID, String tableName) {
        WebserviceClient client = new WebserviceClient(this, ServiceName, true, getDiseases_clincal_diagnostic_usefull(userID, tableName), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("Disease: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    int responseStatus = jsonObject.getInt("status");
                    if (responseStatus == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        if (resourceType.equalsIgnoreCase(getResources().getString(R.string.diseases))){
                            parseDiseaseJSON(jsonArray.toString());
                            new SharedPrefManager(ResourcesSubActivity.this).setStringForKey(jsonArray.toString(),Constants.DISEASES_OBJECT);
                        }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.diagnostic))){
                            parseDiagnosticJSON(jsonArray.toString());
                            new SharedPrefManager(ResourcesSubActivity.this).setStringForKey(jsonArray.toString(),Constants.DIAGNOSTIC_TEST_OBJECT);
                        }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.clinical))){
                            parseClinicalJSON(jsonArray.toString());
                            new SharedPrefManager(ResourcesSubActivity.this).setStringForKey(jsonArray.toString(),Constants.CLINICAL_SIGNS_OBJECT);
                        }else if (resourceType.equalsIgnoreCase(getResources().getString(R.string.useful_link))){
                            parseUseFulJSON(jsonArray.toString());
                            new SharedPrefManager(ResourcesSubActivity.this).setStringForKey(jsonArray.toString(),Constants.USEFUL_LINKS_OBJECT);
                        }
                    }else {
                        UtilityClass.showPopup(ResourcesSubActivity.this,getResources().getString(R.string.alert),jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {

            }
        }, true);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private List<NameValuePair> getDiseases_clincal_diagnostic_usefull(Integer userID, String tableName) {
        try {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(NetworkUtils.USER_ID, String.valueOf(userID)));
            pairs.add(new BasicNameValuePair(NetworkUtils.TABLE_NAME, tableName));
            return pairs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void showPopup(Context context){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.custom_dailog, null);
        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        ((TextView)promptsView.findViewById(R.id.alert_title)).setText(context.getResources().getString(R.string.disc_title));
        ((TextView)promptsView.findViewById(R.id.msg)).setText(context.getResources().getString(R.string.disclaimer));
        AppCompatCheckBox checkBox = (AppCompatCheckBox) promptsView.findViewById(R.id.dont);
        checkBox.setVisibility(View.VISIBLE);
        TextView ok = (TextView) promptsView.findViewById(R.id.alert_ok);
        ok.setOnClickListener(v -> {
            if (checkBox.isChecked()){
                new SharedPrefManager(context).setBooleanForKey(true,Constants.DONT_SHOW);
            }else {
                new SharedPrefManager(context).setBooleanForKey(false,Constants.DONT_SHOW);
            }
            alertDialog.dismiss();
        });
        alertDialog.show();
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
