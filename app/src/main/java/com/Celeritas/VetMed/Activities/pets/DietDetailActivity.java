package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class DietDetailActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;
    private DietModelUpdated model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_diet_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_diet_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));
        initialize();
    }

    private void initialize() {
        mRecyclerView = (RecyclerView) findViewById(R.id.diet_detail);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_DIET_OBJECT), DietModelUpdated.class);
            poplulateList(model);
        }

    }

    private void poplulateList(DietModelUpdated model) {

        toolbarTitle.setText(model.getDietName());
        toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        List<Item> mItemList = new ArrayList<>();
        Item dietType = new Item();
        String type = getResources().getString(R.string.diet_type);
        dietType.setTitle(type);
        dietType.setDescription(model.getDietType());
        dietType.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(dietType);

        Item HowMuch = new Item();
        String howMuch = getResources().getString(R.string.diet_how_much);
        HowMuch.setTitle(howMuch);
        HowMuch.setDescription(model.getDietHowMuch());
        HowMuch.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(HowMuch);

        Item HowOften = new Item();
        String howoften = getResources().getString(R.string.diet_how_often);
        HowOften.setTitle(howoften);

        if (model.getDietHowOften().contains(getResources().getString(R.string.any_other))) {
            HowOften.setDescription(model.getDietHowOften().split(",")[1]);
        } else {
            HowOften.setDescription(model.getDietHowOften());
        }


        HowOften.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(HowOften);

        Item DateStarted = new Item();
        String date = getResources().getString(R.string.diet_started_date);
        DateStarted.setTitle(date);
        DateStarted.setDescription(model.getDietDateStarted());
        DateStarted.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(DateStarted);

        Item EndDate = new Item();
        String dateTitle = getResources().getString(R.string.diet_end_date);
        EndDate.setTitle(dateTitle);
        EndDate.setDescription(model.getDietEndDate());
        EndDate.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(EndDate);

        Item Active = new Item();
        String activeTitle = getResources().getString(R.string.diet_active);
        Active.setTitle(activeTitle);
        Active.setDescription(model.getDietActive());
        Active.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(Active);

        Item Notes = new Item();
        String noteTitle = getResources().getString(R.string.diet_note);
        Notes.setTitle(noteTitle);
        Notes.setDescription(model.getDietNotes());
        Notes.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
        mItemList.add(Notes);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        BaseAdapter mAdapter = new BaseAdapter(mItemList);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        Intent intent = new Intent(DietDetailActivity.this, DietAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PET_DIET_OBJECT, new Gson().toJson(model));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        model = DietModelUpdated.findById(DietModelUpdated.class, model.getId());
        poplulateList(model);
        super.onRestart();
    }
}
