package com.Celeritas.VetMed.Activities.pets;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.JournalModel;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.Models.pets.WeightTrackerModel;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static com.Celeritas.VetMed.Activities.pets.MedicalHistoryAddActivity.isDateValid;
import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDateRequiredFormate;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDateRequiredFormate;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDateRequiredFormate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class JournalAddActivity extends BaseActivity implements IActionBar, View.OnClickListener {


    private TextView jorunalDate;
    private Calendar myCalendar, myCalendarCurrent;
    private Bundle extras;
    private JournalModel model;
    public static Context context;
    private TextView note;
    private ImageView happy, sick, confuse;
    private String petMode = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_journal_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.pet_journal));
        initialize();
    }

    private void initialize() {


        jorunalDate = findViewById(R.id.datepickerStarted);
        jorunalDate.setHint(getCurrentDateRequiredFormate());
        note = findViewById(R.id.note);
        sick = findViewById(R.id.iv_sick);
        happy = findViewById(R.id.iv_happy);
        confuse = findViewById(R.id.iv_confuse);

        sick.setOnClickListener(this);
        happy.setOnClickListener(this);
        confuse.setOnClickListener(this);


        myCalendar = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();
        datePickerClick();
        extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_JOURNAL_OBJECT), JournalModel.class);
            updateWeightTracker(model);
        }

        note.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        note.setImeOptions(EditorInfo.IME_ACTION_DONE);
        note.setRawInputType(InputType.TYPE_CLASS_TEXT);
        note.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

    }


    private void setSelectedDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_TWO, Locale.US);
        jorunalDate.setText(sdf.format(myCalendar.getTime()));
    }

    //set previous data for update
    private void updateWeightTracker(JournalModel journalModel) {


        if (journalModel != null) {

            jorunalDate.setText(journalModel.getDate());
            note.setText(journalModel.getNotes());
            toolbarTitle.setText(getResources().getString(R.string.edit));
            ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
            setMode(journalModel.getPetMode());
            petMode = journalModel.getPetMode();


        }

    }

    private void setMode(String petMode) {
        switch (petMode) {
            case "Happy":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    happy.setImageDrawable(getResources().getDrawable(R.drawable.ic_happy_active, getApplicationContext().getTheme()));

                } else {
                    happy.setImageDrawable(getResources().getDrawable(R.drawable.ic_happy_active));
                }
                break;
            case "Sick":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sick.setImageDrawable(getResources().getDrawable(R.drawable.ic_sick_active, getApplicationContext().getTheme()));

                } else {
                    sick.setImageDrawable(getResources().getDrawable(R.drawable.ic_sick_active));
                }
                break;
            case "Confuse":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    confuse.setImageDrawable(getResources().getDrawable(R.drawable.ic_confuse_active, getApplicationContext().getTheme()));

                } else {
                    confuse.setImageDrawable(getResources().getDrawable(R.drawable.ic_confuse_active));
                }
                break;
        }
    }


    private void datePickerClick() {
        jorunalDate.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class, SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1]) - 1;
            int year1 = Integer.valueOf(array1[2]);
            if (date == myCalendarCurrent.get(Calendar.DAY_OF_MONTH) && month == myCalendarCurrent.get(Calendar.MONTH) && year1 == myCalendarCurrent.get(Calendar.YEAR)) {
                myCalendarCurrent.add(Calendar.DATE, 1);
            }
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (jorunalDate.getText().toString() != null && !jorunalDate.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = jorunalDate.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1]) - 1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });

    }

    private boolean isEmpty() {
        if (
                petMode.length() > 0 &&
                        jorunalDate.getText().length() > 0
                ) {
            return false;
        }


        return true;
    }


    public void Save(View view) {
        if (isEmpty()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }


        String startedDate = jorunalDate.getText().toString();
        //String moode = petMode;
        String notes = note.getText().toString();
        if (isDateValid(startedDate)) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    "You Can't add future Date");
            return;
        }


        if (model == null) {
            String petID = extras.getString(NetworkUtils.PET_ID);
            JournalModel journalModel = new JournalModel(petID, getCurrentDate(), petMode, startedDate, notes);
            journalModel.save();
            onBackPressed();
        } else {

            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    JournalModel selectedJournal = JournalModel.findById(JournalModel.class, model.getId());
                    selectedJournal.setPetID(model.getPetID());
                    selectedJournal.setPetMode(petMode);
                    selectedJournal.setDate(startedDate);
                    selectedJournal.setNotes(notes);
                    selectedJournal.setUpdateDateTime(getCurrentDate());
                    selectedJournal.save();
                    onBackPressed();
                }

                @Override
                public void onNoClick() {
                }
            });


        }

    }

    @Override
    protected void onRestart() {
        if (model != null) {
            model = JournalModel.findById(JournalModel.class, model.getId());
            updateWeightTracker(model);
        }
        super.onRestart();

    }

    public void Cancel(View view) {
        onBackPressed();
    }

    @Override
    public void MenuClick() {
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_sick:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sick.setImageDrawable(getResources().getDrawable(R.drawable.ic_sick_active, getApplicationContext().getTheme()));
                    happy.setImageDrawable(getResources().getDrawable(R.drawable.ic_happy_de_active, getApplicationContext().getTheme()));
                    confuse.setImageDrawable(getResources().getDrawable(R.drawable.ic_confuse_de_active, getApplicationContext().getTheme()));
                } else {
                    sick.setImageDrawable(getResources().getDrawable(R.drawable.ic_sick_active));
                    happy.setImageDrawable(getResources().getDrawable(R.drawable.ic_happy_de_active));
                    confuse.setImageDrawable(getResources().getDrawable(R.drawable.ic_confuse_de_active));
                }
                petMode = getString(R.string.sick);
                break;
            case R.id.iv_happy:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sick.setImageDrawable(getResources().getDrawable(R.drawable.ic_sick_de_active, getApplicationContext().getTheme()));
                    happy.setImageDrawable(getResources().getDrawable(R.drawable.ic_happy_active, getApplicationContext().getTheme()));
                    confuse.setImageDrawable(getResources().getDrawable(R.drawable.ic_confuse_de_active, getApplicationContext().getTheme()));
                } else {
                    sick.setImageDrawable(getResources().getDrawable(R.drawable.ic_sick_de_active));
                    happy.setImageDrawable(getResources().getDrawable(R.drawable.ic_happy_active));
                    confuse.setImageDrawable(getResources().getDrawable(R.drawable.ic_confuse_de_active));
                }
                petMode = getString(R.string.happy);
                break;
            case R.id.iv_confuse:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sick.setImageDrawable(getResources().getDrawable(R.drawable.ic_sick_de_active, getApplicationContext().getTheme()));
                    happy.setImageDrawable(getResources().getDrawable(R.drawable.ic_happy_de_active, getApplicationContext().getTheme()));
                    confuse.setImageDrawable(getResources().getDrawable(R.drawable.ic_confuse_active, getApplicationContext().getTheme()));
                } else {
                    sick.setImageDrawable(getResources().getDrawable(R.drawable.ic_sick_de_active));
                    happy.setImageDrawable(getResources().getDrawable(R.drawable.ic_happy_de_active));
                    confuse.setImageDrawable(getResources().getDrawable(R.drawable.ic_confuse_active));
                }
                petMode = getString(R.string.confuse);
                break;
        }
    }
}
