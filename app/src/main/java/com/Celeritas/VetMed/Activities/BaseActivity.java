package com.Celeritas.VetMed.Activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.pets.AppointmentActivity;
import com.Celeritas.VetMed.Activities.pets.AppointmentAddActivity;
import com.Celeritas.VetMed.Activities.pets.AppointmentDetailActivity;
import com.Celeritas.VetMed.Activities.pets.DietActivity;
import com.Celeritas.VetMed.Activities.pets.DietAddActivity;
import com.Celeritas.VetMed.Activities.pets.DietDetailActivity;
import com.Celeritas.VetMed.Activities.pets.documents.DocumentsActivity;
import com.Celeritas.VetMed.Activities.pets.GalleryActivity;
import com.Celeritas.VetMed.Activities.pets.GeneralInformationAddActivity;
import com.Celeritas.VetMed.Activities.pets.GeneralInformationDetailActivity;
import com.Celeritas.VetMed.Activities.pets.InsuranceInformationActivity;
import com.Celeritas.VetMed.Activities.pets.InsuranceInformationAddActivity;
import com.Celeritas.VetMed.Activities.pets.InsuranceInformationDetailActivity;
import com.Celeritas.VetMed.Activities.pets.JournalActivity;
import com.Celeritas.VetMed.Activities.pets.JournalAddActivity;
import com.Celeritas.VetMed.Activities.pets.MedicalHistoryActivity;
import com.Celeritas.VetMed.Activities.pets.MedicalHistoryAddActivity;
import com.Celeritas.VetMed.Activities.pets.MedicalHistoryDetailActivity;
import com.Celeritas.VetMed.Activities.pets.MedicationActivity;
import com.Celeritas.VetMed.Activities.pets.MedicationAddActivity;
import com.Celeritas.VetMed.Activities.pets.MedicationDetailActivity;
import com.Celeritas.VetMed.Activities.pets.MyPetsActivity;
import com.Celeritas.VetMed.Activities.pets.NotesActivity;
import com.Celeritas.VetMed.Activities.pets.NotesAddActivity;
import com.Celeritas.VetMed.Activities.pets.NotesDetailActivity;
import com.Celeritas.VetMed.Activities.pets.PetDetailActivity;
import com.Celeritas.VetMed.Activities.pets.PreventativeActivity;
import com.Celeritas.VetMed.Activities.pets.PreventativeDetailActivity;
import com.Celeritas.VetMed.Activities.pets.PreventativeDewormingAddActivity;
import com.Celeritas.VetMed.Activities.pets.PreventativeVaccineAddActivity;
import com.Celeritas.VetMed.Activities.pets.PreventiveCategoryActivity;
import com.Celeritas.VetMed.Activities.pets.VeterinarianInformationActivity;
import com.Celeritas.VetMed.Activities.pets.VeterinarianInformationAddActivity;
import com.Celeritas.VetMed.Activities.pets.VeterinarianInformationDetailActivity;
import com.Celeritas.VetMed.Activities.pets.WeightTrackerActivity;
import com.Celeritas.VetMed.Activities.pets.WeightTrackerAddActivity;
import com.Celeritas.VetMed.Activities.pets.documents.DocumentsGalleryActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    public DrawerLayout drawer;
    private Toolbar toolbar;
    private int id;
    private ActionBarDrawerToggle toggle;
    protected TextView toolbarTitle;
    protected ImageView LeftClick;
    protected ImageView RightClick;
    protected ImageView shareButton;
    protected ImageView MenuClick;
    protected ImageView LeftBack;
    public IActionBar actionListner;
    public Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);
        activity = this;
        setupDrawer();
    }

    //setup navigation drawer
    private void setupDrawer() {
        LeftBack = (ImageView) findViewById(R.id.BackImage);
        MenuClick = (ImageView) findViewById(R.id.LeftImage);
        RightClick = (ImageView) findViewById(R.id.RightImage);
        shareButton = (ImageView) findViewById(R.id.share_button);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);

       // NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);
        TextView Name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.imageView);
        TextView fulName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.name);
        String userName = SessionClass.getInstance().getUser(this).getUserName();
        String array[]= userName.split(" ");
        char firstName,lastName;
        try{
            if (array.length > 1){
                firstName = array[0].charAt(0);
                lastName = array[1].charAt(0);
                Name.setText(String.valueOf(firstName)+String.valueOf(lastName));
            }else {
                firstName = userName.charAt(0);
                Name.setText(String.valueOf(firstName));
            }
            fulName.setText(userName);
        }catch (Exception e){
            e.printStackTrace();
        }


        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }

        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        View hView =  navigationView.getHeaderView(0);
        ((ImageView)hView.findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (activity instanceof MyPetsActivity){
            actionListner = (IActionBar) activity;
            RightClick.setOnClickListener(view -> {
                actionListner.RightClick();
            });
            MenuClick.setOnClickListener(view -> {
                actionListner.MenuClick();
            });
        }else if (activity instanceof GeneralInformationAddActivity || activity instanceof PetDetailActivity
                || activity instanceof ResourcesSubActivity || activity instanceof DiseaseDetailActivity ||
                activity instanceof DianosticDetailActivity || activity instanceof PreventiveCategoryActivity) {
            actionListner = (IActionBar) activity;
            LeftBack.setOnClickListener(view -> {
            actionListner.LeftBackClick();
            });
        }else if (activity instanceof GeneralInformationDetailActivity || activity instanceof GeneralInformationAddActivity || activity instanceof MedicalHistoryActivity
                || activity instanceof MedicalHistoryAddActivity || activity instanceof MedicationActivity || activity instanceof MedicalHistoryDetailActivity
                || activity instanceof MedicationAddActivity || activity instanceof MedicationDetailActivity
                || activity instanceof DietActivity || activity instanceof DietDetailActivity || activity instanceof DietAddActivity
                || activity instanceof PreventativeActivity || activity instanceof PreventativeDetailActivity
                || activity instanceof PreventativeVaccineAddActivity || activity instanceof PreventativeDewormingAddActivity
                || activity instanceof VeterinarianInformationDetailActivity || activity instanceof VeterinarianInformationActivity
                || activity instanceof VeterinarianInformationAddActivity || activity instanceof InsuranceInformationAddActivity || activity instanceof InsuranceInformationDetailActivity
                || activity instanceof InsuranceInformationActivity || activity instanceof AppointmentActivity|| activity instanceof ShareActivity
                || activity instanceof AppointmentAddActivity || activity instanceof AppointmentDetailActivity
                || activity instanceof NotesAddActivity || activity instanceof NotesDetailActivity || activity instanceof PetDetailActivity
                || activity instanceof WeightTrackerActivity ||  activity instanceof WeightTrackerAddActivity
                || activity instanceof JournalActivity ||  activity instanceof JournalAddActivity
                || activity instanceof NotesActivity || activity instanceof GalleryActivity || activity instanceof DocumentsActivity || activity instanceof DocumentsGalleryActivity) {
            actionListner = (IActionBar) activity;
            LeftBack.setOnClickListener(view -> {
                actionListner.LeftBackClick();
            });
            RightClick.setOnClickListener(view -> {
                actionListner.RightClick();
            });
        } else if (activity instanceof ResourcesActivity){
            actionListner = (IActionBar) activity;
            MenuClick.setOnClickListener(view -> {
                actionListner.MenuClick();
            });
        }
        else if (activity instanceof AboutUsActivity){
            actionListner = (IActionBar) activity;
            MenuClick.setOnClickListener(view -> {
                actionListner.MenuClick();
            });
        }
        else if (activity instanceof ContactUsActivity){
            actionListner = (IActionBar) activity;
            MenuClick.setOnClickListener(view -> {
                actionListner.MenuClick();
            });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.ic_navigation){
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.START);
        }

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        id = item.getItemId();

        switch (id){
            case R.id.nav_resource:{
               Intent intent = new Intent( BaseActivity.this, ResourcesActivity.class );
               /*intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
               intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               startActivity(intent);*/
                startActivityDetails(intent);
               //finish();
                break;
            }

            case R.id.nav_pet:{
                Intent intent = new Intent( BaseActivity.this, MyPetsActivity.class );
             /*   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/
                startActivityDetails(intent);
                //finish();
                break;
            }

            case R.id.nav_contact:{
                /*contactUs(CLIENT_EMAIL);*/
                Intent intent = new Intent( BaseActivity.this, ContactUsActivity.class );
                startActivityDetails(intent);
                break;
            }

            case R.id.nav_about:{
             /*   Intent intent = new Intent( BaseActivity.this, PdfActivity.class );
                startActivity(intent);*/
                Intent intent = new Intent( BaseActivity.this, AboutUsActivity.class );
                /*intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/
                startActivityDetails(intent);
                //finish();
                break;
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //when user click on logout this method will call
    public void logoutClick(View view) {
        //
        try {
            SessionClass.getInstance().clearSession(this);
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        }catch (Exception e){
            e.printStackTrace();
        }
        finish();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bacground_gradient);
            //window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
        }
    }
    public static void contactUs(String email, String userEmail, String subject, String name, String comments, Context context) {
        /*User Details:
        Name : sdafa
        Email : ea
        Comments :*/
        String body = "User Details\n Name : "+name+" \n"+"Email : "+userEmail+" \n"+"Comments : "+comments;
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",email, null));
        intent.putExtra(Intent.EXTRA_EMAIL, email);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(intent, "Send Email"));
    }
    public void startActivityDetails(Intent intent){
        //Intent intent = new Intent( BaseActivity.this, activityName.class );
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
