package com.Celeritas.VetMed.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.DiseaseModel;
import com.Celeritas.VetMed.R;
import com.klinker.android.link_builder.Link;
import com.klinker.android.link_builder.LinkBuilder;

public class DiseaseDetailActivity extends BaseActivity implements IActionBar {

    private TextView diseaseType;
    private TextView discription;
    private TextView diagnosed;
    private TextView treatment;
    private LinearLayout DiseaaseType;
    private LinearLayout DescriptionRelative;
    private LinearLayout DiagnosticRelative;
    private LinearLayout TreatmentRelative;
    private View v2;
    private View v3;
    private TextView more1;
    private TextView more2;
    private TextView more3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_disease_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_disease_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        initialiseViews();
    }

    private void initialiseViews() {
        diseaseType = (TextView) findViewById(R.id.disease_type);
        discription = (TextView) findViewById(R.id.discription);
        diagnosed = (TextView) findViewById(R.id.diagnosed);
        treatment = (TextView) findViewById(R.id.treatment);
        DiseaaseType = (LinearLayout) findViewById(R.id.diseaseType);
        DescriptionRelative = (LinearLayout)findViewById(R.id.Description);
        DiagnosticRelative = (LinearLayout)findViewById(R.id.DiagnosticRelative);
        TreatmentRelative = (LinearLayout)findViewById(R.id.TreatmentRelative);
        more1 = (TextView) findViewById(R.id.more1);
        more2 = (TextView) findViewById(R.id.more2);
        more3 = (TextView) findViewById(R.id.more3);

        v2 = (View) findViewById(R.id.v2);
        v3 = (View) findViewById(R.id.v3);

        if (getIntent().getSerializableExtra(Constants.DISEASES_OBJECT) != null){
            DiseaseModel diseaseModel = (DiseaseModel) getIntent().getSerializableExtra(Constants.DISEASES_OBJECT);
            toolbarTitle.setText(diseaseModel.getDiseaseName());
            toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            diseaseType.setText(diseaseModel.getDiseaseCategory());
            discription.setText(diseaseModel.getDiseaseDescription());
            diagnosed.setText(diseaseModel.getDiseaseDiagnosis());
            treatment.setText(diseaseModel.getDiseaseTreatment());
        }

        ((LinearLayout)findViewById(R.id.Description)).setOnClickListener(view -> {
            hideViews(0);
        });

        ((LinearLayout)findViewById(R.id.DiagnosticRelative)).setOnClickListener(view -> {
            hideViews(1);
        });

        ((LinearLayout)findViewById(R.id.TreatmentRelative)).setOnClickListener(view -> {
            hideViews(2);
        });
    }

    void hideViews(int type){

        v2.setVisibility(View.GONE);
        v3.setVisibility(View.GONE);
        DiseaaseType.setVisibility(View.GONE);
        if (type == 0){
            more1.setVisibility(View.GONE);
            DiagnosticRelative.setVisibility(View.GONE);
            TreatmentRelative.setVisibility(View.GONE);
            discription.setEllipsize(null);
            discription.setMaxLines(50);
        }else if (type == 1){
            more2.setVisibility(View.GONE);
            DescriptionRelative.setVisibility(View.GONE);
            TreatmentRelative.setVisibility(View.GONE);
            diagnosed.setEllipsize(null);
            diagnosed.setMaxLines(50);
        }else if (type == 2){
            more3.setVisibility(View.GONE);
            DescriptionRelative.setVisibility(View.GONE);
            DiagnosticRelative.setVisibility(View.GONE);
            treatment.setEllipsize(null);
            treatment.setMaxLines(50);
        }
    }

    void showViews(){
        v2.setVisibility(View.VISIBLE);
        v3.setVisibility(View.VISIBLE);
        DiseaaseType.setVisibility(View.VISIBLE);
        DescriptionRelative.setVisibility(View.VISIBLE);
        DiagnosticRelative.setVisibility(View.VISIBLE);
        TreatmentRelative.setVisibility(View.VISIBLE);
        more1.setVisibility(View.VISIBLE);
        more2.setVisibility(View.VISIBLE);
        more3.setVisibility(View.VISIBLE);
        discription.setMaxLines(3);
        diagnosed.setMaxLines(3);
        treatment.setMaxLines(3);
        discription.setEllipsize(TextUtils.TruncateAt.END);
        diagnosed.setEllipsize(TextUtils.TruncateAt.END);
        treatment.setEllipsize(TextUtils.TruncateAt.END);

    }

    private void setSpecificWordTextColor(String text, TextView textView) {
        Link link = new Link(text)
                .setTextColor(getResources().getColor(R.color.orange))
                .setUnderlined(true)
                .setOnLongClickListener((clickedText)->{})
                .setOnClickListener((clickedText) ->{

                });

        LinkBuilder.on(textView)
                .addLink(link)
                .build();

    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {

    }

    @Override
    public void onBackPressed() {
        if (v2.getVisibility() == View.GONE){
            showViews();
        }else {
            super.onBackPressed();
            finish();
        }

    }
}
