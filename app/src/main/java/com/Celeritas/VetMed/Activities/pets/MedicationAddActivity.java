package com.Celeritas.VetMed.Activities.pets;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.MedicationModelUpdated;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static com.Celeritas.VetMed.Activities.pets.MedicalHistoryAddActivity.isDateValid;
import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Constants.sdf;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class MedicationAddActivity extends BaseActivity implements IActionBar {

    private TextView medicationName;
    private TextView medicationForHow;
    private TextView medicationAnyOtherFreequency;
    private TextView medication_dosage_et;
    private Spinner medicationDosage;
    private Spinner medicationFrequencySpinner;
    private Spinner medicaionRouteSPinner;
    private Spinner medicationForHowSpinner;
    private TextView medicationStartedDate;
    private Spinner medicationActive;
    private Calendar myCalendar,myCalendarCurrent;
    private DatePickerDialog.OnDateSetListener date;
    private ArrayList<String> Active;
    private ArrayList<String> HowLong;
    private ArrayList<String> Route;
    private ArrayList<String> Frequency;
    private ArrayList<String> Dosage;
    private Bundle extras;
    private MedicationModelUpdated model;
    String medicationFrequency,medicationHowLong1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_medication_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_medication_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.medication));
        initialize();

    }

    private void initialize() {
        medicationName = findViewById(R.id.medicationName);
        medication_dosage_et = findViewById(R.id.medication_dosage_et);
        medicationAnyOtherFreequency = findViewById(R.id.medicationFrequency);
        medicationForHow = findViewById(R.id.medicationForHow);
        medicationDosage = findViewById(R.id.medicationDosage);
        medicationFrequencySpinner = findViewById(R.id.spinnerFrequency);
        medicaionRouteSPinner = findViewById(R.id.spinnerRoute);
        medicationForHowSpinner = findViewById(R.id.spinnerHowLong);
        medicationStartedDate = findViewById(R.id.datepicker);
        medicationActive = findViewById(R.id.spinnerActive);
        myCalendar = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();
        setMedicationFrequencySpinner();
        setMedicationRouteSpinner();
        setMedicationHowLongSpinner();
        setMedicationActiveSpinner();
        setMedicationDosage();
        datepickerClick();
        //RightClick.setVisibility(View.GONE);

        UpdatePetMedicationData();
        medicationName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(), new InputFilter.LengthFilter(32)});
        //medicationDosage.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
    }

    //set previous data for update
    private void UpdatePetMedicationData() {
        extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_MEDICATION_OBJECT), MedicationModelUpdated.class);
            if (model != null) {
                medicationName.setText(model.getMedicationName());
                // medicationDosage.setText(model.getMedicationDosage());
                medicationStartedDate.setText(model.getMedicationDateStarted());
                setingDataInSpinners(model);
                toolbarTitle.setText(getResources().getString(R.string.edit));
                ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
                if (model.getMedicationFrequency().contains(getResources().getString(R.string.any_other))) {
                    medicationAnyOtherFreequency.setText(model.getMedicationFrequency().split(",")[1]);
                }
                if (model.getMedicationForHowLong().contains(getResources().getString(R.string.any_other))) {
                    medicationForHow.setText(model.getMedicationForHowLong().split(",")[1]);
                }
                medication_dosage_et.setText(model.getMedicationDosage().split(",")[0]);
                for (int i = 0; i < Dosage.size(); i++) {
                    if (Dosage.get(i).contains(model.getMedicationDosage().split(",")[1])) {
                        medicationDosage.setSelection(i);
                    }
                }
            }
        }
    }

    private void setingDataInSpinners(MedicationModelUpdated model) {
        if (model.getMedicationFrequency().contains(getResources().getString(R.string.any_other))) {
            medicationFrequencySpinner.setSelection(8);
        } else {
            for (int i = 0; i < Frequency.size(); i++) {
                if (Frequency.get(i).contains(model.getMedicationFrequency())) {
                    medicationFrequencySpinner.setSelection(i);
                }
            }

        }
        if (model.getMedicationForHowLong().contains(getResources().getString(R.string.any_other))) {
            medicationForHowSpinner.setSelection(8);
        } else {
            for (int i = 0; i < HowLong.size(); i++) {
                if (HowLong.get(i).contains(model.getMedicationForHowLong())) {
                    medicationForHowSpinner.setSelection(i);
                }
            }
        }
        for (int i = 0; i < Route.size(); i++) {
            if (Route.get(i).contains(model.getMedicationRoute())) {
                medicaionRouteSPinner.setSelection(i);
            }
        }

        for (int i = 0; i < Active.size(); i++) {
            if (Active.get(i).contains(model.getMedicationActive())) {
                medicationActive.setSelection(i);
            }
        }
        for (int i = 0; i < Dosage.size(); i++) {
            if (Dosage.get(i).contains(model.getMedicationDosage())) {
                medicationDosage.setSelection(i);
            }
        }
    }

    private void setMedicationActiveSpinner() {
        Active = new ArrayList<>();
        Active.add(getResources().getString(R.string.select));
        Active.add(getResources().getString(R.string.yes));
        Active.add(getResources().getString(R.string.no));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, Active);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        medicationActive.setAdapter(dataAdapter);
        medicationActive.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        medicationActive.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard();
                return false;
            }
        });

    }

    private void setMedicationDosage() {
        Dosage = new ArrayList<>();
        Dosage.add(getResources().getString(R.string.select));
        Dosage.add(getResources().getString(R.string.mg));
        Dosage.add(getResources().getString(R.string.g));
        Dosage.add(getResources().getString(R.string.ml));
        Dosage.add(getString(R.string.units));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, Dosage);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        medicationDosage.setAdapter(dataAdapter);
        medicationDosage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        medicationDosage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard();
                return false;
            }
        });


    }

    private void setMedicationHowLongSpinner() {
        HowLong = new ArrayList<>();
        HowLong.add(getResources().getString(R.string.select));
        HowLong.add(getResources().getString(R.string.indefinitely));
        HowLong.add(getResources().getString(R.string.days_3));
        HowLong.add(getResources().getString(R.string.days_5));
        HowLong.add(getResources().getString(R.string.days_7));
        HowLong.add(getResources().getString(R.string.days_10));
        HowLong.add(getResources().getString(R.string.days_21));
        HowLong.add(getResources().getString(R.string.days_30));
        HowLong.add(getResources().getString(R.string.any_other));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, HowLong);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        medicationForHowSpinner.setAdapter(dataAdapter);
        medicationForHowSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (medicationForHowSpinner.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.any_other))) {
                    medicationForHow.setVisibility(View.VISIBLE);
                    medicationFrequency = medicationForHow.getText().toString();
                } else {
                    medicationForHow.setVisibility(View.GONE);
                    medicationHowLong1 = medicationForHowSpinner.getSelectedItem().toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        medicationForHowSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard();
                return false;
            }
        });
    }

    private void setMedicationRouteSpinner() {
        Route = new ArrayList<>();
        Route.add(getResources().getString(R.string.select));
        Route.add(getString(R.string.intra_muscular));
        Route.add(getResources().getString(R.string.subcutaneously));
        Route.add(getResources().getString(R.string.by_mouth));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, Route);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        medicaionRouteSPinner.setAdapter(dataAdapter);
        medicaionRouteSPinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        medicaionRouteSPinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard();
                return false;
            }
        });
    }

    private void setMedicationFrequencySpinner() {
        Frequency = new ArrayList<>();
        Frequency.add(getResources().getString(R.string.select));
        Frequency.add(getResources().getString(R.string.every_4H));
        Frequency.add(getResources().getString(R.string.every_6H));
        Frequency.add(getResources().getString(R.string.every_8H));
        Frequency.add(getResources().getString(R.string.every_12H));
        Frequency.add(getResources().getString(R.string.every_24H));
        Frequency.add(getResources().getString(R.string.every_48H));
        Frequency.add(getResources().getString(R.string.every_72H));
        Frequency.add(getResources().getString(R.string.any_other));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, Frequency);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        medicationFrequencySpinner.setAdapter(dataAdapter);
        medicationFrequencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (medicationFrequencySpinner.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.any_other))) {
                    medicationAnyOtherFreequency.setVisibility(View.VISIBLE);
                    medicationFrequency = medicationAnyOtherFreequency.getText().toString();
                } else {
                    medicationAnyOtherFreequency.setVisibility(View.GONE);
                    medicationFrequency = medicationFrequencySpinner.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        medicationFrequencySpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard();
                return false;
            }
        });
    }

    private boolean isEmpty() {
        if (medicationName.getText().length() > 0
                && !medicationDosage.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                && medicationStartedDate.getText().length() > 0
                && medication_dosage_et.getText().length() > 0
                && !medicationFrequencySpinner.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                && !medicaionRouteSPinner.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                && !medicationForHowSpinner.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                && !medicationActive.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))) {
            return false;
        }
        return true;
    }

    private void setSelectedDate() {
        medicationStartedDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void datepickerClick() {
        medicationStartedDate.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            if (date ==myCalendarCurrent.get(Calendar.DAY_OF_MONTH) && month == myCalendarCurrent.get(Calendar.MONTH) && year1 ==myCalendarCurrent.get(Calendar.YEAR)){
                myCalendarCurrent.add(Calendar.DATE,1);
            }
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (medicationStartedDate.getText().toString() != null && !medicationStartedDate.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = medicationStartedDate.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
    }

    public void Save(View view) {
        if (isEmpty()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }
        if (medicationFrequencySpinner.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.any_other)) && medicationAnyOtherFreequency.getText().toString().length() == 0) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }
        if (medicationFrequencySpinner.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.any_other))) {
            medicationFrequency = getString(R.string.any_other) + "," + medicationAnyOtherFreequency.getText().toString();
        }

        if (medicationForHowSpinner.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.any_other)) && medicationForHow.getText().toString().length() == 0) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }
        if (medicationForHowSpinner.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.any_other))) {
            medicationHowLong1 = getString(R.string.any_other) + "," + medicationForHow.getText().toString();
        }
        String medicationName1 = medicationName.getText().toString();
        String medicationDosage1 =medication_dosage_et.getText().toString()+","+ medicationDosage.getSelectedItem().toString();

        String medicationRoute = medicaionRouteSPinner.getSelectedItem().toString();
        String medicationStartedDate1 = medicationStartedDate.getText().toString();
        String medicationActive1 = medicationActive.getSelectedItem().toString();
        if (isDateValid(medicationStartedDate1)) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    "You Can't add future Date");
            return;
        }


        if (model == null) { // add new Medication
            String petID = extras.getString(NetworkUtils.PET_ID);
            MedicationModelUpdated medicationModelUpdated = new MedicationModelUpdated(petID, medicationName1, medicationDosage1, medicationFrequency,
                    medicationRoute, medicationHowLong1, medicationStartedDate1, medicationActive1, getCurrentDate());
            medicationModelUpdated.save();
            onBackPressed();


        } else {
            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    MedicationModelUpdated selectedPetMedication = MedicationModelUpdated.findById(MedicationModelUpdated.class, model.getId());
                    selectedPetMedication.setPetID(model.getPetID());
                    selectedPetMedication.setMedicationName(medicationName1);
                    selectedPetMedication.setMedicationForHowLong(medicationHowLong1);
                    selectedPetMedication.setMedicationDosage(medicationDosage1);
                    selectedPetMedication.setMedicationFrequency(medicationFrequency);
                    selectedPetMedication.setMedicationRoute(medicationRoute);
                    selectedPetMedication.setMedicationDateStarted(medicationStartedDate1);
                    selectedPetMedication.setMedicationActive(medicationActive1);
                    selectedPetMedication.setUpdateDateTime(getCurrentDate());
                    selectedPetMedication.save();
                    onBackPressed();

                }

                @Override
                public void onNoClick() {

                }
            });


        }
    }

    public void Cancel(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
    }

    public void hideOrShowKeyboard() {
        InputMethodManager imm = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) {
            Log.e("Key", "Software Keyboard was shown");
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } else {
            Log.e("Key", "Software Keyboard was not shown");
        }

    }
}
