package com.Celeritas.VetMed.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.ClinicalSigns;
import com.Celeritas.VetMed.Models.DiagnosticTestModel;
import com.Celeritas.VetMed.Models.DiseaseModel;
import com.Celeritas.VetMed.R;

import org.w3c.dom.Text;

public class DianosticDetailActivity extends BaseActivity implements IActionBar{

    private TextView discription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_dianostic_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_dianostic_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        initialiseViews();
    }

    private void initialiseViews() {
        discription = (TextView) findViewById(R.id.discription);
        if (getIntent().getSerializableExtra(Constants.DIAGNOSTIC_TEST_OBJECT) != null){
            DiagnosticTestModel diagnosticTestModel = (DiagnosticTestModel) getIntent().getSerializableExtra(Constants.DIAGNOSTIC_TEST_OBJECT);
            toolbarTitle.setText(diagnosticTestModel.getDiagnosticTestName());
            toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            discription.setText(diagnosticTestModel.getDiagnosticTestDescription());
        }else if (getIntent().getSerializableExtra(Constants.CLINICAL_SIGNS_OBJECT) != null){
            ClinicalSigns clinicalSigns = (ClinicalSigns) getIntent().getSerializableExtra(Constants.CLINICAL_SIGNS_OBJECT);
            toolbarTitle.setText(clinicalSigns.getClinicalSignName());
            toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            discription.setText(clinicalSigns.getClinicalSignDescription());
        }
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
