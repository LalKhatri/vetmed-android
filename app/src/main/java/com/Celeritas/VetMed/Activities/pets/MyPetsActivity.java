package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.MyPetAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.CustomFontsWidget.RecyclerItemTouchHelper;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.UtilityClass;

import java.util.ArrayList;
import java.util.List;

import static com.Celeritas.VetMed.Constants.PETS;

public class MyPetsActivity extends BaseActivity implements IActionBar, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{

    RecyclerView recyclerView;
    private RelativeLayout card_view;
    RelativeLayout relativeLayout;
    private MyPetAdapter adapter;
    Integer userId = null;
    public List<PetModelUpdated> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_my_pets);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_my_pets, content, true);
        //ButterKnife.bind(this,content);

            toolbarTitle.setText(getResources().getString(R.string.my_pets));
            RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.add));
            LeftBack.setVisibility(View.GONE);



       // checkUpdatedPets();
        initialize();
    }
    private void initialize() {
        card_view = (RelativeLayout) findViewById(R.id.card_view);
        recyclerView = (RecyclerView) findViewById(R.id.petRecyler);
        relativeLayout = findViewById(R.id.main);
        ((TextView) findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to)+" "+getString(R.string.add_pets));
        card_view.setOnClickListener(view ->{ addDetails();
        });
        updateData();

    }


    private void updateData() {
        list = new ArrayList<>();
        list = PetModelUpdated.findWithQuery(PetModelUpdated.class, "Select * from "+ PETS +" where user_id = ? ORDER BY pet_name", String.valueOf(SessionClass.getInstance().getUser(this).getID()));
        //list = PetModelUpdated.listAll(PetModelUpdated.class);
        populateList();
    }

    private void populateList() {
        if ((list.size() == 0)) {
            card_view.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);


        } else {
            recyclerView.setVisibility(View.VISIBLE);
            card_view.setVisibility(View.GONE);
        }
//        Toast.makeText(activity, "Pet: "+list.get(0).getPetName(), Toast.LENGTH_SHORT).show();
        adapter = new MyPetAdapter(list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        adapter.setOnItemClickListner((postion, view) -> {
            Intent intent = new Intent(MyPetsActivity.this,PetDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //intent.putExtra(Constants.PET_MODEL_OBJECT,petModelList.get(postion));
            //SessionClass.getInstance().save_updatePetModel(this,petModelList.get(postion));
            SessionClass.getInstance().saveUpdatePetModelUpdated(this,list.get(postion));
            startActivity(intent);
        });
    }

    @Override
    protected void onRestart() {
        updateData();
        super.onRestart();
    }

    @Override
    public void MenuClick() {
        drawer.openDrawer(GravityCompat.START);
    }
    @Override
    public void LeftBackClick() {}

    @Override
    public void RightClick() {
   addDetails();
    }

    private void addDetails() {
        Intent intent = new Intent(MyPetsActivity.this,GeneralInformationAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof MyPetAdapter.MyViewHolder) {
            // get the removed item name to display it in snack bar
//            String name = petModelList.get(viewHolder.getAdapterPosition()).getPetName();

            // backup of removed item for undo purpose
            /*final PetModel deletedItem = petModelList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();*/

            // remove the item from recycler view
            //adapter.removeItem(viewHolder.getAdapterPosition());
            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.veterinarian_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {


                   // delete_Pet(NetworkUtils.DELETE_PETS,petModelList.get(position).getID(),viewHolder);


                    PetModelUpdated  selectedPet =  list.get(viewHolder.getAdapterPosition());
                    PetModelUpdated note = PetModelUpdated.findById(PetModelUpdated.class, selectedPet.getId());
                    note.delete();
                    list.remove(viewHolder.getAdapterPosition());
                    adapter.notifyDataSetChanged();
                    if ((list.size() == 0)) {
                        card_view.setVisibility(View.VISIBLE);
                    } else {
                        card_view.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNoClick() {
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });


            // showing snack bar with Undo option
           /* Snackbar snackbar = Snackbar
                    .make(relativeLayout, name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    adapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();*/
        }
    }



}
