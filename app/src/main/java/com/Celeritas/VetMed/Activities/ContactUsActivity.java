package com.Celeritas.VetMed.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Activities.pets.InsuranceInformationAddActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.UtilityClass;

import static com.Celeritas.VetMed.Constants.CLIENT_EMAIL;
import static com.Celeritas.VetMed.Constants.TECHNICAL_EMAIL;

public class ContactUsActivity extends BaseActivity implements IActionBar {
    private TextView userEmail,firstName,lastName,subject,comments;
    private AppCompatCheckBox isTechnical;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_disease_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.contact_us, content, true);
        initWidgets();
        LeftBack.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.contact_us));

    }

    private void initWidgets() {
        userEmail  = findViewById(R.id.email_et);
        firstName  = findViewById(R.id.first_name);
        lastName  = findViewById(R.id.last_name);
        subject  = findViewById(R.id.subject);
        comments  = findViewById(R.id.comments);
        isTechnical  = findViewById(R.id.checkbox);
        findViewById(R.id.send_btn).setOnClickListener(view -> {
            sendEmail();
        });
        findViewById(R.id.cancel).setOnClickListener(view -> {
            drawer.openDrawer(GravityCompat.START);
        });
    }

    private void sendEmail() {
        if (isEmpty()){
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }

        if(!UtilityClass.isValidEmail(userEmail.getText().toString())){
            UtilityClass.showPopup(ContactUsActivity.this,getResources().getString(R.string.alert), getResources().getString(R.string.invalid_email));
            return;
        }
        if (isTechnical.isChecked()){
            contactUs(TECHNICAL_EMAIL,userEmail.getText().toString(),subject.getText().toString(),firstName.getText().toString()+"  "+lastName.getText().toString(),comments.getText().toString(),this);
        }else {
            contactUs(CLIENT_EMAIL,userEmail.getText().toString(),subject.getText().toString(),firstName.getText().toString()+"  "+lastName.getText().toString(),comments.getText().toString(),this);
        }
    }

    @Override
    public void MenuClick() { drawer.openDrawer(GravityCompat.START); }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {

    }
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }
    private boolean isEmpty() {
        if (firstName.getText().length() > 0 && lastName.getText().length() > 0 &&
                subject.getText().length() > 0 && userEmail.getText().length() > 0) {
            return false;
        }
        return true;
    }
}

