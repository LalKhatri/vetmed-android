package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.JournalAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Models.pets.JournalModel;
import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;

import java.util.Collections;
import java.util.List;

import static com.Celeritas.VetMed.Constants.JOURNAL;

public class JournalActivity extends BaseActivity implements IActionBar {

    private Bundle extras;
    private RelativeLayout card_view;

    private RecyclerView mRecycler;
    private String petID;
    private List<JournalModel> journalModelList;
    JournalAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_diet);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_weight_tracker, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.add));
        toolbarTitle.setText(getResources().getString(R.string.pet_journal));
        ((TextView) findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to)+" "+toolbarTitle.getText().toString().toLowerCase());
        initialize();
    }

    private void initialize() {
        card_view = (RelativeLayout) findViewById(R.id.no_data_found_layout);

        mRecycler = (RecyclerView) findViewById(R.id.diet);

        extras = getIntent().getExtras();
        setSwipeCallback();
        if (extras != null) {
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            //populateList();
            journalList();
            //checkUpdatedDiet(petID);
        }


        card_view.setOnClickListener(v -> {
            addDetails();
        });
    }

    private void journalList() {
        journalModelList = JournalModel.findWithQuery(JournalModel.class, "Select * from " + JOURNAL + " where petID = ? ", petID);
        Collections.sort(journalModelList, new JournalModel());
        if ((journalModelList.size() == 0)) {
            card_view.setVisibility(View.VISIBLE);

        } else {
            card_view.setVisibility(View.GONE);

        }
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        mRecycler.setLayoutManager(staggeredGridLayoutManager);
        adapter = new JournalAdapter(journalModelList, this, R.layout.journal_row);
        mRecycler.setAdapter(adapter);

    }


    @Override
    public void MenuClick() {

    }

    @Override
    protected void onRestart() {
        journalList();
        super.onRestart();
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        addDetails();
    }

    private void addDetails() {
        Intent intent = new Intent(this,JournalAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(NetworkUtils.PET_ID, petID);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //SwipeToDelete Callback
    public void setSwipeCallback() {


        final SwipeToDeleteCallBack callBack = new SwipeToDeleteCallBack(this) {

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction) {
                if (direction == ItemTouchHelper.LEFT) {

                    UtilityClass.showDeletPopup(JournalActivity.this, getResources().getString(R.string.alert), getResources().getString(R.string.journal_confirmation), new IAlertListner() {
                        @Override
                        public void onYesClick() {
                            JournalModel selectedPet = journalModelList.get(viewHolder.getAdapterPosition());
                            JournalModel note = JournalModel.findById(JournalModel.class, selectedPet.getId());
                            note.delete();
                            journalModelList.remove(viewHolder.getAdapterPosition());
                            adapter.notifyDataSetChanged();
                            journalList();

                        }

                        @Override
                        public void onNoClick() {
                            mRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            journalList();
                        }
                    });

                }
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                if (viewHolder != null) {
                    if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);
                    } else {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);

                    }
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                viewHolder.itemView.setBackgroundColor(Color.WHITE);


            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(callBack);
        helper.attachToRecyclerView(mRecycler);


    }
}
