package com.Celeritas.VetMed.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.Celeritas.VetMed.Adapters.Pdf.DietAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.InsuranceAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.MedicalHistoryAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.MedicationAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.PdfListAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.UpcomingAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.VaccineAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.VeternrainAdapter;
import com.Celeritas.VetMed.BuildConfig;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.AppointmentModelUpdated;
import com.Celeritas.VetMed.Models.pets.DewormingModelUpdated;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.InsuranceModelUpdated;
import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.Models.pets.MedicationModelUpdated;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.Models.pets.VaccineModelUpdated;
import com.Celeritas.VetMed.Models.pets.VeterinarianModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.Celeritas.VetMed.Activities.ShareActivity.loadBitmapFromView;
import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE;
import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.PERMISSIONS_STORAGE;
import static com.Celeritas.VetMed.Activities.pets.PetDetailActivity.getAge;

public class PdfActivity extends AppCompatActivity {
    private LinearLayout llScroll;
    private Bitmap bitmap;
    private RecyclerView medicalHistory, medicationRv, dietRv, preventiveRv, veterinarianRv, upcomingRv, insuranceRv;
    private List<MedicalHistoryModelUpdated> petMedicalHistoryList = new ArrayList<>();
    private List<MedicationModelUpdated> medicationList = new ArrayList<>();
    private List<DietModelUpdated> dietList = new ArrayList<>();
    private List<VeterinarianModelUpdated> veterinarianList = new ArrayList<>();
    private List<InsuranceModelUpdated> insuranceList = new ArrayList<>();
    private List<VaccineModelUpdated> vaccineList = new ArrayList<>();
    private List<DewormingModelUpdated> dewormingList = new ArrayList<>();
    private List<AppointmentModelUpdated> appointmentList = new ArrayList<>();    // upcoming events list
    private String petId;
    private PetModelUpdated model;
    private TextView petName, petSex, petBreed, petDob, howOld, weight;
    private PdfListAdapter pdfListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pdf);
        initializeListAndThereValues();
        /*
        Generating file and send via email in android
        https://www.codeproject.com/Articles/989236/How-to-Convert-Android-View-to-PDF-2
        */

    }

    private void initializeListAndThereValues() {


        llScroll = findViewById(R.id.llScroll);

        petName = findViewById(R.id.pet_name_tv);
        petSex = findViewById(R.id.pet_sex_tv);
        petBreed = findViewById(R.id.pet_breed_tv);
        petDob = findViewById(R.id.dob_tv);
        howOld = findViewById(R.id.how_old_tv);
        weight = findViewById(R.id.pet_weight_tv);


        medicalHistory = findViewById(R.id.medical_history_rv);
        medicationRv = findViewById(R.id.medication_rv);
        dietRv = findViewById(R.id.diet_rv);
        preventiveRv = findViewById(R.id.preventive_rv);
        veterinarianRv = findViewById(R.id.veterinarian_rv);
        upcomingRv = findViewById(R.id.upcoming_rv);
        insuranceRv = findViewById(R.id.insurance_rv);
        dietRv = findViewById(R.id.diet_rv);
        petId = getIntent().getStringExtra(NetworkUtils.PET_ID);
        model = SessionClass.getInstance().getPetModelUpdated(this);
        loadDataInList();
        setPetDetails(model);

        // bitmap = loadBitmapFromView(llScroll, llScroll.getWidth(), llScroll.getHeight());
        //takeScreenShot();
        //createPdf();
        //createPdfss();
    }

    private String petAge(String petDOB) {
        String finalAge = "";
        String dob = model.getPetDOB();
        String array1[] = dob.split("/");
        int date = Integer.valueOf(array1[1]);
        int month = Integer.valueOf(array1[0]);
        int year = Integer.valueOf("20" + array1[2]);
        String age = getAge(year, month, date);
        String array2[] = age.split(":");
        String days = array2[0];
        String months = array2[1];
        String years = array2[2];
        if (years.equalsIgnoreCase("0") && months.equalsIgnoreCase("0")) {
            finalAge = days + " days";
        } else if (years.equalsIgnoreCase("0") && !months.equalsIgnoreCase("0")) {
            finalAge = months + "mo, " + days + " days";
        } else {
            finalAge = years + "yr, " + months + " mo";
        }
        return finalAge;
    }

    private void setPetDetails(PetModelUpdated model) {
        petName.setText(getResources().getString(R.string.pet_name) +" "+model.getPetName());
        petSex.setText(getResources().getString(R.string.pet_sex) +" "+model.getPetSex());
        petBreed.setText(getResources().getString(R.string.pet_breed) +" "+model.getPetBreed());
        petDob.setText(getResources().getString(R.string.pet_date_of_birth) +" "+model.getPetDOB());
        howOld.setText(getResources().getString(R.string.how_old) + petAge(model.getPetDOB()));
        weight.setText(getResources().getString(R.string.pet_weight) +" "+model.getPetWeight() + " " +" "+model.getPetWeightUnit());
        ((TextView) findViewById(R.id.name_tv)).setText(model.getPetName());
    }

    private void loadDataInList() {
        petMedicalHistoryList = /*MedicalHistoryModelUpdated.listAll(MedicalHistoryModelUpdated.class);// */MedicalHistoryModelUpdated.findWithQuery(MedicalHistoryModelUpdated.class, "Select * from MEDICAL_HISTORY_MODEL_UPDATED where petID = ? ", petId);
        medicationList = MedicationModelUpdated.findWithQuery(MedicationModelUpdated.class, "Select * from MEDICATION_MODEL_UPDATED where petID = ? ", petId);
        dietList = DietModelUpdated.findWithQuery(DietModelUpdated.class, "Select * from DIET_MODEL_UPDATED where petID = ? ", petId);
        vaccineList = VaccineModelUpdated.findWithQuery(VaccineModelUpdated.class, "Select * from VACCINE_MODEL_UPDATED where petID = ? ", petId);
        dewormingList = DewormingModelUpdated.findWithQuery(DewormingModelUpdated.class, "Select * from DEWORMING_MODEL_UPDATED where petID = ? ", petId);
        insuranceList = InsuranceModelUpdated.findWithQuery(InsuranceModelUpdated.class, "Select * from INSURANCE_MODEL_UPDATED where petID = ? ", petId);
        appointmentList = AppointmentModelUpdated.findWithQuery(AppointmentModelUpdated.class, "Select * from APPOINTMENT_MODEL_UPDATED where petID = ? ", petId);
        veterinarianList = VeterinarianModelUpdated.findWithQuery(VeterinarianModelUpdated.class, "Select * from VETERINARIAN_MODEL_UPDATED where petID = ? ", petId);
        setMedicalHistory(petMedicalHistoryList, medicalHistory, this);
        setMedication(medicationList, medicationRv, this);
        setVaccineList(vaccineList, preventiveRv, this);
        setVeterinarianList(veterinarianList, veterinarianRv, this);
        setUpcomingEventsList(appointmentList, upcomingRv, this);
        setInsuranceList(insuranceList, insuranceRv, this);
        setDietList(dietList, dietRv, this);
    }


    private void openGeneratedPDF() {
        File file = new File("/sdcard/VetMed/VetMed_PET_1.pdf");
        if (file.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(PdfActivity.this, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }

    /*private void createPdf() {
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        //  Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels;
        float width = displaymetrics.widthPixels;
        int convertHighet = (int) hight, convertWidth = (int) width;
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, 2).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);


        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        document.finishPage(page);

        // write the document content/sdcard/VetMed/.VetMed_PET_
        String targetPdf = "/sdcard/VetMed/VetMed_PET_1.pdf";
        File filePath;
        filePath = new File(targetPdf);
        try {
            document.writeTo(new FileOutputStream(filePath));

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();
        Toast.makeText(this, "PDF of Scroll is created!!!", Toast.LENGTH_SHORT).show();

        openGeneratedPDF();

    }*/



    private void setMedicalHistory(final List<MedicalHistoryModelUpdated> medicalHistoryList, RecyclerView recyclerView, final Context context) {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        MedicalHistoryAdapter surveyListAdapter = new MedicalHistoryAdapter(medicalHistoryList, context);
        recyclerView.setAdapter(surveyListAdapter);
    }

    private void setMedication(final List<MedicationModelUpdated> medicationList, RecyclerView recyclerView, final Context context) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        MedicationAdapter surveyListAdapter = new MedicationAdapter(medicationList, context);
        recyclerView.setAdapter(surveyListAdapter);
    }

    private void setDietList(List<DietModelUpdated> dietList, RecyclerView dietRv, Context context) {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        dietRv.setLayoutManager(staggeredGridLayoutManager);
        DietAdapter insuranceAdapter = new DietAdapter(dietList, context);
        dietRv.setAdapter(insuranceAdapter);
    }

    private void setVaccineList(final List<VaccineModelUpdated> vaccineList, RecyclerView recyclerView, final Context context) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        VaccineAdapter vaccineAdapter = new VaccineAdapter(vaccineList, context);
        recyclerView.setAdapter(vaccineAdapter);
    }

    private void setVeterinarianList(final List<VeterinarianModelUpdated> veternarianList, RecyclerView recyclerView, final Context context) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        VeternrainAdapter veternrainAdapter = new VeternrainAdapter(veternarianList, context);
        recyclerView.setAdapter(veternrainAdapter);
    }

    private void setUpcomingEventsList(final List<AppointmentModelUpdated> veternarianList, RecyclerView recyclerView, final Context context) { // appointments details.

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        UpcomingAdapter appointmentsAdapter = new UpcomingAdapter(veternarianList, context);
        recyclerView.setAdapter(appointmentsAdapter);
    }

    private void setInsuranceList(final List<InsuranceModelUpdated> veternarianList, RecyclerView recyclerView, final Context context) { // appointments details.

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        InsuranceAdapter insuranceAdapter = new InsuranceAdapter(veternarianList, context);
        recyclerView.setAdapter(insuranceAdapter);
    }

    private void takeScreenShot() {
        {
            View u = ((Activity) this).findViewById(R.id.scrollView);
            u.setDrawingCacheEnabled(true);
            u.buildDrawingCache();
            ScrollView z = (ScrollView) ((Activity) this).findViewById(R.id.scrollView);
            int totalHeight = z.getChildAt(0).getHeight();
            int totalWidth = z.getChildAt(0).getWidth();

            Bitmap b = getBitmapFromView(u, totalHeight, totalWidth);

            //Save bitmap
            String extr = Environment.getExternalStorageDirectory() + "/VetMed/";
            String fileName = "Report.jpg";
            File myPath = new File(extr, fileName);
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(myPath);
                b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
                MediaStore.Images.Media.insertImage(this.getContentResolver(), b, "Screen", "screen");
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    public Bitmap getBitmapFromView(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    public void createPdfs() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        RelativeLayout root = (RelativeLayout) inflater.inflate
                (R.layout.activity_pdf, null); //RelativeLayout is root view of my UI(xml) file.
        root.setDrawingCacheEnabled(true);
        // Bitmap screen= getBitmapFromView(this.getWindow().findViewById(R.id.pdf_layout_ll));
    }

    private void createPdfss() {
        readWritePermision();
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels;
        float width = displaymetrics.widthPixels;
        PdfDocument document = null;
        PdfDocument.PageInfo pageInfo = null;
        PdfDocument.Page page = null;
        Canvas canvas = null;
        bitmap = loadBitmapFromView(llScroll, llScroll.getWidth(), llScroll.getHeight());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            document = new PdfDocument();
            pageInfo = new PdfDocument.PageInfo.Builder(bitmap.getWidth(), bitmap.getHeight(), 1).create();
            page = document.startPage(pageInfo);
            canvas = page.getCanvas();
        }

        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#ffffff"));
        canvas.drawPaint(paint);


        bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            document.finishPage(page);
        }


        // write the document content
        String targetPdf = "/sdcard/VetMed/VetMed_PET_1.pdf";
        File filePath = new File(targetPdf);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                document.writeTo(new FileOutputStream(filePath));
            }



            Toast.makeText(this, "Share ", Toast.LENGTH_LONG).show();
            //boolean_save=true;
        } catch (IOException e) {
            e.printStackTrace();
            readWritePermision();
            //Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

        // close the document
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            document.close();
        }
        shareDocument();

        /*Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"a@mailinator.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback for App " + getAppVersion(getContext()));
        intent.putExtra(Intent.EXTRA_TEXT, "\n\n\n\n\nApp Version:" + getAppVersion(getContext()) + "\nAndroid Version:" + getAndroidVersion() + "\nDevice:" + getDeviceName());
        if (screenshot != null) {
            intent.putExtra(Intent.EXTRA_STREAM, bitmap);        }
        startActivity(Intent.createChooser(intent, "Send feedback..."));*/
    }

    private void shareDocument() {
        String filename = "VetMed_PET_1.pdf";

        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/VetMed/", filename);
        Uri uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", filelocation);
        //Uri path = Uri.fromFile(filelocation);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
// set the type to 'email'
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {"a@mailinator.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
// the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
// the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public void readWritePermision() {
        if (ActivityCompat.checkSelfPermission(PdfActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(PdfActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(PdfActivity.this, PERMISSIONS_STORAGE, MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE);
        }
    }

    /*public void alertDialogue(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialoge);



        selectAllCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadList(allSelectedList(selectAllCheckbox.isChecked()), recyclerView, context);
            }
        });


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "Share ", Toast.LENGTH_SHORT).show();
                // bitmap = loadBitmapFromView(llScroll, llScroll.getWidth(), llScroll.getHeight());
                //  createPdfss();

                for (PdfDetails pdfDetails : pdfListAdapter.selectedQuesList()) {
                    if (pdfDetails.isSelected()) {
                        Log.e("Selected Files -> : ", pdfDetails.getName());
                        Log.d("Selected Files -> : ", pdfDetails.getName());
                        Log.i("Selected Files -> : ", pdfDetails.getName());
                        Toast.makeText(context, "Selected List", Toast.LENGTH_SHORT).show();

                    }

                }
                //dialog.dismiss();


            }
        });
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();


            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }*/




}
