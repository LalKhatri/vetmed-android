package com.Celeritas.VetMed.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.Celeritas.VetMed.Utility.WebserviceClient;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;

public class ForgotPassword extends Activity {

    @BindView(R.id.forgot_email)
    EditText forgot_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    public void backClick(View view) {
        finish();
    }

    private boolean isEmpty() {
        if (forgot_email.getText().length() > 0) {
            return false;
        }

        return true;
    }

    @OnClick(R.id.reset_btn)
    public void ResetClick(){
        if(isEmpty()){
            UtilityClass.showPopup(ForgotPassword.this,getResources().getString(R.string.alert), getResources().getString(R.string.email_required));
            return;
        }

        if (!UtilityClass.isValidEmail(forgot_email.getText().toString())){
            UtilityClass.showPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.invalid_email));
            return;
        }

        sendUserDataOnServer(NetworkUtils.FORGOT_PASSWORD,forgot_email.getText().toString());

    }

    private void sendUserDataOnServer(String ServiceName, String userEmail) {
        WebserviceClient client = new WebserviceClient(this, ServiceName, true, activateUserAccount(userEmail), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("Response: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    String responseStatus = jsonObject.getString("status");
                    if (responseStatus.equalsIgnoreCase("1")) {
                        UtilityClass.showPopup(ForgotPassword.this, getResources().getString(R.string.alert),
                                getResources().getString(R.string.reset_link),() -> {
                                    Intent intent = new Intent(ForgotPassword.this, SignInActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                });
                    } else {
                        UtilityClass.showPopup(ForgotPassword.this, getResources().getString(R.string.alert),
                                jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {

            }
        }, false);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private List<NameValuePair> activateUserAccount(String userEmail) {
        try {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(NetworkUtils.USER_EMAIL, userEmail));
            return pairs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
