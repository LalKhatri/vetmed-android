package com.Celeritas.VetMed.Activities.pets;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.TabsAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.Fragments.PastAppointmentFragment;
import com.Celeritas.VetMed.Fragments.UpcomingAppointmentFragment;
import com.Celeritas.VetMed.Models.AppointmentModel;
import com.Celeritas.VetMed.Models.pets.AppointmentModelUpdated;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.Celeritas.VetMed.Utility.WebserviceClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.Celeritas.VetMed.Constants.APPOINTMENTS;


public class AppointmentActivity extends BaseActivity implements IActionBar {
    private Bundle extras;
    private String petID=null;
    private List<AppointmentModelUpdated> petAppointmentModelList;
    private TabLayout tabLayout;
    private ViewPager tabs_mViewPager;
    private TabsAdapter adapter;
    IAppointmentResponseListner listner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_appointment);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_appointment, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.add));
        toolbarTitle.setText(getResources().getString(R.string.appointment));

        initialize();
    }
    private void initialize() {
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabs_mViewPager = (ViewPager) findViewById(R.id.pager);

        extras = getIntent().getExtras();
        if (extras != null){
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            loadData(petID);
            setupViewPager(tabs_mViewPager);
        }

    }
    private void setupViewPager(ViewPager viewPager) {
        adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragment(UpcomingAppointmentFragment.newInstance(), getResources().getString(R.string.upcoming));
        adapter.addFragment(PastAppointmentFragment.newInstance(), getResources().getString(R.string.past));
        viewPager.setAdapter(adapter);
        //add view pager to tab layout
        tabLayout.setupWithViewPager(tabs_mViewPager);
        tabs_mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int pageNumber) {
                TabsAdapter adapter = ((TabsAdapter)tabs_mViewPager.getAdapter());
                Fragment fragment = adapter.getItem(pageNumber);
                if (fragment instanceof PastAppointmentFragment){
                    listner = (IAppointmentResponseListner) fragment;
                    listner.onResponse(petAppointmentModelList);

                }else {
                    listner = (IAppointmentResponseListner) fragment;
                    listner.onResponse(petAppointmentModelList);
                }
                loadData(petID);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

    }
    private Fragment getVisibleFragment() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }
    private void loadData(String petID) {
        petAppointmentModelList = AppointmentModelUpdated.findWithQuery(AppointmentModelUpdated.class, "Select * from "+ APPOINTMENTS +" where petID = ? ", petID);
        if (getVisibleFragment() != null){
            this.listner = (IAppointmentResponseListner) getVisibleFragment();
            listner.onResponse(petAppointmentModelList);
        }
    }
    @Override
    public void MenuClick() {

    }
    @Override
    public void LeftBackClick() {
        onBackPressed();
    }
    @Override
    public void RightClick() {
    addDetails();
    }

    public void addDetails() {
        Intent intent = new Intent(this,AppointmentAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(NetworkUtils.PET_ID, petID);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    public interface IAppointmentResponseListner{
        void onResponse(List<AppointmentModelUpdated> appointmentModels);
        //void addDetails();
    }
    @Override
    protected void onRestart() {
        if(petID !=null){
            loadData(petID);
            tabLayout.getTabAt(0).select();
        }

        super.onRestart();
    }
}
