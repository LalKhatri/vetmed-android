package com.Celeritas.VetMed.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.Celeritas.VetMed.Adapters.JournalAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.DewormingAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.DietAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.InsuranceAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.MedicalHistoryAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.MedicationAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.NotesAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.PdfListAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.UpcomingAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.VaccineAdapter;
import com.Celeritas.VetMed.Adapters.Pdf.VeternrainAdapter;
import com.Celeritas.VetMed.Adapters.WeightTrackerAdapter;
import com.Celeritas.VetMed.BuildConfig;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.NotesModel;
import com.Celeritas.VetMed.Models.PdfDetails;
import com.Celeritas.VetMed.Models.pets.AppointmentModelUpdated;
import com.Celeritas.VetMed.Models.pets.DewormingModelUpdated;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.InsuranceModelUpdated;
import com.Celeritas.VetMed.Models.pets.JournalModel;
import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.Models.pets.MedicationModelUpdated;
import com.Celeritas.VetMed.Models.pets.NotesModelUpdated;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.Models.pets.VaccineModelUpdated;
import com.Celeritas.VetMed.Models.pets.VeterinarianModelUpdated;
import com.Celeritas.VetMed.Models.pets.WeightTrackerModel;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;

import com.Celeritas.VetMed.Utility.Style;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.util.ArrayList;


import java.util.Arrays;
import java.util.List;


import static com.Celeritas.VetMed.Activities.ShareActivity.TableBuilder.fontWhite;
import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE;
import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.PERMISSIONS_STORAGE;
import static com.Celeritas.VetMed.Activities.pets.PetDetailActivity.getAge;
import static com.Celeritas.VetMed.Constants.APPOINTMENTS;
import static com.Celeritas.VetMed.Constants.DEWORMING;
import static com.Celeritas.VetMed.Constants.DIET;
import static com.Celeritas.VetMed.Constants.JOURNAL;
import static com.Celeritas.VetMed.Constants.NOTES;
import static com.Celeritas.VetMed.Constants.PET_INSURANCE_UPDATED;
import static com.Celeritas.VetMed.Constants.MED_HISTORY;
import static com.Celeritas.VetMed.Constants.MEDICATIONS;
import static com.Celeritas.VetMed.Constants.VACCINE;
import static com.Celeritas.VetMed.Constants.PET_VETERINARIAN_UPDATED;
import static com.Celeritas.VetMed.Constants.WEIGHT_TRACKER;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class ShareActivity extends BaseActivity implements IActionBar {

    private RecyclerView recyclerView;
    private CheckBox selectAllCheckbox;
    private ScrollView pdf_layout;
    private RelativeLayout shareLayout;
    private PdfListAdapter pdfListAdapter;
    private LinearLayout llScroll;

    private static String FILE = ""+Environment.getExternalStorageDirectory().getAbsolutePath() + "/.VetMed/"+"PET_INFORMATION.pdf";
    private static String HEADER = ""+Environment.getExternalStorageDirectory().getAbsolutePath() + "header";
    private static String FOOTER = ""+Environment.getExternalStorageDirectory().getAbsolutePath() + "footer";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private RecyclerView medicalHistory, medicationRv, dietRv, preventiveRv,dewormingRv, veterinarianRv, upcomingRv, insuranceRv,journalRv,notes_rv,weightTrackerRv;
    private List<MedicalHistoryModelUpdated> petMedicalHistoryList = new ArrayList<>();
    private List<MedicationModelUpdated> medicationList = new ArrayList<>();
    private List<DietModelUpdated> dietList = new ArrayList<>();
    private List<JournalModel> journalList = new ArrayList<>();
    private List<WeightTrackerModel> weightTrackerList = new ArrayList<>();
    private List<VeterinarianModelUpdated> veterinarianList = new ArrayList<>();
    private List<InsuranceModelUpdated> insuranceList = new ArrayList<>();
    private List<VaccineModelUpdated> vaccineList = new ArrayList<>();
    private List<DewormingModelUpdated> dewormingList = new ArrayList<>();
    private List<NotesModelUpdated> notesList = new ArrayList<>();
    private List<AppointmentModelUpdated> appointmentList = new ArrayList<>();    // upcoming events list
    public String petId;
    public PetModelUpdated model;
    private TextView petName, petSex, petBreed, petDob, howOld, weight,petType;
    private int selectedItemSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_share, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.share));
        initialize();
    }

    private void initialize() {
        // share layout data start
        recyclerView = findViewById(R.id.recyclerView);
        selectAllCheckbox = findViewById(R.id.select_all_cb);
        shareLayout = findViewById(R.id.share_layout);
        pdf_layout = findViewById(R.id.pdf_layout);
        RightClick.setClickable(false);
        findViewById(R.id.layout_footer).setVisibility(View.GONE);
        selectAllCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadCheckList(allSelectedList(selectAllCheckbox.isChecked()), recyclerView, ShareActivity.this);
            }
        });
        // share layout and  data end
        // pdf layout and data Start
        llScroll = findViewById(R.id.llScroll);
        petName = findViewById(R.id.pet_name_tv);
        petType = findViewById(R.id.pet_type);
        petSex = findViewById(R.id.pet_sex_tv);
        petBreed = findViewById(R.id.pet_breed_tv);
        petDob = findViewById(R.id.dob_tv);
        howOld = findViewById(R.id.how_old_tv);
        weight = findViewById(R.id.pet_weight_tv);
        medicalHistory = findViewById(R.id.medical_history_rv);
        medicationRv = findViewById(R.id.medication_rv);
        dietRv = findViewById(R.id.diet_rv);
        preventiveRv = findViewById(R.id.preventive_rv);
        dewormingRv = findViewById(R.id.deworming_rv);
        notes_rv = findViewById(R.id.notes_rv);
        veterinarianRv = findViewById(R.id.veterinarian_rv);
        upcomingRv = findViewById(R.id.upcoming_rv);
        insuranceRv = findViewById(R.id.insurance_rv);
        journalRv = findViewById(R.id.journal_rv);
        weightTrackerRv = findViewById(R.id.weight_tracker_rv);
        dietRv = findViewById(R.id.diet_rv);
        petId = getIntent().getStringExtra(NetworkUtils.PET_ID);
        loadCheckList(allSelectedList(false), recyclerView, this);
        // pdf layout and data end
    }

    public void Share(View view) {
        selectedItemSize=0;
        if (selectedOptionCount(pdfListAdapter.selectedQuesList()) > 0) {
            shareLayout.setVisibility(View.GONE);
            pdf_layout.setVisibility(View.VISIBLE);
            RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_share));
            RightClick.setClickable(true);
            findViewById(R.id.layout_footer).setVisibility(View.VISIBLE);
            loadSelectedDataForPdf(pdfListAdapter.selectedQuesList());
        } else {
            Toast.makeText(activity, "Please Select At-least One Field", Toast.LENGTH_SHORT).show();
        }

    }
// this method is for validation of selection of data for pdf
    private int selectedOptionCount(List<PdfDetails> list) {
        for (PdfDetails pdfDetails : list) {
            if (pdfDetails.isSelected()) {
                selectedItemSize++;
            }
        }
        return selectedItemSize;
    }

    public void Cancel(View view) {
        onBackPressed();
    }

    @Override
    public void MenuClick() {
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        createA4SizePdf();
    }

    private void loadCheckList(List<PdfDetails> list, RecyclerView recyclerView, final Context context) {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        pdfListAdapter = new PdfListAdapter(list, context);
        recyclerView.setAdapter(pdfListAdapter);
    }

    private List<PdfDetails> allSelectedList(boolean selectAllOrNot) {
        List<PdfDetails> list = new ArrayList<>();
        list.add(new PdfDetails(getResources().getString(R.string.general_information), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.medical_history), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.medication), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.diet), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.preventive_medicine), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.veterinarian_information), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.appointment), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.insurance_information), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.weight_tracker), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.pet_journal), selectAllOrNot));
        list.add(new PdfDetails(getResources().getString(R.string.notes), selectAllOrNot));
        return list;
    }
    private void setPetDetails(PetModelUpdated model) {
        if (model!=null){
            findViewById(R.id.general_info_layout).setVisibility(View.VISIBLE);
            petName.setText(getResources().getString(R.string.pet_name) +" "+model.getPetName());
            petSex.setText(getResources().getString(R.string.pet_sex) +" "+model.getPetSex());
            petBreed.setText(getResources().getString(R.string.pet_breed) +" "+model.getPetBreed());
            petDob.setText(getResources().getString(R.string.pet_date_of_birth) +" "+model.getPetDOB());
            howOld.setText(getResources().getString(R.string.how_old) + petAge(model.getPetDOB()));
            petType.setText(getResources().getString(R.string.pet_type)+": "+model.getPetType());
            weight.setText(getResources().getString(R.string.pet_weight) +" "+model.getPetWeight() + " " +" "+model.getPetWeightUnit());
            ((TextView) findViewById(R.id.name_tv)).setText(model.getPetName());
            //((TextView) findViewById(R.id.name_tv)).setText(model.getPetName()+" "+SessionClass.getInstance().getUser(this).getUserName());
        }else {
            findViewById(R.id.general_info_layout).setVisibility(View.GONE);
        }

    }
    public  static String petAge(String petDOB) {
        String finalAge = "";
        String dob = petDOB;
        String array1[] = dob.split(" ");
        int date = Integer.valueOf(array1[0]);
        int month = Arrays.asList(months).indexOf(array1[1]);
        int year = Integer.valueOf(array1[2]);
        String age = getAge(year, month, date);
        String array2[] = age.split(":");
        String days = array2[0];
        String months = array2[1];
        String years = array2[2];
        if (years.equalsIgnoreCase("0") && months.equalsIgnoreCase("0")) {
            finalAge = days + " days";
        } else if (years.equalsIgnoreCase("0") && !months.equalsIgnoreCase("0")) {
            finalAge = months + "mo, " + days + " days";
        } else {
            finalAge = years + "yr, " + months + " mo";
        }
        return finalAge;
    }
    private void setMedicalHistory(final List<MedicalHistoryModelUpdated> medicalHistoryList, RecyclerView recyclerView, final Context context) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        MedicalHistoryAdapter surveyListAdapter = new MedicalHistoryAdapter(medicalHistoryList, context);
        recyclerView.setAdapter(surveyListAdapter);
    }
    private void setMedication(final List<MedicationModelUpdated> medicationList, RecyclerView recyclerView, final Context context) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        MedicationAdapter surveyListAdapter = new MedicationAdapter(medicationList, context);
        recyclerView.setAdapter(surveyListAdapter);
    }
    private void setDietList(List<DietModelUpdated> dietList, RecyclerView dietRv, Context context) {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        dietRv.setLayoutManager(staggeredGridLayoutManager);
        DietAdapter insuranceAdapter = new DietAdapter(dietList, context);
        dietRv.setAdapter(insuranceAdapter);
    }
    private void setVaccineList(final List<VaccineModelUpdated> vaccineList, RecyclerView recyclerView, final Context context) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        VaccineAdapter vaccineAdapter = new VaccineAdapter(vaccineList, context);
        recyclerView.setAdapter(vaccineAdapter);
    }
    private void setDewromingList(final List<DewormingModelUpdated> dewormingList, RecyclerView recyclerView, final Context context) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        DewormingAdapter vaccineAdapter = new DewormingAdapter(dewormingList, context);
        recyclerView.setAdapter(vaccineAdapter);
    }
    private void setVeterinarianList(final List<VeterinarianModelUpdated> veternarianList, RecyclerView recyclerView, final Context context) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        VeternrainAdapter veternrainAdapter = new VeternrainAdapter(veternarianList, context);
        recyclerView.setAdapter(veternrainAdapter);
    }
    private void setUpcomingEventsList(final List<AppointmentModelUpdated> veternarianList, RecyclerView recyclerView, final Context context) { // appointments details.

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        UpcomingAdapter appointmentsAdapter = new UpcomingAdapter(veternarianList, context);
        recyclerView.setAdapter(appointmentsAdapter);
    }
    private void setInsuranceList(final List<InsuranceModelUpdated> veternarianList, RecyclerView recyclerView, final Context context) { // appointments details.

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        InsuranceAdapter insuranceAdapter = new InsuranceAdapter(veternarianList, context);
        recyclerView.setAdapter(insuranceAdapter);
    }
    private void setJournalList(final List<JournalModel> journalModelList, RecyclerView recyclerView, final Context context) { // appointments details.

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        JournalAdapter journalAdapter = new JournalAdapter(journalModelList, context,R.layout.journal_item);
        recyclerView.setAdapter(journalAdapter);
    }
    private void setWeightTrackerList(final List<WeightTrackerModel> journalModelList, RecyclerView recyclerView, final Context context) { // appointments details.

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        WeightTrackerAdapter journalAdapter = new WeightTrackerAdapter(journalModelList, context,R.layout.weight_tracker_item);
        recyclerView.setAdapter(journalAdapter);
    }
    private void setNotesList(final List<NotesModelUpdated> notesList, RecyclerView recyclerView, final Context context) { // appointments details.

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        NotesAdapter notesAdapter = new NotesAdapter(notesList, context,R.layout.notes_item);
        recyclerView.setAdapter(notesAdapter);
    }
    private void createA4SizePdf() {
        readWritePermision();
        try {
            readWritePermision();
            takeScreenShoot(findViewById(R.id.pdf_header),"header");
            takeScreenShoot(findViewById(R.id.layout_footer),"footer");
            Document document = new Document();
            document.setMargins(10, 10, 75, 40);
            document.setMarginMirroring(false);
            Rectangle pageSize = new Rectangle(PageSize.A4);
            pageSize.setBackgroundColor(new BaseColor(244,244,244));
            //Document document = new Document( pageSize );
            document.setPageSize(pageSize);

            File file = new File (FILE.replace("PET_INFORMATION.pdf",""));
            if (!file.exists()) {
                File wallpaperDirectory = new File(FILE.replace("PET_INFORMATION.pdf",""));
                wallpaperDirectory.mkdirs();
            }
            if(file.exists()){ }
                else{
                file.createNewFile();
            }
            // step 2
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(FILE));
            // step 3

            document.open();
            writer.setPageEvent(new MyFooter());
            writer.setPageEmpty(false);
            // step 4 create PDF contents
            // general info data
            if (model !=null){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.general_information)));
                document.add(TableBuilder.createGeneralInfoTable(model,this));
                createSpaceBetweenTables(document);
            }

//  medical history data
            if (petMedicalHistoryList !=null && petMedicalHistoryList.size()>0 ){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.medical_history)));
                document.add(TableBuilder.createMedicalHistoryTable(petMedicalHistoryList,this));
                createSpaceBetweenTables(document);
            }

//  medication data
            if (medicationList !=null && medicationList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.medication)));
                document.add(TableBuilder.createMedicationTable(medicationList,this));
                createSpaceBetweenTables(document);
            }

//  diet data
            if (dietList !=null && dietList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.diet)));
                document.add(TableBuilder.createDietTable(dietList,this));
                createSpaceBetweenTables(document);
            }
//  preventive Vaccine data
            if (vaccineList !=null && vaccineList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.preventive_medicine)+" ("+getString(R.string.vaccines)+") "));
                document.add(TableBuilder.createPreventiveVaccineTable(vaccineList,this));
                createSpaceBetweenTables(document);
            }
            //  preventive Deworming  data
            if (dewormingList !=null && dewormingList.size()>0){

                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.preventive_medicine)+" ("+getString(R.string.deworming)+") "));
                document.add(TableBuilder.createPreventiveDewormingTable(dewormingList,this));
                createSpaceBetweenTables(document);
            }


//  veterinarian data
            if (veterinarianList !=null && veterinarianList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.veterinarian_information)));
                document.add(TableBuilder.createVeterinarianTable(veterinarianList,this));
                createSpaceBetweenTables(document);
            }

//  upcoming events data
            if (appointmentList !=null && appointmentList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.upcoming_appointment)));
                document.add(TableBuilder.createUpcomingTable(appointmentList,this));
                createSpaceBetweenTables(document);
            }
//  insurance events data
            if (insuranceList !=null && insuranceList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.insurance_information)));
                document.add(TableBuilder.createInsuranceTable(insuranceList,this));
                createSpaceBetweenTables(document);
            }
            if (weightTrackerList !=null && weightTrackerList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.weight_tracker)));
                document.add(TableBuilder.createWeightTrackerTable(weightTrackerList,this));
                createSpaceBetweenTables(document);
            }
            if (journalList !=null && journalList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.pet_journal)));
                document.add(TableBuilder.createPetJournalTable(journalList,this));
                createSpaceBetweenTables(document);
            }
            if (notesList !=null && notesList.size()>0){
                document.add(TableBuilder.createTableHeader(getResources().getString(R.string.notes)));
                document.add(TableBuilder.createNotesTable(notesList,this));
            }


            document.close();
            System.out.println( "PDF Created!" );
        } catch (Exception e) {
            e.printStackTrace();
        }
        shareDocument();
    }

    private void createSpaceBetweenTables(Document document) throws DocumentException {
        Chunk whiteText = new Chunk("       ", fontWhite);

        Paragraph p = new Paragraph();
        p.add(whiteText);
        for (int i = 0; i < 2; i++) {document.add(p);
        }
    }

    private void shareDocument() {
        String filename = "PET_INFORMATION.pdf";

        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.VetMed/", filename);
        Uri uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", filelocation);
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.VetMed/"+filename;
        File pdf = new File(path);
        if (pdf.exists()) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, "Vet Med Pet Details pdf\n please find the attachment");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setType("application/pdf");
            startActivity(intent);
        } else {
            Log.i("DEBUG", "File doesn't exist");
        }

    }
    public void readWritePermision() {
        if (ActivityCompat.checkSelfPermission(ShareActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ShareActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ShareActivity.this, PERMISSIONS_STORAGE, MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE);
        }
    }
    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }
    @Override
    public void onBackPressed() {
        if (pdf_layout.getVisibility() == View.VISIBLE) {
            pdf_layout.setVisibility(View.GONE);
            shareLayout.setVisibility(View.VISIBLE);
            RightClick.setImageDrawable(null);
            findViewById(R.id.layout_footer).setVisibility(View.GONE);
            RightClick.setClickable(false);
        } else {
            super.onBackPressed();
        }

    }
    public void loadSelectedDataForPdf(List<PdfDetails> list){
        model = null;
        petMedicalHistoryList = new ArrayList<>();
        medicationList = new ArrayList<>();
        dietList = new ArrayList<>();
        vaccineList = new ArrayList<>();
        dewormingList = new ArrayList<>();
        insuranceList = new ArrayList<>();
        veterinarianList = new ArrayList<>();
        appointmentList = new ArrayList<>();
        notesList = new ArrayList<>();
        for (PdfDetails pdfDetails:list){
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.general_information))){
                    model = PetModelUpdated.findById(PetModelUpdated.class, Long.valueOf(petId));
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.medical_history))){
                petMedicalHistoryList = MedicalHistoryModelUpdated.findWithQuery(MedicalHistoryModelUpdated.class, "Select * from "+ MED_HISTORY + " where petID = ? ", petId);
            }

            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.medication))){
                medicationList = MedicationModelUpdated.findWithQuery(MedicationModelUpdated.class, "Select * from "+ MEDICATIONS +" where petID = ? ", petId);
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.diet))){
                dietList = DietModelUpdated.findWithQuery(DietModelUpdated.class, "Select * from "+ DIET +" where petID = ? ", petId);
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.preventive_medicine))){
                vaccineList = VaccineModelUpdated.findWithQuery(VaccineModelUpdated.class, "Select * from "+ VACCINE +" where petID = ? ", petId);
                dewormingList = DewormingModelUpdated.findWithQuery(DewormingModelUpdated.class, "Select * from "+ DEWORMING +" where petID = ? ", petId);
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.insurance_information))){
                insuranceList = InsuranceModelUpdated.findWithQuery(InsuranceModelUpdated.class, "Select * from "+PET_INSURANCE_UPDATED+" where petID = ? ", petId);
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.veterinarian_information))){
                veterinarianList = VeterinarianModelUpdated.findWithQuery(VeterinarianModelUpdated.class, "Select * from "+PET_VETERINARIAN_UPDATED+" where petID = ? ", petId);
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.appointment))){
                appointmentList = AppointmentModelUpdated.findWithQuery(AppointmentModelUpdated.class, "Select * from "+ APPOINTMENTS +" where petID = ? ", petId);
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.pet_journal))){
                journalList = JournalModel.findWithQuery(JournalModel.class, "Select * from "+ JOURNAL +" where petID = ? ", petId);
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.weight_tracker))){
                weightTrackerList = WeightTrackerModel.findWithQuery(WeightTrackerModel.class, "Select * from "+ WEIGHT_TRACKER +" where petID = ? ", petId);
            }
            if (pdfDetails.isSelected() && pdfDetails.getName().equalsIgnoreCase(getResources().getString(R.string.notes))){
                notesList = NotesModelUpdated.findWithQuery(NotesModelUpdated.class, "Select * from "+ NOTES +" where petID = ? ", petId);
            }
        }
        setMedicalHistory(petMedicalHistoryList, medicalHistory, this);
        setMedication(medicationList, medicationRv, this);
        setVaccineList(vaccineList, preventiveRv, this);
        setDewromingList(dewormingList, dewormingRv, this);
        setVeterinarianList(veterinarianList, veterinarianRv, this);
        setUpcomingEventsList(appointmentList, upcomingRv, this);
        setInsuranceList(insuranceList, insuranceRv, this);
        setDietList(dietList, dietRv, this);
        setPetDetails(model);
        setJournalList(journalList,journalRv,this);
        setWeightTrackerList(weightTrackerList,weightTrackerRv,this);
        setNotesList(notesList,notes_rv,this);
        if (model !=null){
            findViewById(R.id.general_info_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.general_info_layout).setVisibility(View.GONE);
        }
        if (petMedicalHistoryList !=null && petMedicalHistoryList.size()>0){
            findViewById(R.id.medical_history_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.medical_history_layout).setVisibility(View.GONE);
        }
        if (medicationList !=null && medicationList.size()>0){
            findViewById(R.id.medication_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.medication_layout).setVisibility(View.GONE);
        }
        if (vaccineList !=null && vaccineList.size()>0){
            findViewById(R.id.preventive_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.preventive_layout).setVisibility(View.GONE);
        }
        if (dewormingList !=null && vaccineList.size()>0){
            findViewById(R.id.preventive_deworming_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.preventive_deworming_layout).setVisibility(View.GONE);
        }
        if (veterinarianList !=null && veterinarianList.size()>0){
            findViewById(R.id.veterinarian_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.veterinarian_layout).setVisibility(View.GONE);
        }
        if (appointmentList !=null && appointmentList.size()>0){
            findViewById(R.id.upcoming_events_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.upcoming_events_layout).setVisibility(View.GONE);
        }
        if (insuranceList !=null && insuranceList.size()>0){
            findViewById(R.id.inssurance_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.inssurance_layout).setVisibility(View.GONE);
        }
        if (dietList !=null && dietList.size()>0){
            findViewById(R.id.diet_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.diet_layout).setVisibility(View.GONE);
        }
        if (journalList !=null && journalList.size()>0){
            findViewById(R.id.journal_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.journal_layout).setVisibility(View.GONE);
        }
        if (weightTrackerList !=null && weightTrackerList.size()>0){
            findViewById(R.id.weight_tracker_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.weight_tracker_layout).setVisibility(View.GONE);
        }
        if (notesList !=null && notesList.size()>0){
            findViewById(R.id.notes_layout).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.notes_layout).setVisibility(View.GONE);
        }


    }
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    public static class TableBuilder {

        public static Font font = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.DARK_GRAY);
        public static Font fontWhite = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE);
        // create table
        public static PdfPTable createGeneralInfoTable(PetModelUpdated model, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            table.addCell(createLabelCell(context.getResources().getString(R.string.pet_name)+" "+model.getPetName()));
            table.addCell(createLabelCell(context.getResources().getString(R.string.pet_sex)+model.getPetSex()));
            table.addCell(createLabelCell(context.getResources().getString(R.string.pet_breed)+" "+model.getPetBreed()));
            table.addCell(createLabelCell(context.getResources().getString(R.string.pet_date_of_birth)+" "+model.getPetDOB()));
            table.addCell(createLabelCell(context.getResources().getString(R.string.how_old)+" "+ petAge(model.getPetDOB())));
            table.addCell(createLabelCell(context.getResources().getString(R.string.pet_weight)+" "+model.getPetWeight() + " " +" "+model.getPetWeightUnit()));
            PdfPCell petType = new PdfPCell(new Phrase(context.getResources().getString(R.string.pet_type)+": "+model.getPetType(),font));
            petType.setColspan(2);
            petType.setRowspan(2);
            petType.setPaddingLeft(12f);
            petType.setPaddingTop(12f);
            petType.setPaddingBottom(12f);
            petType.setPaddingRight(12f);
            petType.setBorderColor(BaseColor.LIGHT_GRAY);
            petType.setBackgroundColor(BaseColor.WHITE);
            table.addCell(petType);
            table.setWidthPercentage(97f);

            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createMedicalHistoryTable(List<MedicalHistoryModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansbold.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 12, Font.NORMAL, BaseColor.DARK_GRAY);
            for (MedicalHistoryModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.medical_date_of_diagnosis)+" "+model.getDiseaseDate()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.medi_disease_name)+" "+model.getDiseaseName()));
                PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.disease_note)+" "+model.getDiseaseName(),font));
                noteCell.setColspan(2);
                noteCell.setRowspan(2);
                noteCell.setPaddingLeft(12f);
                noteCell.setPaddingTop(12f);
                noteCell.setPaddingBottom(12f);
                noteCell.setPaddingRight(12f);
                noteCell.setBorderColor(BaseColor.LIGHT_GRAY);
                noteCell.setBackgroundColor(BaseColor.WHITE);
                table.addCell(noteCell);

                //table.addCell(createLabelCell(""));
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createPetJournalTable(List<JournalModel> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansbold.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 12, Font.NORMAL, BaseColor.DARK_GRAY);
            String mode = "";
            Image img1 = null;
            for (JournalModel model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.medical_date)+" "+model.getDate()));
                if (model.getPetMode().equalsIgnoreCase(context.getResources().getString(R.string.happy))) {
                    mode = ":-)";
                    try {
                        img1 = Image.getInstance(getImageBytes("happy.png",context));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (model.getPetMode().equalsIgnoreCase(context.getResources().getString(R.string.sick))) {
                    mode = ":-|";
                    try {
                        img1 = Image.getInstance(getImageBytes("sick.png",context));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (model.getPetMode().equalsIgnoreCase(context.getResources().getString(R.string.confuse))) {
                    mode = ":-(";
                    try {
                        img1 = Image.getInstance(getImageBytes("confuse.png",context));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                //table.addCell(createLabelCell(context.getResources().getString(R.string.pet_mode)+"\t"+mode));
                PdfPCell imageCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.pet_mode),font));
                imageCell.setPaddingLeft(12f);
                imageCell.setPaddingTop(12f);
                imageCell.setPaddingBottom(12f);
                imageCell.setPaddingRight(12f);
                imageCell.setBorderColor(BaseColor.LIGHT_GRAY);
                imageCell.setBackgroundColor(BaseColor.WHITE);
                img1.scaleAbsolute(24,24);



                imageCell.addElement(img1);

                table.addCell(imageCell);
                PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.disease_note)+" "+model.getNotes(),font));
                noteCell.setColspan(2);
                noteCell.setRowspan(2);
                noteCell.setPaddingLeft(12f);
                noteCell.setPaddingTop(12f);
                noteCell.setPaddingBottom(12f);
                noteCell.setPaddingRight(12f);
                noteCell.setBorderColor(BaseColor.LIGHT_GRAY);
                noteCell.setBackgroundColor(BaseColor.WHITE);
                table.addCell(noteCell);

                //table.addCell(createLabelCell(""));
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createNotesTable(List<NotesModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansbold.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 12, Font.NORMAL, BaseColor.DARK_GRAY);
            String mode = "";
            Image img1 = null;
            for (NotesModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.note_title)+" "+model.getNoteTitle()));
                table.addCell(createLabelCell(""));
                PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.disease_note)+" "+model.getNoteText(),font));
                noteCell.setColspan(2);
                noteCell.setRowspan(2);
                noteCell.setPaddingLeft(12f);
                noteCell.setPaddingTop(12f);
                noteCell.setPaddingBottom(12f);
                noteCell.setPaddingRight(12f);
                noteCell.setBorderColor(BaseColor.LIGHT_GRAY);
                noteCell.setBackgroundColor(BaseColor.WHITE);
                table.addCell(noteCell);

                //table.addCell(createLabelCell(""));
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        private static byte[] getImageBytes(String s,Context context) {
            InputStream ims = null;
            try {
                ims = context.getAssets().open(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

            return stream.toByteArray();
        }
        public static PdfPTable createWeightTrackerTable(List<WeightTrackerModel> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansregular.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            for (WeightTrackerModel model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.medical_date)+" "+model.getDate()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.weights)+" "+model.getPetWeight()));
                //table.addCell(createLabelCell(""));
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createMedicationTable(List<MedicationModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            for (MedicationModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.started)+" "+model.getMedicationDateStarted()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.medi_name)+" "+model.getMedicationName()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.dosage)+" "+model.getMedicationDosage().replace(","," ")));

                table.addCell(createLabelCell(context.getResources().getString(R.string.route)+" "+model.getMedicationRoute()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.how_long)+" "+model.getMedicationForHowLong()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.medi_how_often)+" "+model.getMedicationFrequency()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.med_active)+" "+model.getMedicationActive()));
                table.addCell(createLabelCell(""));
              /*  PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.med_active)+model.getMedicationActive()));
                noteCell.setColspan(2);
                noteCell.setRowspan(1);
                table.addCell(noteCell);*/
                //set footer
            }

            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createDietTable(List<DietModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansregular.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 12, Font.NORMAL, BaseColor.DARK_GRAY);
            for (DietModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.medi_name)+" "+model.getDietName()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.type)+" "+model.getDietType()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.how_much)+" "+model.getDietHowMuch()));
                if (model.getDietHowOften().contains(context.getResources().getString(R.string.any_other))){
                    table.addCell(createLabelCell(context.getResources().getString(R.string.medi_how_often)+" "+model.getDietHowOften().split(",")[1]));
                }else {
                    table.addCell(createLabelCell(context.getResources().getString(R.string.medi_how_often)+" "+model.getDietHowOften()));
                }
                table.addCell(createLabelCell(context.getResources().getString(R.string.diet_date_started)+" "+model.getDietDateStarted()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.date_end)+" "+model.getDietEndDate()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.med_active)+" "+model.getDietActive()));
                table.addCell(createLabelCell(""));
                PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.disease_note)+" "+model.getDietNotes(),font));
                noteCell.setColspan(2);
                noteCell.setRowspan(2);
                noteCell.setPaddingLeft(12f);
                noteCell.setPaddingTop(12f);
                noteCell.setPaddingBottom(12f);
                noteCell.setPaddingRight(12f);
                noteCell.setBorderColor(BaseColor.LIGHT_GRAY);
                noteCell.setBackgroundColor(BaseColor.WHITE);
                table.addCell(noteCell);
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createPreventiveVaccineTable(List<VaccineModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansregular.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 10, Font.NORMAL, BaseColor.DARK_GRAY);
            for (VaccineModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.vaccine_name)+model.getVaccineName()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.date_given)+" "+model.getVaccineDateGiven()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.vi_side_effects)+" "+model.getVaccineSideEffects()));
                table.addCell(createLabelCell(""));
                PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.disease_note)+" "+model.getVaccineNotes(),font));
                noteCell.setColspan(2);
                noteCell.setRowspan(2);
                noteCell.setPaddingLeft(12f);
                noteCell.setPaddingTop(12f);
                noteCell.setPaddingBottom(12f);
                noteCell.setPaddingRight(12f);
                noteCell.setBorderColor(BaseColor.LIGHT_GRAY);
                noteCell.setBackgroundColor(BaseColor.WHITE);
                table.addCell(noteCell);
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createPreventiveDewormingTable(List<DewormingModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansregular.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 10, Font.NORMAL, BaseColor.DARK_GRAY);
            for (DewormingModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.product_name)+" "+model.getDFTProductName()));
                if (model.getDFTHowOften().contains(context.getResources().getString(R.string.any_other))){
                    table.addCell(createLabelCell(context.getResources().getString(R.string.medi_how_often)+" "+model.getDFTHowOften().split(",")[1]));
                }else {
                    table.addCell(createLabelCell(context.getResources().getString(R.string.medi_how_often)+model.getDFTHowOften()));
                }
                table.addCell(createLabelCell(context.getResources().getString(R.string.diet_date_started)+" "+model.getDFTDateStarted()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.date_end)+" "+model.getDFTEndDate()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.med_active)+" "+model.getDFTActive()));
                table.addCell(createLabelCell(""));
                PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.disease_note)+" "+model.getDFTNotes(),font));
                noteCell.setColspan(2);
                noteCell.setRowspan(2);
                noteCell.setPaddingLeft(12f);
                noteCell.setPaddingTop(12f);
                noteCell.setPaddingBottom(12f);
                noteCell.setPaddingRight(12f);
                noteCell.setBorderColor(BaseColor.LIGHT_GRAY);
                noteCell.setBackgroundColor(BaseColor.WHITE);
                table.addCell(noteCell);
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createVeterinarianTable(List<VeterinarianModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansbold.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 12, Font.NORMAL, BaseColor.DARK_GRAY);
            for (VeterinarianModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.hospital_name)+" "+model.getVIHospital()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.dr_name)+" "+model.getVIDoctorName()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.vet_address)+" "+model.getVIAddress()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.vet_phone)+" "+model.getVIPhoneNumber()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.vet_email)+" "+model.getVIEmail()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.vet_website)+" "+model.getVIWebsite()));
                PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.disease_note)+" "+model.getVINotes(),font));
                noteCell.setColspan(2);
                noteCell.setRowspan(2);
                noteCell.setPaddingLeft(12f);
                noteCell.setPaddingTop(12f);
                noteCell.setPaddingBottom(12f);
                noteCell.setPaddingRight(12f);
                noteCell.setBorderColor(BaseColor.LIGHT_GRAY);
                noteCell.setBackgroundColor(BaseColor.WHITE);
                table.addCell(noteCell);

            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createUpcomingTable(List<AppointmentModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansbold.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 10, Font.NORMAL, BaseColor.DARK_GRAY);
            for (AppointmentModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.medical_date)+" "+model.getAppointmentDate()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.time_12h_or_24h)+" "+model.getAppointmentTime()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.location)+" "+model.getAppointmentLocation()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.dr_name)+" "+model.getAppointmentDoctorName()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.add_to_calendar)+" "+model.getAddToCalendar()));
                table.addCell(createLabelCell(""));
                PdfPCell noteCell = new PdfPCell(new Phrase(context.getResources().getString(R.string.disease_note)+" "+model.getAppointmentNotes(),font));
                noteCell.setColspan(2);
                noteCell.setRowspan(2);
                noteCell.setPaddingLeft(12f);
                noteCell.setPaddingTop(12f);
                noteCell.setPaddingBottom(12f);
                noteCell.setPaddingRight(12f);
                noteCell.setBorderColor(BaseColor.LIGHT_GRAY);
                noteCell.setBackgroundColor(BaseColor.WHITE);

                table.addCell(noteCell);
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createInsuranceTable(List<InsuranceModelUpdated> list, Context context) throws DocumentException {
            PdfPTable table = new PdfPTable(2);
            for (InsuranceModelUpdated model:list){
                table.addCell(createLabelCell(context.getResources().getString(R.string.company_name)+" "+model.getInsuranceInfoName()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.insurance_policy)+" "+model.getInsuranceInfoPolicyNumber()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.vet_email)+" "+model.getInsuranceInfoEmail()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.vet_website)+" "+model.getInsuranceInfoWebsite()));
                table.addCell(createLabelCell(context.getResources().getString(R.string.vet_phone)+" "+model.getInsuranceInfoPhoneNumber()));
                table.addCell(createLabelCell(""));
            }
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        public static PdfPTable createTableHeader(String header) throws DocumentException {
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansregular.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 14, Font.NORMAL, BaseColor.WHITE);
            PdfPTable table = new PdfPTable(1);
            PdfPCell cell = new PdfPCell(new Phrase(header,font));

            Style.headerCellStyle(cell);
            table.addCell(cell);
            table.setWidthPercentage(97f);
            table.setSplitLate(false);
            return table;
        }
        private static PdfPCell createLabelCell(String text){
            // font
            BaseFont urName = null;
            try {
                urName = BaseFont.createFont("assets/opensansbold.ttf", "UTF-8",BaseFont.EMBEDDED);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font font = new Font(urName, 10, Font.NORMAL, BaseColor.DARK_GRAY);

            // create cell
            PdfPCell cell = new PdfPCell(new Phrase(text,font));
            cell.setBorderColor(BaseColor.LIGHT_GRAY);
            cell.setBackgroundColor(BaseColor.WHITE);

            // set style
            Style.labelCellStyle(cell);
            return cell;
        }
    }
    class MyFooter extends PdfPageEventHelper {
        private  String HEADER = "/sdcard/."+ "header.jpg";
        private  String FOOTER = "/sdcard/."+ "footer.jpg";

        public void onEndPage(PdfWriter writer, Document document) {
            //android:src="@drawable/vet_med_logo"
            Image header = null;
            Image footer = null;
            try {
                 header = Image.getInstance(HEADER);

                header.setAbsolutePosition(0, document.getPageSize().getHeight() - 55);
                header.scaleAbsolute(document.getPageSize().getWidth(), 55);

                writer.getDirectContent().addImage(header);
                footer = Image.getInstance(FOOTER);
                footer.setAbsolutePosition(0, 0);
                footer.scaleAbsolute(595, 40);
                writer.getDirectContent().addImage(footer);

            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    public Bitmap takeScreenShoot(View v, String fileName) {
        Bitmap bitmap;
        v.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        saveBitmap(bitmap,fileName);
        return bitmap;
    }
    public void saveBitmap(Bitmap bitmap,String fileName) {
        File imagePath = new File("/sdcard/."+fileName+".jpg");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            //Toast.makeText(getApplicationContext(), imagePath.getAbsolutePath() + "", Toast.LENGTH_SHORT).show();
           // boolean_save = true;

            //btn_screenshot.setText("Check image");

            Log.e("ImageSave", "Saveimage");
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }

}
