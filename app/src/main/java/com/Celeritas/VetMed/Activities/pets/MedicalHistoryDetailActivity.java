package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

public class MedicalHistoryDetailActivity extends BaseActivity implements IActionBar {

    private TextView noteText, medicalHistoryDate;
    private MedicalHistoryModelUpdated model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_notes_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_medical_history_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));
        toolbarTitle.setText(getResources().getString(R.string.notes));

        initialize();
    }

    private void initialize() {
        noteText = findViewById(R.id.notes);
        medicalHistoryDate = findViewById(R.id.medical_history_date);
        Bundle extras = getIntent().getExtras();
        model = new Gson().fromJson(extras.getString(Constants.PET_MEDICAL_HISTORY_OBJECT),MedicalHistoryModelUpdated.class);
        updateDetails(model);
    }

    private void updateDetails(MedicalHistoryModelUpdated model) {
        if (model != null){
            toolbarTitle.setText(model.getDiseaseName());
            toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            noteText.setText(model.getDiseaseNotes());
            medicalHistoryDate.setText(model.getDiseaseDate());
        }
    }

    @Override
    public void MenuClick() {

    }
    @Override
    public void RightClick() {
        Intent intent = new Intent(MedicalHistoryDetailActivity.this,MedicalHistoryAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PET_MEDICAL_HISTORY_OBJECT,new Gson().toJson(model));
        startActivity(intent);
    }
    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    protected void onRestart() {
        model = MedicalHistoryModelUpdated.findById(MedicalHistoryModelUpdated.class,model.getId());
        updateDetails(model);
        super.onRestart();
    }
}
