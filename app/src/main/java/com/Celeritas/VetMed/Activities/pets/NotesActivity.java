package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.NotesModel;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.InsuranceModelUpdated;
import com.Celeritas.VetMed.Models.pets.NotesModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.Celeritas.VetMed.Utility.WebserviceClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.Celeritas.VetMed.Constants.NOTES;

public class NotesActivity extends BaseActivity implements IActionBar {

    private RelativeLayout card_view;

    private RecyclerView mRecycler;
    private Bundle extras;
    private String petID;
    private BaseAdapter adapter;
    private List<NotesModelUpdated> petModelListNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_notes);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_notes, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.add));
        toolbarTitle.setText(getResources().getString(R.string.notes));
        ((TextView) findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to)+" "+toolbarTitle.getText().toString().toLowerCase());
        initialize();
    }

    private void initialize() {
        card_view = (RelativeLayout) findViewById(R.id.no_data_found_layout);

        mRecycler = (RecyclerView) findViewById(R.id.mReyclerView);
        extras = getIntent().getExtras();

        card_view.setOnClickListener(v -> {
            addDetails();
        });


        if (extras != null) {
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            populateList(petID);

        }
    }

    private void checkUpdatedNotes(Integer petID) {
        WebserviceClient client = new WebserviceClient(this, NetworkUtils.GET_NOTES, true, getPetNotesData(petID), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("Notes: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    Integer responseStatus = jsonObject.getInt("status");
                    if (responseStatus == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        parseJSON(jsonArray.toString());
                        Log.d("Pet Notes", jsonArray.toString());
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {

            }
        }, false);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private List<NameValuePair> getPetNotesData(Integer petID) {
        try {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(NetworkUtils.PET_ID, String.valueOf(petID)));
            return pairs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void parseJSON(String jsonString) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<NotesModel>>() {
        }.getType();
        petModelListNotes = gson.fromJson(jsonString, type);
        // populateList();
    }

    private void populateList(String petID) {
        petModelListNotes = NotesModelUpdated.findWithQuery(NotesModelUpdated.class, "Select * from " + NOTES + " where petID = ? ", petID);
        if ((petModelListNotes.size() == 0)) {
            card_view.setVisibility(View.VISIBLE);


        } else {
            card_view.setVisibility(View.GONE);

        }
        List<Item> mItemList = new ArrayList<>();
        for (NotesModelUpdated notesModel : petModelListNotes) {
            Item noteDetail = new Item();
            noteDetail.setTitle(notesModel.getNoteTitle());
            noteDetail.setDescription(notesModel.getNoteText());
            noteDetail.setViewType(Constants.APPOINTMENT);
            mItemList.add(noteDetail);
        }
        adapter = new BaseAdapter(mItemList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setAdapter(adapter);
        setSwipeCallback();
        adapter.setOnItemClickListner((postion, view) -> {
            Intent intent = new Intent(this, NotesDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.PET_NOTES_OBJECT, new Gson().toJson(petModelListNotes.get(postion)));
            startActivity(intent);
        });
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        addDetails();
    }

    private void addDetails() {
        Intent intent = new Intent(this, NotesAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(NetworkUtils.PET_ID, petID);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        populateList(petID);
        super.onRestart();
    }

    //SwipeToDelete Callback
    public void setSwipeCallback() {


        final SwipeToDeleteCallBack callBack = new SwipeToDeleteCallBack(this) {

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction) {
                if (direction == ItemTouchHelper.LEFT) {

                    UtilityClass.showDeletPopup(NotesActivity.this, getResources().getString(R.string.alert), getResources().getString(R.string.notes_confirmation), new IAlertListner() {
                        @Override
                        public void onYesClick() {
                            NotesModelUpdated selectedPet = petModelListNotes.get(viewHolder.getAdapterPosition());
                            NotesModelUpdated note = NotesModelUpdated.findById(NotesModelUpdated.class, selectedPet.getId());
                            note.delete();
                            petModelListNotes.remove(viewHolder.getAdapterPosition());
                            adapter.notifyDataSetChanged();
                            populateList(petID);

                        }

                        @Override
                        public void onNoClick() {
                            mRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    });

                }
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                if (viewHolder != null) {
                    if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);
                    } else {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);

                    }
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                viewHolder.itemView.setBackgroundColor(Color.WHITE);


            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(callBack);
        helper.attachToRecyclerView(mRecycler);


    }
}
