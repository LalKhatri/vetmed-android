package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.Models.pets.MedicationModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;

import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.List;

import static com.Celeritas.VetMed.Constants.MEDICATIONS;


public class MedicationActivity extends BaseActivity implements IActionBar {

    private RelativeLayout card_view;

    private RecyclerView mRecycler;
    private Bundle extras;
    private String petID;
    private List<MedicationModelUpdated> petMedicationModelList;
    BaseAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_medication);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_medication, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.add));
        toolbarTitle.setText(getResources().getString(R.string.medication));
        ((TextView) findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to)+" "+toolbarTitle.getText().toString().toLowerCase());
        initialize();
    }

    private void initialize() {
        card_view = (RelativeLayout) findViewById(R.id.no_data_found_layout);

        mRecycler = (RecyclerView) findViewById(R.id.medication);
        extras = getIntent().getExtras();
        card_view.setOnClickListener(v -> {
            addDetails();
        });

        petMedicationModelList = new ArrayList<>();
        if (extras != null) {
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            populateList(petID);
            //checkUpdatedMedication(petID);
        }
    }

    private void populateList(String petID) {
        petMedicationModelList = MedicationModelUpdated.findWithQuery(MedicationModelUpdated.class, "Select * from " + MEDICATIONS + " where petID = ? ", petID);
        if ((petMedicationModelList.size() == 0)) {
            card_view.setVisibility(View.VISIBLE);

        } else {
            card_view.setVisibility(View.GONE);


        }
        List<Item> mItemList = new ArrayList<>();
        for (MedicationModelUpdated medicationModel : petMedicationModelList) {
            Item nameDetail = new Item();
            nameDetail.setTitle(medicationModel.getMedicationName());
            nameDetail.setViewType(Constants.PET_MAIN_DETAIL);
            mItemList.add(nameDetail);
        }
        adapter = new BaseAdapter(mItemList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setAdapter(adapter);
        setSwipeCallback();
        adapter.setOnItemClickListner((postion, view) -> {
            Intent intent = new Intent(this, MedicationDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.PET_MEDICATION_OBJECT, new Gson().toJson(petMedicationModelList.get(postion)));
            startActivity(intent);
        });
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        addDetails();

    }

    private void addDetails() {
        Intent intent = new Intent(this, MedicationAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(NetworkUtils.PET_ID, petID);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        populateList(petID);
        super.onRestart();
    }

    //SwipeToDelete Callback
    public void setSwipeCallback() {


        final SwipeToDeleteCallBack callBack = new SwipeToDeleteCallBack(this) {

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction) {
                if (direction == ItemTouchHelper.LEFT) {

                    UtilityClass.showDeletPopup(MedicationActivity.this, getResources().getString(R.string.alert), getResources().getString(R.string.medication_confirmation), new IAlertListner() {
                        @Override
                        public void onYesClick() {
                            MedicationModelUpdated selectedPet = petMedicationModelList.get(viewHolder.getAdapterPosition());
                            MedicationModelUpdated note = MedicalHistoryModelUpdated.findById(MedicationModelUpdated.class, selectedPet.getId());
                            note.delete();
                            petMedicationModelList.remove(viewHolder.getAdapterPosition());
                            adapter.notifyDataSetChanged();
                            populateList(petID);
                        }

                        @Override
                        public void onNoClick() {
                            mRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    });

                }
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                if (viewHolder != null) {
                    if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);
                    } else {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);

                    }
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                viewHolder.itemView.setBackgroundColor(Color.WHITE);


            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(callBack);
        helper.attachToRecyclerView(mRecycler);


    }
}
