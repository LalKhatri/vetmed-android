package com.Celeritas.VetMed.Activities.pets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.Models.pets.WeightTrackerModel;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static com.Celeritas.VetMed.Activities.pets.MedicalHistoryAddActivity.isDateValid;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDateRequiredFormate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class WeightTrackerAddActivity extends BaseActivity implements IActionBar {


    private TextView weightDate;
    private Calendar myCalendar, myCalendarCurrent;
    private Bundle extras;
    private WeightTrackerModel model;
    public static Context context;
    String petWeightUnit = "Kg";
    private TextView weight;
    private TextView kg;
    private TextView lbs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_diet_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_weight_tracker_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.weight_tracker));
        initialize();
    }

    private void initialize() {


        weightDate = findViewById(R.id.datepickerStarted);
        weightDate.setHint(getCurrentDateRequiredFormate());
        weight = findViewById(R.id.weight);


        kg = findViewById(R.id.kg);
        lbs = findViewById(R.id.lbs);

        myCalendar = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();
        datePickerClick();
        toogle();
        extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_WEIGHT_TRACKER_OBJECT), WeightTrackerModel.class);
            updateWeightTracker(model);
        }
        weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model == null && weight.getText().length()==0){
                    showPopup(WeightTrackerAddActivity.this,weight,1,0);
                }else{
                    try{
                        String wt = weight.getText().toString().split(" ")[0];
                        String wtOne =wt.split("\\.")[0]
                                ,wtTwo = wt.split("\\.")[1];
                        showPopup(WeightTrackerAddActivity.this,weight,Integer.valueOf(wtOne),Integer.valueOf(wtTwo));
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }

            }
        });


    }


    private void setSelectedDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_TWO, Locale.US);
        weightDate.setText(sdf.format(myCalendar.getTime()));
    }

    //set previous data for update
    private void updateWeightTracker(WeightTrackerModel weightTrackerModel) {


        if (weightTrackerModel != null) {

            weightDate.setText(weightTrackerModel.getDate());
            weight.setText(weightTrackerModel.getPetWeight().split(" ")[0]);
            toolbarTitle.setText(getResources().getString(R.string.edit));
            ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
            petWeightUnit = weightTrackerModel.getPetWeight().split(" ")[1];
            if (petWeightUnit.equalsIgnoreCase(getResources().getString(R.string.kg))) {
                kg.setBackgroundResource(R.drawable.toogle_on);
                kg.setTextColor(getResources().getColor(R.color.white));
                lbs.setBackgroundResource(R.drawable.toogle_off);
                lbs.setTextColor(getResources().getColor(R.color.orange));
            } else {
                lbs.setBackgroundResource(R.drawable.toogle_on);
                lbs.setTextColor(getResources().getColor(R.color.white));
                kg.setBackgroundResource(R.drawable.toogle_off);
                kg.setTextColor(getResources().getColor(R.color.orange));
            }

        }

    }


    private void datePickerClick() {
        weightDate.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            if (date ==myCalendarCurrent.get(Calendar.DAY_OF_MONTH) && month == myCalendarCurrent.get(Calendar.MONTH) && year1 ==myCalendarCurrent.get(Calendar.YEAR)){
             myCalendarCurrent.add(Calendar.DATE,1);
            }
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (weightDate.getText().toString() != null && !weightDate.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = weightDate.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });

    }

    private boolean isEmpty() {
        if (
                weight.getText().length() > 0 &&
                weightDate.getText().length() > 0 &&
                !weight.getText().toString().equalsIgnoreCase("0.0")
                ) {
            return false;
        }


        return true;
    }

    private void toogle() {
        kg.setOnClickListener(view -> {
            kg.setBackgroundResource(R.drawable.toogle_on);
            kg.setTextColor(getResources().getColor(R.color.white));
            lbs.setBackgroundResource(R.drawable.toogle_off);
            lbs.setTextColor(getResources().getColor(R.color.orange));
            petWeightUnit = getResources().getString(R.string.kg);
        });

        lbs.setOnClickListener(view -> {
            lbs.setBackgroundResource(R.drawable.toogle_on);
            lbs.setTextColor(getResources().getColor(R.color.white));
            kg.setBackgroundResource(R.drawable.toogle_off);
            kg.setTextColor(getResources().getColor(R.color.orange));
            petWeightUnit = getResources().getString(R.string.lbs);
        });
    }

    public void Save(View view) {
        if (isEmpty()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }


        String startedDate = weightDate.getText().toString();

        String weightWithUnit = weight.getText().toString() + " " + petWeightUnit;
        if (isDateValid(startedDate)) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    "You Can't add future Date");
            return;
        }


        if (model == null) {
            String petID = extras.getString(NetworkUtils.PET_ID);
            WeightTrackerModel weightTrackerModel = new WeightTrackerModel(petID, getCurrentDate(), weightWithUnit, startedDate);
            weightTrackerModel.save();
            onBackPressed();
        } else {

            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    WeightTrackerModel selectedWeightTracker = WeightTrackerModel.findById(WeightTrackerModel.class, model.getId());
                    selectedWeightTracker.setPetID(model.getPetID());
                    selectedWeightTracker.setPetWeight(weightWithUnit);
                    selectedWeightTracker.setDate(startedDate);

                    selectedWeightTracker.setUpdateDateTime(getCurrentDate());
                    selectedWeightTracker.save();
                    onBackPressed();
                }

                @Override
                public void onNoClick() {
                }
            });


        }

    }

    @Override
    protected void onRestart() {
        if (model != null) {
            model = WeightTrackerModel.findById(WeightTrackerModel.class, model.getId());
            updateWeightTracker(model);
        }
        super.onRestart();

    }

    public void Cancel(View view) {
        onBackPressed();
    }

    @Override
    public void MenuClick() {
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public void showPopup(Context context, TextView weight,int weight1,int weight2){
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.bottomsheet, null);
        alertDialogBuilder.setView(promptsView);
        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        TextView cancelButton = promptsView.findViewById(R.id.cancel_button);
        TextView setButton = promptsView.findViewById(R.id.set_button);
        TextView weightDisplay = promptsView.findViewById(R.id.weight);

        NumberPicker weightOne = promptsView.findViewById(R.id.weight_one);
        NumberPicker weightTwo = promptsView.findViewById(R.id.weight_two);
        weightTwo.setMinValue(0);
        weightTwo.setMaxValue(9);
        weightOne.setMinValue(0);
        weightOne.setMaxValue(100);
        weightOne.setValue(weight1);
        weightTwo.setValue(weight2);
        weightDisplay.setText(weight1+"."+weight2);
        cancelButton.setOnClickListener(v -> {
            alertDialog.dismiss();

        });

        setButton.setOnClickListener(v -> {
            alertDialog.dismiss();
            weight.setText(""+weightOne.getValue()+"."+weightTwo.getValue());

        });
        weightOne.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i2) {
                weightDisplay.setText(Integer.toString(i2)+"."+weightDisplay.getText().toString().split("\\.")[1]);

            }
        });
        weightTwo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i2) {

                weightDisplay.setText(weightDisplay.getText().toString().split("\\.")[0]+"."+Integer.toString(i2));

            }
        });
        alertDialog.show();
    }
}
