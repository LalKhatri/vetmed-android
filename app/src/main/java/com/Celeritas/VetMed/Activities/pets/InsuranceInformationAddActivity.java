package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.InsuranceModelUpdated;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;

import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.isValidPhone;

public class InsuranceInformationAddActivity extends BaseActivity implements IActionBar {

    private TextView companyName;
    private TextView policyNumber;
    private TextView email;
    private TextView webSite;
    private TextView phoneNumber;
    private Bundle extras;
    private InsuranceModelUpdated model;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_insurance_information_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_insurance_information_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.insurance_information));
        toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        initialize();
    }
    private void initialize() {
        companyName = findViewById(R.id.companyName);
        policyNumber = findViewById(R.id.policyNum);
        email = findViewById(R.id.email_et);
        webSite = findViewById(R.id.webSite);
        phoneNumber = findViewById(R.id.phoneNumber);
        extras = getIntent().getExtras();
        //RightClick.setVisibility(View.GONE);
        if (extras != null){
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_INSURANCE_OBJECT),InsuranceModelUpdated.class);
            UpdateInsuranceInformationData(model);
        }
        companyName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
        policyNumber.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
    }
    //set previous data for update
    private void UpdateInsuranceInformationData(InsuranceModelUpdated model) {

            if (model != null) {
                companyName.setText(model.getInsuranceInfoName());
                policyNumber.setText(model.getInsuranceInfoPolicyNumber());
                email.setText(model.getInsuranceInfoEmail());
                webSite.setText(model.getInsuranceInfoWebsite());
                phoneNumber.setText(model.getInsuranceInfoPhoneNumber());

                toolbarTitle.setText(getResources().getString(R.string.edit));
                ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
                //RightClick.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.delete));
            }

    }
    private boolean isEmpty() {
        if (companyName.getText().length() > 0 && policyNumber.getText().length() > 0) {
            return false;
        }
        return true;
    }
    public void Save(View view){
        if (isEmpty()){
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }
        if(!email.getText().toString().isEmpty() &&
                email.getText().toString()!=null &&
                !email.getText().toString().equalsIgnoreCase("")&&
                !UtilityClass.isValidEmail(email.getText().toString())){
            UtilityClass.showPopup(InsuranceInformationAddActivity.this,getResources().getString(R.string.alert), getResources().getString(R.string.invalid_email));
            return;
        }

        if(!webSite.getText().toString().isEmpty() &&
                webSite.getText().toString()!=null &&
                !webSite.getText().toString().equalsIgnoreCase("")&&
                !UtilityClass.isValidWebSite(webSite.getText().toString())){
            UtilityClass.showPopup(InsuranceInformationAddActivity.this,getResources().getString(R.string.alert), getResources().getString(R.string.invalid_website));
            return;
        }
        if(!phoneNumber.getText().toString().isEmpty()&&
                phoneNumber.getText().toString()!=null&&
                !phoneNumber.getText().toString().equalsIgnoreCase("")&&
                !isValidPhone(phoneNumber.getText().toString())){

            UtilityClass.showPopup(InsuranceInformationAddActivity.this,getResources().getString(R.string.alert), getResources().getString(R.string.invalid_phone));
            return;
        }

        String comName = companyName.getText().toString();
        String policyNum = policyNumber.getText().toString();
        String email1 = email.getText().toString();
        String phone = phoneNumber.getText().toString();
        String web = webSite.getText().toString();


        if (model == null) {
            String petID = extras.getString(NetworkUtils.PET_ID);
            InsuranceModelUpdated insuranceModel = new InsuranceModelUpdated(petID,comName,policyNum,email1,web,phone,getCurrentDate());
            insuranceModel.save();
            onBackPressed();
        } else {

            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    InsuranceModelUpdated selectedInsurance = InsuranceModelUpdated.findById(InsuranceModelUpdated.class, model.getId());
                    selectedInsurance.setPetID(model.getPetID());
                    selectedInsurance.setInsuranceInfoName(comName);
                    selectedInsurance.setInsuranceInfoPolicyNumber(policyNum);
                    selectedInsurance.setInsuranceInfoEmail(email1);
                    selectedInsurance.setInsuranceInfoWebsite(web);
                    selectedInsurance.setInsuranceInfoPhoneNumber(phone);
                    selectedInsurance.setUpdateDateTime(getCurrentDate());
                    selectedInsurance.save();
                    onBackPressed();

                }

                @Override
                public void onNoClick() {

                }
            });


        }

    }
    public void Cancel(View view){
        onBackPressed();
    }
    @Override
    public void MenuClick() {}
    @Override
    public void LeftBackClick() {
        onBackPressed();
    }
    @Override
    public void RightClick() {    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
