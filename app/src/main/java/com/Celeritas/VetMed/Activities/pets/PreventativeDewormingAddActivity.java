package com.Celeritas.VetMed.Activities.pets;


import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.DewormingModelUpdated;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


import static com.Celeritas.VetMed.Activities.pets.DietAddActivity.hideOrShowKeyboard;
import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Constants.sdf;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class PreventativeDewormingAddActivity extends BaseActivity implements IActionBar {
    private TextView productName;
    private TextView oftenAnyOther;
    private Spinner spinnerHowOften;
    private TextView startDate;
    private TextView endDate;
    private Spinner spinnerActive;
    private TextView dewormingNote;
    private Calendar myCalendar;
    private Calendar myCalendar1;
    private Calendar myCalendarCurrent;
    private ArrayList<String> HowOften;
    private ArrayList<String> Active;
    private Bundle extras;
    private DewormingModelUpdated model;
    private String howOften;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_preventative_deworming_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_preventative_deworming_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.deworming));
        initialize();
    }
    private void initialize() {
        productName = findViewById(R.id.productName);
        oftenAnyOther = findViewById(R.id.oftenAnyOther);
        spinnerHowOften = findViewById(R.id.spinnerHowOften);
        startDate = findViewById(R.id.datepickerStarted);
        endDate = findViewById(R.id.datepickerEnd);
        spinnerActive = findViewById(R.id.spinnerActive);
        dewormingNote = findViewById(R.id.note);

        //RightClick.setVisibility(View.GONE);
        myCalendar = Calendar.getInstance();
        myCalendar1 = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();
        datepickerClick();
        setHowOftenSpinner();
        setActiveSpinner();
        extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(extras.getString(Constants.PET_DEWORMING_OBJECT), DewormingModelUpdated.class);
            UpdatePetDietData(model);
        }
        productName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
        dewormingNote.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        oftenAnyOther.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        dewormingNote.setImeOptions(EditorInfo.IME_ACTION_DONE);
        dewormingNote.setRawInputType(InputType.TYPE_CLASS_TEXT);
        dewormingNote.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
    }
    //set previous data for update
    private void UpdatePetDietData(DewormingModelUpdated model) {

        if (model != null) {
            productName.setText(model.getDFTProductName());
            startDate.setText(model.getDFTDateStarted());
            endDate.setText(model.getDFTEndDate());
            dewormingNote.setText(model.getDFTNotes());
            for (int i = 0; i < HowOften.size(); i++) {
                if (HowOften.get(i).contains(model.getDFTHowOften())) {
                    spinnerHowOften.setSelection(i);
                }
            }

            for (int i = 0; i < Active.size(); i++) {
                if (Active.get(i).contains(model.getDFTActive())) {
                    spinnerActive.setSelection(i);
                }
            }
            if (model.getDFTHowOften().contains(getResources().getString(R.string.any_other))){
                oftenAnyOther.setText(model.getDFTHowOften().split(",")[1].split(" ")[1]);
                spinnerHowOften.setSelection(3);
            }

            toolbarTitle.setText(getResources().getString(R.string.edit));
            ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
            //RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.delete));
        }

    }
    private void setActiveSpinner() {
        Active = new ArrayList<>();
        Active.add(getResources().getString(R.string.select));
        Active.add(getResources().getString(R.string.yes));
        Active.add(getResources().getString(R.string.no));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, Active);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerActive.setAdapter(dataAdapter);
        spinnerActive.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(PreventativeDewormingAddActivity.this);
                return false;
            }
        });
        spinnerActive.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!Active.get(position).equalsIgnoreCase("Yes")){
                    //endDate.setClickable(true);
                    findViewById(R.id.end_date_layout).setVisibility(View.VISIBLE);
                }else {
                    findViewById(R.id.end_date_layout).setVisibility(View.GONE);
                    endDate.setText(null);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void setSelectedDate1() {
        endDate.setText(sdf.format(myCalendar1.getTime()));
    }
    private void setSelectedDate() {
        startDate.setText(sdf.format(myCalendar.getTime()));
    }
    private void setHowOftenSpinner() {
        HowOften = new ArrayList<>();
        HowOften.add(getResources().getString(R.string.select));
        HowOften.add(getResources().getString(R.string.every_week));
        HowOften.add(getResources().getString(R.string.every_month));
        HowOften.add(getResources().getString(R.string.any_other));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, HowOften);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHowOften.setAdapter(dataAdapter);
        spinnerHowOften.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(PreventativeDewormingAddActivity.this);
                return false;
            }
        });
        spinnerHowOften.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.any_other))) {
                    oftenAnyOther.setVisibility(View.VISIBLE);
                    howOften = oftenAnyOther.getText().toString();
                } else {
                    oftenAnyOther.setVisibility(View.GONE);
                    howOften = spinnerHowOften.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private void datepickerClick() {
        startDate.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (startDate.getText().toString() != null && !startDate.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = startDate.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
        endDate.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            if (date ==myCalendarCurrent.get(Calendar.DAY_OF_MONTH) && month == myCalendarCurrent.get(Calendar.MONTH) && year1 ==myCalendarCurrent.get(Calendar.YEAR)){
                myCalendarCurrent.add(Calendar.DATE,1);
            }
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (endDate.getText().toString() != null && !endDate.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = endDate.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar1.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate1();
                })
                        .minDate(year1, month, date)
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar1.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate1();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .maxDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
    }
    private boolean isEmpty() {
        if(spinnerActive.getSelectedItem().toString().equalsIgnoreCase("Yes")){
            if (productName.getText().length() > 0 && startDate.getText().length() > 0
                    && !spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                    && !spinnerActive.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))) {
                return false;
            }
        }else {
            if (productName.getText().length() > 0 && startDate.getText().length() > 0 &&
                    endDate.getText().length() > 0
                    && !spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                    && !spinnerActive.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))) {
                return false;
            }
        }

        return true;
    }
    public boolean isDateValid() {
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
        try {
            Date arg0Date = format.parse(startDate.getText().toString());
            Date arg1Date = format.parse(endDate.getText().toString());
            if (arg0Date.before(arg1Date) || arg1Date.equals(arg0Date)) {
                return false;
            } else {
                return true;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public void Save(View view) {
        if (isEmpty()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }

        if (isDateValid()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.start_date_should_be_greater));
            return;
        }
        if (spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.any_other)) && oftenAnyOther.getText().toString().length() == 0) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }
        if (spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.any_other))) {
            howOften = getString(R.string.any_other) + ",Every " + oftenAnyOther.getText().toString();
        }

        String productName1 = productName.getText().toString();
        String startedDate1 = startDate.getText().toString();
        String endDate1 = endDate.getText().toString();
        String dewormingActive = spinnerActive.getSelectedItem().toString();
        String dewormingNote1 = dewormingNote.getText().toString();


        if (model == null) {

            String petID = extras.getString(NetworkUtils.PET_ID);
            DewormingModelUpdated dewormingModel = new DewormingModelUpdated(petID, productName1, startedDate1, endDate1, howOften, dewormingActive, dewormingNote1, getCurrentDate());
            dewormingModel.save();
            onBackPressed();
        } else {
            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    DewormingModelUpdated selectedDeworming = DewormingModelUpdated.findById(DewormingModelUpdated.class, model.getId());
                    selectedDeworming.setPetID(model.getPetID());
                    selectedDeworming.setDFTProductName(productName1);
                    selectedDeworming.setDFTDateStarted(startedDate1);
                    selectedDeworming.setDFTEndDate(endDate1);
                    selectedDeworming.setDFTHowOften(howOften);
                    selectedDeworming.setDFTActive(dewormingActive);
                    selectedDeworming.setDFTNotes(dewormingNote1);
                    selectedDeworming.setUpdateDateTime(getCurrentDate());
                    selectedDeworming.save();
                    onBackPressed();

                }

                @Override
                public void onNoClick() {

                }
            });

        }

    }
    public void Cancel(View view) {
        onBackPressed();
    }
    @Override
    public void MenuClick() {}
    @Override
    public void LeftBackClick() {
        onBackPressed();
    }
    @Override
    public void RightClick() {
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    protected void onRestart() {
      if (model !=null){
          model = DewormingModelUpdated.findById(DewormingModelUpdated.class, model.getId());
          UpdatePetDietData(model);
      }
        super.onRestart();
    }
}
