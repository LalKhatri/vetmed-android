package com.Celeritas.VetMed.Activities.pets;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Constants.sdf;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class DietAddActivity extends BaseActivity implements IActionBar {

    private TextView dietName;
    private TextView dietAnyOtherHowOfn;
    private Spinner spinnerType;
    private TextView howMuch;
    private Spinner spinnerHowOften;
    private TextView startDate;
    private TextView endDate;
    private Spinner spinnerActive,spinnerHowMuchType;
    private Calendar myCalendar,myCalendarCurrent;
    private DatePickerDialog.OnDateSetListener date;
    private Calendar myCalendar1;
    private DatePickerDialog.OnDateSetListener date1;
    private TextView dietNote;
    private ArrayList<String> Active;
    private ArrayList<String> howMuchType;
    private ArrayList<String> HowOften;
    private ArrayList<String> Type;
    private Bundle extras;
    private DietModelUpdated model;
    private String howOften;
    public static Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_diet_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_diet_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.diet));
        initialize();
    }

    private void initialize() {
        dietName = findViewById(R.id.dietName);
        spinnerType = findViewById(R.id.spinnerType);
        spinnerHowMuchType = findViewById(R.id.spinnerHowMuchType);
        dietAnyOtherHowOfn = findViewById(R.id.medicationFrequency);
        howMuch = findViewById(R.id.diet_how_much);
        spinnerHowOften = findViewById(R.id.spinnerHowOften);
        startDate = findViewById(R.id.datepickerStarted);
        endDate = findViewById(R.id.datepickerEnd);
        spinnerActive = findViewById(R.id.spinnerActive);
        dietNote = findViewById(R.id.note);
        //RightClick.setVisibility(View.GONE);

        myCalendar = Calendar.getInstance();
        myCalendar1 = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();
        datePickerClick();
        setTypeSpinner();
        setHowOftenSpinner();
        setActiveSpinner();
        setspinnerHowMuchTypeSpinner();
        extras = getIntent().getExtras();
        dietNote.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        if (extras != null){
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_DIET_OBJECT), DietModelUpdated.class);
            UpdatePetDietData(model);
        }
        dietNote.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        dietNote.setImeOptions(EditorInfo.IME_ACTION_DONE);
        dietNote.setRawInputType(InputType.TYPE_CLASS_TEXT);
        dietNote.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        dietName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(),new InputFilter.LengthFilter(32)});
        howMuch.setRawInputType(Configuration.KEYBOARD_12KEY);
    }

    private void setspinnerHowMuchTypeSpinner() {
        howMuchType = new ArrayList<>();
        howMuchType.add(getResources().getString(R.string.select));
        howMuchType.add(getResources().getString(R.string.cups));
        howMuchType.add(getResources().getString(R.string.cans));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, howMuchType);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHowMuchType.setAdapter(dataAdapter);
        spinnerHowMuchType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(DietAddActivity.this);
                return false;
            }
        });
    }

    private void setSelectedDate() {
        startDate.setText(sdf.format(myCalendar.getTime()));
    }

    //set previous data for update
    private void UpdatePetDietData(DietModelUpdated dietModelUpdated) {


            if (dietModelUpdated != null) {
                dietName.setText(dietModelUpdated.getDietName());
                howMuch.setText(dietModelUpdated.getDietHowMuch().split(" ")[0]);
                startDate.setText(dietModelUpdated.getDietDateStarted());
                endDate.setText(dietModelUpdated.getDietEndDate());
                dietNote.setText(dietModelUpdated.getDietNotes());
                for (int i = 0; i < Type.size(); i++) {
                    if (Type.get(i).contains(dietModelUpdated.getDietType())) {
                        spinnerType.setSelection(i);
                    }
                }

                for (int i = 0; i < howMuchType.size(); i++) {
                    if (howMuchType.get(i).contains(dietModelUpdated.getDietHowMuch().split(" ")[1])) {
                        spinnerHowMuchType.setSelection(i);
                    }
                }

                for (int i = 0; i < HowOften.size(); i++) {
                    if (HowOften.get(i).contains(dietModelUpdated.getDietHowOften().split(",")[0])) {
                        spinnerHowOften.setSelection(i);
                    }
                }
                for (int i = 0; i < Active.size(); i++) {
                    if (Active.get(i).contains(dietModelUpdated.getDietActive())) {
                        spinnerActive.setSelection(i);
                    }
                }

                toolbarTitle.setText(getResources().getString(R.string.edit));
                ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
                if (model.getDietHowOften().contains(getResources().getString(R.string.any_other))) {
                    dietAnyOtherHowOfn.setText(model.getDietHowOften().split(",")[1]);
                }
            }

    }

    private void setActiveSpinner() {
        Active = new ArrayList<>();
        Active.add(getResources().getString(R.string.select));
        Active.add(getResources().getString(R.string.yes));
        Active.add(getResources().getString(R.string.no));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, Active);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerActive.setAdapter(dataAdapter);
        spinnerActive.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(DietAddActivity.this);
                return false;
            }
        });
        spinnerActive.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!Active.get(position).equalsIgnoreCase("Yes")){
                    //endDate.setClickable(true);
                    findViewById(R.id.end_date_layout).setVisibility(View.VISIBLE);
                }else {
                    findViewById(R.id.end_date_layout).setVisibility(View.GONE);
                    endDate.setText(null);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setHowOftenSpinner() {
        HowOften = new ArrayList<>();
        HowOften.add(getResources().getString(R.string.select));
        HowOften.add(getResources().getString(R.string.every_4H));
        HowOften.add(getResources().getString(R.string.every_6H));
        HowOften.add(getResources().getString(R.string.every_8H));
        HowOften.add(getResources().getString(R.string.every_12H));
        HowOften.add(getResources().getString(R.string.every_24H));
        HowOften.add(getResources().getString(R.string.every_48H));
        HowOften.add(getResources().getString(R.string.every_72H));
        HowOften.add(getResources().getString(R.string.any_other));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, HowOften);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHowOften.setAdapter(dataAdapter);
        spinnerHowOften.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(DietAddActivity.this);
                return false;
            }
        });
        spinnerHowOften.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.any_other))) {
                    dietAnyOtherHowOfn.setVisibility(View.VISIBLE);
                    howOften = dietAnyOtherHowOfn.getText().toString();
                } else {
                    dietAnyOtherHowOfn.setVisibility(View.GONE);
                    howOften = spinnerHowOften.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setTypeSpinner() {
        Type = new ArrayList<>();
        Type.add(getResources().getString(R.string.select));
        Type.add(getResources().getString(R.string.food));
        Type.add(getResources().getString(R.string.treat));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, Type);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(dataAdapter);
        spinnerType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(DietAddActivity.this);
                return false;
            }
        });
    }

    private void setSelectedDate1() {
        endDate.setText(sdf.format(myCalendar1.getTime()));
    }

    private void datePickerClick() {
        startDate.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (startDate.getText().toString() != null && !startDate.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = startDate.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
        endDate.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            if (date ==myCalendarCurrent.get(Calendar.DAY_OF_MONTH) && month == myCalendarCurrent.get(Calendar.MONTH) && year1 ==myCalendarCurrent.get(Calendar.YEAR)){
                myCalendarCurrent.add(Calendar.DATE,1);
            }
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (endDate.getText().toString() != null && !endDate.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = endDate.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar1.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate1();
                })
                        .minDate(year1, month, date)
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar1.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate1();
                })
                        .minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
    }

    private boolean isEmpty() {
        if(spinnerActive.getSelectedItem().toString().equalsIgnoreCase("Yes")){
            if (dietName.getText().length() > 0 && howMuch.getText().length() > 0 &&
                    startDate.getText().length() > 0
                    && !spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                    && !spinnerHowMuchType.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                    && !spinnerActive.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                    && !spinnerType.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))) {
                return false;
            }
        }else {
            if (dietName.getText().length() > 0 && howMuch.getText().length() > 0 &&
                    startDate.getText().length() > 0 && endDate.getText().length() > 0
                    && !spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                    && !spinnerHowMuchType.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                    && !spinnerActive.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))
                    && !spinnerType.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))) {
                return false;
            }
        }

        return true;
    }

    public boolean isDateValid(){
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
            try {
                Date arg0Date = format.parse(startDate.getText().toString());
                Date arg1Date = format.parse(endDate.getText().toString());
                if (arg0Date.before(arg1Date) || arg1Date.equals(arg0Date)) {
                    return false;
                } else {
                    return true;

                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return false;
    }

    public void Save(View view){
        if (isEmpty()){
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }


        if (isDateValid() && spinnerActive.getSelectedItem().toString().equalsIgnoreCase("No")){
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.start_date_should_be_greater));
            return;
        }
        if (spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.any_other)) && dietAnyOtherHowOfn.getText().toString().length() == 0) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }
        if (spinnerHowOften.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.any_other))) {
            howOften = getString(R.string.any_other) + "," + dietAnyOtherHowOfn.getText().toString();
        }

        String dietName1 = dietName.getText().toString();
        String dietType = spinnerType.getSelectedItem().toString();
        String howMuch1 = howMuch.getText().toString()+" "+spinnerHowMuchType.getSelectedItem().toString();
        String startedDate1 = startDate.getText().toString();
        String endDate1 = endDate.getText().toString();
        String dietActive = spinnerActive.getSelectedItem().toString();
        String dietNote1 = dietNote.getText().toString();
        if (model == null) {
            String petID = extras.getString(NetworkUtils.PET_ID);
            DietModelUpdated dietModelUpdated = new DietModelUpdated(petID,dietName1,dietType,howMuch1, howOften, startedDate1,endDate1,dietActive,dietNote1,getCurrentDate());
            dietModelUpdated.save();
            onBackPressed();
        } else {

            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    DietModelUpdated selectedDiet = DietModelUpdated.findById(DietModelUpdated.class, model.getId());
                    selectedDiet.setPetID(model.getPetID());
                    selectedDiet.setDietName(dietName1);
                    selectedDiet.setDietHowMuch(howMuch1);
                    selectedDiet.setDietHowOften(howOften);
                    selectedDiet.setDietEndDate(endDate1);
                    selectedDiet.setDietDateStarted(startedDate1);
                    selectedDiet.setDietType(dietType);
                    selectedDiet.setDietNotes(dietNote1);
                    selectedDiet.setDietActive(dietActive);
                    selectedDiet.setUpdateDateTime(getCurrentDate());
                    selectedDiet.save();
                    onBackPressed();
                }
                @Override
                public void onNoClick() {}
            });


        }

    }
    @Override
    protected void onRestart() {
        if (model !=null){
            model = DietModelUpdated.findById(DietModelUpdated.class,model.getId());
            UpdatePetDietData(model);
        }
        super.onRestart();

    }
    public void Cancel(View view){
        onBackPressed();
    }
    @Override
    public void MenuClick() {}
    @Override
    public void LeftBackClick() {
        onBackPressed();
    }
    @Override
    public void RightClick() { }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    public static void hideOrShowKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) {
            Log.e("Key", "Software Keyboard was shown");
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } else {
            Log.e("Key", "Software Keyboard was not shown");
        }

    }
}
