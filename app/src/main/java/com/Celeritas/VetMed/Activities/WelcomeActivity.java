package com.Celeritas.VetMed.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.Celeritas.VetMed.R;

import java.security.MessageDigest;


public class WelcomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        //GenerateKeyHashes();

    }

    private void GenerateKeyHashes() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.Celeritas.VetMed",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {

        }
    }

    //this method will navigate to sign in activity.
    public void signInClick(View view){
        Intent i = new Intent(this, SignInActivity.class);
        startActivity(i);

    }


    //It is called when activity pause.
    @Override
    protected void onPause() {
        super.onPause();

    }

    //Navigate to create an account activity
    public void create_an_accountClick(View view) {
        startActivity(new Intent(this, CreateAccountActivity.class));
    }
}
