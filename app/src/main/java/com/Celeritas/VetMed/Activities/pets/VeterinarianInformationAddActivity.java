package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.pets.VaccineModelUpdated;
import com.Celeritas.VetMed.Models.pets.VeterinarianModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;

import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.isValidPhone;

public class VeterinarianInformationAddActivity extends BaseActivity implements IActionBar {

    private TextView hospitalName;
    private TextView doctorName;
    private TextView address;
    private TextView email;
    private TextView phoneNumber;
    private TextView webSite;
    private TextView note;
    private VeterinarianModelUpdated model;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_veterinarian_information_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_veterinarian_information_add, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.veterinarian_information));
        initialize();
    }

    private void initialize() {
        hospitalName = findViewById(R.id.hospitalName);

        doctorName = findViewById(R.id.doctorName);
        address = findViewById(R.id.address);
        email = findViewById(R.id.email_et);
        phoneNumber = findViewById(R.id.phoneNumber);
        webSite = findViewById(R.id.webSite);
        note = findViewById(R.id.note);

        note.setImeOptions(EditorInfo.IME_ACTION_DONE);
        note.setRawInputType(InputType.TYPE_CLASS_TEXT);
        extras = getIntent().getExtras();
        note.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        if (extras != null) {
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_VETERINARIAN_OBJECT), VeterinarianModelUpdated.class);
            UpdateVeterinarianInformationData(model);
        }
        hospitalName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(), new InputFilter.LengthFilter(32)});
        doctorName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace(), new InputFilter.LengthFilter(32)});
        address.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        note.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        note.setImeOptions(EditorInfo.IME_ACTION_DONE);
        note.setRawInputType(InputType.TYPE_CLASS_TEXT);
        note.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

    }

    //set previous data for update
    private void UpdateVeterinarianInformationData(VeterinarianModelUpdated model) {
        if (model != null) {
            hospitalName.setText(model.getVIHospital());
            doctorName.setText(model.getVIDoctorName());
            address.setText(model.getVIAddress());
            email.setText(model.getVIEmail());
            phoneNumber.setText(model.getVIPhoneNumber());
            webSite.setText(model.getVIWebsite());
            note.setText(model.getVINotes());
            toolbarTitle.setText(getResources().getString(R.string.edit));
            ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
            //RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.delete));
        }
    }

    private boolean isEmpty() {
        if (hospitalName.getText().length() > 0 && doctorName.getText().length() > 0
                /*&& email.getText().length() > 0
                && phoneNumber.getText().length() > 0 && address.getText().length() > 0 &&
                 webSite.getText().length() > 0*/
                ) {
            return false; //LalKhatri git config --global user.name "LalKhatri"
        }
        return true;
    }

    public void Save(View view) {
        if (isEmpty()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }

        if(!email.getText().toString().isEmpty() &&
                email.getText().toString()!=null &&
                !email.getText().toString().equalsIgnoreCase("")&&
                !UtilityClass.isValidEmail(email.getText().toString())){
            UtilityClass.showPopup(VeterinarianInformationAddActivity.this,getResources().getString(R.string.alert), getResources().getString(R.string.invalid_email));
            return;
        }

        if(!webSite.getText().toString().isEmpty() &&
                webSite.getText().toString()!=null &&
                !webSite.getText().toString().equalsIgnoreCase("")&&
                 !UtilityClass.isValidWebSite(webSite.getText().toString())){
            UtilityClass.showPopup(VeterinarianInformationAddActivity.this,getResources().getString(R.string.alert), getResources().getString(R.string.invalid_website));
            return;
        }
        if(!phoneNumber.getText().toString().isEmpty()&&
                phoneNumber.getText().toString()!=null&&
                !phoneNumber.getText().toString().equalsIgnoreCase("")&&
                !isValidPhone(phoneNumber.getText().toString())){

            UtilityClass.showPopup(VeterinarianInformationAddActivity.this,getResources().getString(R.string.alert), getResources().getString(R.string.invalid_phone));
            return;
        }

        String hospital = hospitalName.getText().toString();
        String doctor = doctorName.getText().toString();
        String address1 = address.getText().toString();
        String email1 = email.getText().toString();
        String phone = phoneNumber.getText().toString();
        String web = webSite.getText().toString();
        String note = this.note.getText().toString();


        if (model == null) {
            String petID = extras.getString(NetworkUtils.PET_ID);
            VeterinarianModelUpdated veterinarianModel = new VeterinarianModelUpdated(petID, hospital, doctor, address1, email1, phone, web, note, getCurrentDate());
            veterinarianModel.save();
            onBackPressed();
        } else {

            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    VeterinarianModelUpdated selectedVeterinarian = VeterinarianModelUpdated.findById(VeterinarianModelUpdated.class, model.getId());
                    selectedVeterinarian.setPetID(selectedVeterinarian.getPetID());

                    selectedVeterinarian.setVIHospital(hospital);
                    selectedVeterinarian.setVIDoctorName(doctor);

                    selectedVeterinarian.setVIAddress(address1);
                    selectedVeterinarian.setVIEmail(email1);
                    selectedVeterinarian.setVIPhoneNumber(phone);
                    selectedVeterinarian.setVIWebsite(web);
                    selectedVeterinarian.setVINotes(note);
                    selectedVeterinarian.setUpdateDateTime(getCurrentDate());
                    selectedVeterinarian.save();
                    onBackPressed();

                }

                @Override
                public void onNoClick() {

                }
            });

        }

    }

    @Override
    protected void onRestart() {
        if (model != null) {
            model = VeterinarianModelUpdated.findById(VeterinarianModelUpdated.class, model.getId());
            UpdateVeterinarianInformationData(model);
        }
        super.onRestart();
    }

    public void Cancel(View view) {
        onBackPressed();
    }

    @Override
    public void MenuClick() {
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
