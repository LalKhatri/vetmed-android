package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.DewormingModel;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.VaccineModel;
import com.Celeritas.VetMed.Models.pets.DewormingModelUpdated;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.VaccineModelUpdated;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class PreventativeDetailActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;
    private VaccineModelUpdated vaccineModel;
    private DewormingModelUpdated dewormingModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_preventative_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_preventative_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));
        initialize();
    }

    private void initialize() {
        mRecyclerView = (RecyclerView)findViewById(R.id.prevent_detail);
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            vaccineModel = new Gson().fromJson(extras.getString(Constants.PET_VACCINE_OBJECT),VaccineModelUpdated.class);
            dewormingModel = new Gson().fromJson(extras.getString(Constants.PET_DEWORMING_OBJECT),DewormingModelUpdated.class);
            poplulateList();
        }


    }

    private void poplulateList() {
        List<Item> mItemList = new ArrayList<>();

            if (vaccineModel!= null){
                vaccineModel = VaccineModelUpdated.findById(VaccineModelUpdated.class, vaccineModel.getId());
                toolbarTitle.setText(vaccineModel.getVaccineName());
                toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);

                Item GivenDate = new Item();
                String date = getResources().getString(R.string.given_given);
                GivenDate.setTitle(date);
                GivenDate.setDescription(vaccineModel.getVaccineDateGiven());
                GivenDate.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(GivenDate);

                Item nexDue = new Item();
                String due = getResources().getString(R.string.next_due);
                nexDue.setTitle(due);
                nexDue.setDescription(vaccineModel.getNextDueDate());
                nexDue.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(nexDue);

                Item SideEffects = new Item();
                String sideEffects = getResources().getString(R.string.side_effects);
                SideEffects.setTitle(sideEffects);
                SideEffects.setDescription(vaccineModel.getVaccineSideEffects());
                SideEffects.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(SideEffects);

                Item VaccineNote = new Item();
                String howoften = getResources().getString(R.string.diet_note);
                VaccineNote.setTitle(howoften);
                VaccineNote.setDescription(vaccineModel.getVaccineNotes());
                VaccineNote.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(VaccineNote);
            }else {
                dewormingModel = DewormingModelUpdated.findById(DewormingModelUpdated.class, dewormingModel.getId());
                toolbarTitle.setText(dewormingModel.getDFTProductName());
                toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                Item DateStarted = new Item();
                String startDate = getResources().getString(R.string.date_started);
                DateStarted.setTitle(startDate);
                DateStarted.setDescription(dewormingModel.getDFTDateStarted());
                DateStarted.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(DateStarted);

                Item EndDate = new Item();
                String dateTitle = getResources().getString(R.string.end_date);
                EndDate.setTitle(dateTitle);
                EndDate.setDescription(dewormingModel.getDFTEndDate());
                EndDate.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(EndDate);

                Item HowOften = new Item();
                String howoften = getResources().getString(R.string.how_often);
                HowOften.setTitle(howoften);
                if (dewormingModel.getDFTHowOften().contains(getResources().getString(R.string.any_other))){
                    HowOften.setDescription(dewormingModel.getDFTHowOften().split(",")[1]);
                }else {
                    HowOften.setDescription(dewormingModel.getDFTHowOften());
                }



                HowOften.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(HowOften);



                Item Active = new Item();
                String activeTitle = getResources().getString(R.string.active);
                Active.setTitle(activeTitle);
                Active.setDescription(dewormingModel.getDFTActive());
                Active.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(Active);

                Item Notes = new Item();
                String noteTitle = getResources().getString(R.string.diet_note);
                Notes.setTitle(noteTitle);
                Notes.setDescription(dewormingModel.getDFTNotes());
                Notes.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
                mItemList.add(Notes);
            }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        BaseAdapter mAdapter = new BaseAdapter(mItemList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        if (vaccineModel != null) {
            Intent intent = new Intent(this, PreventativeVaccineAddActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.PET_VACCINE_OBJECT, new Gson().toJson(vaccineModel));
            startActivity(intent);
        }else {
            Intent intent = new Intent(this, PreventativeDewormingAddActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.PET_DEWORMING_OBJECT, new Gson().toJson(dewormingModel));
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        poplulateList();
        super.onRestart();
    }
}
