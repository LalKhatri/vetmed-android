package com.Celeritas.VetMed.Activities.pets;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Activities.ShareActivity;
import com.Celeritas.VetMed.Activities.pets.documents.DocumentsActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.AgeCalculation;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE;
import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.PERMISSIONS_STORAGE;
import static com.Celeritas.VetMed.Constants.IMAGES_PATH;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class PetDetailActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;
    private TextView petAge;
    private TextView petWeight;
    private BaseAdapter mAdapter;
    private PetModelUpdated model;
    String finalAge = "";
    private CircleImageView profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_pet_detail);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_pet_detail, content, true);

        MenuClick.setVisibility(View.GONE);

        initailizeViews();
    }

    private void initailizeViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.pet_detail);
        petAge = (TextView) findViewById(R.id.date);
        petWeight = (TextView) findViewById(R.id.weight);
        profile = findViewById(R.id.profile);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_share));


        model = SessionClass.getInstance().getPetModelUpdated(this);

        loadInformation(model);

        RightClick.setOnClickListener(view -> {
            shareClicked();
        });

        populateList();
        profileClick();


    }

    private void loadInformation(PetModelUpdated model) {
        model = PetModelUpdated.findById(PetModelUpdated.class, model.getId());
        setProfileImage(model);
        toolbarTitle.setText(model.getPetName());
        String dob = model.getPetDOB();
        String array1[] = dob.split(" ");
        int date = Integer.valueOf(array1[0]);
        int month = Arrays.asList(months).indexOf(array1[1]);
        int year = Integer.valueOf(array1[2]);
        String age = getAge(year, month, date);
        String array2[] = age.split(":");
        String days = array2[0];
        String months = array2[1];
        String years = array2[2];
        if (years.equalsIgnoreCase("0") && months.equalsIgnoreCase("0")) {
            finalAge = days + " days";
        } else if (years.equalsIgnoreCase("0") && !months.equalsIgnoreCase("0")) {
            finalAge = months + "mo, " + days + " days";
        } else {
            finalAge = years + "yr, " + months + " mo";
        }
        petAge.setText(finalAge);
        petWeight.setText(model.getPetWeight() + " " +" "+model.getPetWeightUnit());
        //  }
    }

    private void shareClicked() {

        Intent intent = new Intent(PetDetailActivity.this, ShareActivity.class);
        intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
        startActivity(intent);
    }


    private void setProfileImage(PetModelUpdated model) {
        if (!model.getProfileImage().equalsIgnoreCase("NA") && !model.getProfileImage().equalsIgnoreCase("Randomly")) {
            if (model.getProfileImage().contains("Image_")) {
                profile.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH +model.getId() + "/"+model.getProfileImage() + ".jpg")));
            } else {
                profile.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH +model.getId() + "/" +model.getPetImages() + ".jpg")));
            }

        } else {
            if (model.getPetImages() != null && model.getPetImages().split(",").length != 0) {
                if (model.getPetImages().split(",").length >= 2) {
                    if (model.getProfileImage().equalsIgnoreCase("Randomly")) {
                        int x = new Random().nextInt(model.getPetImages().split(",").length);
                        profile.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH +model.getId() + "/" +model.getPetImages().split(",")[x] + ".jpg")));
                    }
                    //profile.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH+model.getPetImages().split(",")[1]+".jpg")));
                } else {
                    if (!model.getPetImages().split(",")[0].equalsIgnoreCase("NA")) {
                        profile.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH +model.getId() + "/"+model.getPetImages().split(",")[0] + ".jpg")));
                    }
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        model = PetModelUpdated.findById(PetModelUpdated.class, model.getId());
        loadInformation(model);
        super.onRestart();

    }

    private void profileClick() {
        profile.setOnClickListener(view -> {

            if (ActivityCompat.checkSelfPermission(PetDetailActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(PetDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PetDetailActivity.this, PERMISSIONS_STORAGE, MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE);
            } else {
                Intent intent = new Intent(PetDetailActivity.this, GalleryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                intent.putExtra(NetworkUtils.USER_ID, SessionClass.getInstance().getUser(this).getID());
                intent.putExtra(Constants.PET_PROFILE_IMAGE, model.getProfileImage());
                intent.putExtra(Constants.PET_PROFILE_IMAGE_LIST, model.getPetImages());
                startActivity(intent);
            }

        });
    }

    public static String getAge(int year, int month, int day) {
        AgeCalculation age = new AgeCalculation();
        age.getCurrentDate();
        age.setDateOfBirth(year, month, day);
        int calculatedYear = age.calcualteYear();
        int calculatedMonth = age.calcualteMonth();
        int calculatedDate = age.calcualteDay();
        Integer year1 = new Integer(calculatedYear);
        Integer month1 = new Integer(calculatedMonth);
        Integer day1 = new Integer(calculatedDate);
        String yearS = year1.toString();
        String monthS = month1.toString();
        String dayS = day1.toString();
        return dayS + ":" + monthS + ":" + yearS;

    }

    private void populateList() {
        List<String> PetDetail = new ArrayList<>();
        List<Item> mItemList = new ArrayList<>();
        PetDetail.add("General Information");
        PetDetail.add("Medical History");
        PetDetail.add("Medications");
        PetDetail.add("Diet");
        PetDetail.add("Preventative Medicine");
        PetDetail.add("Veterinarian Information");
        PetDetail.add("Appointments");
        PetDetail.add("Insurance Information");
        PetDetail.add("Documents");
        PetDetail.add(getString(R.string.weight_tracker));
        PetDetail.add(getString(R.string.pet_journal));
        PetDetail.add("Notes");

        for (int i = 0; i < PetDetail.size(); i++) {
            Item nameDetail = new Item();
            String name = PetDetail.get(i);
            nameDetail.setTitle(name);
            nameDetail.setViewType(Constants.PET_MAIN_DETAIL);
            mItemList.add(nameDetail);
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PetDetailActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);

        mAdapter = new BaseAdapter(mItemList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListner((postion, view) -> {
            switch (postion) {
                case 0: {
                    Intent intent = new Intent(PetDetailActivity.this, GeneralInformationDetailActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(Constants.PET_MODEL_OBJECT, new Gson().toJson(model)); // put pet model
                    //intent.putExtra(Constants.PET_AGE, finalAge);
                    startActivity(intent);
                    break;
                }
                case 1: {
                    Intent intent = new Intent(PetDetailActivity.this, MedicalHistoryActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 2: {
                    Intent intent = new Intent(PetDetailActivity.this, MedicationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 3: {
                    Intent intent = new Intent(PetDetailActivity.this, DietActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 4: {
                    Intent intent = new Intent(PetDetailActivity.this, PreventiveCategoryActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 5: {
                    Intent intent = new Intent(PetDetailActivity.this, VeterinarianInformationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 6: {
                    Intent intent = new Intent(PetDetailActivity.this, AppointmentActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 7: {
                    Intent intent = new Intent(PetDetailActivity.this, InsuranceInformationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 9: {
                    Intent intent = new Intent(PetDetailActivity.this, WeightTrackerActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 10: {
                    Intent intent = new Intent(PetDetailActivity.this, JournalActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                case 11: {

                    Intent intent = new Intent(PetDetailActivity.this, NotesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
                default: {
                    //UtilityClass.showPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.under_construction));
                    Intent intent = new Intent(PetDetailActivity.this, DocumentsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(NetworkUtils.PET_ID, String.valueOf(model.getId()));
                    startActivity(intent);
                    break;
                }
            }

        });

    }


    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
