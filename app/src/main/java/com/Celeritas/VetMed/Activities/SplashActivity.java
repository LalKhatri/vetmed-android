package com.Celeritas.VetMed.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.Celeritas.VetMed.Activities.pets.MyPetsActivity;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.UserModel;
import com.Celeritas.VetMed.R;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStatusBarGradiant(this);
        setContentView(R.layout.activity_splash);

        Handler handler=  new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                UserModel userModel = SessionClass.getInstance().getUser(SplashActivity.this);
                if (userModel != null){
                    startActivity(new Intent(SplashActivity.this,MyPetsActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(SplashActivity.this,SignInActivity.class));
                    finish();
                }


            }
        },3000);
    }
}
