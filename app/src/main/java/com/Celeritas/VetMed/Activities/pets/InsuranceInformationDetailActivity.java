package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.InsuranceModelUpdated;
import com.Celeritas.VetMed.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class InsuranceInformationDetailActivity extends BaseActivity implements IActionBar {

    private RecyclerView mRecyclerView;
    private InsuranceModelUpdated model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_insurance_information_detail);

        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_insurance_information_detail, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.white_pencil));
        toolbarTitle.setText(getResources().getString(R.string.insurance_information));
        toolbarTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);

        initialize();
    }

    private void initialize() {
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            model = new Gson().fromJson(getIntent().getStringExtra(Constants.PET_INSURANCE_OBJECT), InsuranceModelUpdated.class);
        }
        populateList(model);
    }

    private void populateList(InsuranceModelUpdated model) {


            List<Item> mItemList = new ArrayList<>();
            Item Name = new Item();
            String name = getResources().getString(R.string.name_of_insurance_company);
            Name.setTitle(name);
            Name.setDescription(model.getInsuranceInfoName());
            Name.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(Name);

            Item Policy = new Item();
            String polic = getResources().getString(R.string.policy);
            Policy.setTitle(polic);
            Policy.setDescription(model.getInsuranceInfoPolicyNumber());
            Policy.setViewType(Constants.GENERAL_INFORMATION_DETAIL);
            mItemList.add(Policy);

            Item EMAIL = new Item();
            String email = getResources().getString(R.string.veterinarian_email);
            EMAIL.setTitle(email);
            EMAIL.setDescription(model.getInsuranceInfoEmail());
            EMAIL.setImage(R.drawable.email_orange);
            EMAIL.setViewType(Constants.VETERINARIAN);
            mItemList.add(EMAIL);

            Item phoneNumber = new Item();
            String phone = getResources().getString(R.string.phone);
            phoneNumber.setTitle(phone);
            phoneNumber.setDescription(model.getInsuranceInfoPhoneNumber());
            phoneNumber.setImage(R.drawable.phone);
            phoneNumber.setViewType(Constants.VETERINARIAN);
            mItemList.add(phoneNumber);

            Item webSite = new Item();
            String site = getResources().getString(R.string.website);
            webSite.setTitle(site);
            webSite.setDescription(model.getInsuranceInfoWebsite());
            webSite.setImage(R.drawable.web);
            webSite.setViewType(Constants.VETERINARIAN);
            mItemList.add(webSite);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            BaseAdapter mAdapter = new BaseAdapter(mItemList);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.setOnItemClickListner((postion, view) -> {
                switch (postion){
                    case 2:{
                        //UtilityClass.showPopup(this,model.getInsuranceInfoEmail(),"Need clarification");
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{model.getInsuranceInfoEmail()});
                        i.putExtra(Intent.EXTRA_SUBJECT, model.getInsuranceInfoName());
                        //testtesi.putExtra(Intent.EXTRA_TEXT   , "Need clarification");
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(InsuranceInformationDetailActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    }
                    case 3:{
                        String number = model.getInsuranceInfoPhoneNumber();
                        String uri = "tel:" + number.trim() ;
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(uri));
                        startActivity(intent);
                        break;
                    }
                    case 4:{
                        String url = model.getInsuranceInfoWebsite();
                        if (!url.startsWith("http://") && !url.startsWith("https://"))
                            url = "http://" + url;
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                        break;
                    }
                    default:{
                        break;
                    }
                }
            });

    }

    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
    onBackPressed();
    }

    @Override
    public void RightClick() {
        Intent intent = new Intent(InsuranceInformationDetailActivity.this,InsuranceInformationAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PET_INSURANCE_OBJECT,new Gson().toJson(model));
        startActivity(intent);
    }

    @Override
    protected void onRestart() {
        model = InsuranceModelUpdated.findById(InsuranceModelUpdated.class,model.getId());
        populateList(model);
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
