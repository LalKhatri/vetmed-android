package com.Celeritas.VetMed.Activities.pets;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.GalleryAdapter;
import com.Celeritas.VetMed.BuildConfig;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.GalleryModel;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static com.Celeritas.VetMed.Adapters.GalleryAdapter.rotateImage;
import static com.Celeritas.VetMed.Constants.IMAGES_PATH;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;

public class GalleryActivity extends BaseActivity implements IActionBar {

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE = 112;
    private LinearLayout uploadPicture;
    private RecyclerView mRecycler;
    private static final int NO_OF_COLUMNS = 3;
    private static final int MAX_IMAGES_ALLOWED = 10;
    public static final int CAMERA_REQUEST_CODE = 110;
    private static final int GALLERY_REQUEST_CODE = 111;
    public static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    List<String> imageKeyList;
    List<String> selectedImagePathList;
    List<String> capturedImageList;
    List<String> imageKeyListUpdate;
    List<String> selectedImagePathListUpdate;
    private Uri fileUri;
    private String petID;
    private long petIDSelected;
    private Integer userID;
    private CheckBox randomCheckbox;
    private AmazonS3Client s3Client;
    private String selectedPetProfileImage = "NA";
    private GalleryAdapter adapter;
    private ArrayList<GalleryModel> galleryModelList;
    private boolean isFromOptions = false;
    private String tempImageName;
    private String petImages;
    private ImageView iv_selected_image;
    private Bitmap bmp;
    private PetModelUpdated model;
    // image rotation issue resolved by below link
    //https://stackoverflow.com/questions/14066038/why-does-an-image-captured-using-camera-intent-gets-rotated-on-some-devices-on-a/31720143

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_gallary);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_gallary, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.add));
        toolbarTitle.setText(getResources().getString(R.string.gallery));

        initialize();
    }

    private void initialize() {
        randomCheckbox = findViewById(R.id.checkbox);
        uploadPicture = findViewById(R.id.add);
        mRecycler = findViewById(R.id.recyclerView);
        iv_selected_image = findViewById(R.id.iv_selected_image);
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Bundle extras = getIntent().getExtras();
        if (extras != null){
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            petIDSelected = Long.valueOf(petID);
            userID = getIntent().getIntExtra(NetworkUtils.USER_ID,0);
            petImages = getIntent().getStringExtra(Constants.PET_PROFILE_IMAGE_LIST);
            imageKeyList = new ArrayList<>();
            selectedImagePathList = new ArrayList<>();
            capturedImageList = new ArrayList<>();
            try {
                model = PetModelUpdated.findById(PetModelUpdated.class,petIDSelected);
                if (petImages !=null && !petImages.equalsIgnoreCase("NA")){
                    // String petImages = getIntent().getStringExtra(Constants.PET_PROFILE_IMAGE_LIST);
                    imageKeyListUpdate = new ArrayList<>();
                    selectedImagePathListUpdate = new ArrayList<>();
                    List<String> items = Arrays.asList(petImages.split("\\s*,\\s*"));
                    imageKeyList.addAll(items);
                    for (int i=0; i<imageKeyList.size();i++){
                        capturedImageList.add("");
                        selectedImagePathList.add("");
                    }
                    populateList(imageKeyList,capturedImageList);
                }
            }catch (Exception e){
                e.printStackTrace();
            }



        }


        uploadPictureClick();
        randomCheckboxClick();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,PERMISSIONS_STORAGE,MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE);
        }
        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(activity, "Save", Toast.LENGTH_SHORT).show();
                if (imageKeyList.size() > 0) {
                    if (!selectedPetProfileImage.equalsIgnoreCase("NA")) {
/*                        try {
                            if (petImages != null && petImages.equalsIgnoreCase("NA")) {
                                for (int i = 0; i < imageKeyList.size(); i++) {
                                    //uploadImage(imageKeyList.get(i), selectedImagePathList.get(i));
                                }
                            } else {
                                for (int i = 0; i < imageKeyListUpdate.size(); i++) {
                                    //uploadImage(imageKeyListUpdate.get(i), selectedImagePathListUpdate.get(i));
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/
                        PetModelUpdated selectedPet = PetModelUpdated.findById(PetModelUpdated.class,petIDSelected);
                        if (bmp!=null){
                            if (!selectedPet.getPetImages().equalsIgnoreCase("NA")){
                                createDirectoryAndSaveFile(bmp,selectedPet.getId()+"","Image_"+""+selectedPet.getPetImages().split(",").length);
                                if (selectedPet.getPetImages() !=null && selectedPet.getPetImages().equalsIgnoreCase("NA")){
                                    selectedPet.setPetImages(selectedPet.getPetImages().replace("NA","")+""+"Image_"+selectedPet.getPetImages().split(",").length);
                                }else {
                                    if (selectedPet.getPetImages() !=null){
                                        selectedPet.setPetImages(selectedPet.getPetImages().replace("NA,","")+","+"Image_"+selectedPet.getPetImages().split(",").length);
                                    }

                                }
                            }else {
                                createDirectoryAndSaveFile(bmp,selectedPet.getId()+"","Image_0");
                                selectedPet.setPetImages(selectedPet.getPetImages().replace("NA","")+""+"Image_0");

                            }
                        }

                        selectedPet.setPetName(selectedPet.getPetName());
                        selectedPet.setPetSex(selectedPet.getPetSex());
                        selectedPet.setPetBreed(selectedPet.getPetBreed());
                        selectedPet.setPetDOB(selectedPet.getPetDOB());
                        selectedPet.setPetWeight(selectedPet.getPetWeight());
                        selectedPet.setPetWeightUnit(selectedPet.getPetWeightUnit());
                        selectedPet.setUpdateDateTime(getCurrentDate());
                        selectedPet.setProfileImage(selectedPetProfileImage);
                        selectedPet.save();
                        showPopup(GalleryActivity.this,"Success","Picture successfully added" );
                        //String petImages = converListToComaSeperated(imageKeyList);
                        //sendPetImagesData(NetworkUtils.ADD_PET_IMAGES, petID, selectedPetProfileImage, petImages);
                    } else
                        UtilityClass.showPopup(GalleryActivity.this, getString(R.string.alert), getString(R.string.please_select_1_image));


                } else
                    UtilityClass.showPopup(GalleryActivity.this, getString(R.string.alert), getString(R.string.please_add_atleast_1_image));
            }
        });

    }

    private void randomCheckboxClick() {
        randomCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                adapter = new GalleryAdapter(galleryModelList,petImages);
                mRecycler.setLayoutManager(new GridLayoutManager(getBaseContext(), NO_OF_COLUMNS));
                mRecycler.setAdapter(adapter);
                adapter.setOnItemClickListner((postion, view) -> {
                    randomCheckbox.setChecked(false);
                    selectedPetProfileImage = imageKeyList.get(postion);
                    if (!selectedPetProfileImage.contains("Image_")){
                        selectedPetProfileImage = "Image_"+postion;
                    }else {
                        selectedPetProfileImage = imageKeyList.get(postion);
                    }

                    //Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
                });
                if (b) {
                    selectedPetProfileImage = "Randomly";
                }
                else{
                    selectedPetProfileImage = "NA";
                }

            }
        });
    }

    public void Save(View view){
/*        Toast.makeText(activity, "Save", Toast.LENGTH_SHORT).show();
        if (imageKeyList.size() > 0) {
            if (!selectedPetProfileImage.equalsIgnoreCase("NA")) {
                try {
                    if (petImages.equalsIgnoreCase("NA")) {
                        for (int i = 0; i < imageKeyList.size(); i++) {
                            //uploadImage(imageKeyList.get(i), selectedImagePathList.get(i));
                        }
                    } else {
                        for (int i = 0; i < imageKeyListUpdate.size(); i++) {
                            //uploadImage(imageKeyListUpdate.get(i), selectedImagePathListUpdate.get(i));
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                createDirectoryAndSaveFile(bmp,"Image1");
                PetModelUpdated selectedPet = PetModelUpdated.findById(PetModelUpdated.class,petIDSelected);
                selectedPet.setPetName(selectedPet.getPetName());
                selectedPet.setPetSex(selectedPet.getPetSex());
                selectedPet.setPetBreed(selectedPet.getPetBreed());
                selectedPet.setPetDOB(selectedPet.getPetDOB());
                selectedPet.setPetMode(selectedPet.getPetMode());
                selectedPet.setPetWeightUnit(selectedPet.getPetWeightUnit());
                selectedPet.setUpdateDateTime(getCurrentDate());
                selectedPet.setPetImages(selectedPet.getPetImages()+","+"Image1");
                selectedPet.setProfileImage(selectedPetProfileImage);
                selectedPet.save();
                //String petImages = converListToComaSeperated(imageKeyList);
                //sendPetImagesData(NetworkUtils.ADD_PET_IMAGES, petID, selectedPetProfileImage, petImages);
            } else
                UtilityClass.showPopup(this, getString(R.string.alert), getString(R.string.please_select_1_image));


        } else
            UtilityClass.showPopup(this, getString(R.string.alert), getString(R.string.please_add_atleast_1_image));*/
    }

    public void Cancel(View view){
        onBackPressed();
    }

    private void uploadPictureClick() {
        uploadPicture.setOnClickListener(view -> {
            if (imageKeyList.size() != MAX_IMAGES_ALLOWED)
                selectImage();
            else
                UtilityClass.showPopup(this,getString(R.string.alert),getString(R.string.message));
        });

    }

    private void populateList(@Nullable List<String> imageKeyList, @Nullable List<String> capturedImageList){
        galleryModelList = new ArrayList<>();
        List<String> imagesList = new ArrayList<>();
        /*imagesList.add("https://www.gettyimages.co.uk/gi-resources/images/Embed/new/embed2.jpg");
        imagesList.add("https://cdn.pixabay.com/photo/2016/10/27/22/53/heart-1776746_960_720.jpg");
        imagesList.add("https://cdn.pixabay.com/photo/2017/05/09/21/49/gecko-2299365_960_720.jpg");
        imagesList.add("http://lusile17.l.u.pic.centerblog.net/debbf893.jpg");
        imagesList.add("https://www.yamatojk.co.jp/wordpress/wp-content/uploads/2016/12/mv_04.jpg");
        imagesList.add("https://media.koreus.com/201504/135-insolite-01.jpg");
        imagesList.add("https://www.elastic.co/assets/bltada7771f270d08f6/enhanced-buzz-1492-1379411828-15.jpg");
        imagesList.add("https://data.whicdn.com/images/77650762/original.jpg");*/
        uploadPicture.setVisibility(View.GONE);
        randomCheckbox.setVisibility(View.VISIBLE);
        for (int i = 0; i < imageKeyList.size(); i++){
            GalleryModel galleryModel = new GalleryModel();
            galleryModel.setServerImagePath(IMAGES_PATH+model.getId()+"/"+imageKeyList.get(i)+ ".jpg");
            galleryModel.setLocalImagePath(capturedImageList.get(i));
            galleryModelList.add(galleryModel);
        }

        adapter = new GalleryAdapter(galleryModelList,petImages);
        mRecycler.setLayoutManager(new GridLayoutManager(getBaseContext(), NO_OF_COLUMNS));
        mRecycler.setAdapter(adapter);
        adapter.setOnItemClickListner((postion, view) -> {
            selectedPetProfileImage = imageKeyList.get(postion);
            if (!selectedPetProfileImage.contains("Image_")){
                selectedPetProfileImage = "Image_"+postion;
            }else {
                selectedPetProfileImage = imageKeyList.get(postion);
            }

            //Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
        });

        adapter.setOnItemLongClickListner((position, view) -> {
            //Toast.makeText(this, "Long", Toast.LENGTH_SHORT).show();
        });
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    cameraIntent();
                    isFromOptions = true;
                } else if (items[item].equals("Choose from Gallery")) {
                    galleryIntent();
                    isFromOptions = false;
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }

    public void cameraIntent() {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempImageName = String.valueOf(System.currentTimeMillis()) + ".jpg";
        File file = new File(getExternalCacheDir(),
                tempImageName);
        // 7.0 FileProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fileUri = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);
            i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            fileUri = Uri.fromFile(file);
        }

        i.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (ActivityCompat.checkSelfPermission(GalleryActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(GalleryActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(GalleryActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                ) {
            ActivityCompat.requestPermissions(GalleryActivity.this, PERMISSIONS_STORAGE, MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE);
        }else {
            if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA}, PackageManager.PERMISSION_GRANTED);
            }else {
                startActivityForResult(i, CAMERA_REQUEST_CODE);
            }

        }

    }

// camera permission help
    //https://stackoverflow.com/questions/43042725/revoked-permission-android-permission-camera/43070198
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    handleSamplingAndRotationBitmap(this,fileUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                getImageData(fileUri);
            }
        }
        if (requestCode == GALLERY_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                fileUri = data.getData();
                try {
                    handleSamplingAndRotationBitmap(this,fileUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                getImageData(fileUri);
            }
        }
        selectedPetProfileImage = "NA";
        randomCheckbox.setChecked(false);
    }

    private void getImageData(Uri selectedImageUri) {
        if (selectedImageUri != null) {
            if (!selectedImageUri.equals("")) {
                SimpleDateFormat sdf = new SimpleDateFormat(Constants.IMAGE_FORMAT);
                String currentDateandTime = sdf.format(new Date());
                String ImageData = userID + "_" +petID + "_"+currentDateandTime;
                try {
                    handleSamplingAndRotationBitmap(this,selectedImageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                imageKeyList.add(ImageData);
                String selectedImagePath = null;

                try {
                    bmp =handleSamplingAndRotationBitmap(this,selectedImageUri);// MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (isFromOptions)
                    selectedImagePath = getCacheFile(this,tempImageName);
                else
                    selectedImagePath = getRealPathFromURI(getImageUri(this,bmp));

                selectedImagePathList.add(selectedImagePath);
                // bmp = BitmapFactory.decodeFile(getRealPathFromURI(selectedImageUri));
                    selectedImageUri = getImageUri(this,bmp);
                capturedImageList.add("");
                int i = imageKeyList.size();
                capturedImageList.set(i-1,selectedImageUri.toString());
                try{
                    if (petImages !=null && !petImages.equalsIgnoreCase("NA")){
                        imageKeyListUpdate.add(ImageData);
                        selectedImagePathListUpdate.add(selectedImagePath);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                //iv_selected_image.setImageBitmap(getImageFileFromSDCard("VetMedImage_"));

                //iv_selected_image.setImageBitmap(BitmapFactory.decodeFile((IMAGES_PATH+"Image1.jpg")));

                populateList(imageKeyList,capturedImageList);
            }
        }

    }

    public String getRealPathFromURI(Uri contentURI) {
        try {
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = null;
            int columnIndex = 0;
            String fullPicturePath = null;
            cursor = getContentResolver().query(contentURI, filePathColumn, null, null, null);
            cursor.moveToFirst();
            columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            fullPicturePath = cursor.getString(columnIndex);

            return fullPicturePath;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getCacheFile(Context context, String uniqueName) {
        String cachePath = null;
        if ((Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable())
                && context.getExternalCacheDir() != null) {
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        // return new File(cachePath + File.separator + uniqueName);
        return cachePath+ File.separator + uniqueName;
    }
    @Override
    public void MenuClick() {

    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        if (imageKeyList.size() != MAX_IMAGES_ALLOWED)
            selectImage();
        else
            UtilityClass.showPopup(this,getString(R.string.alert),getString(R.string.message));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void createDirectoryAndSaveFile(Bitmap imageToSave,String folderName, String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.VetMed/.VetMed_PET_"+folderName+"/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.VetMed/.VetMed_PET_"+folderName+"/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.VetMed/.VetMed_PET_"+folderName+"/"), fileName+".jpg");
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            bmp = null;
            showPopup(this,"Success","Picture successfully added" );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void showPopup(Context context, String title, String message){
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.custom_dailog, null);
        alertDialogBuilder.setView(promptsView);
        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        ((TextView)promptsView.findViewById(R.id.alert_title)).setText(title);
        ((TextView)promptsView.findViewById(R.id.msg)).setText(message);
        TextView ok = (TextView) promptsView.findViewById(R.id.alert_ok);
        ok.setOnClickListener(v -> {
            alertDialog.dismiss();
            onBackPressed();
        });
        alertDialog.show();
    }
    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }
    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }
    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }
    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
