package com.Celeritas.VetMed.Activities.pets.documents;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Activities.ResourcesActivity;
import com.Celeritas.VetMed.Activities.ResourcesSubActivity;
import com.Celeritas.VetMed.Activities.pets.GalleryActivity;
import com.Celeritas.VetMed.Activities.pets.PetDetailActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Adapters.WeightTrackerAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.Models.pets.WeightTrackerModel;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE;
import static com.Celeritas.VetMed.Activities.pets.GalleryActivity.PERMISSIONS_STORAGE;
import static com.Celeritas.VetMed.Constants.WEIGHT_TRACKER;

public class DocumentsActivity extends BaseActivity implements IActionBar {

    private RecyclerView recyclerView;
    private BaseAdapter mAdapter;
    private String petId;
    private String documentType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_diet);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_documents, content, true);

        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.documents));

        initialize();
    }

    private void initialize() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        Bundle extras = getIntent().getExtras();
        if (extras != null){

        }
        populateList();

    }



    @Override
    public void MenuClick() {

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    private void populateList() {
        List<Item> mItemList = new ArrayList<>();
        Item diagnosticTest = new Item();
        String name1 = getResources().getString(R.string.diagnostic);
        diagnosticTest.setTitle(name1);
        diagnosticTest.setViewType(Constants.PET_MAIN_DETAIL);
        mItemList.add(diagnosticTest);

        Item dischargestatements = new Item();
        String name = getResources().getString(R.string.discharge_statements);
        dischargestatements.setTitle(name);
        dischargestatements.setViewType(Constants.PET_MAIN_DETAIL);
        mItemList.add(dischargestatements);



        Item billsReceipts = new Item();
        String name2 = getResources().getString(R.string.bills_receipts);
        billsReceipts.setTitle(name2);
        billsReceipts.setViewType(Constants.PET_MAIN_DETAIL);
        mItemList.add(billsReceipts);

        Item others = new Item();
        String name3 = getResources().getString(R.string.others);
        others.setTitle(name3);
        others.setViewType(Constants.PET_MAIN_DETAIL);
        mItemList.add(others);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BaseAdapter(mItemList);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListner((postion, view) -> {
            /*Intent intent = new Intent(this,ResourcesSubActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.RESOURSE_NAME,mItemList.get(postion).getTitle());
            startActivity(intent);*/
            //Toast.makeText(activity, "", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(Intent.ACTION_PICK);
//            intent.setType("*/*");
//            startActivityForResult(intent, 7);
            openDocumentsGallery(mItemList.get(postion).getTitle());


        });
    }

    private void openDocumentsGallery(String documentType) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, MY_PERMISSIONS_REQUEST_ACCESS_WRITE_STORAGE);
        } else {
            Intent intent = new Intent(this, DocumentsGalleryActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(NetworkUtils.PET_ID,getIntent().getStringExtra(NetworkUtils.PET_ID));
            intent.putExtra(getString(R.string.doc_type),documentType);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub

        switch (requestCode) {
            case 7:
                if (resultCode == RESULT_OK) {
                    String PathHolder = data.getData().getPath();
                    File myFile = new File(PathHolder);
                    Toast.makeText(this, PathHolder, Toast.LENGTH_LONG).show();
                    try {
                        FileOpen.openFile(this, myFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(activity, "File does not exist", Toast.LENGTH_SHORT).show();
                    }

                }
                break;
        }
    }
    public static class FileOpen {

        public static void openFile(Context context, File url) throws IOException {
            // Create URI
            File file=url;
            Uri uri = Uri.fromFile(file);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if(url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if(url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if(url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if(url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if(url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if(url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if(url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if(url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if(url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if(url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);
        }
    }
}
