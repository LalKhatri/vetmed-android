package com.Celeritas.VetMed.Activities.pets;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.pets.AppointmentModelUpdated;
import com.Celeritas.VetMed.Models.pets.PetModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.Celeritas.VetMed.Activities.pets.DietAddActivity.hideOrShowKeyboard;
import static com.Celeritas.VetMed.Constants.ignoreFirstWhiteSpace;
import static com.Celeritas.VetMed.Constants.sdf;
import static com.Celeritas.VetMed.Utility.UtilityClass.getCurrentDate;
import static com.Celeritas.VetMed.Utility.UtilityClass.months;

public class AppointmentAddActivity extends BaseActivity implements IActionBar {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_WRITE_CALENDER = 1001;
    private TextView appointmentDate;
    private TextView appointmentTime;
    private TextView appointmentLocation;
    private TextView appointmentDrName;
    private Spinner spinnerAddToCalender;
    private TextView appointmentNote;
    private Calendar myCalendar,myCalendarCurrent;
    private ArrayList<String> AddToCalender;
    private Bundle extras;
    private AppointmentModelUpdated model;
    private boolean addToCalender = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_appointment_add);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_appointment_add, content, true);
        MenuClick.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.appointment));
        initialize();
    }
    private void initialize() {
        appointmentDate = findViewById(R.id.datepicker);
        appointmentTime = findViewById(R.id.timepicker);
        appointmentLocation = findViewById(R.id.appointmentLocation);
        appointmentDrName = findViewById(R.id.appointmentDrName);
        spinnerAddToCalender = findViewById(R.id.spinnerAddToCalender);
        appointmentNote = findViewById(R.id.note);
       // RightClick.setVisibility(View.GONE);


        myCalendar = Calendar.getInstance();
        myCalendarCurrent = Calendar.getInstance();

        datepickerClick();
        setSpinnerAddToCalender();
        extras = getIntent().getExtras();
        if (extras != null) {
            model = new Gson().fromJson(extras.getString(Constants.PET_APPOINTMENT_OBJECT), AppointmentModelUpdated.class);
            UpdatePetAppointmentData(model);
        }else {
            toolbarTitle.setText(getResources().getString(R.string.appointment));
        }

        timePickerClick();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CALENDAR}, MY_PERMISSIONS_REQUEST_ACCESS_WRITE_CALENDER);

        }
        appointmentNote.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        appointmentNote.setImeOptions(EditorInfo.IME_ACTION_DONE);
        appointmentNote.setRawInputType(InputType.TYPE_CLASS_TEXT);
        appointmentNote.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        appointmentDrName.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
        appointmentLocation.setFilters(new InputFilter[]{ignoreFirstWhiteSpace()});
    }
    private void timePickerClick() {
        appointmentTime.setOnClickListener((View view) -> {
            Calendar mcurrentTime = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.TIME_FORMAT);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    mcurrentTime.set(Calendar.HOUR_OF_DAY, selectedHour);
                    mcurrentTime.set(Calendar.MINUTE, selectedMinute);
                    String time = sdf.format(mcurrentTime.getTime());
                    appointmentTime.setText(time);
                }
            }, mcurrentTime.get(Calendar.HOUR_OF_DAY), mcurrentTime.get(Calendar.MINUTE), false);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        });
    }
    //set previous data for update
    private void UpdatePetAppointmentData(AppointmentModelUpdated model) {

        if (model != null) {
            appointmentDate.setText(model.getAppointmentDate());
            appointmentTime.setText(model.getAppointmentTime());
            appointmentLocation.setText(model.getAppointmentLocation());
            appointmentDrName.setText(model.getAppointmentDoctorName());
            appointmentNote.setText(model.getAppointmentNotes());
                /*for (int i = 0; i < AddToCalender.size(); i++) {
                    if (AddToCalender.get(i).contains(model.get)) {
                        spinnerAddToCalender.setSelection(i);
                    }
                }*/


            toolbarTitle.setText(getResources().getString(R.string.edit));
            ((TextView) findViewById(R.id.save)).setText(getResources().getString(R.string.update));
            findViewById(R.id.addToCalenderTextview).setVisibility(View.GONE);
            for (int i = 0; i < AddToCalender.size(); i++) {
                if (AddToCalender.get(i).contains(model.getAddToCalendar())) {
                    spinnerAddToCalender.setSelection(i);
                }
            }
            spinnerAddToCalender.setVisibility(View.GONE);
           // RightClick.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.delete));
        }

    }
    private void datepickerClick() {
        appointmentDate.setOnClickListener(view -> {
            PetModelUpdated petModel = PetModelUpdated.findById(PetModelUpdated.class,SessionClass.getInstance().getPetModelUpdated(this).getId());
            String dob = petModel.getPetDOB();
            String array1[] = dob.split(" ");
            int date = Integer.valueOf(array1[0]);
            int month = Arrays.asList(months).indexOf(array1[1])-1;
            int year1 = Integer.valueOf(array1[2]);
            int mDate = 0;
            int mMonth = 0;
            int mYear1 = 0;
            if (appointmentDate.getText().toString() != null && !appointmentDate.getText().toString().equalsIgnoreCase("")) {
                String medicationDate[] = appointmentDate.getText().toString().split(" ");
                mDate = Integer.valueOf(medicationDate[0]);
                mMonth = Arrays.asList(months).indexOf(medicationDate[1])-1;
                mYear1 = Integer.valueOf(medicationDate[2]);
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                }).minDate(year1, month, date)
                        .defaultDate(/*myCalendar.get(Calendar.YEAR)*/mYear1, /*myCalendar.get(Calendar.MONTH)*/mMonth, /*myCalendar.get(Calendar.DAY_OF_MONTH)*/mDate)
                        .build()
                        .show();
            } else {
                new SpinnerDatePickerDialogBuilder().context(this).callback((view1, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(year, monthOfYear, dayOfMonth);
                    setSelectedDate();
                }).minDate(year1, month, date)
                        .defaultDate(myCalendarCurrent.get(Calendar.YEAR), myCalendarCurrent.get(Calendar.MONTH), myCalendarCurrent.get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });
    }
    private void setSpinnerAddToCalender() {
        AddToCalender = new ArrayList<>();
        AddToCalender.add(getResources().getString(R.string.select));
        AddToCalender.add(getResources().getString(R.string.yes));
        AddToCalender.add(getResources().getString(R.string.no));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.row_spinner, AddToCalender);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAddToCalender.setAdapter(dataAdapter);
        spinnerAddToCalender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideOrShowKeyboard(AppointmentAddActivity.this);
                return false;
            }
        });
        spinnerAddToCalender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 2: {
                        addToCalender = false;
                        break;
                    }
                    case 1: {
                        addToCalender = true;
                        break;
                    }
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private void setSelectedDate() {
        appointmentDate.setText(sdf.format(myCalendar.getTime()));
    }
    private boolean isEmpty() {
        if (appointmentDate.getText().length() > 0 && appointmentTime.getText().length() > 0 &&
                appointmentDrName.getText().length() > 0 && appointmentLocation.getText().length() > 0
                && !spinnerAddToCalender.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select))) {
            return false;
        }
        return true;
    }
    public void Save(View view) {
        if (isEmpty()) {
            UtilityClass.showPopup(this, getResources().getString(R.string.alert),
                    getResources().getString(R.string.all_field_must_be_enter));
            return;
        }

        String appointmentdate1 = appointmentDate.getText().toString();
        String appointmentTime1 = appointmentTime.getText().toString();
        String appointmentDrName1 = appointmentDrName.getText().toString();
        String appointmentLocation1 = appointmentLocation.getText().toString();
        String appointmentNotes = appointmentNote.getText().toString();
        String petName = SessionClass.getInstance().getPetModelUpdated(this).getPetName();

        if (model == null) {
            String petID = extras.getString(NetworkUtils.PET_ID);
            AppointmentModelUpdated appointment = new AppointmentModelUpdated(petID, appointmentdate1, appointmentTime1, appointmentLocation1, appointmentDrName1, appointmentNotes, getCurrentDate(), spinnerAddToCalender.getSelectedItem().toString());
            appointment.save();
            if (addToCalender) {
                addToDeviceCalendar(appointmentdate1 + " " + appointmentTime1, appointmentdate1, petName+"'s appointment", appointmentNotes, appointmentLocation1);
            }
            onBackPressed();
        } else {
            UtilityClass.showDeletPopup(this, getResources().getString(R.string.alert), getResources().getString(R.string.update_confirmation), new IAlertListner() {
                @Override
                public void onYesClick() {
                    AppointmentModelUpdated selectedAppoinment = AppointmentModelUpdated.findById(AppointmentModelUpdated.class, model.getId());
                    selectedAppoinment.setPetID(model.getPetID());
                    selectedAppoinment.setAppointmentDate(appointmentdate1);
                    selectedAppoinment.setAppointmentTime(appointmentTime1);
                    selectedAppoinment.setAppointmentLocation(appointmentLocation1);
                    selectedAppoinment.setAppointmentDoctorName(appointmentDrName1);
                    selectedAppoinment.setAppointmentNotes(appointmentNotes);
                    selectedAppoinment.setUpdateDateTime(getCurrentDate());
                    selectedAppoinment.setAddToCalendar(spinnerAddToCalender.getSelectedItem().toString());
                    selectedAppoinment.save();
                    if (addToCalender) {
                        addToDeviceCalendar(appointmentdate1 + " " + appointmentTime1, appointmentdate1, petName, appointmentNotes, appointmentLocation1);
                    }
                    onBackPressed();

                }

                @Override
                public void onNoClick() {
                }
            });


        }


    }
    public void Cancel(View view) {
        onBackPressed();
    }
    private void addToDeviceCalendar(String startDate, String endDate, String title, String description, String location) {

        String stDate = startDate;
        String enDate = endDate;
        GregorianCalendar calDate = new GregorianCalendar();
        SimpleDateFormat originalFormat = new SimpleDateFormat(Constants.DATE_FORMAT_TWO + " " + Constants.TIME_FORMAT);
        SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy,MM,dd,HH,mm");
        Date date, edate;
        try {
            date = originalFormat.parse(startDate);
            stDate = targetFormat.format(date);

        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        long startMillis = 0;
        long endMillis = 0;
        String dates[] = stDate.split(",");
        String SD_YeaR = dates[0];
        String SD_MontH = dates[1];
        String SD_DaY = dates[2];
        String SD_HouR = dates[3];
        String SD_MinutE = dates[4];
        calDate.set(Integer.parseInt(SD_YeaR), Integer.parseInt(SD_MontH) - 1, Integer.parseInt(SD_DaY), Integer.parseInt(SD_HouR), Integer.parseInt(SD_MinutE));
        startMillis = calDate.getTimeInMillis();
        int hasAlarm = 1;
        try {
            ContentResolver cr = this.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, calDate.getTimeInMillis() + 60 * 60 * 1000);
            values.put(CalendarContract.Events.TITLE, title);
            values.put(CalendarContract.Events.DESCRIPTION, description);
            values.put(CalendarContract.Events.EVENT_LOCATION, location);
            values.put(CalendarContract.Events.HAS_ALARM, true);
            values.put(CalendarContract.Events.CALENDAR_ID, 1);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance()
                    .getTimeZone().getID());
            System.out.println(Calendar.getInstance().getTimeZone().getID());
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            @SuppressLint("MissingPermission")
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

            long eventId = Long.parseLong(uri.getLastPathSegment());
            Log.d("Ketan_Event_Id", String.valueOf(eventId));
            if (hasAlarm == 1) {
                ContentValues reminders = new ContentValues();
                reminders.put(CalendarContract.Reminders.EVENT_ID, eventId);
                reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
                reminders.put(CalendarContract.Reminders.MINUTES, 60);

                Uri uri2 = cr.insert(CalendarContract.Reminders.CONTENT_URI, reminders);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void MenuClick() {}
    @Override
    public void LeftBackClick() {
        onBackPressed();
    }
    @Override
    public void RightClick() {}
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    protected void onRestart() {
        if (model != null) {
            model = AppointmentModelUpdated.findById(AppointmentModelUpdated.class, model.getId());
            UpdatePetAppointmentData(model);
        }
        super.onRestart();
    }
}
