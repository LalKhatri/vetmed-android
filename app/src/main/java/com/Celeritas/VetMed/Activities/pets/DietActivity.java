package com.Celeritas.VetMed.Activities.pets;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.BaseActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IActionBar;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Models.DietModel;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.DietModelUpdated;
import com.Celeritas.VetMed.Models.pets.MedicalHistoryModelUpdated;
import com.Celeritas.VetMed.Models.pets.MedicationModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.Celeritas.VetMed.Utility.WebserviceClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.Celeritas.VetMed.Constants.DIET;

public class DietActivity extends BaseActivity implements IActionBar {

    private Bundle extras;
    private RelativeLayout card_view;

    private RecyclerView mRecycler;
    private String petID;
    private List<DietModelUpdated> petDietModelList;
    BaseAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_diet);
        ViewGroup content = (ViewGroup) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_diet, content, true);

        MenuClick.setVisibility(View.GONE);
        RightClick.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.add));
        toolbarTitle.setText(getResources().getString(R.string.diet));


        initialize();
        card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDetails();
            }
        });
    }

    private void initialize() {
        card_view = (RelativeLayout) findViewById(R.id.no_data_found_layout);
        ((TextView) findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to)+" "+toolbarTitle.getText().toString().toLowerCase());
        mRecycler = (RecyclerView) findViewById(R.id.diet);
        extras = getIntent().getExtras();
        /*findViewById(R.id.info_text).setOnClickListener(view -> {
            addDetails();
        });*/


        if (extras != null){
            petID = getIntent().getStringExtra(NetworkUtils.PET_ID);
            populateList();
            //checkUpdatedDiet(petID);
        }
    }
    private void populateList() {
        petDietModelList = DietModelUpdated.findWithQuery(DietModelUpdated.class, "Select * from "+ DIET +" where petID = ? ", petID);
        if ((petDietModelList.size() == 0)) {
            card_view.setVisibility(View.VISIBLE);

        } else {
            card_view.setVisibility(View.GONE);

        }
        List<Item> mItemList = new ArrayList<>();
        for (DietModelUpdated dietModel: petDietModelList){
            Item nameDetail = new Item();
            nameDetail.setTitle(dietModel.getDietName());
            nameDetail.setViewType(Constants.PET_MAIN_DETAIL);
            mItemList.add(nameDetail);
        }
        adapter = new BaseAdapter(mItemList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setAdapter(adapter);
        setSwipeCallback();
        adapter.setOnItemClickListner((postion, view) -> {
            Intent intent = new Intent(this,DietDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.PET_DIET_OBJECT,new Gson().toJson(petDietModelList.get(postion)));
            startActivity(intent);
        });
    }


    @Override
    public void MenuClick() {

    }

    @Override
    protected void onRestart() {
        populateList();
        super.onRestart();
    }

    @Override
    public void LeftBackClick() {
        onBackPressed();
    }

    @Override
    public void RightClick() {
        addDetails();
    }

    private void addDetails() {
        Intent intent = new Intent(this,DietAddActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(NetworkUtils.PET_ID, petID);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    //SwipeToDelete Callback
    public void setSwipeCallback() {


        final SwipeToDeleteCallBack callBack = new SwipeToDeleteCallBack(this) {

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction) {
                if (direction == ItemTouchHelper.LEFT) {

                    UtilityClass.showDeletPopup(DietActivity.this, getResources().getString(R.string.alert), getResources().getString(R.string.diet_confirmation), new IAlertListner() {
                        @Override
                        public void onYesClick() {
                            DietModelUpdated selectedPet =  petDietModelList.get(viewHolder.getAdapterPosition());
                            DietModelUpdated note = DietModelUpdated.findById(DietModelUpdated.class, selectedPet.getId());
                            note.delete();
                            petDietModelList.remove(viewHolder.getAdapterPosition());
                            adapter.notifyDataSetChanged();
                            populateList();

                        }

                        @Override
                        public void onNoClick() {
                            mRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    });

                }
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                if (viewHolder != null) {
                    if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);
                    } else {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);

                    }
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                viewHolder.itemView.setBackgroundColor(Color.WHITE);


            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(callBack);
        helper.attachToRecyclerView(mRecycler);


    }
}
