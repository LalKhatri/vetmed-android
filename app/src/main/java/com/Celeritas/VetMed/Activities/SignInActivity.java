package com.Celeritas.VetMed.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.pets.MyPetsActivity;
import com.Celeritas.VetMed.Callback.WebserviceCallback;
import com.Celeritas.VetMed.CustomFontsWidget.EditextOpensenseRegular;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Helper.SharedPrefManager;
import com.Celeritas.VetMed.Models.UserModel;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.NetworkUtils;
import com.Celeritas.VetMed.Utility.ResourcesService;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.Celeritas.VetMed.Utility.WebserviceClient;
import com.klinker.android.link_builder.Link;
import com.klinker.android.link_builder.LinkBuilder;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

public class SignInActivity extends Activity {
    @BindView(R.id.email_et)
    EditText email_et;
    @BindView(R.id.pass_et)
    EditextOpensenseRegular pass_et;
    @BindView(R.id.create_an_account)
    TextView footer_tv;
    @BindView(R.id.checkbox)
    AppCompatCheckBox remember;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        pass_et.setTag(R.drawable.hide_pass_icon);

        setSpecificWordTextColor();

        initialize();

    }
    private void initialize() {
        if (getIntent().getStringExtra(NetworkUtils.USER_EMAIL) != null) {
            email_et.setText(getIntent().getStringExtra(NetworkUtils.USER_EMAIL));
        }else {
            if (new SharedPrefManager(this).getStringByKey(NetworkUtils.USER_EMAIL) != null && !new SharedPrefManager(this).getStringByKey(NetworkUtils.USER_EMAIL).isEmpty()){
                email_et.setText(new SharedPrefManager(this).getStringByKey(NetworkUtils.USER_EMAIL));
                pass_et.setText(new SharedPrefManager(this).getStringByKey(NetworkUtils.USER_PASSWORD));
                remember.setChecked(true);
            }
            else {
                remember.setChecked(false);
            }
        }


    }
    //this method is used to change color of textview word
    //like Click here
    private void setSpecificWordTextColor() {
        //Footer tv change Click here color & make it clickable
        Link link = new Link("Create new account")
                .setTextColor(getResources().getColor(R.color.orange))
                .setUnderlined(false)
                .setOnLongClickListener(new Link.OnLongClickListener() {
                    @Override
                    public void onLongClick(String clickedText) {
                        // long clicked
                    }
                })
                .setOnClickListener(new Link.OnClickListener() {
                    @Override
                    public void onClick(String clickedText) {
                        // single clicked
                        startActivity(new Intent(SignInActivity.this,
                                CreateAccountActivity.class));
                        //finish();
                    }
                });

        LinkBuilder.on(footer_tv)
                .addLink(link)
                .build();


    }
    //when user press sign in button this method will call
    public void signInClick(View view) {

        if (isEmpty()) {
            UtilityClass.showPopup(SignInActivity.this, getResources().getString(R.string.alert), getResources().getString(R.string.email_password_required));
            return;
        }

        if (!UtilityClass.isValidEmail(email_et.getText().toString())) {
            UtilityClass.showPopup(SignInActivity.this, getResources().getString(R.string.alert), getResources().getString(R.string.invalid_email));
            return;
        }

        String userEmail = email_et.getText().toString();
        String userPassword = pass_et.getText().toString();

        getLogin(NetworkUtils.LOGIN, userEmail, userPassword);
    }
    private void getLogin(String ServiceName, String userEmail, String userPassword) {
        WebserviceClient client = new WebserviceClient(this, ServiceName, true, loginUser(userEmail, userPassword), new WebserviceCallback() {
            @Override
            public void onResponseReceived(String jsonResponse) {
                Log.d("Response: ", jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    int responseStatus = jsonObject.getInt("status");
                    if (responseStatus == 1) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        Integer activationStatus = jsonObject1.getInt("Status");
                        Integer userId = jsonObject1.getInt("ID");
                        String userEmail = jsonObject1.getString("UserEmail");
                        String userName = jsonObject1.getString("UserName");
                        if (activationStatus == 1) {
                            if (remember.isChecked()){
                                new SharedPrefManager(SignInActivity.this).setStringForKey(userEmail,NetworkUtils.USER_EMAIL);
                                new SharedPrefManager(SignInActivity.this).setStringForKey(userPassword,NetworkUtils.USER_PASSWORD);
                            }else {
                                new SharedPrefManager(SignInActivity.this).setStringForKey("",NetworkUtils.USER_EMAIL);
                                new SharedPrefManager(SignInActivity.this).setStringForKey("",NetworkUtils.USER_PASSWORD);
                            }
                            UserModel userModel = new UserModel();
                            userModel.setID(userId);
                            userModel.setUserEmail(userEmail);
                            userModel.setUserName(userName);
                            SessionClass.getInstance().saveUserInfo(SignInActivity.this, userModel);
                            Intent intent = new Intent(SignInActivity.this, MyPetsActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();


                            Intent resoursService = new Intent(SignInActivity.this, ResourcesService.class);
                            resoursService.putExtra(NetworkUtils.USER_ID,userId);
                            startService(resoursService);

                        } else {
                            UtilityClass.showPopup(SignInActivity.this, getResources().getString(R.string.alert),
                                    getResources().getString(R.string.activate_your_account), () -> {
                                        Intent intent = new Intent(SignInActivity.this, ActivationActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra(NetworkUtils.USER_EMAIL,userEmail);
                                        startActivity(intent);
                                    });

                        }
                    } else {
                        UtilityClass.showPopup(SignInActivity.this, getResources().getString(R.string.alert),
                                getResources().getString(R.string.incorrect));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }

            }

            @Override
            public void onResponseNotReceived() {
                UtilityClass.showPopup(SignInActivity.this, getResources().getString(R.string.alert),
                        getResources().getString(R.string.internet));
            }
        }, false);
        client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    private List<NameValuePair> loginUser(String userEmail, String userPassword) {
        try {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(NetworkUtils.USER_EMAIL, userEmail));
            pairs.add(new BasicNameValuePair(NetworkUtils.USER_PASSWORD, userPassword));
            return pairs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    private boolean isEmpty() {
        if (email_et.getText().length() > 0 && pass_et.getText().length() > 0) {
            return false;
        }

        return true;
    }
    public void forgotClick(View view) {
        startActivity(new Intent(this, ForgotPassword.class));
    }
    public void showhidepassClick(View view) {
        Integer integer = (Integer) pass_et.getTag();
        switch (integer) {
            case R.drawable.hide_pass_icon: {
                pass_et.setInputType(InputType.TYPE_CLASS_TEXT);
                ((ImageView) findViewById(R.id.showhidepass)).
                        setImageResource(R.drawable.show_pass_icon);
                pass_et.setSelection(pass_et.getText().length());
                Log.d("drawableclick", "true");

                pass_et.setTag(R.drawable.show_pass_icon);
                break;
            }

            case R.drawable.show_pass_icon: {
                pass_et.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                Log.d("drawableclick", "true");
                pass_et.setSelection(pass_et.getText().length());
                ((ImageView) findViewById(R.id.showhidepass)).
                        setImageResource(R.drawable.hide_pass_icon);
                pass_et.setTag(R.drawable.hide_pass_icon);
                break;
            }

        }
    }
}
