package com.Celeritas.VetMed.Callback;

public interface WebserviceCallback {
    void onResponseReceived(String jsonResponse);
    void onResponseNotReceived();
}
