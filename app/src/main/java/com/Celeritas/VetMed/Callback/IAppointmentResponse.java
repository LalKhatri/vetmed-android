package com.Celeritas.VetMed.Callback;

import com.Celeritas.VetMed.Models.AppointmentModel;

import java.util.List;

public interface IAppointmentResponse {
  void onResponse(List<AppointmentModel> appointmentModels);
}
