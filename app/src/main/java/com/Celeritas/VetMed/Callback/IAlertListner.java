package com.Celeritas.VetMed.Callback;

public interface IAlertListner {
    void onYesClick();
    void onNoClick();
}
