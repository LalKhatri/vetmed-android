package com.Celeritas.VetMed.Callback;

public interface IActionBar {
    void MenuClick();
    void LeftBackClick();
    void RightClick();
}
