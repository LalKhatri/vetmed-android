package com.Celeritas.VetMed.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Celeritas.VetMed.Activities.pets.AppointmentActivity;
import com.Celeritas.VetMed.Activities.pets.AppointmentDetailActivity;
import com.Celeritas.VetMed.Adapters.BaseAdapter;
import com.Celeritas.VetMed.Callback.IAlertListner;
import com.Celeritas.VetMed.Constants;
import com.Celeritas.VetMed.Helper.SessionClass;
import com.Celeritas.VetMed.Models.Item;
import com.Celeritas.VetMed.Models.pets.AppointmentModelUpdated;
import com.Celeritas.VetMed.R;
import com.Celeritas.VetMed.Utility.SwipeToDeleteCallBack;
import com.Celeritas.VetMed.Utility.UtilityClass;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.Celeritas.VetMed.Constants.APPOINTMENTS;

public class UpcomingAppointmentFragment extends Fragment implements AppointmentActivity.IAppointmentResponseListner{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = "UpcomingAppointmentFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private RelativeLayout card_view;
    private RecyclerView mRecycler;
    private BaseAdapter adapter;

    private  List<AppointmentModelUpdated> appointmentList;
    List<Item> mItemList;

    public UpcomingAppointmentFragment() {
        // Required empty public constructor
    }

    public static UpcomingAppointmentFragment newInstance() {
        UpcomingAppointmentFragment fragment = new UpcomingAppointmentFragment();
        /*Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, (Serializable) list);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) { //SessionClass.getInstance().getPetModelUpdated(getContext()).getId()
           // appointmentList1 = (List<AppointmentModelUpdated>) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming_appointment, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        card_view = (RelativeLayout) view.findViewById(R.id.card_view);
        mRecycler = (RecyclerView) view.findViewById(R.id.mReyclerView);
        ((TextView)view.findViewById(R.id.tv_no_data)).setText(getResources().getString(R.string.tap_to)+" "+getResources().getString(R.string.appointment).toLowerCase());
        appointmentList  =  AppointmentModelUpdated.findWithQuery(AppointmentModelUpdated.class, "Select * from "+ APPOINTMENTS +" where petID = ? ",String.valueOf(SessionClass.getInstance().getPetModelUpdated(getContext()).getId()));
        populateList(appointmentList);
        card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppointmentActivity) getActivity()).addDetails();
            }
        });
        //progress = (ProgressBar) view.findViewById(R.id.progress);
        //populateList(appointmentModelList);

    }

    private void populateList(List<AppointmentModelUpdated> petAppointmentModelList) {
        if (petAppointmentModelList != null) {

            appointmentList = new ArrayList<>();
             mItemList = new ArrayList<>();
            for (AppointmentModelUpdated appointmentModel : petAppointmentModelList) {
                Item appointmentDetail = new Item();
                String appointmet_date = getString(R.string.appointment_date1);
                appointmentDetail.setTitle(appointmet_date);
                appointmentDetail.setDescription(appointmentModel.getAppointmentDate());
                appointmentDetail.setViewType(Constants.APPOINTMENT);

                try {
                    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
                    Calendar calendar = Calendar.getInstance();
                    String date = format.format(calendar.getTime());
                    Date arg0Date = format.parse(appointmentModel.getAppointmentDate());
                    Date arg1Date = format.parse(date);

                    if (arg1Date.before(arg0Date) || arg1Date.equals(arg0Date)){
                        mItemList.add(appointmentDetail);
                        appointmentList.add(appointmentModel);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            Collections.sort(mItemList, (Comparator<Item>) (o1, o2) -> {
                SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
                try {
                    Date arg0Date = format.parse(o1.getDescription());
                    Date arg1Date = format.parse(o2.getDescription());
                    if (arg0Date.after(arg1Date)) {
                        return 1;
                    } else {
                        return -1;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                return 0;
            });

            Collections.sort(appointmentList, (Comparator<AppointmentModelUpdated>) (o1, o2) -> {
                SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_TWO);
                try {
                    Date arg0Date = format.parse(o1.getAppointmentDate());
                    Date arg1Date = format.parse(o2.getAppointmentDate());
                    if (arg0Date.after(arg1Date)) {
                        return 1;
                    } else {
                        return -1;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                return 0;
            });
            if ((mItemList.size() > 0)) {
                card_view.setVisibility(View.GONE);
            } else {
                card_view.setVisibility(View.VISIBLE);
            }
            adapter = new BaseAdapter(mItemList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRecycler.setLayoutManager(mLayoutManager);
            mRecycler.setAdapter(adapter);
            setSwipeCallback();
            adapter.setOnItemClickListner((postion, view) -> {
                Intent intent = new Intent(getActivity(), AppointmentDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.PET_APPOINTMENT_OBJECT,new Gson().toJson(appointmentList.get(postion)));
                startActivity(intent);
            });
        }
    }

    @Override
    public void onResponse(List<AppointmentModelUpdated> appointmentModels) {
        populateList(appointmentModels);
    }
    public void setSwipeCallback() {


        final SwipeToDeleteCallBack callBack = new SwipeToDeleteCallBack(getContext()) {

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction) {
                if (direction == ItemTouchHelper.LEFT) {

                    UtilityClass.showDeletPopup(getActivity(), getResources().getString(R.string.alert), getResources().getString(R.string.appointment_confirmation), new IAlertListner() {
                        @Override
                        public void onYesClick() {
                            AppointmentModelUpdated selectedPet =  appointmentList.get(viewHolder.getAdapterPosition());
                            AppointmentModelUpdated note = AppointmentModelUpdated.findById(AppointmentModelUpdated.class, selectedPet.getId());
                            note.delete();
                            appointmentList.remove(viewHolder.getAdapterPosition());
                            mItemList.remove(viewHolder.getAdapterPosition());
                            adapter.notifyDataSetChanged();
                            populateList(appointmentList);


                        }

                        @Override
                        public void onNoClick() {
                            mRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    });

                }
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                if (viewHolder != null) {
                    if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);
                    } else {
                        viewHolder.itemView.setBackgroundColor(Color.WHITE);

                    }
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                viewHolder.itemView.setBackgroundColor(Color.WHITE);


            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(callBack);
        helper.attachToRecyclerView(mRecycler);

    }

}
