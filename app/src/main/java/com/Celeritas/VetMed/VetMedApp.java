package com.Celeritas.VetMed;

import android.app.Application;

import com.orm.SugarContext;


/**
 * Created by Kanaya Lal on 7/21/2018.
 */

public class VetMedApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
