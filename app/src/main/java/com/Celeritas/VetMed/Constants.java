package com.Celeritas.VetMed;

import android.content.Context;
import android.os.Environment;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.Celeritas.VetMed.CustomFontsWidget.EditextOpensenseRegular;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Constants {
    public static String PROFILE = "profile";
    public static String DISEASES_OBJECT = "diseases_object";
    public static String DIAGNOSTIC_TEST_OBJECT = "diagnostic_test_object";
    public static String CLINICAL_SIGNS_OBJECT = "clinical_signs_object";
    public static String USEFUL_LINKS_OBJECT = "useful_links_object";
    public static String DATE_FORMAT = "MM/dd/yy";
    public static String DATE_FORMAT_TWO = "dd MMM yyyy";
    public static String TIME_FORMAT = "hh:mm aa";
    public static String IMAGE_FORMAT = "yyyyMMdd_HHmmss";
    public static String CLIENT_EMAIL = "palbento@gmail.com";
    public static String TECHNICAL_EMAIL = "technology.helpdesk@celeritas-solutions.com";
    public static final String confuse = "resources/images/confuse.png";
    public static final String happy = "resources/images/happy.png";
    public static final String sick = "resources/images/sick.png";
    public static  String[] mimeTypes =
            {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                    "text/plain",
                    "application/pdf"};

    public static final int GENERAL_INFORMATION_DETAIL = 0;
    public static final int PET_MAIN_DETAIL = 1;
    public static final int RESOURSE_MAIN = 2;
    public static final int VETERINARIAN = 3;
    public static final int INSURANCE = 4;
    public static final int APPOINTMENT = 5;
    public static final int GALLERY = 6;
    public static final String IMAGES_PATH = ""+Environment.getExternalStorageDirectory()+"/.VetMed/.VetMed_PET_";
    public static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_TWO, Locale.US);

    public static final String PET_MODEL_OBJECT = "pet_model_object";
    public static final String PET_MEDICAL_HISTORY_OBJECT = "pet_medical_history_object";
    public static final String PET_MEDICATION_OBJECT = "pet_medication_object";
    public static final String PET_DIET_OBJECT = "pet_diet_object";
    public static final String PET_WEIGHT_TRACKER_OBJECT = "weight_tracker_model";
    public static final String PET_JOURNAL_OBJECT = "journal_model";
    public static final String PET_VACCINE_OBJECT = "pet_vaccine_object";
    public static final String PET_DEWORMING_OBJECT = "pet_deworming_object";
    public static final String PET_VETERINARIAN_OBJECT = "pet_veterinarian_object";
    public static final String PET_INSURANCE_OBJECT = "pet_insurance_object";
    public static final String PET_APPOINTMENT_OBJECT = "pet_appointment_object";
    public static final String PET_NOTES_OBJECT = "pet_note_object";
    public static final String PET_PROFILE_IMAGE =  "pet_profile_image";
    public static final String PET_PROFILE_IMAGE_LIST = "pet_profile_images_list";
    public static final String RESOURSE_NAME = "resource_name";

    public static final String DONT_SHOW = "dont_show";
    public static final String MEDICINE_TYPE = "medicine_type";

    // after update saving data local table names
    public static final String PETS = "pet_model_updated";
    public static final String MED_HISTORY = "medical_history_model_updated";
    public static final String MEDICATIONS = "medication_model_updated";
    public static final String DIET = "diet_model_updated";
    public static final String VACCINE = "vaccine_model_updated";
    public static final String DEWORMING = "deworming_model_updated";
    public static final String PET_VETERINARIAN_UPDATED = "veterinarian_model_updated";
    public static final String PET_INSURANCE_UPDATED = "insurance_model_updated";
    public static final String APPOINTMENTS = "appointment_model_updated";
    public static final String DOCUMENTS = "documents";
    public static final String NOTES = "notes_model_updated";
    public static final String WEIGHT_TRACKER = "weight_tracker_model";
    public static final String JOURNAL = "journal_model";

    /*Amazon Aws Bucket Credentials*/
    public static final String DOMAIN_NAME = "UserCredentials";
    public static final String ACCESS_KEY_ID = "AKIAJ4DDXAATIVJBCJPQ";
    public static final String SECRET_KEY = "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";
    public static final String PICTURE_BUCKET = "jamshed";
    public static final String PICTURE_NAME = "NameOfThePicture";
    // ignore enter First space on edit text
    public static InputFilter ignoreFirstWhiteSpace() {
        //editText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(32)});
        return new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isWhitespace(source.charAt(i)) && (dstart == 0)) {
                        return "";
                    }
                }
                return null;
            }
        };
    }
    /*public void dateChoser(final int checkInDay, final Context context) {
        int mYear, mMonth, mDay;
        // Process to get Current Date
        final Calendar c = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        final String month_name = month_date.format(c.getTime());
        long checkDate = 0;
        startDate =0;

        Log.e("Month Nam", "" + month_name);
        if (checkInDay == 0) {
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            checkDate = c.getTimeInMillis();
        } else {
            c.add(Calendar.DATE, 1);
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            try {
                if (selectedDate!=null){
                    checkDate = selectedDate.getTime();
                }else {
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    checkDate = c.getTimeInMillis();
                }
            }catch (Exception e){
                Log.e("First Date Not Selected",""+e.getMessage());
            }



            Log.e("Date", "D " + mDay + " M " + mMonth + " Y " + mYear);


        }

        //month_name = c.get(Calendar.MONTH);

        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox

                        String day = "";
                        if (dayOfMonth < 10) {
                            day = "0" + dayOfMonth;
                        } else {
                            day = (String.valueOf(dayOfMonth));
                        }
                        if (isFromSearchOrOther) {
                            checkInDate.setText(day + " "
                                    + (MONTHS[monthOfYear]) + " " + year);
                            c.set(year, monthOfYear, Integer.parseInt(day));

                            checkOutDate.setText(c.get(Calendar.DAY_OF_MONTH) + " "
                                    + (MONTHS[c.get(Calendar.MONTH)]) + " " + c.get(Calendar.YEAR));
                            if (checkInDay == 0) {
                                startDate = c.getTime().getTime();
                                c.add(Calendar.DATE, 1);
                                selectedDate = c.getTime();
                                if (c.get(Calendar.DAY_OF_MONTH) > 9) {
                                    checkOutDate.setText(c.get(Calendar.DAY_OF_MONTH) + " "
                                            + (MONTHS[c.get(Calendar.MONTH)]) + " " + c.get(Calendar.YEAR));
                                } else {
                                    checkOutDate.setText("0" + c.get(Calendar.DAY_OF_MONTH) + " "
                                            + (MONTHS[c.get(Calendar.MONTH)]) + " " + c.get(Calendar.YEAR));
                                }

                            } else {
                                if (Integer.parseInt(day) > 9) {
                                    checkOutDate.setText((Integer.parseInt(day)) + " "
                                            + (MONTHS[monthOfYear]) + " " + year);
                                } else {
                                    checkOutDate.setText("0" + (Integer.parseInt(day)) + " "
                                            + (MONTHS[monthOfYear]) + " " + year);
                                }

                            }

                        } else {
                            checkInDate.setText(day + " "
                                    + (MONTHS[monthOfYear]) + "\n" + year);
                            c.set(year, monthOfYear, Integer.parseInt(day));
                            selectedDate = c.getTime();
                            startDate =selectedDate.getTime();
                            //c.add(Calendar.DATE,1);
                            checkOutDate.setText(c.get(Calendar.DAY_OF_MONTH) + " "
                                    + (MONTHS[c.get(Calendar.MONTH)]) + "\n" + c.get(Calendar.YEAR));
                            if (checkInDay == 0) {
                                c.add(Calendar.DATE, 1);
                                selectedDate = c.getTime();

                                if (c.get(Calendar.DAY_OF_MONTH) > 9) {
                                    checkOutDate.setText(c.get(Calendar.DAY_OF_MONTH) + " "
                                            + (MONTHS[c.get(Calendar.MONTH)]) + "\n" + c.get(Calendar.YEAR));
                                } else {
                                    checkOutDate.setText("0" + c.get(Calendar.DAY_OF_MONTH) + " "
                                            + (MONTHS[c.get(Calendar.MONTH)]) + "\n" + c.get(Calendar.YEAR));
                                }

                            } else {
                                if (Integer.parseInt(day) > 9) {
                                    checkOutDate.setText((Integer.parseInt(day)) + " "
                                            + (MONTHS[monthOfYear]) + "\n" + year);
                                } else {
                                    checkOutDate.setText("0" + (Integer.parseInt(day)) + " "
                                            + (MONTHS[monthOfYear]) + "\n" + year);
                                }

                            }

                        }


                    }
                }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMinDate((checkDate));
        dpd.show();
    }*/

}
